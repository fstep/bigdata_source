package com.porva.util;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;

import junit.framework.TestCase;

public class FioUtilsTest extends TestCase
{

  public void testWriteToFileStringStringString() throws Exception
  {
    try {
      FioUtils.writeToFile(null, "content", null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      FioUtils.writeToFile("test/tmp", null, null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      FioUtils.writeToFile("test/tmp", "content2", "unknown");
      fail();
    } catch (UnsupportedEncodingException e) {
    }
    try {
      // there is a directory test/exist
      FioUtils.writeToFile("test/exist", "content", null);
      fail();
    } catch (IOException e) {
    }
    
    FioUtils.writeToFile("test/tmp", "content", null);
  }

  public void testWriteToFile() throws Exception 
  {
    try {
      FioUtils.writeToFile(null, "content".getBytes());
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      FioUtils.writeToFile("test/tmp", null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      // there is a directory test/exist
      FioUtils.writeToFile("test/exist", "content".getBytes());
      fail();
    } catch (IOException e) {
    }
    
    FioUtils.writeToFile("test/tmp", "content".getBytes());
  }

  public void testReadFileAsString() throws Exception 
  {
    try {
      FioUtils.readFileAsString("test/does_not_exist", null);
      fail();
    } catch (IOException e) {
    }
    try {
      FioUtils.readFileAsString("test/tmp", "unknown");
      fail();
    } catch (UnsupportedEncodingException e) {
    }
    
    assertEquals("content", FioUtils.readFileAsString("test/tmp", null));
  }

  public void testGetFilesRecursively() throws Exception 
  {
    try {
      FioUtils.getFilesRecursively(null, null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      FioUtils.getFilesRecursively(new File("test/not_a_dir"), null);
      fail();
    } catch (IllegalArgumentException e) {
    }
    
    List<File> files = FioUtils.getFilesRecursively(new File("test"), null);
    assertEquals(1, files.size());
  }

}
