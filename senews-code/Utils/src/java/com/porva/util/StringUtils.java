/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.util;

public class StringUtils
{
  public static String parseString(byte[] val)
  {
    if (val == null)
      return null;
    return new String(val);
  }
  
  public static Double parseDouble(byte[] val)
  {
    if (val == null)
      return null;
    String dStr = new String(val);
    Double d = null;
    try {
      d = new Double(dStr);
    } catch(NumberFormatException e) {
      return null;
    }
    return d;
  }
  
  public static Integer parseInteger(byte[] val)
  {
    if (val == null)
      return null;
    String dStr = new String(val);
    Integer d = null;
    try {
      d = new Integer(dStr);
    } catch(NumberFormatException e) {
      return null;
    }
    return d;
  }
  
}
