/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.Map;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/** Provides access to configuration parameters loaded form xml file.<p>
 * Here is an example of config xml file:<br>
 * <code>
<p>&lt;?xml version=&quot;1.0&quot;?&gt;</p>
<p>&lt;root-tag-name&gt;</p>
<p>&lt;property&gt;<br>
&nbsp;&nbsp;&nbsp; &lt;name&gt;name.of.parameter&lt;/name&gt;<br>
&nbsp;&nbsp;&nbsp; &lt;value&gt;value of parameter&lt;/value&gt;<br>
&nbsp;&nbsp;&nbsp; &lt;description&gt;description of property&lt;/description&gt;<br>
&lt;/property&gt;<br>
&nbsp;</p>
<p>&lt;property&gt;<br>
&nbsp;&nbsp;&nbsp; &lt;name&gt;some.integer.value&lt;/name&gt;<br>
&nbsp;&nbsp;&nbsp; &lt;value&gt;100200&lt;/value&gt;<br>
&lt;/property&gt;</p>
<p>&lt;property&gt;<br>
&nbsp;&nbsp;&nbsp; &lt;name&gt;some.boolean.value&lt;/name&gt;<br>
&nbsp;&nbsp;&nbsp; &lt;value&gt;true&lt;/value&gt;<br>
&nbsp;</p>
<p>&lt;/root-tag-name&gt;</p><br>
 *
 * </code>
 * <br> <br>
 */

public class Configuration
{
  private static Properties properties = new Properties();
  private static final Log logger = LogFactory.getLog(Configuration.class);

  /**
   * Filename of extracted from jar resource.
   */
  public static final String JAR_EXTRACTED_FILE = "_extracted";

  private Configuration() {}

  public static Properties getProp()
  {
    return properties;
  }

  /**
   * Returns properties from specified resource.
   * @param resource filename of configuraiton resource.
   * @return {@link Properties} object from <code>resource</code> or <code>null</code> if <code>resource</code>
   * is not found or failed to load.
   */
  public static synchronized Properties getPropFromResource(String resource)
  {
    return loadResource(resource);
  }

  /**
   * Adds a resource <code>confResource</code> to the chain of resources read.
   * Properties from the new added confing resource will overwite the same old properties.
   * <br><br>
   * &nbsp;&nbsp;Added by <code>addConfResource</code> resources will be searched
   * in following places:<br>
   * <code>
   *     1) ClassLoader().getResource(confResource)<br>
   *     2) URL url = new URL("file:/" + confResource))<br>
   *     3) URL url = new URL("file:" + confResource))<br>
   *     4) URL url = new URL("file:/" + System.getProperty("user.dir") + File.separator + confResource))<br>
   *     5) URL url = new URL("file:" + System.getProperty("user.dir") + File.separator + confResource))<br>
   * </code>
   * <br>
   * &nbsp;&nbsp;Config resource can be placed in jar file.
   * In this case <code>confResource</code> must be something like: jar:/package.jar/path/in/jar/config.xml
   * Jar file must have .jar extension but can have additional path before it, i.e. <code>confResource</code>
   * jar:/some/path/package.jar/path/in/jar/config.xml is also valid.
   * @param confResource filename of xml config file to add
   */
  public static synchronized boolean addConfResource(String confResource)
  {
    if (confResource == null)
      throw new NullPointerException("Trying to add null conf resource url");
    if (searchResouce(confResource) == null) {
      logger.warn("Failed to load configuration resource: " + confResource);
      return false;
    }

    Properties prop = loadResource(confResource);
    if (prop != null) {
      properties.putAll(prop);
    }

    logger.info("New configuration resource added: " + confResource);
    return true;
  }

  /**
   * Loads resource. Resource can be also path in jar file.
   * @param name resource as string
   * @return <code>true</code> if resource is valid or <code>false</code> otherwise.
   */
  private static Properties loadResource(String name)
  {
    URL url = searchResouce(name);
    Properties properties = new Properties();
    try {
      if (url == null) {
        logger.warn("Resource not found: " + name);
        return null;
      }
      properties.load(url.openStream());
    } catch (Exception e) {
      logger.warn("Invalid configuration resource: " + e);
      return null;
    }
    return properties;
  }

  /**
   * Construct URL from given string resource. Resource can be also path in jar file.
   * @param resStr resource as string
   * @return  URL representation of resource if it's valid or <code>null</code> otherwise.
   */
  public static synchronized URL searchResouce(String resStr)
  {
    URL url = null;
    if (resStr.startsWith("jar:/"))
      url = extractResourceFromJar(resStr);
    else
      url = searchNonJarResouce(resStr);
    return url;
  }

  /**
   * Searches resource specified by given String and constructs URL of it.
   * @param resStr resource to be searched.
   * @return URL representation of resource if it's valid or <code>null</code> otherwise.
   */
  private static URL searchNonJarResouce(String resStr)
  {
    URL url = Configuration.class.getClassLoader().getResource(resStr);
    if (url == null)
      url = openResource("file:/" + resStr);
    if (url == null)
      url = openResource("file:" + resStr);
    if (url == null)
      url = openResource("file:/" + System.getProperty("user.dir") + File.separator + resStr);
    if (url == null)
      url = openResource("file:" + System.getProperty("user.dir") + File.separator + resStr);
    return url;
  }

  /**
   * Consructs URL from String location if it is possible. Else returns null.
   * @param resStr resource to be opened as URL.
   * @return URL representation of resource if it's valid or <code>null</code> otherwise.
   */
  private static URL openResource(String resStr)
  {
    URL url = null;
    try {
      url = new URL(resStr);
      if (!(new File(url.getPath())).exists()) {
        url = null;
      }
    } catch (MalformedURLException e) {
      url = null;
    }
    return url;
  }

  /**
   * Extractes resource from jar file and store it in local file JAR_EXTRACTED_FILE.
   * For instance, <code>jar:/crawler.jar/conf/crawler.xml</code> is a path of resource <code>crawler.xml</code> inside
   * jar <code>crawler.jar</code>.
   * @param resStr String of resource inside jar file.
   * @return extracted URL representation of resource if it's valid or <code>null</code> otherwise.
   */
  private static URL extractResourceFromJar(String resStr)
  {
    //Pattern pattern = Pattern.compile("jar:/([^/]+)/(.+)");
    Pattern pattern = Pattern.compile("jar:/(.+\\.jar)/(.+)");
    Matcher matcher = pattern.matcher(resStr);
    String pathInsideJar = null;
    String jarFilename = null;
    if (matcher.matches()) {
      jarFilename = matcher.group(1);        // jar filename, for example, crawler.jar
      pathInsideJar = matcher.group(2);      // path inside jar, for example, conf/crawler.xml
    } else {
      return null;
    }

    URL url =  searchNonJarResouce(jarFilename);

    try {
      if (url != null) {
        JarResources jar = new JarResources(url.getPath());
        byte[] fileContent = jar.getResource(pathInsideJar);
        File file = new File(JAR_EXTRACTED_FILE);

        file.createNewFile();
        FileOutputStream out = new FileOutputStream(file);
        out.write(fileContent);
        out.close();

        url = searchNonJarResouce(file.getAbsolutePath());
      }
    } catch (IOException e) {
      e.printStackTrace();
    }

    return url;
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // get functions

  /**
   * Returns the value of the <code>name</code> property as a <code>String</code>.
   * @param name name of property.
   * @return value of property if it exists; <code>null</code> otherwise.
   */
  public static synchronized String get(String name)
  {
    return properties.getProperty(name);
  }

  /**
   * Returns the value of the <code>name</code> property as a <code>String</code>.
   * @param name name of property.
   * @param defaultValue will be returned if such property does not exist.
   * @return value of property if it exists; <code>defaultValue</code> otherwise.
   */
  public static synchronized String get(String name, String defaultValue)
  {
    return properties.getProperty(name, defaultValue);
  }

  /**
   * Returns the value of the <code>name</code> property as an integer.
   * @param name name of property.
   * @param defaultValue will be returned if such property does not exist or value is not a valid
   * integer.
   * @return value of property if it exists and valid integer;
   * <code>defaultValue</code> otherwise.
   */
  public static synchronized int getInt(String name, int defaultValue)
  {
    String valueString = get(name);
    if (valueString == null)
      return defaultValue;
    try {
      return Integer.parseInt(valueString);
    } catch (NumberFormatException e) {
      logger.warn("Cast error: value of " + name + " is not an Integer");
      return defaultValue;
    }
  }

  /**
   * Returns the value of the <code>name</code> property as a long.
   * @param name name of property.
   * @param defaultValue will be returned if such property does not exist or value is not a valid
   * long.
   * @return value of property if it exists and valid long;
   * <code>defaultValue</code> otherwise.
   */
  public static synchronized long getLong(String name, long defaultValue) {
    String valueString = get(name);
    if (valueString == null)
      return defaultValue;
    try {
      return Long.parseLong(valueString);
    } catch (NumberFormatException e) {
      logger.warn("Cast error: value of " + name + " is not a Long");
      return defaultValue;
    }
  }

  /**
   * Returns the value of the <code>name</code> property as a float.
   * @param name name of property.
   * @param defaultValue will be returned if such property does not exist or value is not a valid
   * float.
   * @return value of property if it exists and valid float;
   * <code>defaultValue</code> otherwise.
   */
  public static synchronized float getFloat(String name, float defaultValue)
  {
    String valueString = get(name);
    if (valueString == null)
      return defaultValue;
    try {
      return Float.parseFloat(valueString);
    } catch (NumberFormatException e) {
      logger.warn("Cast error: value of " + name + " is not a Float");
      return defaultValue;
    }
  }

  /**
   * Returns the value of the <code>name</code> property as an boolean.
   * Valid boolean values are "true" and "false".
   * @param name name of property.
   * @param defaultValue will be returned if such property does not exist or value is not a valid
   * boolean
   * @return value of property if it exists and valid boolean;
   * <code>defaultValue</code> otherwise.
   */
  public static synchronized boolean getBoolean(String name, boolean defaultValue)
  {
    String valueString = get(name);
    if ("true".equals(valueString))
      return true;
    else if ("false".equals(valueString))
      return false;
    else {
      logger.warn("Cast error: value of " + name + " is not a Boolean 'true' or 'false' strings");
      return defaultValue;
    }
  }

  public static synchronized Map<String,String> parseParamString(String paramStr)
  {
    Map<String,String> res = new HashMap<String,String>();

    // mode:server;params:localhost,12255
    // mode:local
    String[] ar = paramStr.split(";");
    for (int i = 0; i < ar.length; i++) {
      String[] param = ar[i].split(":", 2);
      String key = param[0];
      String value = param[1];
      res.put(key, value);
    }
    return res;
  }

}
