/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;

/**
 * Set of static file IO utility methods.<br>
 * All static methods are synchronized.
 * 
 * @author poroshin
 */
public class FioUtils
{
  /**
   * Writes <code>content</code> to a file <code>filename</code> with specified encoding
   * <code>encoding</code>.<br>
   * If file <code>filename</code> does not exist then it will be created; otherwise it will be
   * replaced with a new file.
   * 
   * @param filename name of a file to write to.
   * @param content content to write to the file.
   * @param encoding encoding that will be used to write to file. If <code>null</code> then a
   *          default system encoding will be used.
   * @throws IOException if some IO exception occurs.
   * @throws NullArgumentException if <code>filename</code> or <code>content</code> is
   *           <code>null</code>.
   */
  public synchronized static void writeToFile(final String filename,
                                              final String content,
                                              final String encoding) throws IOException
  {
    if (filename == null)
      throw new NullArgumentException("filename");
    if (content == null)
      throw new NullArgumentException("content");

    File f = new File(filename);
    if (!f.exists())
      f.createNewFile();
    OutputStream out = new FileOutputStream(f);
    Writer stWriter;
    if (encoding == null)
      stWriter = new OutputStreamWriter(out);
    else
      stWriter = new OutputStreamWriter(out, encoding);
    Writer writer = new BufferedWriter(stWriter);

    writer.write(content);
    writer.flush();
    out.close();
    stWriter.close();
    writer.close();
  }

  /**
   * Writes bytes <code>content</code> to the file <code>filename</code>. Bytes are writen to
   * the file as is without any conversions.<br>
   * If file <code>filename</code> does not exist then it will be created; otherwise it will be
   * replaced with a new file.
   * 
   * @param filename name of a file to write to.
   * @param content bytes to write.
   * @throws IOException if some IO exception occurs.
   * @throws NullArgumentException if <code>filename</code> or <code>content</code> is
   *           <code>null</code>.
   */
  public synchronized static void writeToFile(String filename, byte[] content) throws IOException
  {
    if (filename == null)
      throw new NullArgumentException("filename");
    if (content == null)
      throw new NullArgumentException("content");

    File f = new File(filename);
    if (!f.exists())
      f.createNewFile();
    FileOutputStream fin = new FileOutputStream(f);
    fin.write(content);
    fin.flush();
    fin.close();
  }

  /**
   * Read specified file <code>filename</code> and returns its content as string.
   * 
   * @param filename file to read.
   * @param encoding character encoding of the file or <code>null</code> to use default system
   *          econding.
   * @return content of specified file as string.
   * @throws IOException if some IO exceptoin occures.
   * @throws NullArgumentException if <code>filename</code> is <code>null</code>.
   */
  public synchronized static String readFileAsString(File filename, String encoding)
      throws IOException
  {
    if (filename == null)
      throw new NullArgumentException("filename");

    return readFileAsString(filename.getAbsolutePath(), encoding);
  }

  /**
   * Read specified file <code>filename</code> and returns its content as string.
   * 
   * @param filename file to read.
   * @param encoding character encoding of the file or <code>null</code> to use default system
   *          econding.
   * @return content of specified file as string.
   * @throws IOException if some IO exceptoin occures.
   * @throws NullArgumentException if <code>filename</code> is <code>null</code>.
   */
  public synchronized static String readFileAsString(String filename, String encoding)
      throws IOException
  {
    if (filename == null)
      throw new NullArgumentException("filename");

    InputStream inStream = new FileInputStream(filename);
    Reader in;
    if (encoding == null)
      in = new InputStreamReader(inStream);
    else
      in = new InputStreamReader(inStream, encoding);
    BufferedReader br = new BufferedReader(in);
    String str;
    StringBuffer buf = new StringBuffer();
    while ((str = br.readLine()) != null) {
      buf.append(str).append("\n");
    }
    inStream.close();
    in.close();
    br.close();
    return buf.substring(0, buf.length() - 1).toString();
  }

  /**
   * Returns file <code>filename</code> content as array of bytes.
   * 
   * @param filename file to read.
   * @return byte content of the file.
   * @throws IOException if some IO exception occures.
   * @throws NullArgumentException if <code>filename</code> is <code>null</code>.
   */
  public synchronized static byte[] readFileAsBytes(final String filename) throws IOException
  {
    if (filename == null)
      throw new NullArgumentException("file");

    File f = new File(filename);
    FileInputStream fin = new FileInputStream(f);
    byte[] buff = new byte[(int) f.length()];
    int readed = fin.read(buff);
    if (readed != f.length() || readed == -1)
      throw new IOException("Failed to read file: " + f.getAbsolutePath());
    fin.close();
    return buff;
  }

  /**
   * Deletes all files and subdirectories under dir. Returns true if all deletions were successful.
   * If a deletion fails, the method stops attempting to delete and returns false.
   * 
   * @param dir directory to remove recursively.
   * @return <code>true</code> if deletion completed; <code>false</code> otherwise.
   */
  public synchronized static boolean deleteDir(File dir)
  {
    if (dir.isDirectory()) {
      String[] children = dir.list();
      for (int i = 0; i < children.length; i++) {
        boolean success = deleteDir(new File(dir, children[i]));
        if (!success) {
          return false;
        }
      }
    }

    // The directory is now empty so delete it
    return dir.delete();
  }

  /**
   * Returns a list of files (real files that satisfy <code>file.isFile()</code>) in given
   * directory <code>startingDir</code>, which is scanned recursively.
   * 
   * @param startingDir top directory to recursively collect files.
   * @param fileFilter filter for files. If <code>null</code> then all files will be accepted.
   * @return list of files in <code>startingDir</code> being scanned recursively.
   * @throws IOException if some I/O exception occures.
   * @throws IllegalArgumentException if <code>startingDir</code> is not a directory.
   * @throws NullArgumentException if <code>startingDir</code> is <code>null</code>.
   */
  static synchronized public List<File> getFilesRecursively(File startingDir, FileFilter fileFilter)
      throws IOException
  {
    if (startingDir == null)
      throw new NullArgumentException("startingDir");
    if (!startingDir.isDirectory())
      throw new IllegalArgumentException("Not a directory to list files: " + startingDir);

    List<File> result = new ArrayList<File>();
    for (File file : startingDir.listFiles()) {
      if (file.isFile() && (fileFilter == null || fileFilter.accept(file)))
        result.add(file);
      if (file.isDirectory()) {
        List<File> deeperList = getFilesRecursively(file, fileFilter);
        result.addAll(deeperList);
      }
    }
    return result;
  }

}
