/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.util;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

/**
 * Provides access to resources in Jar of Zip files.
 */

public final class JarResources
{

  // jar resource mapping tables
  private Hashtable<String, Integer> htSizes = new Hashtable<String, Integer>();
  private Hashtable<String, byte[]> htJarContents = new Hashtable<String, byte[]>();

  // a jar file
  private String jarFileName;
  private static final Log logger = LogFactory.getLog(JarResources.class);

  /**
   * Creates a JarResources. It extracts all resources from a Jar
   * into an internal hashtable, keyed by resource names.
   * @param jarFileName a jar or zip file
   * @throws IOException if fails to load file <code>jarFileName</code>
   */
  public JarResources(String jarFileName) throws IOException
  {
    this.jarFileName = jarFileName;
    init();
  }

  /**
   * Extracts a jar resource as a blob.
   * Resource name is relative path in arhive: for instance "META-INF/MANIFEST.MF"
   * @param name a resource name in jar or zip file.
   * @return requested resource as blob or null if such resource was not loaded. 
   */
  public byte[] getResource(String name)
  {
    return (byte[])htJarContents.get(name);
  }

  /**
   * initializes internal hash tables with Jar file resources.
   */
  private void init() throws IOException
  {
      // extracts just sizes only.
      ZipFile zf=new ZipFile(jarFileName);
      Enumeration e=zf.entries();
      while (e.hasMoreElements()) {
        ZipEntry ze=(ZipEntry)e.nextElement();
        logger.debug(dumpZipEntry(ze));
        htSizes.put(ze.getName(),new Integer((int)ze.getSize()));
      }
      zf.close();

      // extract resources and put them into the hashtable.
      FileInputStream fis=new FileInputStream(jarFileName);
      BufferedInputStream bis=new BufferedInputStream(fis);
      ZipInputStream zis=new ZipInputStream(bis);
      ZipEntry ze=null;
      while ((ze=zis.getNextEntry())!=null) {
        if (ze.isDirectory()) {
          continue;
        }

        if (logger.isDebugEnabled())
          logger.debug("ze.getName()=" + ze.getName() + "," + "getSize()=" + ze.getSize());
        int size=(int)ze.getSize();

        if (size==-1) { // -1 means unknown size.
          size=((Integer)htSizes.get(ze.getName())).intValue();
        }
        byte[] b=new byte[(int)size];
        int rb=0;
        int chunk=0;
        while (((int)size - rb) > 0) {
          chunk=zis.read(b,rb,(int)size - rb);
          if (chunk==-1) {
            break;
          }
          rb+=chunk;
        }
        // add to internal resource hashtable
        htJarContents.put(ze.getName(),b);
        if (logger.isDebugEnabled())
          logger.debug(ze.getName() + "  rb=" + rb + ",size="+ size +  ",csize=" + ze.getCompressedSize());
      }

  }

  /**
   * Dumps a zip entry into a string.
   * @param ze a ZipEntry
   */
  private String dumpZipEntry(ZipEntry ze)
  {
    StringBuffer sb=new StringBuffer();
    if (ze.isDirectory()) {
      sb.append("d ");
    } else {
      sb.append("f ");
    }
    if (ze.getMethod()==ZipEntry.STORED) {
      sb.append("stored   ");
    } else {
      sb.append("defalted ");
    }
    sb.append(ze.getName());
    sb.append("\t");
    sb.append(""+ze.getSize());
    if (ze.getMethod()==ZipEntry.DEFLATED) {
      sb.append("/"+ze.getCompressedSize());
    }
    return (sb.toString());
  }

}
