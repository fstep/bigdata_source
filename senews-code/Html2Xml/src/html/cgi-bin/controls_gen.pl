#!/usr/bin/perl -w

use strict;


sub getTr
{
    my $num = shift;
    my $linenum = $num;
    $linenum = "0".$num if ($linenum < 10);
    my $bgcolor = "#CCFFCC";
    $bgcolor = "#CCCCCC" if ($num % 2 == 0);
        
    print <<EOD;
<tr bgcolor='$bgcolor'>
      <td width='3\%'><div align='center'>$linenum</div></td>
      <td width='17\%'><select name='$num' onChange='newTask(this.options[this.selectedIndex].text, this.name)'>
      	  <option selected>EMPTY</option>
          <option>FIND_NODE</option>
          <option>STORE_TEXT</option>
          <option>STORE_NODE</option>
          <option>STORE_ISODATE</option>
    	  <option>SAVE_TEXT</option>
          <option>SAVE_POS</option>
          <option>SET_POS</option>
          <option>GOTO_TASK</option>
          <option>END_OF_ARTICLE</option>
          <option>END_OF_DOCUMENT</option>
      </select></td>
      <td width='46\%' id='parametersCell_$num'>\&nbsp;</td>
      <td width='17\%' id='trueFlowTd_$num'>\&nbsp;</td>
      <td width='17\%' id='falseFlowTd_$num'>\&nbsp;</td>
    </tr>\n\n
EOD
}

for (my $i = 1; $i <= $ARGV[0]; $i++) {
    getTr($i);
}