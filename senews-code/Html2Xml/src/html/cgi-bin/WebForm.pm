#!/usr/bin/perl
######################################################################
#
######################################################################
package WebForm;

require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(%field); #todo
######################################################################
%field = ();
######################################################################
sub GetFormInput {
	(*fval) = @_ if @_ ;
	local ($buf);
	if ($ENV{'REQUEST_METHOD'} eq 'POST') {
		read(STDIN,$buf,$ENV{'CONTENT_LENGTH'});
	}
	else {
		$buf=$ENV{'QUERY_STRING'};
	}
	if ($buf eq "") {
			return 0 ;
		}
	else {
 		@fval=split(/&/,$buf);
		foreach $i (0 .. $#fval){
			($name,$val)=split (/=/,$fval[$i],2);
			$val=~tr/+/ /;
			$val=~ s/%(..)/pack("c",hex($1))/ge;
			$name=~tr/+/ /;
			$name=~ s/%(..)/pack("c",hex($1))/ge;

			if (!defined($field{$name})) {
				$field{$name}=$val;
			}
			else {
				$field{$name} .= ",$val";
				
				#if you want multi-selects to goto into an array change to:
				#$field{$name} .= "\0$val";
			}


		   }
		}
    return 1;
}
######################################################################
sub print_header {
    print "Content-Type: text/html\n\n";
    print '<html><head><title>';
    print $_[0];
    print '</title></head><body>';
}
######################################################################
sub print_end {
    print "</body></html>";
}
######################################################################
sub add_tablecol {
    print "<tr>\n";
    for my $el (@_) {
        print "\t<td>".$el."</td>\n";
    }
    print "</tr>\n";
}
######################################################################
sub add_tablecol_colored {
    print "<tr>\n";
    for my $el (@_) {
        print "\t<td bgcolor=\"#FFFFCC\">".$el."</td>\n";
    }
    print "</tr>\n";
}
######################################################################
return 1;
######################################################################