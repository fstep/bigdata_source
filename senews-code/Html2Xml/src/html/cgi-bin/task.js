
var allTasks = new Array();
var maxTasksNum = 20;
var curTaskNum;

	 
function getSelectLabel(num)
{
	id = "selectLabel_" + num;
	html = "Label:<select name=\"" + id + "\" id=\"" + id  +  "\">" +
	"<option selected>TITLE</option><option>CONTENT</option><option>AUTHOR</option><option>DATE</option><option>ISO-DATE</option>"
	 + "</select>";
	return html;
}

function getPositionSelectFrom(num)
{
	id = "posSelectFrom_" + num;
	html =  
		'<select name="' + id + '" id="' + id + '">' +
			'<option selected>last found pos</option>' +
			getVarsOptions() + 
		'</select>';

	return html;
}

function getVarsOptions()
{
	var html = new String();
	for (var i = 1; i <= curTaskNum; i++) {
		if (allTasks[i] == "SAVE_POS") {
			html += '<option>' + getTaskParams("SAVE_POS", i) + '</option>';
		}
	}
	return html;
}

/*
function getTextVarsOptions()
{
	var html = new String();
	for (var i = 1; i <= curTaskNum; i++) {
		if (allTasks[i] == "SAVE_TEXT") {
			var params = getTaskParams("SAVE_TEXT", i);
			params.
			html += '<option>' +  + '</option>';
		}
	}
	return html;
}
*/


function getPositionSelectTo(num)
{
	id = "posSelectTo_" + num;
	html =  
		'<select name="' + id + '" id="' + id + '">' +
			'<option selected>end of tag</option>' +
			getVarsOptions() +
		'</select>';

	return html;
}


function getAllPositions()
{
	
}

function getSelectedText()
{
    var txt = '';
    var doc = parent.page.document;
    var win = parent.page;
    if (win.getSelection) {
      txt = win.getSelection();
    }
    else if (doc.getSelection) {
      txt = doc.getSelection();
    }
    else if (doc.selection) {
      txt = doc.selection.createRange().text;
    }
    else return "";
    return txt;
}
    
function newTask(selectedTask, num)
{
    if (selectedTask == "FIND_NODE") {
        cell = document.getElementById("parametersCell_" + num);
        var text = new String(getSelectedText());
        text = text.replace(/\"/g, "&quot;");
        text = text.replace(/\'/g, "&quot;");
        text = "... " + text;
		id = "nodePattern_" + num;
		html = "<input name=\"" + id + "\" id=\"" + id + "\" type=\"text\" size=\"60\" value=\"" + text + "\">";
		cell.innerHTML = html;
    }
    else if (selectedTask == "STORE_TEXT") {
        cell = document.getElementById("parametersCell_" + num);
		labelId = "label_" + num;
		fromId = "from_" + num;
		toId = "to_" + num;
        cell.innerHTML = getSelectLabel(num) + ' from ' + getPositionSelectFrom(num) + ' to ' + getPositionSelectTo(num);
    }
    else if (selectedTask == "SAVE_POS") {
        cell = document.getElementById("parametersCell_" + num);
        id = "posVar_" + num;
        cell.innerHTML = 'Variable name: <input name="' + id + '" id="' + id + '" type="text" size="42" value="$var' + num + '">';
    }
    else if (selectedTask == "GOTO_TASK") {
        cell = document.getElementById("parametersCell_" + num);
        id = "gotoNum_" + num;
        var html = 'Goto task N:<select name="' + id + '" id="' + id + '">';
        for (var i = 1; i <= maxTasksNum; i++) {
            html += "<option>" + i + "</option>";
        }
        cell.innerHTML = html + "</select>"
    }
    else if (selectedTask == "STORE_NODE") {
        cell = document.getElementById("parametersCell_" + num);
        var id = "storeNode_" + num;
        cell.innerHTML = getSelectLabel(num) + 'Text: <input name="' + id + '" id="' + id + '" type="text" size="42">';
    }
    else if (selectedTask == "END_OF_ARTICLE") {
        cell = document.getElementById("parametersCell_" + num);
        cell.innerHTML = "This labels the end of article.";
    }
    else if (selectedTask == "END_OF_DOCUMENT") {
        cell = document.getElementById("parametersCell_" + num);
        cell.innerHTML = "This labels the end of document end parsing process.";
    }
    else if (selectedTask == "SET_POS") {
        cell = document.getElementById("parametersCell_" + num);
        id = "setPosVar_" + num;
        cell.innerHTML = "Position variable: " + '<select name="' + id + '" id="' + id + '">' + 
        	getVarsOptions() + '</select>';
    }
    else if (selectedTask == "EMPTY") {
    	cell = document.getElementById("parametersCell_" + num);
    	cell.innerHTML = '&nbsp;';
    }
    else if (selectedTask == "STORE_ISODATE") {
    	cell = document.getElementById("parametersCell_" + num);
    	var templateId = 'dateTemplate_' + num;
    	var varId = 'var_' + num;
    	cell.innerHTML = getSelectLabel(num) + 
    		'Template:' + '<input name="' + templateId + '" id="' + templateId + '" type="text" size="20">' + 
    		'Text: <input name="' + varId + '" id="' + varId + '" type="text" size="15">';
    		;
    		
    }
    else if (selectedTask == "SAVE_TEXT") {
        cell = document.getElementById("parametersCell_" + num);
		fromId = "from_" + num;
		toId = "to_" + num;
		id = "var_" + num;
        cell.innerHTML = '<input name="' + id + '" id="' + id + '" type="text" size="26" value="$text' + num + '">' + 
        	' from ' + getPositionSelectFrom(num) + 
        	' to ' + getPositionSelectTo(num);
    }

	curTaskNum = num;
    allTasks[num] = selectedTask;

	cell = document.getElementById("trueFlowTd_" + num);
	cell.innerHTML = createTrueFlowSelect(num);
	cell = document.getElementById("falseFlowTd_" + num);
	cell.innerHTML = createFalseFlowSelect(num);
    // ...
}

function createTrueFlowSelect(num)
{
	var html = new String();
	id = "trueFlow_" + num;
	
	var gotoTasks = new String();
//	for (var i = 1; i < curTaskNum; i++) {
//		gotoTasks += "<option>" + "GOTO_TASK\t" + i + " " + allTasks[i] + "</option>";
//	}
	gotoTasks += '<option>EMPTY</option>';
	
	for (var i = 1; i <= maxTasksNum; i++) {
		gotoTasks += "<option>" + "GOTO_TASK\t" + i + "</option>";
	}
	
	html = "<select name=\"" + id + "\" id=\"" + id + "\"><option selected>next</option><option>END_OF_DOCUMENT</option>" + gotoTasks + "</select>";
	return html;
}

function createFalseFlowSelect(num)
{
	var html = new String();
	id = "falseFlow_" + num;
		var gotoTasks = new String();
//	for (var i = 1; i < curTaskNum; i++) {
//		gotoTasks += "<option>" + "GOTO_TASK\t" + i + " " + allTasks[i] + "</option>";
//	}
	gotoTasks += '<option>EMPTY</option>';
	for (var i = 1; i <= maxTasksNum; i++) {
		gotoTasks += "<option>" + "GOTO_TASK\t" + i + "</option>";
	}

	html = "<select name=\"" + id + "\" id=\"" + id + "\"><option>next</option><option selected>END_OF_DOCUMENT</option>" + gotoTasks + "</select>";
	return html;
}



//function doFindNode()
//{
//  var selected = getSelectedText();
//  document.forms[0].textarea.value += "FindNode\t... " + selected + "\t[next][END_OF_DOCUMENT]";
//}
//
//function doFindStoreText()
//{
//  var selected = getSelectedText();
//      var selIdx = document.forms[0].storeLabel.selectedIndex;
//  var label = document.forms[0].storeLabel[selIdx].text;
//  document.forms[0].textarea.value += "FindStoreText\t... " + selected + "\t" + label + "\n";
//}
//
//function doStartOfContent()
//{
//  document.forms[0].textarea.value += "StartOfContent\n";
//}
//
//function doEndOfContent()
//{
//  document.forms[0].textarea.value += "EndOfContent\n";
//}
//
//function doStoreArticle()
//{
//  document.forms[0].textarea.value += "StoreArticle\n";
//}
//

function doSubmit()
{
  	var pageContent = parent.page.document.documentElement.innerHTML;
  	document.forms[0].pageContent.value = pageContent;
  	document.forms[0].scriptContent.value = getScript();
  	document.forms[0].submit();
}

function viewScript()
{
	alert(getScript());
}

function getScript()
{
	var script = new String();
	for (var i = 1; i <= curTaskNum; i++) {
		script += allTasks[i] + '\t' + getTaskParams(allTasks[i], i) + '\t' + getTaskFlow(allTasks[i], i) + "\n";
	}
	return script;
}

function getTaskParams(selectedTask, num)
{
	var paramStr = new String();
	if (selectedTask == "FIND_NODE") {
		nodePattern = document.getElementById("nodePattern_" + num);
		paramStr = nodePattern.value;
    }
    else if (selectedTask == "STORE_TEXT") {
		selectLabel = document.getElementById("selectLabel_" + num);
		posSelectFrom = document.getElementById("posSelectFrom_" + num);
		posSelectTo = document.getElementById("posSelectTo_" + num);
		
		fromPos = posSelectFrom.options[posSelectFrom.selectedIndex].text;
		if (fromPos == "last found pos") {
			fromPos = -1;
		}
		toPos = posSelectTo.options[posSelectTo.selectedIndex].text;
		if (toPos == "end of tag") {
			toPos = -1;
		}
		
		paramStr = selectLabel.options[selectLabel.selectedIndex].text + '\t' +
			fromPos + '\t' + toPos;
    }
    else if (selectedTask == "SAVE_POS") {
	    posVar = document.getElementById("posVar_" + num);
	    paramStr = posVar.value;
    }
    else if (selectedTask == "GOTO_TASK") {
	    gotoNum = document.getElementById("gotoNum_" + num);
	    paramStr = gotoNum.options[gotoNum.selectedIndex].text;
    }
    else if (selectedTask == "STORE_NODE") {
    	storeLabel = document.getElementById("storeNode_" + num);
    	selectLabel = document.getElementById("selectLabel_" + num);
    	paramStr = selectLabel.options[selectLabel.selectedIndex].text + '\t' + storeLabel.value;
    }
    else if (selectedTask == "END_OF_ARTICLE") {
    }
    else if (selectedTask == "END_OF_DOCUMENT") {
    }
    else if (selectedTask == "SET_POS") {
	    setPosVar = document.getElementById("setPosVar_" + num);
	    paramStr = setPosVar.options[setPosVar.selectedIndex].text;
    }
    else if (selectedTask == "SAVE_TEXT") {
    	textVar = document.getElementById("var_" + num);
    	posSelectFrom = document.getElementById("posSelectFrom_" + num);
    	posSelectTo = document.getElementById("posSelectTo_" + num);
    	
    	fromPos = posSelectFrom.options[posSelectFrom.selectedIndex].text;
		if (fromPos == "last found pos") {
			fromPos = -1;
		}
		toPos = posSelectTo.options[posSelectTo.selectedIndex].text;
		if (toPos == "end of tag") {
			toPos = -1;
		}
		
		paramStr = textVar.value + '\t' +
			fromPos + '\t' + toPos;
    	
    }
	else if (selectedTask == "STORE_ISODATE") {
		//todo
		var dateTemplate = document.getElementById("dateTemplate_" + num);
		var textVar = document.getElementById("var_" + num);
		var selectLabel = document.getElementById("selectLabel_" + num);
		
		paramStr = selectLabel.options[selectLabel.selectedIndex].text + 
			'\t' + dateTemplate.value + 
			'\t' + textVar.value;
    }
    else if (selectedTask == "EMPTY") {
    }
    

	return paramStr;
}

function getTaskFlow(selectedTask, num)
{
	trueFlow = document.getElementById("trueFlow_" + num);
	falseFlow = document.getElementById("falseFlow_" + num);
	trueTask = trueFlow.options[trueFlow.selectedIndex].text;
	falseTask = falseFlow.options[falseFlow.selectedIndex].text;
	if (trueTask.search("GOTO_TASK") != -1) {
		trueTask = trueTask.replace(' ', "\t");
	}
	if (falseTask.search("GOTO_TASK") != -1) {
		falseTask = falseTask.replace(' ', "\t");
	}
	
	if (selectedTask == "GOTO_TASK") {
		trueTask = "";
		falseTask = "";
	}
	
	return "[" + trueTask + "][" + falseTask + "]";
}



////////////////////////

//function FindNode(selectedText)
//{
//    this.name = "FIND_NODE";
//    this.selectedText = selectedText;
//    this.num = ++NUM;
//    
//    this.paramsCellHTML = "<input name=\"textfield\" type=\"text\" size=\"60\" onClick=\"findNode_TextField_onclick()\">";
//    
//    allNodes[this.num] = this;
//    
//    this.getHTML = function()
//    {
//        
//    }
//}
//
//function findNode_TextField_onclick()
//{
//    alarm("here");
//}
