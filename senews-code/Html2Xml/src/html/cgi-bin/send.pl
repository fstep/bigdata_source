#!/usr/bin/perl -w

use strict;
use TanaSend;
use WebForm;
&WebForm::GetFormInput;

# 1. get script and html source
my $script = $field{'scriptContent'};
$script =~ s/<?\!--(.*?)-->?/#comment \$value='$1'/g;
#my $page_content = $field{pageContent};
my $url = read_url();
my $page_content = read_content();

# 2. send them to html2xml java server via TANA and receive result
my $host = 'localhost';
my $port = 12345;
my %h = ('cmd' => 'html2xml', 'content'=>$page_content, 'script'=>$script, 'url'=>$url, 
'charset' => 'ISO-8859-1');
my $result = TanaSend::sendMsg($host, $port, TanaSend::hashToStr(\%h));

# 3. print result

if (defined($result->{'error'})) {
    #print "Content-Type: text/html\n\n";
print <<EOF;
<html>
<head>
<title>Script test result</title>
</head>
<body>
error: $result->{'error'} <br>
</body>
</html>
EOF
} else {
	my $xml_file = '_out.xml';
	open(OUT, ">$xml_file") or die $!;
	print OUT $result->{'xml-content'};
	close OUT;
  print "<html><META HTTP-EQUIV=\"Refresh\" CONTENT=\"0; URL=$xml_file\"></html>\n\n";
}


sub read_content
{
    open(FP, "_orig.html") or die $!;
    my $content = '';
    while (defined(my $str = <FP>)) {
        $content .= $str."\n";
    }
    close FP;
    return $content;
}

sub read_url
{
	open(FP, "_orig.url") or die $!;
	my $url = <FP>;
	chomp $url;
	$url =~ s/^\s*//;
	$url =~ s/\s*$//;
	close FP;
	return $url;
}
