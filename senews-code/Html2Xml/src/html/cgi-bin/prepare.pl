#!/usr/bin/perl -w

use strict;
require HTTP::Request;
use LWP::UserAgent;

use WebForm;
&WebForm::GetFormInput;

my $ua = LWP::UserAgent->new;
$ua->agent('Mozilla/5.0');


my $address = $field{'address'};

my $content_file = '_content.html';
my $orig_file = '_orig.html';
my $orig_url_file = '_orig.url';
my $out_xml_file = '_out.xml';

unlink $content_file;
unlink $orig_file;
unlink $orig_url_file;
unlink $out_xml_file;


# 1. download page
my $content = getDocByUrl($address);
open(FP, ">$orig_file") or die $!;
print FP $content;
close FP;

# 2. prepare page and save it
my $font_start = '<font size="1" color="#808080">';
my $font_end = '</font>';
$content =~ s/<([^\/][^>]*?)>/<$1>$font_start &lt;$1&gt; __END_OF_FONT__/g;
$content =~ s/<(\/[^>]*?)>/$font_start &lt;$1&gt; __END_OF_FONT__<$1>/g;
$content =~ s/__END_OF_FONT__/$font_end/g;
open(FP, ">$content_file") or die $!;
print FP $content;
close FP;

# 2.1 save url
open(FP, ">$orig_url_file") or die $!;
print FP $address;
close FP;

# 3. print page
#print "Content-Type: text/html\n\n";

print <<EOF;
<html>
<head>
<title>Script builder</title>
</head>
<frameset rows="151,*">
	<frame name="controls" target="main" src="/html2xml/controls.html">
	<frame name="page" src="/cgi-bin/html2xml/redir.html">
	<noframes>
	<body>

	</body>
	</noframes>
</frameset>
</html>
EOF

sub getDocByUrl
{
    my $url = shift;
    my $request = HTTP::Request->new(GET => $url);
    my $response = $ua->request($request);
    return $$response{_content};
}