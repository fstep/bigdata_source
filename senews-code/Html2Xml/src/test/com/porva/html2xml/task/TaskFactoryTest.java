/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.html2xml.task;

import junit.framework.TestCase;

public class TaskFactoryTest extends TestCase
{
  String[] correctTasks = {
      "FIND_NODE\t... div",
      "FIND_NODE\tdiv ...",
      "FIND_NODE\t/div ...",
      "FIND_NODE\t/div ...",
      "FIND_NODE\t... font face='Verdana, Arial, Helvetica, sans-serif' size='+1'",
      "FIND_NODE\t... /font face='Verdana, Arial, Helvetica, sans-serif' size='+1'",

      "STORE_TEXT\tlabel\t0\t0",
      "STORE_TEXT\tlabel\t0\t100",
      "STORE_TEXT\tlabel\t99\t100",
      "STORE_TEXT\tlabel\t-1\t100",

      "END_OF_ARTICLE",
      "END_OF_DOCUMENT",
      "GOTO_TASK\t1",
      "SET_POS\t1",
      "SET_POS\t0",

      "STORE_ISODATE\tiso-date\tMMMMM dd',' yyyy\tsome text\twith tabs",
      "EMPTY",

      "SAVE_TEXT\t$text\t-1\t-1",

      "STORE_NODE\tlabel\tsome content",
      "STORE_NODE\tlabel\tsome\t content",
//      "INVOKE_METHOD\tcom.porva.html2xml.scripts.Script_sew.extractIsoDate(java.lang.String)\twe are here",
      
      "STORE_LINKS\t1\t2",
      
      "REGEXP\ttext here\tpattern"
  };

  String[] incorrectTasks = {
      null,
      "UNKNOWN_TASK\t... div",

      "FIND_NODE  /div ...",
      "FIND_NODE",
      "FIND_NODE\t",
      "FIND_NODE\t   ",

      "STORE_TEXT\t\t0\t0",
      "STORE_TEXT\t0\t100",
      "STORE_TEXT\tlabel\ta\t100",
      "STORE_TEXT\tlabel\t2\t1",

      "GOTO_TASK",
      "GOTO_TASK\t0",
      "GOTO_TASK\t-1",

      "SET_POS",
      "SET_POS\t-1",

      "STORE_ISODATE\t",
      
      "STORE_LINKS\t",
      "STORE_LINKS\t1",
      "STORE_LINKS\t1\tx",
      
      "REGEXP\tpattern",
  };


  public void testNewTask() throws Exception
  {
    for (String aCorrectTask: correctTasks) {
      Task task = TaskFactory.newTask(aCorrectTask.split("\t"));
      assertNotNull(task);
    }

    for (String anIncorrectTask: incorrectTasks) {
      try {
        TaskFactory.newTask(anIncorrectTask.split("\t"));
        fail("Incorrect task \"" + anIncorrectTask + "\" did not produce exception.");
      } catch(Exception e) {}
    }

  }

//  public void testParserParams() throws Exception
//  {
//    Object[] val = TaskFactory.parserParams("5", new Class[]{Integer.class});
//    assertEquals(val.length, 1);
//    assertTrue(val[0] instanceof Integer);
//
//    val = TaskFactory.parserParams("", new Class[]{});
//    assertEquals(val.length, 0);
//
//    val = TaskFactory.parserParams("5\ttest", new Class[]{Integer.class, String.class});
//    assertEquals(val.length, 2);
//    assertTrue(val[0] instanceof Integer);
//    assertTrue(val[1] instanceof String);
//
//    try {
//      TaskFactory.parserParams("5a", new Class[]{Integer.class});
//      fail();
//    } catch(Exception e) {}
//  }
}