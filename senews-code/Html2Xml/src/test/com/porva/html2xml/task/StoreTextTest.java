/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.html2xml.task;

import junit.framework.TestCase;

public class StoreTextTest extends TestCase
{

  public void testStoreText() throws Exception
  {
    new StoreText("test", 0, 0);
    new StoreText("test", 0, 3);
    new StoreText("test", 1, 3);
    new StoreText("test", -1, 3);
    new StoreText("test", 1, -3);

    try {
      new StoreText(null, 1, 3);
      fail();
    } catch(NullPointerException e) {}

    try {
      new StoreText("", 1, 3);
      fail();
    } catch(IllegalArgumentException e) {}

    try {
      new StoreText("test", 3, 2);
      fail();
    } catch(IllegalArgumentException e) {}

  }

//  public void testPerform() throws Exception
//  {
//    byte[] content = FioUtils.readFileAsBytes(TestsConfig.TEST_DIR + "www.searchenginelowdown.com~1.html");
//    Html2XmlParser testParserCtrl = new Html2XmlParser(content, null);
//
//    StoreText storeText = new StoreText("test", 0, 100);
//    storeText.perform(testParserCtrl);
//
//    StoreTaskResult result = testParserCtrl.getResults().get(0);
//    assertTrue(result.getLabel().equals("test"));
//    System.out.println(result.getContent());
//  }
}