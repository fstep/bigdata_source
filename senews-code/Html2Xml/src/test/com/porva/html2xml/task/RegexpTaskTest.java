package com.porva.html2xml.task;

import org.apache.commons.lang.NullArgumentException;

import junit.framework.TestCase;

public class RegexpTaskTest extends TestCase
{
  
  /*
   * Test method for 'com.porva.html2xml.task.RegexpTask.RegexpTask(String, String)'
   */
  public void testRegexpTask()
  {
    try {
      new RegexpTask("text", null);
      fail();
    } catch (NullArgumentException  e) {
    }
    try {
      new RegexpTask(null, "pattern");
      fail();
    } catch (NullArgumentException  e) {
    }
    
    new RegexpTask("Posted 29, 2005 by Nathan Weinberg", "/Posted (\\d+), (\\d+) by (.*)/i");
  }

  /*
   * Test method for 'com.porva.html2xml.task.RegexpTask.perform(ScriptRunner)'
   */
  public void testPerform()
  {
    RegexpTask task = new RegexpTask("Posted 29, 2005 by Nathan Weinberg", "/Posted (\\d+), (\\d+) by (.*)/i");
    task.perform(null);
    assertEquals("Posted 29, 2005 by Nathan Weinberg", task.getVars().get("$0"));
    assertEquals("29", task.getVars().get("$1"));
    assertEquals("2005", task.getVars().get("$2"));
    assertEquals("Nathan Weinberg", task.getVars().get("$3"));
    
    
    task = new RegexpTask("Posted 29, 2005 by &$test @ ", "/Posted (\\d+), (\\d+) by (.*)/i");
    task.perform(null);
  }

}
