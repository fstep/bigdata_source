/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.html2xml;

import junit.framework.TestCase;

public class ScriptParserTest extends TestCase
{

  public void testRemoveComments() throws Exception
  {
    String script = "FIND_NODE\t... <div class=\"Post\">" + "\n" +
        " # SAVE_POS\t$content_start" + "\n" +
        "FIND_NODE\t... <div class=\"Post\">";

    String scriptNoComments = "FIND_NODE\t... <div class=\"Post\">" + "\n" +
        "FIND_NODE\t... <div class=\"Post\">" + "\n";

    StringScriptParser stringScriptParser = new StringScriptParser(script);
    String withoutComments = stringScriptParser.removeComments(script);
    assertEquals(withoutComments, scriptNoComments);
  }

//  public void testPreprocessTask() throws Exception
//  {
//    StringScriptParser stringScriptParser = new StringScriptParser("SAVE_POS\t$content_start\nSTORE_POS\t$content_end");
//    SavePos storeTask = (SavePos)stringScriptParser.getNextTask();
//    storeTask.setPos(10);
//    stringScriptParser.fireTaskPerformed(storeTask);
//    storeTask = (SavePos)stringScriptParser.getNextTask();
//    storeTask.setPos(20);
//    stringScriptParser.fireTaskPerformed(storeTask);
//
//    String preprocessedTaskStr = stringScriptParser.preprocessTask("STORE_TEXT\tcontent\t$content_start\t$content_end", 2);
//    assertEquals(preprocessedTaskStr, "STORE_TEXT\tcontent\t10\t20");
//
//    preprocessedTaskStr = stringScriptParser.preprocessTask("SET_POS\t$content_start", 1);
//    assertEquals(preprocessedTaskStr, "SET_POS\t10");
//  }
}