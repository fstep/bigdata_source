/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.html2xml;

import java.io.File;

/**
 * Paths for testing.
 * 
 * @author Poroshin V.
 * @date Feb 25, 2006
 */
public class TestsConfig
{
  /**
   * Test directory. This is location of html files for testing. 
   */
  public static final String TEST_DIR = System.getProperty("user.dir") + File.separator + "test"
      + File.separator;

  /**
   * Scripts directory. This is location of scripts files.
   */
  public static final String SCRIPTS_DIR = System.getProperty("user.dir") + File.separator + "conf"
      + File.separator + "scripts" + File.separator;

  /**
   * Location of html2xml.properties file.
   */
  public static final String PROP_FILE = System.getProperty("user.dir") + File.separator + "conf"
      + File.separator + "html2xml.properties";

}
