/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.html2xml;

import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.util.List;
import java.util.Vector;

import javax.xml.parsers.ParserConfigurationException;

import junit.framework.TestCase;

import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.porva.util.FioUtils;
import static com.porva.html2xml.TestsConfig.*;

public class ScriptRunnerImplTest extends TestCase
{

  // make sure last on is "iso-8859-1"
  private static final String[] checkedEncodings = { "utf-8", "utf-16", "iso-8859-1" };

  public void testAllScripts() throws Exception
  {
    String testFileContent = FioUtils.readFileAsString(TEST_DIR + "all_sripts.test", "iso-8859-1");
    ScriptTestCase testCase = new ScriptTestCase(testFileContent);
    testCase.runAllTests();
  }

  private static class ScriptTestCase
  {
    List<ScriptTest> tests = new Vector<ScriptTest>();

    public ScriptTestCase(String testFileContent) throws ParserConfigurationException
    {
      String[] lines = testFileContent.split("\n");

      boolean isRunAll = false;
      String testNameToRun = new String();
      boolean isRunTest = false;

      String testName = null;
      String script = null;
      String source = null;
      String expectedPattern = new String();
      String url = null;
      boolean isInExpectedPattern = false;

      for (String aLine : lines) {
        if (aLine.startsWith("#")) {
          continue;
        }

        if (aLine.startsWith("runTests=")) {
          if (aLine.endsWith("=all"))
            isRunAll = true;
          else
            testNameToRun = aLine.substring(9);
        } else if (aLine.startsWith("endOfExpectedPattern")) {
          isInExpectedPattern = false;
        } else if (isInExpectedPattern) {
          expectedPattern += aLine.trim();
        } else if (aLine.startsWith("test=")) {
          testName = aLine.substring(5);
          isRunTest = testName.equals(testNameToRun);
        } else if (aLine.startsWith("script=")) {
          script = aLine.substring(7);
        } else if (aLine.startsWith("source=")) {
          source = aLine.substring(7);
        } else if (aLine.startsWith("url=")) {
          url = aLine.substring(4);
        } else if (aLine.startsWith("beginOfExpectedPattern")) {
          isInExpectedPattern = true;
        } else if (aLine.startsWith("endOfTest")) {
          if (isRunTest || isRunAll)
            tests.add(new ScriptTest(testName, script, source, expectedPattern, url));
          testName = null;
          script = null;
          source = null;
          url = null;
          expectedPattern = new String();
        }
      }
    }

    public void runAllTests() throws Exception
    {
      for (ScriptTest scriptTest : tests) {
        boolean res = scriptTest.test();
        if (!res)
          System.out.println("[failed]\t" + scriptTest.toString());
        else
          System.out.println("[ok]\t" + scriptTest.toString());

        assertTrue(res);
      }
    }
  }

  private static class ScriptTest
  {
    String name;

    String scriptFile;

    String sourceFile;

    String expectedPattern;

    String url;

    DefaultScriptRunner scriptRunnerImpl;

    NewsXMLResultFormatter xmlResultFormatter;

    public ScriptTest(String name,
                      String scriptFile,
                      String sourceFile,
                      String expectedPattern,
                      String url) throws ParserConfigurationException
    {
      this.name = name;
      this.scriptFile = scriptFile;
      this.sourceFile = sourceFile;
      this.expectedPattern = expectedPattern;
      this.url = url;
      scriptRunnerImpl = new DefaultScriptRunner();
      xmlResultFormatter = new NewsXMLResultFormatter();
    }

    private boolean test() throws Exception
    {
      byte[] content = FioUtils.readFileAsBytes(TEST_DIR + sourceFile);
      String script = FioUtils.readFileAsString(SCRIPTS_DIR + scriptFile, "iso-8859-1");
      URI uri = null;
      if (url != null)
        uri = new URI(url);

      String xml = null;
      for (String encoding : checkedEncodings) {
        String contentStr = new String(content, encoding);
        xml = xmlResultFormatter.format(scriptRunnerImpl.performScript(contentStr, uri,
                                                                       new StringScriptParser(
                                                                           script)), encoding,
                                                                           scriptRunnerImpl);
        assertTrue(checkXMLWellFormedness(xml));
      }
      System.out.println(xml);

      return ScriptTester.testResult(xml, expectedPattern);
    }

    private boolean checkXMLWellFormedness(String xmlDoc) throws IOException
    {
      SAXBuilder builder = new SAXBuilder();

      try {
        builder.build(new StringReader(xmlDoc));
        System.out.println("well-formed");
      } catch (JDOMException e) { // indicates a well-formedness error
        System.out.println("is not well-formed.");
        System.out.println(e.getMessage());
        return false;
      }
      return true;
    }

    public String toString()
    {
      return "ScriptTest{" + "name='" + name + "'" + ", scriptFile='" + scriptFile + "'"
          + ", sourceFile='" + sourceFile + "'" + "}";
    }
  }

}