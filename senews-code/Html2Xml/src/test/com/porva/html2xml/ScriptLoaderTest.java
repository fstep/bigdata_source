/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.html2xml;

import java.io.File;
import java.net.URI;
import java.util.List;

import junit.framework.TestCase;

import org.apache.commons.lang.NullArgumentException;

public class ScriptLoaderTest extends TestCase
{
  private File propFilename = new File(TestsConfig.PROP_FILE);

  public void testScriptLoader() throws Exception
  {
    try {
      new ScriptLoader(null, new File(TestsConfig.SCRIPTS_DIR));
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      new ScriptLoader(propFilename, null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      new ScriptLoader(propFilename, new File("does/not/exist"));
      fail();
    } catch (IllegalArgumentException e) {
    }
    try {
      new ScriptLoader(new File("does/not/exist"), new File(TestsConfig.SCRIPTS_DIR));
      fail();
    } catch (IllegalArgumentException e) {
    }
    try {
      new ScriptLoader(new File(TestsConfig.SCRIPTS_DIR), new File(TestsConfig.SCRIPTS_DIR));
      fail("First param must not be a dir");
    } catch (IllegalArgumentException e) {
    }
    try {
      new ScriptLoader(propFilename, propFilename);
      fail("Second param must be a dir");
    } catch (IllegalArgumentException e) {
    }

    ScriptLoader scriptLoader = new ScriptLoader(propFilename, new File(TestsConfig.SCRIPTS_DIR));
    assertNotNull(scriptLoader);
  }

  public void testGetScript() throws Exception
  {
    ScriptLoader scriptLoader = new ScriptLoader(propFilename, new File(TestsConfig.SCRIPTS_DIR));
    List<String> scripts = scriptLoader
        .getScripts(new URI("http://www.researchbuzz.com/index.html"));
    assertTrue(scripts.size() == 1);
    assertTrue(scripts.get(0).startsWith("FIND_NODE\t... div class='blogbody' ..."));
  }
}