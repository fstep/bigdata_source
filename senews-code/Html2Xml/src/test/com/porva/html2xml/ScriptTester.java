/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.html2xml;

import org.apache.oro.text.perl.Perl5Util;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.CharArrayWriter;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ScriptTester
{
  private static Perl5Util perl5Regexp = new Perl5Util();
  private static SAXParserFactory factory = SAXParserFactory.newInstance();
  private static Pattern p = Pattern.compile("^(.+)\\[(\\d+)\\]$");
//  private static ScriptRunner parser = Html2XmlParser.newInstance();

//  public static String performScript(String filename, String charset, URI uri, Class script) throws Exception
//  {
//    String dir = System.getProperty("user.dir") + File.separator + "src" + File.separator + "test" + File.separator;
//    String file = dir + filename;
//
//    String content = FioUtils.readFileAsString(file);
//    if (charset == null)
//      charset = "iso-8859-1";
//    NewsScriptImpl newsScript = (NewsScriptImpl)script.newInstance();
//
//    return parser.performScript(content.getBytes(), charset, uri, newsScript);
//  }
//
  public static boolean testResult(String xml, String xmlPattern)
  {
    Map<String, String> xmlMap = xml2map(xml);
    String[] cmd = xmlPattern.split(";");
    for (int i = 0; i < cmd.length; i++) {
      String[] tok = cmd[i].split("\\s+", 3);
      String var = tok[0];
      String op  = tok[1];
      String res = tok[2];
      int pos = -1;

      Matcher m = p.matcher(var);
      if (m.matches()) {
        var = m.group(1);
        pos = Integer.parseInt(m.group(2));
      }

      if (op.equals("==")) {
        int expectedNum = Integer.parseInt(res);
        String numStr = xmlMap.get(var + ".num");
        if (numStr == null || Integer.parseInt(numStr) != expectedNum) {
          System.err.println("[failed]  " + cmd[i] + " [val=" + numStr + "]");
          return false;
        }
      } else if (op.equals("=~")) {
        String val = xmlMap.get(var + "[" + pos + "]");
        if (val == null || !perl5Regexp.match(res, val)) {
          System.err.println("[failed]  " + cmd[i] + "\n'" + val + "'");
          return false;
        }
      } else if (op.equals("eq")) {
        String val = xmlMap.get(var + "[" + pos + "]");
        res = res.substring(1, res.length() - 1);
        if (val == null || !val.equals(res)) {
          System.err.println("[failed]  " + cmd[i] + "\n" + val );
          return false;
        }
      } else
        throw new IllegalArgumentException("Invalid xmlPattern: unknown command '" + op + "'");
    }

    System.out.println("[ok]  test passed.");
    return true;
  }

//  /**
//   * @deprecated replace this method to {@link #performScript} and {@link #testResult}.
//   */
//  public static boolean test(String filename, String charset, URI uri, Class script, Map res) throws Exception
//  {
//    String dir = System.getProperty("user.dir") + File.separator + "src" + File.separator + "test" + File.separator;
//    String file = dir + filename;
//
//    String content = FioUtils.readFileAsString(file);
//    if (charset == null)
//      charset = "iso-8859-1";
//    NewsScriptImpl newsScript = (NewsScriptImpl)script.newInstance();
//    String xml = parser.performScript(content.getBytes(), charset, uri, newsScript);
//
//
//    if (res == null)
//      return true;
//    Map actualRes = xml2map(xml);
//    Iterator it = res.entrySet().iterator();
//    while (it.hasNext()) {
//      Map.Entry entry = (Map.Entry)it.next();
//      String key = (String)entry.getKey();
//      String val = (String)entry.getValue();
//
//      String actVal = (String)(actualRes.get(key));
//      if (actVal == null)
//        return false;
//
//      Pattern p = Pattern.compile("^" + val + "$", Pattern.DOTALL);
//      Matcher m = p.matcher(actVal);
//      if (!m.matches())
//        return false;
//    }
//    return true;
//  }
//
  public static void stdoutNotifyer(boolean res, String testname)
  {
    if (res)
      System.out.println("[ok] test " + testname);
    else
      System.err.println("[failed] test " + testname);
  }

  private static Map<String, String> xml2map(String xml)
  {
    Map<String, String> map = null;
    try {
      SAXParser parser = factory.newSAXParser();
      XmlMapper xmlMapper = new XmlMapper();
      parser.parse(new InputSource(new StringReader(xml)), xmlMapper);
      map = xmlMapper.getResult();
    } catch (Exception e) {
      e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
    }

    return map;
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  static class XmlMapper extends DefaultHandler
  {
    private CharArrayWriter contents = new CharArrayWriter();
    private Map<String, String> result = new HashMap<String, String>();
    private static String[] allowedTags = {"article", "date", "iso-date", "title", "content", "meta", "author"};

    public void startElement(String namespaceURI, String localName, String qName, Attributes attr) throws SAXException
    {
      boolean isAllowedTag = false;
      for (String allowedTag: allowedTags) {
        if (qName.equalsIgnoreCase(allowedTag)) {
          isAllowedTag = true;
          break;
        }
      }
      if (!isAllowedTag)
        return;
      
      contents.reset();

      String numStr = result.get(qName + ".num");
      if (numStr == null)
        result.put(qName + ".num", Integer.toString(1));
      else {
        int num = Integer.parseInt(numStr);
        result.put(qName + ".num", Integer.toString(num + 1));
      }
    }

    public void endElement(String namespaceURI, String localName, String qName) throws SAXException
    {
      result.put(qName + "[" + result.get(qName + ".num") + "]", contents.toString().trim());
    }

    public void characters( char[] ch, int start, int length ) throws SAXException
    {
      String chunk = new String(ch, start, length);
      chunk = perl5Regexp.substitute("s/\\s+/ /g", chunk);
      
      contents.write(chunk.toCharArray(), 0, chunk.length());
    }

    public Map<String, String> getResult()
    {
      return result;
    }
  }

//  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  private static class TesterLogFormatter extends Formatter
//  {
//
//    public synchronized String format(LogRecord record)
//    {
//      StringBuffer buffer = new StringBuffer(50);
//      buffer.append("[Script trace] ");
//      buffer.append(getCurTime());
//      buffer.append("\t");
//      buffer.append(record.getMessage());
//      buffer.append("\n");
//      return buffer.toString();
//    }
//
//    public static String getCurTime()
//    {
//      Calendar currTime = Calendar.getInstance();
////      int year = currTime.get(Calendar.YEAR);
////      int mon = currTime.get(Calendar.MONTH);
////      mon = mon + 1; //since month starts from 0
////      int date = currTime.get(Calendar.DATE);
//      int hour = currTime.get(Calendar.HOUR_OF_DAY); // ranges from 0..23
//      int min = currTime.get(Calendar.MINUTE);
//      int secs = currTime.get(Calendar.SECOND);
//
//      return hour + ":" + min + ":" + secs + " ";
//    }
//  }
//  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
