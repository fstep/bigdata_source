// InvalidDateException.java
// $Id$
// (c) COPYRIGHT MIT, INRIA and Keio, 2000.
// Please first read the full copyright statement in file COPYRIGHT.html2xml
package org.w3c.util;

/**
 * @version $Revision$
 * @author  Beno�t Mah� (bmahe@w3.org)
 */
public class InvalidDateException extends Exception 
{
  
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public InvalidDateException(String msg) 
  {
    super(msg);
  }
  
}

