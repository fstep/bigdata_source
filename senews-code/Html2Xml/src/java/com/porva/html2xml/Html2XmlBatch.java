/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.html2xml;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.oro.text.perl.Perl5Util;

import com.porva.html.CpDetector;
import com.porva.html.HTMLCpDetector;
import com.porva.html.keycontent.NewsXMLCleaner;
import com.porva.html2xml.task.Task;
import com.porva.util.FioUtils;

public class Html2XmlBatch
{

  public static final String DEF_SOURCE_ENC = "autodetect";

  public static final String DEF_META_ENC = "iso-8859-1";

  public static final String DEF_XML_ENC = "utf-8";

  public static final Charset DEF_CHARSET = Charset.forName("utf-8");

  private static Perl5Util perl5Regexp = new Perl5Util();

  private static final Log logger = LogFactory.getLog(Html2XmlBatch.class);

  public void runBatchProcess(String dir,
                              boolean isRecursively,
                              String sourceEncoding,
                              String metaEncoding,
                              String xmlEncoding) throws URISyntaxException, IOException,
      ParserConfigurationException
  {
    if (sourceEncoding == null)
      sourceEncoding = DEF_SOURCE_ENC;
    if (metaEncoding == null)
      metaEncoding = DEF_META_ENC;
    if (xmlEncoding == null)
      xmlEncoding = DEF_XML_ENC;

    ScriptLoader scriptLoader = new ScriptLoader(new File("conf/html2xml.properties"), new File(
        "conf/scripts"));
    DefaultScriptRunner scriptRunnerImpl = new DefaultScriptRunner();

    File[] files = getFilesToProcess(dir, isRecursively);
    int fileNum = 0;
    for (File file : files) {

      fileNum++;
      logger.info("Processing file " + file.getAbsolutePath());
      if ((fileNum % 50) == 0)
        System.out.println(fileNum + " files from " + files.length + " processed");

      // read meta file
      String metafile = perl5Regexp.substitute("s/original$/meta/", file.getAbsolutePath());
      Map<String, String> metaMap = null;
      try {
        metaMap = mapMeta(FioUtils.readFileAsString(metafile, metaEncoding));
      } catch (Exception e) {
        logger.warn("Invalid meta file " + metafile);
        continue;
      }
      URI uri = new URI(metaMap.get("url"));

      List<String> scriptsList = scriptLoader.getScripts(uri);
      if (scriptsList == null || scriptsList.size() == 0)
        continue;

      byte[] content = FioUtils.readFileAsBytes(file.getAbsolutePath());
      String contentStr = new String(content, getSourceEncoding(sourceEncoding, metaMap, content));

      String xml = null;
      for (String script : scriptsList) {
        try {
          List<Task> tasks = scriptRunnerImpl.performScript(contentStr, uri,
                                                            new StringScriptParser(script));
          NewsXMLResultFormatter xmlResultFormatter = new NewsXMLResultFormatter();
          xml = xmlResultFormatter.format(tasks, xmlEncoding, scriptRunnerImpl);
        } catch (Exception e) {
          logger.info("Html2Xml exception", e); // todo
          xml = "<CONTENT></CONTENT>";
        }

        if (xml.contains("<DOCUMENT/>") || xml.contains("<CONTENT></CONTENT>")
            || xml.contains("<CONTENT/>") || xml.contains("<content/>")
            || xml.contains("<content></content>")) {
          logger.debug(xml);
          xml = null;
        }

        if (xml != null)
          break;
      }

      if (xml != null) {
        xml = kce(xml);
        String parsedFile = perl5Regexp.substitute("s/original$/xml/", file.getAbsolutePath());
        FioUtils.writeToFile(parsedFile, xml, xmlEncoding);
        if (logger.isDebugEnabled())
          logger.debug("File parsed " + file.getAbsolutePath());
      } else {
        logger.warn("Failed to parse file " + file.getAbsolutePath());
      }
    }
  }

  private File[] getFilesToProcess(String dir, boolean isRecursively) throws IOException
  {
    File file = new File(dir);
    File[] files;
    if (isRecursively) {
      List<File> filesList = FioUtils.getFilesRecursively(file, new MyFileFilter());
      files = filesList.toArray(new File[filesList.size()]);
    } else {
      files = file.listFiles(new MyFileFilter());
    }
    return files;
  }

  private String getSourceEncoding(String sourceEncoding,
                                   Map<String, String> metaMap,
                                   byte[] content)
  {
    if (sourceEncoding.equals("autodetect")) {
      sourceEncoding = detectCharset(metaMap.get("Meta-content-type"), content).toString();
      logger.info("Detected encoding is " + sourceEncoding);
    }
    return sourceEncoding;
  }

  private Map<String, String> mapMeta(String metaContent)
  {
    Map<String, String> metaMap = new HashMap<String, String>();
    for (String str : metaContent.split("\n")) {
      String[] pair = str.split("\\s+", 2);
      metaMap.put(pair[0], pair[1]);
    }
    return metaMap;
  }

  private String kce(final String xml)
  {
    String cleaned = xml;
    try {
      NewsXMLCleaner newsXmlCleaner = new NewsXMLCleaner();
      cleaned = newsXmlCleaner.clean(xml);
    } catch (Exception e) {
      logger.warn("Failed to clean news xml", e);
      e.printStackTrace();
    }
    return cleaned;
  }

  private Charset detectCharset(String contentType, byte[] content)
  {
    HTMLCpDetector htmlCpDetector = new HTMLCpDetector(new CpDetector(), DEF_CHARSET);

    String httpCharset = HTMLCpDetector.getResponseCharset(contentType);
    String htmlCharset = HTMLCpDetector.getMetaTagCharset(content);

    return htmlCpDetector.detect(httpCharset, content, htmlCharset, null);
  }

  static class MyFileFilter implements FileFilter
  {
    public boolean accept(File pathname)
    {
      return pathname.getAbsolutePath().endsWith(".original");
    }
  }

}
