/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.html2xml;

public class NewsConstants
{
  public static final String DEF_ROOT_NAME = "DOCUMENT";
  //private String rootElement = DEF_ROOT_NAME;
  public static final String ROOT_STR = "DOCUMENT";
  public static final String ARTICLE_STR = "article";
  public static final String CONTENT_STR = "content";
  public static final String TITLE_STR = "title";
  public static final String DATE_STR = "date";
  public static final String ISODATE_STR = "iso-date";
  public static final String LINK_STR = "link";
  public static final String META_STR = "meta";
  public static final String AUTHOR_STR = "author";
}
