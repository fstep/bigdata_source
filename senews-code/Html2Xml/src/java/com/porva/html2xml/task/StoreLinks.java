/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.html2xml.task;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.porva.html.simpledom.SimpleAttribute;
import com.porva.html.simpledom.SimpleDOM;
import com.porva.html.simpledom.SimpleElement;
import com.porva.html2xml.ScriptRunner;

// STORE_LINKS $start_pos $end_pos
public class StoreLinks extends AbstractTask implements StoreTask
{
  private int start = -1;

  private int end = -1;

  private List<String> links = new ArrayList<String>();

  private List<String> anchorText = new ArrayList<String>();

  public StoreLinks(int start, int end)
  {
    super(TaskType.STORE_LINKS);
    this.start = start;
    this.end = end;

    if (this.start > this.end && this.end > 0)
      throw new IllegalArgumentException("start position cannot be more then end position: "
          + this.start + " > " + this.end);
  }

  public List<String> getLinks()
  {
    return links;
  }
  
  public List<String> getAnchorText()
  {
    return anchorText;
  }
  
  public String getLabel()
  {
    return "links";
  }

  public String getContent()
  {
    return null; // FIXME
  }

  public void perform(final ScriptRunner scriptRunnerCtrl)
  {
    if (scriptRunnerCtrl == null)
      throw new NullArgumentException("ScriptRunner object cannot be null.");

    if (start < 0)
      start = scriptRunnerCtrl.getCurIdx();

    final SimpleDOM nodes = scriptRunnerCtrl.getHTMLModel();
    URI base = scriptRunnerCtrl.getCurrentURI();
    if (end < 0)
      end = nodes.getEndElementNum(start);

    for (int i = start; i <= end; i++) {
      SimpleElement node = nodes.getElement(i);
      String name = node.getName();
      if (name.equalsIgnoreCase("STYLE") || name.equalsIgnoreCase("SCRIPT"))
        continue;
      if (node.getType() == SimpleElement.Type.ELEMENT_START && node.getName().equals("A")) {
        SimpleAttribute at = node.getAttribute("href", false);
        if (at != null) {
          anchorText.add(getAnchorText(i, nodes));
          links.add(resolve(base, at.getValue()));
        }
      }
    }

    setResultCode(ResultCode.COMPLETE);
  }

  private String resolve(URI base, String loc)
  {
    URI resolved = null;
    try {
      resolved = base.resolve(loc);
    } catch (Exception e) {
    }
    if (resolved != null)
      return resolved.toString();
    return loc;
  }

  // todo: test it
  private String getAnchorText(int hrefNodeNum, final SimpleDOM dom)
  {
    int endNum = dom.getEndElementNum(hrefNodeNum);
    StringBuffer sb = new StringBuffer();
    for (int i = hrefNodeNum + 1; i < endNum; i++) {
      if (dom.getElement(i).getType() == SimpleElement.Type.TEXT && dom.getElement(i).getValue() != null)
        sb.append(dom.getElement(i).getValue());
    }
    return sb.toString();
  }

  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("start", start)
        .append("end", end).append("links.num", links.size()).toString();
  }

}
