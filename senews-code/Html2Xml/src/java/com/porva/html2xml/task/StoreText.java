/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.html2xml.task;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.porva.html.simpledom.SimpleDOM;
import com.porva.html.simpledom.SimpleDOMRender;
import com.porva.html2xml.ScriptRunner;

/**
 * Stores all text inside tags defined by start and end position.
 */
class StoreText extends AbstractTask implements StoreTask, RangeableTask
{
  private String label;

  private int start = -1;

  private int end = -1;

  private String content;

  private SimpleDOMRender nodeRender = new SimpleDOMRender();
  
  private SimpleDOM simpleDOM;

  StoreText(String contentLabel, int start, int end)
  {
    super(Task.TaskType.STORE_TEXT);
    this.label = contentLabel;
    this.start = start;
    this.end = end;

    if (this.label == null)
      throw new NullPointerException("label cannot be null.");
    if (this.label.trim().length() == 0)
      throw new IllegalArgumentException("label cannot be empty string.");
    if (this.start > this.end && this.end > 0)
      throw new IllegalArgumentException("start position cannot be more then end position: "
          + this.start + " > " + this.end);
  }

  public void perform(final ScriptRunner scriptRunnerCtrl)
  {
    if (scriptRunnerCtrl == null)
      throw new NullPointerException("ScriptRunner object cannot be null.");

    if (start < 0)
      start = scriptRunnerCtrl.getCurIdx();

    simpleDOM = scriptRunnerCtrl.getHTMLModel();
    if (end < 0)
      end = simpleDOM.getEndElementNum(start);

    content = nodeRender.renderAsVisibleText(simpleDOM, start, end);
    setResultCode(ResultCode.COMPLETE);
  }

  public String getLabel()
  {
    return label;
  }

  public String getContent()
  {
    return content;
  }
  
  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("label", label)
        .append("start", start).append("end", end).append("content", content).toString();
  }

  public int getStart()
  {
    return start;
  }

  public int getEnd()
  {
    return end;
  }

}
