/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.html2xml.task;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.oro.text.perl.Perl5Util;

import com.porva.html.simpledom.SimpleAttribute;
import com.porva.html.simpledom.SimpleDOM;
import com.porva.html.simpledom.SimpleElement;

class NodePattern
{
  private String nodeName;

  private List<AttributeMatcher> attributes = null;

  private static Perl5Util perl5Regexp = new Perl5Util();

  public NodePattern(String nodeName, List<AttributeMatcher> attributes)
  {
    this.nodeName = nodeName;
    this.attributes = attributes;

    if (this.nodeName == null)
      throw new NullPointerException("nodeName cannot be null.");
  }

  public String getNodeName()
  {
    return nodeName;
  }

  public boolean matches(final SimpleDOM model, int num)
  {
    SimpleElement node = model.getElement(num);
    SimpleElement firstChildNode = null;
    if (model.size() > num + 1)
      firstChildNode = model.getElement(num + 1);

    if (node.getType() == SimpleElement.Type.ELEMENT_END)
      return false;

    if (node == null) {
      // logger.warning("Node is null!");
      return false;
    }

    String name = node.getName();
    if (!nodeName.equalsIgnoreCase(name))
      return false;

    if (attributes != null) {
      for (AttributeMatcher attr : attributes) {
        if (attr.variable != null) {
          if (!attr.matches(node, firstChildNode))
            return false;
        } else {
          SimpleAttribute an = node.getAttribute(attr.name, false); // todo: attrMap can be null !!!
          if (an == null)
            return false;
          if (!attr.matches(an, firstChildNode))
            return false;
        }
      }
    }
    return true;
  }

  // ////////////////////////////////////////////////////////////////////////
  static class AttributeMatcher
  {
    String name = null;

    Variable variable = null;

    String value = null;

    Operator operator = null;

    public AttributeMatcher(String name, String value, Operator operator)
    {
      this.name = name;
      this.value = value;
      this.operator = operator;
      if (this.name == null)
        throw new NullPointerException("name of Attribute cannot be null");
      if (this.value == null)
        throw new NullPointerException("value of Attribute cannot be null");
      if (this.operator == null)
        throw new NullPointerException("operator of Attribute cannot be null");
    }

    public AttributeMatcher(Variable var, String value, Operator operator)
    {
      this.variable = var;
      this.value = value;
      this.operator = operator;
      if (this.variable == null)
        throw new NullPointerException("variable of Attribute cannot be null");
      if (this.value == null)
        throw new NullPointerException("value of Attribute cannot be null");
      if (this.operator == null)
        throw new NullPointerException("operator of Attribute cannot be null");
    }

    private boolean matches(final SimpleElement iNode, final SimpleElement firstChild)
    {
      String value = iNode.getValue();
      if (variable == null) {
        if (name == null)
          throw new NullPointerException();
        return this.matches(value);
      }

      // FIXME
      String firstChildNodeName = null;
      String firstChildNodeValue = null;
      if (firstChild != null) {
        firstChildNodeName = firstChild.getName();
        firstChildNodeValue = firstChild.getValue();
      }

      String valToMatch = null;
      switch (variable) {
      case VALUE:
        valToMatch = value;
        break;
      case FIRST_CHILD_NODE:
        valToMatch = firstChildNodeName;
        break;
      case FIRST_CHILD_NODE_VALUE:
        valToMatch = firstChildNodeValue;
        break;
      }

      return matches(valToMatch);
    }

    private boolean matches(final SimpleAttribute iNode, final SimpleElement firstChild)
    {
      String value = iNode.getValue();
      if (variable == null) {
        if (name == null)
          throw new NullPointerException();
        return this.matches(value);
      }

      // FIXME
      String firstChildNodeName = null;
      String firstChildNodeValue = null;
      if (firstChild != null) {
        firstChildNodeName = firstChild.getName();
        firstChildNodeValue = firstChild.getValue();
      }

      String valToMatch = null;
      switch (variable) {
      case VALUE:
        valToMatch = value;
        break;
      case FIRST_CHILD_NODE:
        valToMatch = firstChildNodeName;
        break;
      case FIRST_CHILD_NODE_VALUE:
        valToMatch = firstChildNodeValue;
        break;
      }

      return matches(valToMatch);
    }

    public boolean matches(String str)
    {
      if (str == null)
        return false;
      if (operator == Operator.REGEXP && !perl5Regexp.match(value, str))
        return false;
      if (operator == Operator.EQUAL && !str.equals(value))
        return false;
      return true;
    }

    public String toString()
    {
      return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("name", name)
          .append("variable", variable).append("value", value).append("operator", operator)
          .toString();
    }
  }

  // ////////////////////////////////////////////////////////////////////////

  static enum Variable
  {
    VALUE("$value"),
    FIRST_CHILD_NODE("$firstChildNode"),
    FIRST_CHILD_NODE_VALUE("$firstChildNodeValue");

    private String varStr;

    Variable(String varStr)
    {
      this.varStr = varStr;
    }

    public String toString()
    {
      return varStr;
    }

    public static Variable fromString(String varStr)
    {
      for (int i = 0; i < Variable.values().length; i++) {
        if (varStr.equals(Variable.values()[i].toString())) {
          return Variable.values()[i];
        }
      }
      return null; // todo: return null ???
    }
  }

  static enum Operator
  {
    EQUAL("="),
    REGEXP("=~");

    private String operatorStr;

    Operator(String operatorStr)
    {
      this.operatorStr = operatorStr;
    }

    public String toString()
    {
      return operatorStr;
    }

    public static Operator fromString(String operatorStr)
    {
      for (int i = 0; i < Operator.values().length; i++) {
        if (operatorStr.equals(Operator.values()[i].toString())) {
          return Operator.values()[i];
        }
      }
      return null; // todo: return null ???
    }
  }

  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("nodeName", nodeName)
        .append("attributes", attributes).toString();
  }

}
