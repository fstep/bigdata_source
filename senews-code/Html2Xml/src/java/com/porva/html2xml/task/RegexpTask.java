/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.html2xml.task;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.oro.text.perl.Perl5Util;

import com.porva.html2xml.ScriptRunner;

// REGEXP text pattern
class RegexpTask extends AbstractTask implements VarTask
{
  private String text;

  private String pattern;

  private Perl5Util perl5Regexp = new Perl5Util();

  private Map<String, String> res = new HashMap<String, String>();

  public RegexpTask(final String text, final String pattern)
  {
    super(TaskType.REGEXP);
    this.text = text;
    this.pattern = pattern;

    if (this.text == null)
      throw new NullArgumentException("text");
    if (this.pattern == null)
      throw new NullArgumentException("pattern");
  }

  public Map<String,String> getVars()
  {
    return res;
  }

  public void perform(final ScriptRunner scriptRunnerCtrl)
  {
    if (perl5Regexp.match(pattern, text)) {
      setResultCode(ResultCode.COMPLETE);
      for (int i = 0; i < perl5Regexp.groups(); i++) {
        res.put("$" + i, perl5Regexp.group(i));
      }
    } else {
      setResultCode(ResultCode.FAILED);
    }
  }

  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("text", text)
        .append("pattern", pattern).append("res", res).toString();
  }

}
