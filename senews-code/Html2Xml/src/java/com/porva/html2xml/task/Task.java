/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.html2xml.task;

import com.porva.html2xml.ScriptRunner;

public interface Task
{
  public static enum ResultCode
  {
    COMPLETE,
    FAILED,
    NOT_EXECUTED
  }

  static enum TaskType
  {
    FIND_NODE("FIND_NODE"),
    STORE_TEXT("STORE_TEXT"),
    END_OF_ARTICLE("END_OF_ARTICLE"),
    CONTENT_START("CONTENT_START"),
    SAVE_POS("SAVE_POS"),
    GOTO_TASK("GOTO_TASK"),
    SET_POS("SET_POS"),
    STORE_ISODATE("STORE_ISODATE"),
    END_OF_DOCUMENT("END_OF_DOCUMENT"),
    EMPTY("EMPTY"),
    SAVE_TEXT("SAVE_TEXT"),
    STORE_NODE("STORE_NODE"),
    INVOKE_METHOD("INVOKE_METHOD"),
    STORE_LINKS("STORE_LINKS"),
    REGEXP("REGEXP");
  
    private final String taskType;
  
    private TaskType(final String taskType)
    {
      this.taskType = taskType;
      assert this.taskType != null;
    }
  
    public final String toString()
    {
      return taskType;
    }
  }

  public TaskType getType();

  public ResultCode getResultCode();

  public void perform(final ScriptRunner scriptRunnerCtrl);
}
