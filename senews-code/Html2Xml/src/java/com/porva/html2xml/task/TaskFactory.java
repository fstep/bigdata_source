/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.html2xml.task;

import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.NullArgumentException;

import com.porva.html2xml.task.Task.TaskType;

public class TaskFactory
{
  private TaskFactory()
  {
  }
  
  /**
   * Factory method to allocate a new {@link Task} objects from spedified initialization string array.
   * 
   * @param token tokens of the task to allocate.
   * @return new {@link Task} object according to initialization string.
   * @throws NullArgumentException if <code>token</code> is <code>null</code>.
   * @throws IllegalArgumentException if <code>token</code> is not correct.
   */
  public static Task newTask(final String[] token)
  {
    if (token == null)
      throw new NullArgumentException("token");

    // String[] param = token.split("\t");

    if (token.length == 0)
      throw new IllegalArgumentException("Cannot construct a new task");

    if (token[0].equals(TaskType.FIND_NODE.toString())) {
      return newFindNodeTask(token[1]);
    } else if (token[0].equals(TaskType.STORE_TEXT.toString())) {
      return new StoreText((String) token[1], Integer.parseInt(token[2]), Integer
          .parseInt(token[3]));
    } else if (token[0].equals(TaskType.END_OF_ARTICLE.toString())) {
      return EndOfArticle.getInstance();
    } else if (token[0].equals(TaskType.SAVE_POS.toString())) {
      return new SavePos(token[1]);
    } else if (token[0].equals(TaskType.GOTO_TASK.toString())) {
      return new GotoTask(Integer.parseInt(token[1]));
    } else if (token[0].equals(TaskType.SET_POS.toString())) {
      return new SetPos(Integer.parseInt(token[1]));
    } else if (token[0].equals(TaskType.STORE_ISODATE.toString())) {
      return new StoreIsoDate(token[1], token[2], token[3]);
    } else if (token[0].equals(TaskType.END_OF_DOCUMENT.toString())) {
      return EndOfDocument.getInstance();
    } else if (token[0].equals(TaskType.EMPTY.toString())) {
      return EmptyTask.getInstance();
    } else if (token[0].equals(TaskType.SAVE_TEXT.toString())) {
      return new SaveText(token[1], Integer.parseInt(token[2]), Integer.parseInt(token[3]));
    } else if (token[0].equals(TaskType.STORE_NODE.toString())) {
      return new StoreNode(token[1], token[2]);
    } else if (token[0].equals(TaskType.INVOKE_METHOD.toString())) {
      return newInvokeMethod(token[1], token[2]); // todo: only one parameter --
      // String !!!
    } else if (token[0].equals(TaskType.STORE_LINKS.toString())) {
      return new StoreLinks(Integer.parseInt(token[1]), Integer.parseInt(token[2]));
    } else if (token[0].equals(TaskType.REGEXP.toString())) {
      return new RegexpTask(token[1], token[2]);
    }

    throw new IllegalArgumentException("Cannot construct new task. Unknown task requested: "
        + token[0]);
  }

  // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  static Object[] parserParams(String paramStr, Class[] paramsTypes)
//  {
//    try {
//      int paramNum = paramsTypes.length;
//      String[] params = paramStr.split("\t", paramNum);
//      Object[] values = new Object[paramNum];
//      for (int i = 0; i < paramNum; i++) {
//        if (paramsTypes[i] == String.class) {
//          values[i] = params[i];
//        } else if (paramsTypes[i] == Integer.class) {
//          values[i] = Integer.parseInt(params[i]);
//        }
//        // todo: add more classes ???
//      }
//      return values;
//    } catch (Exception e) {
//      throw new IllegalArgumentException(e);
//    }
//  }

  private static Task newInvokeMethod(String paramStr, String param)
  {
    Pattern p = Pattern.compile("^(.+)\\.(.+)\\((.*)\\)$");
    Matcher m = p.matcher(paramStr);
    if (!m.matches())
      throw new IllegalArgumentException("Cannot invoke method from string: " + paramStr);

    String className = m.group(1);
    String methodName = m.group(2);

    return new InvokeMethod(className, methodName, param);
  }

  private static Task newFindNodeTask(String patternStr)
  {
    Pattern p1 = Pattern.compile("(\\.\\.\\.)?\\s*(\\/?)(\\S+)(.*?)\\s*(\\.\\.\\.)?");
    Pattern p2 = Pattern.compile("\\s*([^\\s=]+)([=~]+)'([^']+)'\\s*");
    SeekDirection seekDirection = SeekDirection.SEEK_DOWN;
    FindPosition findPosition = FindPosition.FIND_BEGIN_OF_NODE;

    Matcher m1 = p1.matcher(patternStr);
    if (!m1.matches())
      throw new IllegalArgumentException("Pattern is not valid.");

    String seekDown = m1.group(1);
    String findEndOfNode = m1.group(2);
    String nodeName = m1.group(3);
    String attrs = m1.group(4);
    String seekUp = m1.group(5);

    boolean isSeekDown = false;
    if (seekDown != null && seekDown.equals("...")) {
      isSeekDown = true;
      seekDirection = SeekDirection.SEEK_DOWN;
    }
    if (findEndOfNode != null && findEndOfNode.equals("/"))
      findPosition = FindPosition.FIND_END_OF_NODE;
    if (seekUp != null && seekUp.equals("...")) {
      if (isSeekDown)
        seekDirection = SeekDirection.SEEK_FRAGMENTAL;
      else
        seekDirection = SeekDirection.SEEK_UP;
    }

    List<NodePattern.AttributeMatcher> attributes = null;
    if (attrs != null && attrs.length() != 0) {
      attributes = new Vector<NodePattern.AttributeMatcher>();
      Matcher m2 = p2.matcher(attrs);
      while (m2.find()) {
        String attrName = m2.group(1);
        NodePattern.Variable variable = NodePattern.Variable.fromString(attrName);
        NodePattern.Operator operator = NodePattern.Operator.fromString(m2.group(2));
        String attrVal = m2.group(3);
        if (variable != null)
          attributes.add(new NodePattern.AttributeMatcher(variable, attrVal, operator));
        else
          attributes.add(new NodePattern.AttributeMatcher(attrName, attrVal, operator));
      }
    }
    NodePattern nodePattern = new NodePattern(nodeName, attributes);
    return new FindNode(nodePattern, findPosition, seekDirection);
  }

}
