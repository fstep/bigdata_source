/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.html2xml.task;

import com.porva.html.simpledom.SimpleDOM;


/**
 * An interface for tasks that have range, i.e. start and end positions in the 
 * {@link SimpleDOM} model they belong to. 
 * 
 * @author poroshin
 */
public interface RangeableTask extends Task
{
  /**
   * Returns start position of this task in the {@link SimpleDOM} model.
   * 
   * @return start position of this task in the {@link SimpleDOM} model.
   */
  public int getStart();

  /**
   * Returns end position of this task in the {@link SimpleDOM} model.
   * 
   * @return end position of this task in the {@link SimpleDOM} model.
   */
  public int getEnd();
}
