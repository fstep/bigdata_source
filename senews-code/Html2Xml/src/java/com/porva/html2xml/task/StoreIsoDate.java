/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.html2xml.task;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.oro.text.perl.Perl5Util;
import org.w3c.util.DateParser;

import com.porva.html2xml.ScriptRunner;

class StoreIsoDate extends AbstractTask implements StoreTask
{
  private String label;

  private String pattern;

  private String isodate;

  private String text;

  Perl5Util perl5Regexp = new Perl5Util();

  private Locale locale = Locale.US; // todo: locale depending on uri

  private static long defaultTime = 12 * 60 * 60 * 1000; // if there is no hours specified then it
                                                          // is midday

  StoreIsoDate(String lable, String pattern, String text)
  {
    super(Task.TaskType.STORE_ISODATE);
    this.label = lable;
    this.pattern = pattern;
    this.text = text;
  }

  public void perform(ScriptRunner scriptRunnerCtrl)
  {
    isodate = getIsoDate(pattern, text);

    if (isodate != null)
      setResultCode(ResultCode.COMPLETE);
    else
      setResultCode(ResultCode.FAILED);
  }

  public String getLabel()
  {
    return label;
  }
  
  public String getContent()
  {
    return isodate;
  }
  
  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("label", label)
        .append("isodate", isodate).toString();
  }

  String getIsoDate(String format, String dateLine)
  {
    String tmp = perl5Regexp.substitute("s/'.+?'//g", format);
    boolean hasHours = perl5Regexp.match("/.*h+.*/i", tmp);

    String isoDate = null;
    DateFormat formatter = new SimpleDateFormat(format, locale);
    try {
      Date date = formatter.parse(dateLine);
      if (!hasHours) {
        date.setTime(date.getTime() + defaultTime);
        isoDate = DateParser.getIsoDateNoHours(date);
      } else
        isoDate = DateParser.getIsoDateNoMillis(date);
    } catch (ParseException e) {
      // todo:
      // if (logger.isDebugEnabled())
      // logger.debug("[failed]\textraction of iso8601 date: " + e.toString());
    }
    return isoDate;
  }

}
