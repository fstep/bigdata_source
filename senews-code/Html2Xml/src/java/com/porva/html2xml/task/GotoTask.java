/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.html2xml.task;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.porva.html2xml.ScriptRunner;

class GotoTask  extends AbstractTask implements VarTask
{
  public static final String GOTO_VAR = "$GOTO_VAR"; // todo

  private Map<String, String> vars = new HashMap<String, String>(); 
  
  private int pos;

  GotoTask(int aPos)
  {
    super(Task.TaskType.GOTO_TASK);
    pos = aPos;
    if (pos <= 0)
      throw new IllegalArgumentException("position cannot be <=0: " + pos);
  }

  public int getPos()
  {
    return pos;
  }

  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("pos", pos).toString();
  }

  public void perform(ScriptRunner scriptRunnerCtrl)
  {
    vars.put(GOTO_VAR, String.valueOf(pos));
    setResultCode(ResultCode.COMPLETE);
  }

  public Map<String, String> getVars()
  {
    return vars;
  }
  
}
