/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.html2xml.task;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.porva.html2xml.ScriptRunner;

class StoreNode extends AbstractTask implements StoreTask
{
  private String label;

  private String content;

  public StoreNode(final String label, final String content)
  {
    super(Task.TaskType.STORE_NODE);
    this.label = label;
    this.content = content;
    
    if (this.label == null)
      throw new NullArgumentException("label");
    if (this.content == null)
      throw new NullArgumentException("content");
  }

  public String getLabel()
  {
    return label;
  }

  public String getContent()
  {
    return content;
  }
  
  public void perform(final ScriptRunner scriptRunnerCtrl)
  {
    setResultCode(Task.ResultCode.COMPLETE);
  }

  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("label", label)
        .append("content", content).toString();
  }

}
