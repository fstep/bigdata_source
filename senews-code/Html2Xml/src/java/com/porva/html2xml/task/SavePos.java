/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.html2xml.task;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.porva.html2xml.ScriptRunner;

class SavePos extends AbstractTask implements VarTask, RangeableTask
{
  private int pos;

  private String label;
  
  private Map<String, String> vars = new HashMap<String, String>();

  SavePos(final String aLabel)
  {
    super(Task.TaskType.SAVE_POS);
    label = aLabel;
    if (label == null)
      throw new NullPointerException();
    if (label.matches("^\\s*$"))
      throw new IllegalArgumentException();
  }

  public void perform(final ScriptRunner scriptRunnerCtrl)
  {
    pos = scriptRunnerCtrl.getCurIdx();
    vars.put(label, String.valueOf(pos));
    setResultCode(Task.ResultCode.COMPLETE);
  }
  
  public Map<String, String> getVars()
  {
    return vars;
  }

  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("pos", pos)
        .append("label", label).toString();
  }

  public int getStart()
  {
    return pos;
  }

  public int getEnd()
  {
    return pos;
  }

}
