/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.html2xml.task;

import com.porva.html2xml.ScriptRunner;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.builder.ToStringBuilder;

class InvokeMethod extends AbstractTask
{
  private Class cls;
  private Method method;
  private String param;

  private Object returnedValue;

  InvokeMethod(String className, String methodName, String param)
  {
    super(Task.TaskType.INVOKE_METHOD);
    this.param = param;

    try {
      cls = Class.forName(className);
      method = cls.getMethod(methodName, new Class[]{ScriptRunner.class, String.class});
    } catch (ClassNotFoundException e) {
      e.printStackTrace();  //todo: log error here
    } catch (NoSuchMethodException e) {
      e.printStackTrace();  //todo: log error here
    }
  }

//  Class[] parseParams(String parameters) throws ClassNotFoundException
//  {
//    String[] params = parameters.split(",");
//    if (params[0].equals("")) {
//      return new Class[0];
//    }
//
//    Class[] paramClasses = new Class[params.length];
//    for (int i = 0; i < params.length; i++) {
//      params[i] = params[i].trim();
//      paramClasses[i] = Class.forName(params[i]);
//    }
//    return  paramClasses;
//  }

  public void perform(ScriptRunner scriptRunnerCtrl)
  {
    try {
      Object obj = cls.newInstance();
      returnedValue = method.invoke(obj, new Object[]{scriptRunnerCtrl, param});
      if (returnedValue instanceof Boolean) {
        if ((Boolean) returnedValue)
          setResultCode(ResultCode.COMPLETE);
        else
          setResultCode(ResultCode.FAILED);
      }
    } catch (InstantiationException e) {
      e.printStackTrace();  //todo: log error here
    } catch (IllegalAccessException e) {
      e.printStackTrace();  //todo: log error here
    } catch (InvocationTargetException e) {
      e.printStackTrace();  //todo: log error here
    }
  }

  public Object getReturnedValue()
  {
    return returnedValue;
  }

  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("cls", cls)
        .append("method", method).append("param", param).append("returnedValue", returnedValue)
        .toString();
  }
}
