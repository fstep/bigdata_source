/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.html2xml.task;

import org.apache.commons.lang.NullArgumentException;

abstract class AbstractTask implements Task
{
  private Task.TaskType type;
  private ResultCode resultCode = ResultCode.NOT_EXECUTED;

  public AbstractTask(final Task.TaskType type)
  {
    this.type = type;
    if (this.type == null)
      throw new NullArgumentException("type");
  }

  public TaskType getType()
  {
    return type;
  }

  public ResultCode getResultCode()
  {
    return resultCode;
  }

  void setResultCode(final ResultCode aResultCode)
  {
    resultCode = aResultCode;
    if (resultCode == null)
      throw new NullArgumentException("resultCode");
  }

}
