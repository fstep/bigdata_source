/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.html2xml.task;

import com.porva.html.simpledom.SimpleDOM;
import com.porva.html2xml.ScriptRunner;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * This task finds node defined by string pattern. It changes ScriptRunner:
 * scriptRunner.setPrevIdx(parserCtrl.getCurIdx()); scriptRunner.setCurIdx(foundIdx);
 */
class FindNode extends AbstractTask implements RangeableTask
{
  private NodePattern nodePattern;

  private FindPosition findPosition = FindPosition.FIND_BEGIN_OF_NODE;

  private SeekDirection seekDirection = SeekDirection.SEEK_DOWN;

  private int foundIdx;
  
  FindNode(NodePattern nodePattern, FindPosition findPosition, SeekDirection seekDirection)
  {
    super(Task.TaskType.FIND_NODE);
    this.nodePattern = nodePattern;
    this.findPosition = findPosition;
    this.seekDirection = seekDirection;

    if (this.nodePattern == null || this.findPosition == null || this.seekDirection == null)
      throw new NullPointerException();
  }
  
  public void perform(ScriptRunner scriptRunnerCtrl)
  {
    if (scriptRunnerCtrl == null)
      throw new NullPointerException("ScriptRunner object cannot be null.");

    SimpleDOM nodes = scriptRunnerCtrl.getHTMLModel();
    int start = scriptRunnerCtrl.getCurIdx();
    int end = nodes.size() - 1;

    int foundIdx = -1;
    if (seekDirection == SeekDirection.SEEK_DOWN) {
      for (int i = start; i <= end; i++) {
        if (nodePattern.matches(nodes, i)) {
          foundIdx = i;
          break;
        }
      }
    } else if (seekDirection == SeekDirection.SEEK_UP) {
      for (int i = start; i > 0; i--) {
        if (nodePattern.matches(nodes, i)) {
          foundIdx = i;
          break;
        }
      }
    } else if (seekDirection == SeekDirection.SEEK_FRAGMENTAL) {
      if (scriptRunnerCtrl.isFragmental())
        seekDirection = SeekDirection.SEEK_UP;
      else
        seekDirection = SeekDirection.SEEK_DOWN;
      perform(scriptRunnerCtrl);
      return;
    }

    if (foundIdx == -1) {
      setResultCode(ResultCode.FAILED);
      return;
    }

    if (findPosition == FindPosition.FIND_END_OF_NODE) {
      // PositionTrackerHandler positionTrackerHandler = new PositionTrackerHandler();
      // NodeTraveller.travelNode(nodes.get(foundIdx), positionTrackerHandler);
      // foundIdx += positionTrackerHandler.getNum() + 1;
      foundIdx = nodes.getEndElementNum(foundIdx) + 1; // todo: + 1 ??
    }

    scriptRunnerCtrl.setPrevIdx(scriptRunnerCtrl.getCurIdx());
    scriptRunnerCtrl.setCurIdx(foundIdx);
    this.foundIdx = foundIdx;

    setResultCode(ResultCode.COMPLETE);
  }
  
  public int getStart()
  {
    return foundIdx;
  }

  public int getEnd()
  {
    return foundIdx;
  }

  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("nodePattern",
                                                                              nodePattern)
        .append("findPosition", findPosition).append("seekDirection", seekDirection)
        .append("foundIdx", foundIdx).toString();
  }

}
