///*
// *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
// *
// *   This program is free software; you can redistribute it and/or modify
// *   it under the terms of the GNU General Public License as published by
// *   the Free Software Foundation; either version 2 of the License, or
// *   (at your option) any later version.
// *
// *   This program is distributed in the hope that it will be useful,
// *   but WITHOUT ANY WARRANTY; without even the implied warranty of
// *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// *   GNU General Public License for more details.
// *
// *   You should have received a copy of the GNU General Public License
// *   along with this program; if not, write to the Free Software
// *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// */
//package com.porva.html2xml.task;
//
//import org.apache.commons.lang.NotImplementedException;
//import org.apache.commons.lang.NullArgumentException;
//import org.w3c.dom.Document;
//import org.w3c.dom.Element;
//import org.w3c.dom.Node;
//
//import com.porva.html.simpledom.SimpleDOMRender;
//import com.porva.html.simpledom.SimpleDOM;
//import com.porva.html2xml.ScriptRunner;
//import org.apache.commons.lang.builder.ToStringStyle;
//import org.apache.commons.lang.builder.ToStringBuilder;
//
///**
// * Stores all text inside tags defined by start and end position.
// */
//class StoreFormattedText extends DefaultTaskImpl implements PerformableTask, StoreTask
//{
//  private String label;
//  private int start = -1;
//  private int end = -1;
//  private String content;
////  private CountFunctor countFunctor = new CountFunctor();
//  
//  private SimpleDOMRender nodeRender = new SimpleDOMRender();
//
//  StoreFormattedText(String contentLabel, int start, int end)
//  {
//    super(TaskFactory.TaskType.STORE_TEXT);
//    this.label = contentLabel;
//    this.start = start;
//    this.end = end;
//
//    if (this.label == null)
//      throw new NullPointerException("label cannot be null.");
//    if (this.label.trim().length() == 0)
//      throw new IllegalArgumentException("label cannot be empty string.");
//    if (this.start > this.end && this.end > 0)
//      throw new IllegalArgumentException("start position cannot be more then end position: " + this.start + " > " + this.end);
//  }
//
//  // todo: refactor method StoreText->perform
//  public void perform(final ScriptRunner scriptRunnerCtrl)
//  {
//    if (scriptRunnerCtrl == null)
//      throw new NullPointerException("ScriptRunner object cannot be null.");
//
//    if (start < 0)
//      start = scriptRunnerCtrl.getCurIdx();
//
//    SimpleDOM nodes = scriptRunnerCtrl.getHTMLModel();
//    if (end < 0) 
//      end = nodes.getEndElementNum(start);
//
//    
//    
//    content = nodeRender.renderAsVisibleText(nodes, start, end).trim();
////    StringBuffer sb = new StringBuffer();
////    for (int i = start; i <= end; i++) {
////      com.porva.html.Node node = nodes.getNode(i);
////      boolean followChildren = nodeTextPrint(node, sb);
////      if (!followChildren) {              // todo ???
////        countFunctor.reset();
////        NodeTraveller.travelNode(node, countFunctor);
////        i = i + countFunctor.getCount();
////      }
////    }
////    content = sb.toString().trim();
//    
//    
//    setResultCode(ResultCode.COMPLETE);
//  }
//  
//  public Node renderResult(final Document document, final SimpleDOMRender render)
//  {
//    throw new NotImplementedException();
//  }
//  
////  int findEndPos(List<Node> nodes, int start)
////  {
////    Node node = nodes.get(start);
////    countFunctor.reset();
////    NodeTraveller.travelNode(node, countFunctor);
////    return start + countFunctor.getCount();
////  }
//  
//  public Node createResult(final Document document)
//  {
//    if (document == null)
//      throw new NullArgumentException("document");
//
//    Element el = document.createElement(label);
//    el.appendChild(document.createTextNode(content));
//    return el;
//  }
//
//  public String getLabel()
//  {
//    return label;
//  }
//  
//  public String getContent()
//  {
//    return content;
//  }
//
//  public String toString()
//  {
//    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("label", label)
//        .append("start", start).append("end", end).append("content", content).toString();
//  }
//  
//
//  //////////////////////// private ///////////////////////////
//
////  // todo: introduce this method as separate class -- result renderer
////  private static boolean nodeTextPrint(Node node, StringBuffer sb)
////  {
////    String name = node.getNodeName();
////    int type = node.getNodeType();
////
////    if (name.equalsIgnoreCase("STYLE") || name.equalsIgnoreCase("SCRIPT"))
////      return false;
////
////    if (type == Node.ELEMENT_NODE) {
////      if (node.getNodeName().equalsIgnoreCase("BR")) {
////        sb.append("\n");
////      } else if (node.getNodeName().equalsIgnoreCase("P")) {
////        sb.append("\n\t");
////      } else if (node.getNodeName().equalsIgnoreCase("LI")) {
////        sb.append("\n\t*\t");
////      } else if (node.getNodeName().matches("[h|H]\\d")) {
////        sb.append("\n\n\t\t");
////      }
////    }
////
////    if (type == Node.TEXT_NODE) {
////      String nodeTxt = node.getNodeValue();
////      //String trimmedTxt = node.getNodeValue().trim();
////      String trimmedTxt = trim(node.getNodeValue());
////      if (!(trimmedTxt.equals(""))) {
////        nodeTxt = nodeTxt.replaceAll("[\n\\s]+", " ");
////        Pattern p = Pattern.compile("([\\s\\n]*)(\\S.*?)([\\s\\n]*)");
////        Matcher m = p.matcher(nodeTxt);
////        if (m.matches()) {
////          if (m.group(1) != null && m.group(1).length() != 0)
////            sb.append(' ');
////          sb.append(m.group(2));
////          if (m.group(3) != null && m.group(3).length() != 0)
////            sb.append(' ');
////        }
////        else {
////          sb.append(trimmedTxt);
////        }
////        //return true;
////      }
////    }
////
////    return true;
////  }
////
////  private static String trim(String str)
////  {
////    int i = 0;
////    for (; i < str.length(); i++) {
////      char ch = str.charAt(i);
////      if (ch != 160 && ch > 32)
////        break;
////    }
////
////    int j = str.length();
////    for (; j > str.length(); j--) {
////      char ch = str.charAt(j);
////      if (ch != 160 && ch > 32)
////        break;
////    }
////
////    return str.substring(i, j);
////  }
//
//}
