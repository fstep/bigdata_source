/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.html2xml;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.html.CpDetector;
import com.porva.html.HTMLCpDetector;
import com.porva.html.keycontent.NewsXMLCleaner;
import com.porva.html2xml.task.Task;
import com.porva.net.tana.DefaultServerHandler;
import com.porva.net.tana.TanaServer;

/**
 * Html2Xml processing as a TANA server.
 * 
 * @author poroshin
 */
public class Html2XmlServer
{
  private TanaServer tanaServer;

  /**
   * Default encoding for output xml files.
   */
  public static final String DEF_XML_CHARSET = "UTF-8";

  /**
   * Default encoding for input html files.
   */
  public static final String DEF_SOURCE_CHARSET = "UTF-8";

  private static final Log logger = LogFactory.getLog(Html2XmlServer.class);

  private int completeFilesNum = 0;

  private int failedFilesNum = 0;

  private BufferedWriter out;

  private NewsXMLCleaner newsXmlCleaner;

  /**
   * Allocates new {@link Html2XmlServer} server object.
   * 
   * @param port port number to start server on.
   * @throws ParserConfigurationException
   * @throws IOException
   */
  public Html2XmlServer(int port) throws ParserConfigurationException, IOException
  {
    ServerHandler serverHandler = new ServerHandler();
    tanaServer = new TanaServer(port, serverHandler, true);
    newsXmlCleaner = new NewsXMLCleaner();

    File file = new File("scripts.log");
    file.createNewFile();
    out = new BufferedWriter(new FileWriter(file));
  }

  /**
   * Starts the server.
   */
  public void start()
  {
    tanaServer.start();
  }

  // ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  private class ServerHandler extends DefaultServerHandler
  {
    private DefaultScriptRunner scriptRunnerImpl;

    private NewsXMLResultFormatter xmlResultFormatter;

    public ServerHandler() throws ParserConfigurationException, IOException
    {
      super(logger, "Html2Xml");
      scriptRunnerImpl = new DefaultScriptRunner();
      xmlResultFormatter = new NewsXMLResultFormatter();
    }

    public Map<String, byte[]> handleClientMsg(Map<String, byte[]> recvMap)
    {
      Map<String, byte[]> replyMap = new HashMap<String, byte[]>();
      try {
        if (!recvMap.containsKey("cmd")) {
          logger.warn("Command task is not defined: use cmd parameter to specify task");
          replyMap.put("error",
                       "Command task is not defined: use cmd parameter to specify command task"
                           .getBytes());
          return replyMap;
        }
        String cmd = new String(recvMap.get("cmd"));
        if (logger.isDebugEnabled())
          logger.debug("received command: " + cmd);

        if (cmd.equalsIgnoreCase("html2xml")) {
          replyMap.putAll(processCmd_html2xml(recvMap));
        } else if (cmd.equalsIgnoreCase("stat")) {
          replyMap.putAll(processCmd_stat());
        } else {
          logger.warn("unknown command requested: " + cmd);
          replyMap.put("error", ("unknown command requested: " + cmd).getBytes());
        }

        // replyMap.put("cmd", cmd.getBytes());
      } catch (Exception e) {
        replyMap.put("error", e.toString().getBytes());
      }
      return replyMap;
    }

    private String kce(final String xml)
    {
      String cleaned = xml;
      try {
        cleaned = newsXmlCleaner.clean(xml);
      } catch (Exception e) {
        logger.warn("Failed to clean xml", e);
        e.printStackTrace();
      }
      return cleaned;
    }

    private Charset detectCharset(String contentType, byte[] content)
    {
      HTMLCpDetector htmlCpDetector = new HTMLCpDetector(new CpDetector(), Charset
          .forName(DEF_SOURCE_CHARSET));

      String httpCharset = HTMLCpDetector.getResponseCharset(contentType);
      String htmlCharset = HTMLCpDetector.getMetaTagCharset(content);

      return htmlCpDetector.detect(httpCharset, content, htmlCharset, null);
    }

    /**
     * accepted parameters:<br>
     * 'content' html source<br>
     * 'meta' content-type header from http response (optional)<br>
     * 'charset' encoding of the given html source (optional)<br>
     * 'xml-charset' desired encoding of the resulted xml file (optional)<br>
     * 'script' html2xml script<br>
     * 'url' url of the html source<br>
     * 
     * @param recvMap
     * @return
     * @throws Exception
     */
    private Map<String, byte[]> processCmd_html2xml(Map<String, byte[]> recvMap) throws Exception
    {
      Map<String, byte[]> replyMap = new HashMap<String, byte[]>();

      // content
      byte[] content = recvMap.get("content");
      if (content == null)
        throw new Exception("content to parse is null");

      // meta
      String meta = null;
      byte[] metaOb = recvMap.get("meta");
      if (metaOb != null)
        meta = new String(metaOb);

      // source charset
      String contentCharset;
      byte[] charsetOb = recvMap.get("charset");
      if (charsetOb != null)
        contentCharset = new String(charsetOb);
      else {
        contentCharset = detectCharset(meta, content).toString();
        logger.debug("Detected encoding is " + contentCharset);
      }
      
      // xml charset
      String xmlEncoding = DEF_XML_CHARSET;
      byte[] xmlEncodingOb = recvMap.get("xml-charset");
      if (xmlEncodingOb != null)
        xmlEncoding = new String(xmlEncodingOb);

      // url
      byte[] uriOb = recvMap.get("url");
      if (uriOb == null) {
        throw new Exception("url is not defined");
      }
      URI uri = new URI(new String(uriOb, "UTF-8"));

      // script
      byte[] scriptOb = recvMap.get("script");
      if (scriptOb == null)
        throw new Exception("script is null");
      String script = new String(scriptOb);

      String xml = null;
      try {
        String contentStr = new String(content, contentCharset);
        List<Task> tasks = scriptRunnerImpl.performScript(contentStr, uri, new StringScriptParser(
            script));
        xml = xmlResultFormatter.format(tasks, xmlEncoding, scriptRunnerImpl);
      } catch (IOException e) {
        logger.info("Html2Xml exception", e);
      }

      xml = kce(xml);

      replyMap.put("xml-content", xml.getBytes());
      replyMap.put("xml-content-encoding", xmlEncoding.getBytes());

      out.write(script);
      out.write("\n\n");
      out.flush();

      return replyMap;
    }

    private Map<String, byte[]> processCmd_stat() throws Exception
    {
      Map<String, byte[]> replyMap = new HashMap<String, byte[]>();
      replyMap.put("completed", Integer.toString(completeFilesNum).getBytes());
      replyMap.put("failed", Integer.toString(failedFilesNum).getBytes());
      return replyMap;
    }
  }

}
