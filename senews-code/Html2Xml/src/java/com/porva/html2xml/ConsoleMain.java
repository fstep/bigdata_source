/*
 * Copyright (c) 2005 Your Corporation. All Rights Reserved.
 */
package com.porva.html2xml;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.oro.text.perl.Perl5Util;

public class ConsoleMain
{
  static Perl5Util perl5Regexp = new Perl5Util();

  // //////////////////////// command line functions ///////////////////////////////////////////////
  private static Options initCmdParams()
  {
    // common options
    Option optionHelp = new Option("h", "display help message and exit");
    optionHelp.setRequired(false);

    Option optionCmd = new Option("cmd", "commmands: BATCH|SERVER|HTML2XML (required)");
    optionCmd.setRequired(true);
    optionCmd.setArgs(1);
    optionCmd.setArgName("cmd");

    // options for cmd:BATCH
    Option optionDir = new Option("dir", "export directory (required with cmd:BATCH)");
    optionDir.setRequired(false);
    optionDir.setArgs(1);
    optionDir.setArgName("dir");

    Option optionBatchSourceEnc = new Option("sourceEnc",
        "encoding of input html files (use with cmd:BATCH). Default is "
            + Html2XmlBatch.DEF_SOURCE_ENC);
    optionBatchSourceEnc.setRequired(false);
    optionBatchSourceEnc.setArgs(1);
    optionBatchSourceEnc.setArgName("sourceEnc");

    Option optionBatchMetaEnc = new Option("metaEnc",
        "encoding of input meta files (use with cmd:BATCH). Default is "
            + Html2XmlBatch.DEF_META_ENC);
    optionBatchMetaEnc.setRequired(false);
    optionBatchMetaEnc.setArgs(1);
    optionBatchMetaEnc.setArgName("metaEnc");

    Option optionBatchXmlEnc = new Option("xmlEnc",
        "encoding of xml output files (use with cmd:BATCH). Default is "
            + Html2XmlBatch.DEF_XML_ENC);
    optionBatchXmlEnc.setRequired(false);
    optionBatchXmlEnc.setArgs(1);
    optionBatchXmlEnc.setArgName("xmlEnc");

    Option optionBatchRecursively = new Option("R", "get files recursively (use with cmd:BATCH)");
    optionBatchRecursively.setRequired(false);
    optionBatchRecursively.setArgName("R");

    // options for cmd:SERVER
    Option optionPort = new Option("p", "port number (required with cmd:SERVER)");
    optionPort.setRequired(false);
    optionPort.setArgs(1);
    optionPort.setArgName("port");

    // options for cmd:HTML2XML
    Option optionFile = new Option("file", "file to parse (required with cmd:HTML2XML)");
    optionFile.setRequired(false);
    optionFile.setArgs(1);
    optionFile.setArgName("file");

    Options options = new Options();
    options.addOption(optionHelp);
    options.addOption(optionCmd);
    options.addOption(optionDir);
    options.addOption(optionBatchRecursively);
    options.addOption(optionBatchSourceEnc);
    options.addOption(optionBatchMetaEnc);
    options.addOption(optionBatchXmlEnc);
    options.addOption(optionPort);
    options.addOption(optionFile);

    return options;
  }

  private static void printHelpMsg()
  {
    Options options = initCmdParams();
    HelpFormatter formatter = new HelpFormatter();
    System.out.println("Html2Xml ver. 1.0");
    formatter.printHelp("parameters:", options);
  }

  private static CommandLine parseCmdParams(Options options, String[] args) throws ParseException
  {
    CommandLineParser parser = new BasicParser();
    CommandLine cmd = null;

    cmd = parser.parse(options, args);
    return cmd;
  }

  private static void init(String[] args) throws Exception
  {
    Options options = initCmdParams();
    CommandLine cmd = parseCmdParams(options, args);

    // common options
    if (cmd.hasOption("h")) {
      printHelpMsg();
      System.exit(1);
    }

    String command = null;
    if (cmd.hasOption("cmd"))
      command = cmd.getOptionValue("cmd");

    // options for cmd:BATCH
    if (command.equalsIgnoreCase("BATCH")) {
      String dir = null;
      if (cmd.hasOption("dir")) {
        dir = cmd.getOptionValue("dir");
      }
      String sourceEncoding = null;
      if (cmd.hasOption("sourceEnc"))
        sourceEncoding = cmd.getOptionValue("sourceEnc");
      String metaEncoding = null;
      if (cmd.hasOption("metaEnc"))
        metaEncoding = cmd.getOptionValue("metaEnc");
      String xmlEncoding = null;
      if (cmd.hasOption("xmlEnc"))
        xmlEncoding = cmd.getOptionValue("xmlEnc");
      boolean isRecursively = false;
      if (cmd.hasOption("R"))
        isRecursively = true;
      if (dir != null)
        new Html2XmlBatch().runBatchProcess(dir, isRecursively, sourceEncoding, metaEncoding,
                                            xmlEncoding);
    } else if (command.equalsIgnoreCase("SERVER")) {
      int port = -1;
      if (cmd.hasOption("p"))
        port = Integer.parseInt(cmd.getOptionValue("p"));

      Html2XmlServer sesqServer = new Html2XmlServer(port);
      sesqServer.start();
    } else if (command.equalsIgnoreCase("HTML2XML")) {
      // String file = null;
      // if (cmd.hasOption("file"))
      // file = cmd.getOptionValue("file");
      // if (file != null)
      // processSingleFile(file);
    }

  }

  // private static void processSingleFile(String fileName, String script) throws IOException,
  // ParserConfigurationException
  // {
  // String encoding = "utf-8";
  // ScriptLoader scriptLoader = new ScriptLoader(new File("conf/html2xml.properties"), new File(
  // "conf/scripts"));
  // DefaultScriptRunner scriptRunnerImpl = new DefaultScriptRunner();
  // XMLResultFormatter xmlResultFormatter = new XMLResultFormatter();
  // String xmlEncoding = "utf-8";
  //
  // File file = new File(fileName);
  // File[] files = file.listFiles(new MyFileFilter());
  //
  // for (File file1 : files) {
  //
  // logger.info("Processing file " + file1.getAbsolutePath());
  // System.out.println("Processing file " + file1.getAbsolutePath());
  //      
  // List<String> scriptsList = scriptLoader.getScripts(uri);
  // if (scriptsList == null || scriptsList.size() == 0)
  // continue;
  //
  // byte[] content = FioUtils.readFileAsBytes(file1.getAbsolutePath());
  // String contentStr = new String(content, encoding);
  //
  // String xml = null;
  // for (String script : scriptsList) {
  // try {
  // List<Task> tasks = scriptRunnerImpl.performScript(contentStr, uri,
  // new StringScriptParser(script));
  // xml = xmlResultFormatter.format(tasks, xmlEncoding);
  // } catch (IOException e) {
  // logger.info("Html2Xml exception", e);
  // } catch (SAXException e) {
  // logger.info("Html2Xml exception", e);
  // }
  // if (xml != null)
  // continue;
  // }
  //
  // if (xml.contains("<DOCUMENT/>") || xml.contains("<CONTENT></CONTENT>")
  // || xml.contains("<CONTENT/>") || xml.contains("<content/>")
  // || xml.contains("<content></content>"))
  // xml = null;
  //
  // }
  // }

  public static void main(String[] args)
  {
    try {
      init(args);
    } catch (Exception e) {
      System.out.println("***ERROR: " + e.getMessage());
      e.printStackTrace();
      printHelpMsg();
    }
  }

}
