/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.html2xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang.NullArgumentException;

import com.porva.util.FioUtils;

/**
 * Class to load html2xml scripts.
 * 
 * @author Poroshin V.
 * @date Nov 1, 2005
 */
public class ScriptLoader
{
  private File propFilename = null;

  private File scriptsDir = null;
  
  private static String scriptsEncoding = "ISO-8859-1";

  private Properties properties = new Properties();

  /**
   * Constructs a new {@link ScriptLoader} object.
   * 
   * @param propertiesFile filename of html2xml properties.
   * @param scriptsDir directory location of all scripts.
   * @throws IOException if some I/O exception occurs.
   * @throws NullArgumentException if <code>propertiesFile</code> of <code>scriptsDir</code> is
   *           <code>null</code>.
   * @throws IllegalArgumentException if <code>propertiesFile</code> does not exist or it is a
   *           directory.
   * @throws IllegalArgumentException if <code>scriptsDir</code> does not exist or it is not a
   *           directory.
   */
  public ScriptLoader(final File propertiesFile, final File scriptsDir) throws IOException
  {
    this.propFilename = propertiesFile;
    this.scriptsDir = scriptsDir;

    if (this.scriptsDir == null)
      throw new NullArgumentException("scriptsDir");
    if (this.propFilename == null)
      throw new NullArgumentException("propFilename");
    if (!this.scriptsDir.exists() || !this.scriptsDir.isDirectory())
      throw new IllegalArgumentException("Invalid scripts directory: "
          + this.scriptsDir.getAbsolutePath());
    if (!this.propFilename.exists() || this.propFilename.isDirectory())
      throw new IllegalArgumentException("Invalid properties filename: "
          + this.propFilename.getAbsolutePath());

    FileInputStream fin = new FileInputStream(propertiesFile);
    properties.load(fin);
    fin.close();
  }

  /**
   * Returns list of scripts that can be used to parse content of given <code>uri</code>.<br>
   * <b>TODO: cache scripts -- do not read them from disk all the time</b>
   * 
   * @param uri URI of content to be parsed. Can be <code>null</code>.
   * @return list of scripts that can be used to parse content of given <code>uri</code> or
   *         <code>null</code> if no scripts available for this <code>uri</code>.
   * @throws IllegalStateException if properties file is invalid.
   */
  public List<String> getScripts(final URI uri)
  {
    if (uri == null)
      return null;

    String host = uri.getHost();
    if (host == null)
      return null;

    String scriptsFilenames = properties.getProperty(host);
    if (scriptsFilenames == null)
      return null;

    List<String> scriptsList = new ArrayList<String>();
    String[] scripts = scriptsFilenames.split(";");
    for (String scriptFilename : scripts) {
      try {
        String scriptSource = FioUtils.readFileAsString(new File(scriptsDir, scriptFilename), 
                                                        scriptsEncoding);
        scriptsList.add(scriptSource);
      } catch (Exception e) {
        throw new IllegalStateException("Cannot load html2xml script", e);
      }
    }
    if (scriptsList.size() == 0)
      throw new IllegalStateException(
          "Invalid properties: at least one script must be assign to the host: " + host);

    return scriptsList;
  }
}
