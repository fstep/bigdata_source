/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.html2xml;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.porva.html.simpledom.SimpleDOM;
import com.porva.html.simpledom.SimpleDOMRender;
import com.porva.html2xml.task.RangeableTask;
import com.porva.html2xml.task.StoreLinks;
import com.porva.html2xml.task.StoreTask;
import com.porva.html2xml.task.Task;
import com.porva.html2xml.task.Task.ResultCode;

/**
 * Formatting to xml document string.
 * 
 * @author Poroshin V.
 */
public class NewsXMLResultFormatter implements ResultFormatter
{
  /**
   * Root tag for xml document.
   */
  public static final String rootElement = "DOCUMENT";

  /**
   * Tag to enclose articles in xml document.
   */
  public static final String articleElement = "article";

  private Document resultDoc;

  private DocumentBuilder builder;

  private SimpleDOMRender render = new SimpleDOMRender();

  private static final Log logger = LogFactory.getLog(NewsXMLResultFormatter.class);

  /**
   * Allocates a new {@link NewsXMLResultFormatter} object.
   * 
   * @throws ParserConfigurationException
   */
  public NewsXMLResultFormatter() throws ParserConfigurationException
  {
    final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    builder = factory.newDocumentBuilder();
  }

  public String format(final List<Task> tasks,
                       final String encoding,
                       final ScriptRunner scriptRunner)
  {
    if (encoding == null)
      throw new NullArgumentException("encoding");

    if (tasks == null || tasks.size() == 0)
      return null;

    String res = null;
    resultDoc = builder.newDocument();
    Document doc = createDomResult(tasks, scriptRunner.getHTMLModel());
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    try {
      prettyPrint(doc, encoding, out);
      out.flush();
      res = new String(out.toByteArray(), encoding);
      out.close();
    } catch (Exception e) {
      logger.warn("Failed to fomat xml document: " + e.getMessage());
    }

    return res;
  }

  private Document createDomResult(final List<Task> tasks, final SimpleDOM simpleDOM)
  {
    assert tasks != null;

    Element root = resultDoc.createElement(rootElement);
    resultDoc.appendChild(root);

    Element record = resultDoc.createElement(articleElement);
    for (Task task : tasks) {
      if (task.getResultCode() != ResultCode.COMPLETE)
        continue;
      if (task instanceof StoreTask) {
        if (((StoreTask) task).getLabel().equalsIgnoreCase("content"))
          record.appendChild(renderResult(task, resultDoc, render, simpleDOM));
        else
          record.appendChild(createResult((StoreTask) task, resultDoc));
      } else if (task.getType() == Task.TaskType.END_OF_ARTICLE) {
        root.appendChild(record);
        record = resultDoc.createElement(articleElement);
      }
    }

    return resultDoc;
  }

  private Node renderResult(final Task task,
                            final Document document,
                            final SimpleDOMRender render,
                            final SimpleDOM simpleDOM)
  {
    if (!(task instanceof StoreTask))
      throw new IllegalArgumentException("Task is not an instance of StoreTask: " + task);
    if (!(task instanceof RangeableTask))
      throw new IllegalArgumentException("Task is not an instance of RangeableTask: " + task);

    return render.renderAsXML(simpleDOM, ((StoreTask) task).getLabel(), document,
                              ((RangeableTask) task).getStart(), ((RangeableTask) task).getEnd());
  }

  private Node createResult(final StoreTask task, final Document document)
  {
    if (task.getType() != Task.TaskType.STORE_LINKS) {
      Element el = document.createElement(task.getLabel());
      el.appendChild(document.createTextNode(task.getContent()));
      return el;
    }

    // todo: refactor StoreLinks
    StoreLinks storeLinks = (StoreLinks) task;
    List<String> links = storeLinks.getLinks();
    List<String> anchorText = storeLinks.getAnchorText();
    Element linksEl = document.createElement("links");
    for (int i = 0; i < links.size(); i++) {
      Element linkNode = document.createElement("link");
      linkNode.setAttribute("type", "a");
      Element anchorTextNode = document.createElement("anchorText");
      anchorTextNode.appendChild(document.createTextNode(anchorText.get(i)));
      linkNode.appendChild(anchorTextNode);

      Element location = document.createElement("location");
      // trim location cos it can start with empty space
      location.appendChild(document.createTextNode(links.get(i).trim()));
      linkNode.appendChild(location);

      linksEl.appendChild(linkNode);
    }
    return linksEl;
  }

  // private static void prettyPrint(final Document iNode, final String charset, final OutputStream
  // out)
  // throws IOException
  // {
  // OutputFormat format = new OutputFormat(iNode, charset, true);
  // XMLSerializer printer = new XMLSerializer(out, format);
  // printer.serialize(iNode);
  // }

  private void prettyPrint(final Document iNode, final String charset, final OutputStream out)
      throws TransformerFactoryConfigurationError, TransformerException
  {
    assert iNode != null;
    assert charset != null;
    assert out != null;

    Source source = new DOMSource(iNode);
    Result result = new StreamResult(out);
    Transformer xformer = TransformerFactory.newInstance().newTransformer();
    xformer.setOutputProperty(OutputKeys.ENCODING, charset);
    xformer.transform(source, result);
  }

}
