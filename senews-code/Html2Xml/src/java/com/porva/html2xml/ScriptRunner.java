/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.html2xml;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import org.xml.sax.SAXException;

import com.porva.html.simpledom.SimpleDOM;
import com.porva.html2xml.task.Task;

public interface ScriptRunner
{
  public int getPrevIdx();

  public int getCurIdx();

  public void setPrevIdx(int newIdx);

  public void setCurIdx(int newIdx);

  public SimpleDOM getHTMLModel();

  public boolean isFragmental();
  
  public URI getCurrentURI();

  public List<Task> getProcessedTasks();

  public List<Task> performScript(String content, URI uri, ScriptParser scriptParser)
      throws IOException, SAXException;

  public void performTask(Task task);

}
