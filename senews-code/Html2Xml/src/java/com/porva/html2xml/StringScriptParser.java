/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.html2xml;

import static com.porva.html2xml.task.Task.TaskType.EMPTY;
import static com.porva.html2xml.task.Task.TaskType.END_OF_DOCUMENT;
import static com.porva.html2xml.task.Task.TaskType.INVOKE_METHOD;
import static com.porva.html2xml.task.Task.TaskType.REGEXP;
import static com.porva.html2xml.task.Task.TaskType.SAVE_TEXT;
import static com.porva.html2xml.task.Task.TaskType.SET_POS;
import static com.porva.html2xml.task.Task.TaskType.STORE_ISODATE;
import static com.porva.html2xml.task.Task.TaskType.STORE_LINKS;
import static com.porva.html2xml.task.Task.TaskType.STORE_NODE;
import static com.porva.html2xml.task.Task.TaskType.STORE_TEXT;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.oro.text.perl.Perl5Util;

import com.porva.html2xml.task.Task;
import com.porva.html2xml.task.TaskFactory;
import com.porva.html2xml.task.VarTask;

public class StringScriptParser implements ScriptParser
{
  private Perl5Util perl5Regexp = new Perl5Util();

  private String script;

  private String[] scriptLines;

  // todo: refactor tasksWithFlowControl
  private List<Task> tasks;

  private List<Task> trueTasks;

  private List<Task> falseTasks;

  private TasksWithFlowControl tasksWithFlowControl = new TasksWithFlowControl();

  private Map<String, String> vars = new HashMap<String, String>();

  private int curTaskIdx = 0;

  private boolean isEndOfDocument = false;
  
  private static final Log logger = LogFactory.getLog(StringScriptParser.class);
  
  private Stack<Task> tasksStack = new Stack<Task>();

  public StringScriptParser(String aScript)
  {
    script = preprocessScript(aScript);
    scriptLines = script.split("\n");
    tasks = new ArrayList<Task>(scriptLines.length);
    trueTasks = new ArrayList<Task>(scriptLines.length);
    falseTasks = new ArrayList<Task>(scriptLines.length);
    scriptLines = preprocessFlowControl(scriptLines);
  }

  public Task getNextTask()
  {
    if (isEndOfDocument)
      return TaskFactory.newTask(new String[] {END_OF_DOCUMENT.toString()});
    
    if (!tasksStack.isEmpty())
      return tasksStack.pop();

    if (curTaskIdx >= scriptLines.length)
      return null;
    String[] taskStr = preprocessTask(scriptLines[curTaskIdx], curTaskIdx);
    curTaskIdx++;
    Task task = TaskFactory.newTask(taskStr);

    addToList(tasks, task, curTaskIdx);

    // todo
    tasksWithFlowControl.addTask(task, trueTasks.get(curTaskIdx - 1), falseTasks
        .get(curTaskIdx - 1));

    return task;
  }

  public void fireTaskPerformed(Task aPerformedTask)
  {
    // int ctrlPos = curTaskIdx - 1; // todo: replace to map<Task, [TrueTask, FalseTask]>
    perform(aPerformedTask);

    switch (aPerformedTask.getResultCode()) {
    case FAILED:
      //perform(tasksWithFlowControl.getFalseTask(aPerformedTask));
      Task task = tasksWithFlowControl.getFalseTask(aPerformedTask);
      if (task.getType() != Task.TaskType.EMPTY)
        tasksStack.push(task);
      break;
    case COMPLETE:
      //perform(tasksWithFlowControl.getTrueTask(aPerformedTask));
      task = tasksWithFlowControl.getTrueTask(aPerformedTask); 
      if (task.getType() != Task.TaskType.EMPTY)
        tasksStack.push(task);
      break;
    case NOT_EXECUTED:
      // todo: raise error or what?
      break;
    }
  }

  public String toString()
  {
    final StringBuilder sb = new StringBuilder();
    sb.append("StringScriptParser");
    sb.append("{curTaskIdx=").append(curTaskIdx);
    sb.append(",script=").append(script);
    sb.append('}');
    return sb.toString();
  }

  // ////////////////////////// non public methods
  // ////////////////////////////////////////////////////
  void perform(final Task task)
  {
    if (task instanceof VarTask) 
      vars.putAll(((VarTask)task).getVars());
      
    switch (task.getType()) {
    case GOTO_TASK:
      curTaskIdx = Integer.parseInt(vars.get("$GOTO_VAR")) - 1; // todo $GOTO_VAR
      break;
    case END_OF_DOCUMENT:
      isEndOfDocument = true;
      break;
    }
  }

  String[] preprocessTask(String scriptLine, int taskIdx)
  {
    // scriptLine = preprocessFlowControl(scriptLine, taskIdx);
    scriptLine = preprocessAliases(scriptLine, taskIdx);
    String[] token = scriptLine.split("\t");
    preprocessVars(token);
    return token;
  }

  String preprocessAliases(String scriptLine, int taskIdx)
  {
    if (scriptLine.equals("next")) {
      scriptLine = "GOTO_TASK\t" + (taskIdx + 2);
    } else if (scriptLine.equals("")) {
      scriptLine = "EMPTY";
    }

    return scriptLine;
  }

  void preprocessVars(String[] params)
  {
    if (params[0].equals(STORE_LINKS.toString())) {
      params[1] = resolveVar(params[1]);
      params[2] = resolveVar(params[2]);
    }
    else if (params[0].equals(INVOKE_METHOD.toString())) {
      throw new NotImplementedException();
    }
    else if (params[0].equals(SET_POS.toString())) {
      params[1] = resolveVar(params[1]);
    }
    else if (params[0].equals(STORE_NODE.toString())) {
      params[2] = resolveVar(params[2]);
    }
    else if (params[0].equals(STORE_TEXT.toString())) {
      params[2] = resolveVar(params[2]);
      params[3] = resolveVar(params[3]);
    }
    else if (params[0].equals(STORE_ISODATE.toString())) {
      params[3] = resolveVar(params[3]);
    }
    else if (params[0].equals(REGEXP.toString())) {
      params[1] = resolveVar(params[1]);
    }
    else if (params[0].equals(SAVE_TEXT.toString())) {
      params[2] = resolveVar(params[2]);
      params[3] = resolveVar(params[3]);
    }
  }
  
  private String resolveVar(String var)
  {
    if (var.startsWith("$"))
      return getVarValue(var);
    return var;
  }

  String[] preprocessFlowControl(String[] scriptLine)
  {
    String[] res = new String[scriptLine.length];
    System.arraycopy(scriptLine, 0, res, 0, scriptLine.length);
    for (int i = 0; i < scriptLine.length; i++) {
      if (perl5Regexp.match("/(.+?)\t+\\[(.*)\\]\\[(.*)\\]/", scriptLine[i])) {
        res[i] = perl5Regexp.group(1);
//        String[] trueTaskStr = preprocessAliases(perl5Regexp.group(2), i);
        String[] trueTaskStr = preprocessTask(perl5Regexp.group(2), i);
        addToList(trueTasks, TaskFactory.newTask(trueTaskStr), i);
        String[] falseTaskStr = preprocessTask(perl5Regexp.group(3), i);
        addToList(falseTasks, TaskFactory.newTask(falseTaskStr), i);
        // tasksWithFlowControl.add(new TaskWithFlowControl());
      }
    }
    return res;
  }

  private String getVarValue(String var)
  {
    return vars.get(var);
  }

  String preprocessScript(String script)
  {
    script = perl5Regexp.substitute("s/\"/\'/g", script);
    script = perl5Regexp.substitute("s/[<>]//g", script);
    script = removeComments(script);
    script = removeEmptyLines(script);
    return script;
  }

  String removeComments(String script)
  {
    String[] line = script.split("\n");
    StringBuffer withoutComments = new StringBuffer();
    for (String aLine : line) {
      if (!perl5Regexp.match("/^\\s*#/", aLine))
        withoutComments.append(aLine).append("\n");
    }
    return withoutComments.toString();
  }

  String removeEmptyLines(String script)
  {
    String[] line = script.split("\n");
    StringBuffer withoutComments = new StringBuffer();
    for (String aLine : line) {
      if (!perl5Regexp.match("/^\\s*$/", aLine))
        withoutComments.append(aLine).append("\n");
    }
    return withoutComments.toString();
  }

  void addToList(List<Task> tasksList, Task task, int pos)
  {
    if (pos >= tasksList.size())
      tasksList.add(task);
    else
      tasksList.add(pos, task);
  }
  
  ////////////////////////////////////////////////////////////////////////////////
//  public static class ScriptsTokens
//  {
//    public final TaskFactory.TaskType type;
//    public String[] params;
//    
//    public ScriptsTokens(String[] tokens)
//    {
//      TaskFactory.TaskType type = TaskFactory.TaskType.valueOf(tokens[0]);
//      ArrayUtils.subarray(tokens, 1, tokens.length);
//      return new ScriptsTokens(type, );
//    }
//    
//    public ScriptsTokens(final TaskFactory.TaskType type, String[] params)
//    {
//      this.type = type;
//      this.params = params;
//      if (this.type == null)
//        throw new NullArgumentException("type");
//    }
//  }

  private static class TasksWithFlowControl
  {
    private static class TaskPair
    {
      Task trueTask;

      Task falseTask;

      public TaskPair(Task trueTask, Task falseTask)
      {
        this.trueTask = trueTask;
        this.falseTask = falseTask;
      }
    }

    Map<Task, TaskPair> map = new HashMap<Task, TaskPair>();

    public void addTask(Task task, Task trueTask, Task falsTask)
    {
      map.put(task, new TaskPair(trueTask, falsTask));
    }

    public Task getTrueTask(Task task)
    {
      TaskPair taskPair = map.get(task);
      if (taskPair == null)
        return TaskFactory.newTask(new String[] {EMPTY.toString()});
      return taskPair.trueTask;
    }

    public Task getFalseTask(Task task)
    {
      TaskPair taskPair = map.get(task);
      if (taskPair == null)
        return TaskFactory.newTask(new String[] {EMPTY.toString()});
      return taskPair.falseTask;
    }
  }

}
