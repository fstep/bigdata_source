/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.html2xml;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.SAXException;

import com.porva.html.simpledom.SimpleDOM;
import com.porva.html.simpledom.SimpleDOMParser;
import com.porva.html2xml.task.Task;

public class DefaultScriptRunner implements ScriptRunner
{
  ScriptParser scriptParser = null;

  private SimpleDOM htmlModel;

  private SimpleDOMParser simpleDOMParser = new SimpleDOMParser();

  private int currIdx;

  private int prevIdx;

  // private boolean isFragmental;

  private boolean isEndOfDocument;

  private List<Task> processedTasks = new ArrayList<Task>();

  private URI currentURI = null;

  public static final String DEF_CHARSET = "iso-8859-1";

  private static final Log logger = LogFactory.getLog(DefaultScriptRunner.class);

  public int getPrevIdx()
  {
    return prevIdx;
  }

  public List<Task> getProcessedTasks()
  {
    return processedTasks;
  }

  public int getCurIdx()
  {
    return currIdx;
  }

  public void setPrevIdx(int newIdx)
  {
    prevIdx = newIdx;
  }

  public void setCurIdx(int newIdx)
  {
    currIdx = newIdx;
  }

  public SimpleDOM getHTMLModel()
  {
    return htmlModel;
  }

  public boolean isFragmental()
  {
    // return isFragmental;
    return false;
  }

  public URI getCurrentURI()
  {
    return currentURI;
  }

  public List<Task> performScript(final String content,
                                  final URI uri,
                                  final ScriptParser scriptParser) throws IOException, SAXException
  {
    this.scriptParser = scriptParser;
    this.currentURI = uri;
    if (this.scriptParser == null)
      throw new NullArgumentException("scriptParser");
    if (content == null)
      throw new NullArgumentException("content");

    init(new StringReader(content), uri);
    performScript();
    return processedTasks;
  }

  // //////////////////////// private //////////////////////////

  private void performScript()
  {
    Task aTask = scriptParser.getNextTask();
    while (aTask != null) {
      performTask(aTask);

      if (isEndOfDocument)
        return;

      aTask = scriptParser.getNextTask();
    }
  }

  public void performTask(Task task)
  {
    task.perform(this);

    switch (task.getType()) {
    case END_OF_ARTICLE:
      // if (isFragmental)
      // isEndOfDocument = true;
      break;
    case END_OF_DOCUMENT:
      isEndOfDocument = true;
      break;
    }

    scriptParser.fireTaskPerformed(task);
    processedTasks.add(task);
    traceTaskResult(task);
  }

  private void traceTaskResult(final Task task)
  {
    if (logger.isDebugEnabled()) {
      StringBuffer sb = new StringBuffer();
      sb.append("[").append(task.getResultCode().toString()).append("]\t").append(task.toString());
      logger.debug(sb.toString());
    }
  }

  private void init(final Reader htmlReader, final URI uri) throws IOException, SAXException
  {
    assert htmlReader != null;

    currIdx = 0;
    prevIdx = 0;
    // isFragmental = false;
    isEndOfDocument = false;
    htmlModel = simpleDOMParser.parse(htmlReader);
    processedTasks.clear();

    // if (uri != null && uri.getFragment() != null) {
    // isFragmental = true;
    // Task task = TaskFactory.newTask("FIND_NODE\t... a name='" + uri.getFragment() + "'");
    // performTask(task);
    // traceTaskResult(task);
    // }
  }
}
