/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.db;

import java.io.IOException;
import java.io.Writer;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ToStringDBDumper
{
  private static Log logger = LogFactory.getLog(ToStringDBDumper.class);
  
  public void export(DB<? extends DBValue> db, Writer writer) throws DBException
  {
    if (db == null)
      throw new NullArgumentException("db");
    if (!db.isOpened()) {
      db.open();
    }
    if (writer == null)
      throw new NullArgumentException("writer");

    logger.info("Exporting database " + db);

    try {
      writer.write("\n\ndatabase: " + db.toString());
      db.iterateOverAll(new ToStringExportFunctor(writer));
      writer.flush();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    if (db.isOpened())
      db.close();
  }

  
  static class ToStringExportFunctor implements DBFunctor
  {
    private Writer writer;

    public ToStringExportFunctor(final Writer writer)
    {
      this.writer = writer;

      if (this.writer == null)
        throw new NullArgumentException("writer");
    }

    public void process(final String key, final DBValue value)
    {
      if (key == null)
        throw new NullArgumentException("key");

      StringBuffer sb = new StringBuffer();
      try {
        writer.write(sb.append("\nkey: ").append(key).append("\nvalue: ").append(value.toString())
            .toString());
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  };


}
