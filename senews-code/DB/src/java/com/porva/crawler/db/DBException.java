/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.db;

/**
 * Signals that some database exception has occured.
 * 
 * @author Poroshin V.
 * @date Sep 13, 2005
 * @see com.porva.crawler.db.DB
 */
public class DBException extends Exception
{

  private static final long serialVersionUID = -8709811874419731759L;

  /**
   * Constructs an <code>DBException</code> with <code>null</code> as its error detail message.
   */
  public DBException()
  {
    super();
  }

  /**
   * Constructs a <code>DBException</code> with the specified detail message. The error message
   * string <code>s</code> can later be retrieved by the
   * <code>{@link java.lang.Throwable#getMessage}</code> method of class
   * <code>java.lang.Throwable</code>.
   * 
   * @param msg the detail message.
   */
  public DBException(String msg)
  {
    super(msg);
  }

  /**
   * Constructs a new <code>DBException</code> exception with the specified detail message and
   * cause.
   * 
   * @param message the detail message (which is saved for later retrieval by the
   *          {@link #getMessage()} method).
   * @param cause the cause (which is saved for later retrieval by the {@link #getCause()} method).
   *          (A <tt>null</tt> value is permitted, and indicates that the cause is nonexistent or
   *          unknown.)
   */
  public DBException(String message, Throwable cause)
  {
    super(message, cause);
  }

  /**
   * Constructs a new <code>DBException</code> exception with the specified cause and a detail
   * message of <tt>(cause==null ? null : cause.toString())</tt>.
   * 
   * @param cause the cause (which is saved for later retrieval by the {@link #getCause()} method).
   *          (A <tt>null</tt> value is permitted, and indicates that the cause is nonexistent or
   *          unknown.)
   */
  public DBException(Throwable cause)
  {
    super(cause);
  }

}
