/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.db.berkley;

import org.apache.commons.lang.IllegalClassException;
import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.crawler.db.DBException;
import com.porva.crawler.db.DBInputStream;
import com.porva.crawler.db.DBOutputStream;
import com.porva.crawler.db.DBValue;
import com.porva.crawler.db.DBValueFactory;
import com.sleepycat.bind.tuple.TupleBinding;
import com.sleepycat.bind.tuple.TupleInput;
import com.sleepycat.bind.tuple.TupleOutput;

/**
 * General tuple binder for all {@link DBValue} implementations, registered in
 * {@link CrawlerDBValueFactory}.
 * 
 * @author Poroshin V.
 * @date Sep 16, 2005
 */
public class DBValueTupleBinder extends TupleBinding
{
  private static Log logger = LogFactory.getLog(DBValueTupleBinder.class);

  private DBValueFactory dbValueFactory;

  private long classCode;

  private boolean isOneValue = false;

  /**
   * Creates a new {@link DBValueTupleBinder} object with specified <code>dbValueFactory</code>.
   * 
   * @param dbValueFactory database values factory.
   * @throws NullArgumentException if <code>dbValueFactory</code> is <code>null</code>.
   */
  public DBValueTupleBinder(final DBValueFactory dbValueFactory)
  {
    this.dbValueFactory = dbValueFactory;
    this.isOneValue = false;

    if (this.dbValueFactory == null)
      throw new NullArgumentException("dbValueFactory");
  }

  public DBValueTupleBinder(final DBValueFactory dbValueFactory, final Class cl)
  {
    this.dbValueFactory = dbValueFactory;
    this.classCode = this.dbValueFactory.getClassCode(cl);
    this.isOneValue = true;

    if (this.dbValueFactory == null)
      throw new NullArgumentException("dbValueFactory");
  }

  public Object entryToObject(TupleInput tupleInput)
  {
    long classCode = this.classCode;
    if (!isOneValue)
      classCode = tupleInput.readLong();
    
    DBInputStream dbInputStream = new TupleInputStream(tupleInput);
    DBValue newInstance = null;
    try {
      newInstance = dbValueFactory.newDBValue(dbInputStream, classCode);
    } catch (DBException e) {
      logger.warn("Failed to create new DBValue instance: " + e);
    }
    return newInstance;
  }

  public void objectToEntry(Object o, TupleOutput tupleOutput)
  {
    if (!(o instanceof DBValue)) {
      throw new IllegalClassException("Cannot bind object of this class to databse entry: "
          + o.getClass().toString());
    }

    DBValue dbValue = (DBValue) o;
    if (!isOneValue)
      tupleOutput.writeLong(dbValue.getClassCode());
    DBOutputStream dbOutputTuple = new TupleOutputStream(tupleOutput);
    dbValue.writeTo(dbOutputTuple);
  }

  private transient String toString;

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    if (toString == null) {
      toString = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
          .appendSuper(super.toString()).toString();
    }
    return toString;
  }

}
