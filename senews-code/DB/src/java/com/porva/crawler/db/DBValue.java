/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.db;

/**
 * Interface to store objects into database. Any implementaions of this interface can store their
 * objects in database as values.
 * 
 * @author Poroshin V.
 * @date Sep 16, 2005
 */
public interface DBValue
{

  /**
   * The object implements this method to save its contents to <code>tupleOutput</code>.
   * 
   * @param tupleOutput a data output like stream for writing tuple fields.
   */
  public void writeTo(final DBOutputStream tupleOutput);

  /**
   * Creates new instance of objects initialized by data from <code>tupleInput</code>.
   * 
   * @param tupleInput data input like methods for reading tuple fields.
   * @return new instance of this class initialized from <code>tupleInput</code>.
   * @throws DBException if creation of new instance failed for some reason.
   */
  public DBValue newInstance(final DBInputStream tupleInput) throws DBException;

  /**
   * Returns an unique code for each DBValue class.<br>
   * Note that <b>any object of some DBValue class should return the same class code.</b> <br>
   * This code is used in databases to store and restore data for each DBValue class, so never
   * change this code if you want to manipulate old databases.<br>
   * Be sure also that <b>returned code is unique for this DBValue and no other DBValue classes have
   * it.</b> You can use serial version UID generator to produce such a code.
   * 
   * @return unique code for each DBValue class.
   */
  public long getClassCode();
}
