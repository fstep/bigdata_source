/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.db;

/**
 * Interface for database record.<br>
 * Database record is a pair of key-value. Key is a String object and value is some implementaion of
 * {@link DBValue} interface.
 * 
 * @author Poroshin V.
 * @date Sep 25, 2005
 * @param <T> some implementation of {@link DBValue} interface.
 */
public interface DBRecord<T extends DBValue>
{
  /**
   * Returns key of record.
   * 
   * @return key of record.
   */
  public String getDBKey();

  /**
   * Returns value of record.
   * 
   * @return value of record.
   */
  public T getDBValue();
}
