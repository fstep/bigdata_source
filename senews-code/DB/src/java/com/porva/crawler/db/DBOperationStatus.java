/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.db;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.sleepycat.je.OperationStatus;

/**
 * Status of database operation.
 * 
 * @author Poroshin V.
 * @date Sep 25, 2005
 */
public enum DBOperationStatus
{
  /**
   * Key is empty.
   */
  KEYEMPTY,

  /**
   * Key already exists.
   */
  KEYEXIST,

  /**
   * Not found.
   */
  NOTFOUND,

  /**
   * Operation susseeded.
   */
  SUCCESS,

  /**
   * Some error happened.
   */
  ERROR;

  private Throwable cause = null;

  /**
   * Defined a database operation status {@link #ERROR} with specified error cause.
   * 
   * @param cause Throwable error cause
   * @return {@link DBOperationStatus#ERROR}.
   */
  public static DBOperationStatus ERROR(Throwable cause)
  {
    DBOperationStatus status = ERROR;
    status.cause = cause;
    return status;
  }

  /**
   * Returns error cause of {@link DBOperationStatus#ERROR} operation status.
   * 
   * @return cause of {@link DBOperationStatus#ERROR} operation status.
   */
  public Throwable getErrorCause()
  {
    return cause;
  }

  /**
   * Returns instance of {@link DBOperationStatus} that fitted to given <code>status</code>.
   * 
   * @param status
   * @return fitted instance {@link DBOperationStatus}.
   */
  public static DBOperationStatus getFittedInstance(OperationStatus status)
  {
    if (status == null)
      throw new NullArgumentException("status");

    if (status == OperationStatus.KEYEMPTY)
      return DBOperationStatus.KEYEMPTY;
    if (status == OperationStatus.KEYEXIST)
      return DBOperationStatus.KEYEXIST;
    if (status == OperationStatus.NOTFOUND)
      return DBOperationStatus.NOTFOUND;
    if (status == OperationStatus.SUCCESS)
      return DBOperationStatus.SUCCESS;

    throw new IllegalArgumentException();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    if (this == ERROR)
      return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("status",
                                                                                this.toString())
          .toString();
    else
      return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("status",
                                                                                this.toString())
          .append("cause", cause).toString();
  }

}
