/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.db.berkley;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.crawler.db.DB;
import com.porva.crawler.db.DBCursor;
import com.porva.crawler.db.DBException;
import com.porva.crawler.db.DBFunctor;
import com.porva.crawler.db.DBOperationStatus;
import com.porva.crawler.db.DBRecord;
import com.porva.crawler.db.DBValue;
import com.sleepycat.bind.tuple.TupleBinding;
import com.sleepycat.je.Cursor;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.LockMode;
import com.sleepycat.je.OperationStatus;
import com.sleepycat.je.Transaction;

/**
 * Implementation of {@link DB} interface using berkley db java edition.
 * 
 * @author Poroshin V.
 * @date Sep 15, 2005
 * @param <T>
 */
public class BerkleyDBImpl<T extends DBValue> implements DB<T>
{
  private String name;

  private Database db = null;

  private Environment dbEnv;

  private DatabaseConfig dbConfig;

  private TupleBinding tupleBinder;

  private static Log logger = LogFactory.getLog(BerkleyDBImpl.class);

  private boolean isOpened = false;

  /**
   * Allocates new {@link BerkleyDBImpl} object.
   * 
   * @param dbName name of database.
   * @param tupleBinding {@link TupleBinding} tuple binder for this type of database.
   * @param dbEnv {@link Environment} enviroment.
   * @param dbConfig {@link DatabaseConfig} database configuration.
   * @throws NullArgumentException if any of input arguments is <code>null</code>.
   */
  public BerkleyDBImpl(final String dbName,
                       final TupleBinding tupleBinding,
                       final Environment dbEnv,
                       final DatabaseConfig dbConfig)
  {
    this.name = dbName;
    this.dbEnv = dbEnv;
    this.dbConfig = dbConfig;
    this.tupleBinder = tupleBinding;

    if (this.name == null)
      throw new NullArgumentException("dbType");
    if (this.dbEnv == null)
      throw new NullArgumentException("dbEnv");
    if (this.dbConfig == null)
      throw new NullArgumentException("dbConfig");
    if (this.tupleBinder == null)
      throw new NullArgumentException("tupleBinding");
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DB#open()
   */
  public void open() throws DBException
  {
    if (db != null) {
      logger.warn("Database " + name + " is already opened.");
      return;
    }

    try {
      Transaction txn = beginTransaction();
      db = dbEnv.openDatabase(txn, name, dbConfig);
      endTransaction(txn);

      isOpened = true;
    } catch (DatabaseException e) {
      throw new DBException(e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DB#close()
   */
  public void close() throws DBException
  {
    if (db == null)
      throw new IllegalStateException("Cannot close database because it is not opened.");

    Transaction txn = null;
    try {
      txn = beginTransaction();
      db.close();
    } catch (DatabaseException e) {
      throw new DBException(e);
    } finally {
      isOpened = false;
      db = null;
      try {
        endTransaction(txn);
      } catch (DatabaseException e) {
        throw new DBException(e);
      }
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DB#put(java.lang.String, com.porva.crawler.db.DBValue)
   */
  public DBOperationStatus put(final String key, final T value)
  {
    if (key == null)
      throw new NullArgumentException("key");
    if (value == null)
      throw new NullArgumentException("value");
    if (db == null)
      throw new IllegalStateException("Cannot put to closed or unopened database.");

    OperationStatus status = null;
    try {
      DatabaseEntry keyEntry = new DatabaseEntry(key.getBytes("UTF-8"));
      DatabaseEntry valueEntry = new DatabaseEntry();
      tupleBinder.objectToEntry(value, valueEntry);

      Transaction txn = beginTransaction();
      status = db.put(txn, keyEntry, valueEntry);
      endTransaction(txn);

      keyEntry = null;
      valueEntry = null;
      if (logger.isDebugEnabled())
        logger.debug("Record added: key=" + key + " value=" + value);
    } catch (DatabaseException e) {
      logger.warn(e);
      return DBOperationStatus.ERROR(e);
    } catch (UnsupportedEncodingException e) {
      logger.warn(e);
      return DBOperationStatus.ERROR(e);
    }

    return DBOperationStatus.getFittedInstance(status);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DB#putNoOverwrite(java.lang.String, com.porva.crawler.db.DBValue)
   */
  public DBOperationStatus putNoOverwrite(final String key, final T value)
  {
    if (key == null)
      throw new NullArgumentException("key");
    if (value == null)
      throw new NullArgumentException("value");

    if (db == null)
      throw new IllegalStateException("Cannot put to closed or unopened database.");

    OperationStatus status = null;
    try {
      DatabaseEntry keyEntry = new DatabaseEntry(key.getBytes("UTF-8"));
      DatabaseEntry valueEntry = new DatabaseEntry();
      tupleBinder.objectToEntry(value, valueEntry);

      Transaction txn = beginTransaction();
      status = db.putNoOverwrite(txn, keyEntry, valueEntry);
      endTransaction(txn);

      keyEntry = null;
      valueEntry = null;
      if (logger.isDebugEnabled())
        logger.debug("Record added: key=" + key + " value=" + value);
    } catch (DatabaseException e) {
      logger.warn(e);
      return DBOperationStatus.ERROR(e);
    } catch (UnsupportedEncodingException e) {
      logger.warn(e);
      return DBOperationStatus.ERROR(e);
    }

    return DBOperationStatus.getFittedInstance(status);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DB#get(java.lang.String)
   */
  public T get(final String key)
  {
    if (key == null)
      throw new NullArgumentException("key");
    if (db == null)
      throw new IllegalStateException("Cannot put to closed or unopened database.");

    T dbValue = null;
    DatabaseEntry keyEntry = null;
    DatabaseEntry dataEntry = null;
    try {
      keyEntry = new DatabaseEntry(key.getBytes("UTF-8"));
      dataEntry = new DatabaseEntry();
      if (db.get(null, keyEntry, dataEntry, LockMode.DEFAULT) == OperationStatus.SUCCESS)
        dbValue = entryToObject(dataEntry);
    } catch (UnsupportedEncodingException e) {
      logger.warn(e);
    } catch (DatabaseException e) {
      logger.warn(e);
    } catch (DBException e) {
      logger.warn(e);
    } finally {
      keyEntry = null;
      dataEntry = null;
    }

    return dbValue;
  }
  
  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DB#getAllDup(java.lang.String)
   */
  public DBOperationStatus getAllDup(String key, List<T> listForValues)
  {
    if (key == null)
      throw new NullArgumentException("key");
    if (listForValues == null)
      throw new NullArgumentException("listForValues");
    if (db == null)
      throw new IllegalStateException("Cannot put to closed or unopened database.");

    Cursor cursor = null;
    Transaction txn = null;
    DBOperationStatus status = DBOperationStatus.SUCCESS;
    try {
      DatabaseEntry theKey = new DatabaseEntry(key.getBytes("UTF-8"));
      DatabaseEntry theData = new DatabaseEntry();

      txn = beginTransaction();
      cursor = db.openCursor(txn, null);

      OperationStatus retVal = cursor.getSearchKey(theKey, theData, LockMode.DEFAULT);

      int num = 0;
      while (retVal == OperationStatus.SUCCESS) {
        listForValues.add(entryToObject(theData));
        num++;
        retVal = cursor.getNextDup(theKey, theData, LockMode.DEFAULT);
      }
      if (num == 0)
        status = DBOperationStatus.NOTFOUND;

    } catch (UnsupportedEncodingException e) {
      logger.warn(e);
      return DBOperationStatus.ERROR(e);
    } catch (DatabaseException e) {
      logger.warn(e);
      return DBOperationStatus.ERROR(e);
    } catch (DBException e) {
      logger.warn(e);
      return DBOperationStatus.ERROR(e);
    } finally {
      try {
        cursor.close();
        endTransaction(txn);
      } catch (DatabaseException e) {
        logger.warn(e); // FIXME: should it raise exception ?
        return DBOperationStatus.ERROR(e);
      }
    }
    return status;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DB#isOpened()
   */
  public boolean isOpened()
  {
    return isOpened;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DB#iterateOverAll(com.porva.crawler.db.DBFunctor)
   */
  public void iterateOverAll(DBFunctor<T> functor)
  {
    if (functor == null)
      throw new NullArgumentException("functor");
    if (db == null)
      throw new IllegalStateException("Cannot iterate over closed or unopened database.");

    Cursor cursor = null;
    Transaction txn = null;
    try {
      txn = beginTransaction();
      cursor = db.openCursor(txn, null);

      DatabaseEntry foundKey = new DatabaseEntry();
      DatabaseEntry foundData = new DatabaseEntry();

      while (cursor.getNext(foundKey, foundData, LockMode.DEFAULT) == OperationStatus.SUCCESS) {
        String key = new String(foundKey.getData());
        T value = entryToObject(foundData);
        functor.process(key, value);

        value = null;
        key = null;
      }
    } catch (DatabaseException e) {
      logger.warn(e);
    } catch (DBException e) {
      logger.warn(e);
    } finally {
      try {
        cursor.close();
        endTransaction(txn);
      } catch (DatabaseException e) {
        logger.warn(e);
      }
    }

  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DB#delete(java.lang.String, com.porva.crawler.db.DBValue)
   */
  public DBOperationStatus delete(final String key, final T value)
  {
    if (key == null)
      throw new NullArgumentException("key");
    if (db == null)
      throw new IllegalStateException("Cannot delete record from closed or unopened database.");

    OperationStatus status;
    Cursor cursor = null;
    Transaction txn = null;
    try {
      DatabaseEntry keyEntry = new DatabaseEntry(key.getBytes("UTF-8"));
      DatabaseEntry valueEntry = new DatabaseEntry();
      tupleBinder.objectToEntry(value, valueEntry);

      txn = beginTransaction();
      cursor = db.openCursor(txn, null);
      status = cursor.getSearchBoth(keyEntry, valueEntry, LockMode.DEFAULT);
      if (status == OperationStatus.SUCCESS && cursor.delete() == OperationStatus.SUCCESS) {
        if (logger.isDebugEnabled())
          logger.debug("Record deleted: key=" + key + " value=" + value);
      } else if (logger.isWarnEnabled())
        logger.warn("Cannot find db key-value to delete: key=" + key + " value=" + value);
    } catch (DatabaseException e) {
      logger.warn("Cannot delete db record: ", e);
      return DBOperationStatus.ERROR(e);
    } catch (UnsupportedEncodingException e) {
      logger.warn(e);
      return DBOperationStatus.ERROR(e);
    } finally {
      try {
        cursor.close();
        endTransaction(txn);
      } catch (DatabaseException e) {
        return DBOperationStatus.ERROR(e);
      }
    }
    return DBOperationStatus.getFittedInstance(status);
  }

  /* (non-Javadoc)
   * @see com.porva.crawler.db.DB#deleteByEquals(java.lang.String, com.porva.crawler.db.DBValue)
   */
  public DBOperationStatus deleteByEquals(String key, T value)
  {
    if (key == null)
      throw new NullArgumentException("key");
    if (db == null)
      throw new IllegalStateException("Cannot delete record from closed or unopened database.");

    int deleted = 0;
    OperationStatus status;
    Cursor cursor = null;
    Transaction txn = null;
    try {
      DatabaseEntry keyEntry = new DatabaseEntry(key.getBytes("UTF-8"));
      DatabaseEntry valueEntry = new DatabaseEntry();

      txn = beginTransaction();
      cursor = db.openCursor(txn, null);
      status = cursor.getSearchKey(keyEntry, valueEntry, LockMode.DEFAULT);
      if (status == OperationStatus.SUCCESS) {
        OperationStatus st = OperationStatus.SUCCESS;
        while (st == OperationStatus.SUCCESS) {
          if (tupleBinder.entryToObject(valueEntry).equals(value)) {
            status = cursor.delete();
            deleted++;
            if (logger.isDebugEnabled())
              logger.debug("Record deleted: key=" + key + " value=" + value);
          }
          st = cursor.getNextDup(keyEntry, valueEntry, LockMode.DEFAULT);
        }
      } else if (logger.isWarnEnabled())
        logger.warn("Cannot find db key-value to delete: key=" + key + " value=" + value);
    } catch (DatabaseException e) {
      logger.warn("Cannot delete db record: ", e);
      return DBOperationStatus.ERROR(e);
    } catch (UnsupportedEncodingException e) {
      logger.warn(e);
      return DBOperationStatus.ERROR(e);
    } finally {
      try {
        cursor.close();
        endTransaction(txn);
      } catch (DatabaseException e) {
        return DBOperationStatus.ERROR(e);
      }
    }
    
    if (deleted == 0)
      status = OperationStatus.NOTFOUND;
      
    return DBOperationStatus.getFittedInstance(status);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DB#delete(java.lang.String)
   */
  public DBOperationStatus delete(final String key)
  {
    if (key == null)
      throw new NullArgumentException("key");

    OperationStatus status = OperationStatus.SUCCESS;
    Transaction txn = null;

    try {
      DatabaseEntry keyEntry = new DatabaseEntry(key.getBytes("UTF-8"));
      txn = beginTransaction();
      if (db.delete(null, keyEntry) == OperationStatus.SUCCESS) {
        if (logger.isDebugEnabled())
          logger.debug("Record deleted: key=" + key);
      } else if (logger.isWarnEnabled())
        logger.warn("Cannot delete all records with key: " + key);
    } catch (DatabaseException e) {
      logger.warn("Cannot delete db record: ", e);
      return DBOperationStatus.ERROR(e);
    } catch (UnsupportedEncodingException e) {
      logger.warn(e);
      return DBOperationStatus.ERROR(e);
    } finally {
      try {
        endTransaction(txn);
      } catch (DatabaseException e) {
        return DBOperationStatus.ERROR(e);
      }
    }
    return DBOperationStatus.getFittedInstance(status);
  }

  /**
   * Calculates number of records in this opened database.<br>
   * <b>Warning!</b> This method may be slow since it iterates over all recoreds in database.
   * 
   * @return number of records.
   */
  public int getRecordsNum()
  {
    if (db == null)
      throw new IllegalStateException(
          "Cannot get number of tuples from closed or unopened database.");

    int num = 0;
    Cursor cursor = null;
    Transaction txn = null;
    try {
      txn = beginTransaction();
      cursor = db.openCursor(txn, null);

      DatabaseEntry foundKey = new DatabaseEntry();
      DatabaseEntry foundData = new DatabaseEntry();
      while (cursor.getNext(foundKey, foundData, LockMode.DEFAULT) == OperationStatus.SUCCESS) {
        num++;
      }
    } catch (DatabaseException e) {
      logger.warn(e);
    } finally {
      try {
        cursor.close();
        endTransaction(txn);
      } catch (DatabaseException e) {
        logger.warn(e);
      }
    }
    return num;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DB#getDBName()
   */
  public String getDBName()
  {
    return name;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DB#getIterator()
   */
  public DBCursor<T> getCursor() throws DBException
  {
    if (db == null)
      throw new IllegalStateException("Cannot get cursor from closed or unopened database.");
    return new BerkleyDBCursor<T>(db, tupleBinder);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DB#put(com.porva.crawler.db.DBRecord)
   */
  public DBOperationStatus put(DBRecord<T> record)
  {
    if (record == null)
      throw new NullArgumentException("record");
    return put(record.getDBKey(), record.getDBValue());
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DB#putNoOverwrite(com.porva.crawler.db.DBRecord)
   */
  public DBOperationStatus putNoOverwrite(DBRecord<T> record)
  {
    if (record == null)
      throw new NullArgumentException("record");
    return putNoOverwrite(record.getDBKey(), record.getDBValue());
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DB#delete(com.porva.crawler.db.DBRecord)
   */
  public DBOperationStatus delete(DBRecord<T> record)
  {
    if (record == null)
      throw new NullArgumentException("record");
    return delete(record.getDBKey(), record.getDBValue());
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("name", name)
        .append("isOpened", isOpened).toString();
  }

  // ///////////////////////////////////////////////
  // private methods
  // ///////////////////////////////////////////////
  @SuppressWarnings("unchecked")
  private T entryToObject(final DatabaseEntry dataEntry) throws DBException
  {
    Object objVal = tupleBinder.entryToObject(dataEntry);
    if (objVal == null)
      throw new DBException("Failed to bind this entry to object: " + dataEntry);

    T dbEntry = null;
    try {
      dbEntry = (T) objVal;
    } catch (ClassCastException e) {
      logger.warn(e);
    }
    return dbEntry;
  }

  /**
   * Starts transaction.
   */
  private Transaction beginTransaction() throws DatabaseException
  {
    if (dbConfig.getTransactional())
      return dbEnv.beginTransaction(null, null);
    return null;
  }

  /**
   * Commits transaction.
   */
  private void endTransaction(final Transaction transaction) throws DatabaseException
  {
    if (transaction != null)
      transaction.commit();
  }
}
