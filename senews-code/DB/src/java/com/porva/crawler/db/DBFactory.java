/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.db;

import org.apache.commons.lang.NullArgumentException;

import com.sleepycat.bind.tuple.TupleBinding;

/**
 * Abstract database factory.
 *
 * @author Poroshin V.
 * @date Nov 3, 2005
 */
public interface DBFactory
{
  /**
   * Closes database factory.<br>
   * Once factory is closed, regardless of whether or not it throws an exception, all database
   * handles produced may not be accessable. The databas factory should not be closed while any its
   * database handles not yet closed.
   * 
   * @throws DBException if a failure occurs.
   * @throws IllegalStateException if factory is already closed.
   */
  public void close() throws DBException;

  /**
   * Returns <code>true</code> if factory is closed or <code>false</code> otherwise.
   * 
   * @return <code>true</code> if factory is closed; <code>false</code> otherwise.
   */
  public boolean isClosed();

  /**
   * Removes <b>closed</b> database factory by deleting all its content.<br>
   * This operation supposes to remove all data and structure in all databases of this factory.
   * 
   * @return <code>true</code> if enviroment was successfully deleted; <code>false</code>
   *         otherwise.
   * @throws IllegalStateException if <code>enviroment</code> is not closed.
   */
  public boolean removeAll();

  /**
   * Removes database of given dbName. Applications should never remove databases with opened
   * Database handles.
   * 
   * @param dbName name of database to remove.
   * @throws DBException if a failure occurs.
   * @throws NullArgumentException if dbType is <code>null</code>.
   * @throws IllegalStateException if factory is already closed.
   */
  public void removeDatabase(String dbName) throws DBException;
  
  /**
   * Returns new {@link DB} database handle to store any {@link DBValue} objects. If database does
   * not exist it will be created. If the same type of database was alredy requested a <b>new</b>
   * {@link DB} handle will be returned.
   * 
   * @param isStoreDup <code>true</code> to allow store duplicates; <code>false</code> otherwise.
   * @param tupleBinder 
   * @return database to store any {@link DBValue} objects.
   * @throws IllegalStateException if factory is already closed.
   */
  public DB<DBValue> newGenericDatabaseHandle(boolean isStoreDup, TupleBinding tupleBinder);

}
