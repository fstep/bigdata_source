/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.db;

/**
 * Generic factory interface to create database values.<br>
 * Any database value registered in value factory should implement {@link DBValue} interface.
 * 
 * @author Poroshin V.
 * @date Nov 4, 2005
 */
public interface DBValueFactory
{
  /**
   * Creates new {@link DBValue} instance based on its <code>classCode</code> and restores its
   * values from <code>tupleInput</code>.
   * 
   * @param tupleInput input like data stream to restore data from.
   * @param classCode classCode of requested {@link DBValue} instance.
   * @return newly allocated instance of database value.
   * @throws DBException if some database failier occurs.
   * @throws org.apache.commons.lang.NullArgumentException if <code>tupleInput</code> is
   *           <code>null</code>. 
   */
  public DBValue newDBValue(final DBInputStream tupleInput, final long classCode)
      throws DBException;

  /**
   * Returns class code of given Class <code>cl</code>.<br>
   * 
   * @param cl class to get code of.
   * @return class code.
   * @throws org.apache.commons.lang.NullArgumentException if <code>cl</code> is <code>null</code>.
   * @throws IllegalArgumentException if <code>cl</code> class is not registered in value factory.
   */
  public long getClassCode(final Class cl);

  /**
   * Reads object from <code>tupleInput</code>.<br>
   * Type of readed object depends on class code that should be located at the beginning of
   * <code>tupleInput</code>.
   * 
   * @param tupleInput input like data stream to restore data from.
   * @return readed object.
   * @throws DBException if some database exception occures.
   * @throws org.apache.commons.lang.NullArgumentException if <code>tupleInput</code> is
   *           <code>null</code>.
   */
  public DBValue readObject(final DBInputStream tupleInput) throws DBException;

  /**
   * Creates new {@link DBValue} instance based on its class <code>cl</code> and restores its
   * values from <code>tupleInput</code>.
   * 
   * @param tupleInput input like data stream to restore data from.
   * @param cl class object of requested {@link DBValue} instance.
   * @return newly allocated instance of database value.
   * @throws DBException if some failier occurs.
   * @throws IllegalArgumentException if class <code>cl</code> is not registered.
   */
  public DBValue newDBValueInstance(final DBInputStream tupleInput, final Class cl)
      throws DBException;

}