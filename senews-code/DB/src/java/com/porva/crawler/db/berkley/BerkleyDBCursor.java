/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.db.berkley;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.db.DBCursor;
import com.porva.crawler.db.DBException;
import com.porva.crawler.db.DBRecord;
import com.porva.crawler.db.DBValue;
import com.porva.crawler.db.DefaulDBRecord;
import com.sleepycat.bind.tuple.TupleBinding;
import com.sleepycat.je.Cursor;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.LockMode;
import com.sleepycat.je.OperationStatus;
import com.sleepycat.je.Transaction;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.builder.ToStringBuilder;

class BerkleyDBCursor<T extends DBValue> implements DBCursor<T>
{
  private Cursor cursor;
  
  private Transaction txn = null;

  private Database db;

  private TupleBinding tupleBinding;

  BerkleyDBCursor(Database db, TupleBinding tupleBinding) throws DBException
  {
    this.db = db;
    this.tupleBinding = tupleBinding;
    if (this.db == null)
      throw new NullArgumentException("db");
    if (this.tupleBinding == null)
      throw new NullArgumentException("tupleBinding");

    initCursor();
  }
  
  public void close() throws DBException
  {
    try {
      cursor.close();
    } catch (DatabaseException e) {
      throw new DBException(e);
    } finally {
      try {
        endTransaction();
      } catch (DatabaseException e) {
        throw new DBException(e);
      }
    }
  }

  public DBRecord<T> getNext() throws DBException
  {
    DatabaseEntry foundKey = new DatabaseEntry();
    DatabaseEntry foundData = new DatabaseEntry();
    DBRecord<T> next = null;
    try {
      if (cursor.getNext(foundKey, foundData, LockMode.DEFAULT) == OperationStatus.SUCCESS) {
        String key = new String(foundKey.getData());
        T value = entryToObject(foundData);
        next = new DefaulDBRecord<T>(key, value);
      }
    } catch (DatabaseException e) {
      throw new DBException(e);
    } finally {
      foundKey = null;
      foundData = null;
    }
    return next;
  }

  public DBRecord<T> getFirst() throws DBException
  {
    DatabaseEntry foundKey = new DatabaseEntry();
    DatabaseEntry foundData = new DatabaseEntry();
    DBRecord<T> next = null;
    try {
      if (cursor.getFirst(foundKey, foundData, LockMode.DEFAULT) == OperationStatus.SUCCESS) {
        String key = new String(foundKey.getData());
        T value = entryToObject(foundData);
        next = new DefaulDBRecord<T>(key, value);
      }
    } catch (DatabaseException e) {
      throw new DBException(e);
    } finally {
      foundKey = null;
      foundData = null;
    }
    return next;
  }

  public DBRecord<T> getLast() throws DBException
  {
    DatabaseEntry foundKey = new DatabaseEntry();
    DatabaseEntry foundData = new DatabaseEntry();
    DBRecord<T> next = null;
    try {
      if (cursor.getLast(foundKey, foundData, LockMode.DEFAULT) == OperationStatus.SUCCESS) {
        String key = new String(foundKey.getData());
        T value = entryToObject(foundData);
        next = new DefaulDBRecord<T>(key, value);
      }
    } catch (DatabaseException e) {
      throw new DBException(e);
    } finally {
      foundKey = null;
      foundData = null;
    }
    return next;
  }

  public DBRecord<T> getPrev() throws DBException
  {
    DatabaseEntry foundKey = new DatabaseEntry();
    DatabaseEntry foundData = new DatabaseEntry();
    DBRecord<T> next = null;
    try {
      if (cursor.getPrev(foundKey, foundData, LockMode.DEFAULT) == OperationStatus.SUCCESS) {
        String key = new String(foundKey.getData());
        T value = entryToObject(foundData);
        next = new DefaulDBRecord<T>(key, value);
      }
    } catch (DatabaseException e) {
      throw new DBException(e);
    } finally {
      foundKey = null;
      foundData = null;
    }
    return next;
  }
  
  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("cursor", cursor)
        .append("db", db).append("tupleBinding", tupleBinding).toString();
  }

  ////////////////////////////////////////////////////////// private

  @SuppressWarnings("unchecked")
  private T entryToObject(final DatabaseEntry dataEntry) throws DBException
  {
    Object objVal = tupleBinding.entryToObject(dataEntry);
    if (objVal == null) 
      throw new DBException("Failed to bind this entry to object: " + dataEntry);
    
    T dbEntry = null;
    try {
      dbEntry = (T)objVal;
    }
    catch (ClassCastException e) {
      throw new DBException(e);
    }
    return dbEntry;
  }
  
  /**
   * Starts transaction.
   */
  private void beginTransaction() throws DatabaseException
  {
    if (db.getConfig().getTransactional())
      txn = db.getEnvironment().beginTransaction(null, null);
    else 
      txn = null;
  }

  /**
   * Commits transaction.
   */
  private void endTransaction() throws DatabaseException
  {
    if (txn != null) {
      txn.commit();
      txn = null;
    }
  }
  
  private void initCursor() throws DBException
  {
    try {
      beginTransaction();
      cursor = db.openCursor(txn, null);
    } catch (DatabaseException e) {
      throw new DBException(e);
    }
  }

}
