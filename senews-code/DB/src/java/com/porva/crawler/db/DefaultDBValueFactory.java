/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.db;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Default implementation of {@link DBValueFactory} interface.
 *
 * @author Poroshin V.
 * @date Nov 4, 2005
 */
public class DefaultDBValueFactory implements DBValueFactory
{

  private Map<Long, Class> classes = new HashMap<Long, Class>();

  private Map<Class, Long> clLong = new HashMap<Class, Long>();

  private Map<Long, DBValue> instances = new HashMap<Long, DBValue>();

  private static Log logger = LogFactory.getLog(DefaultDBValueFactory.class);
  
  /* (non-Javadoc)
   * @see com.porva.crawler.db.DBValueFactory#newDBValue(com.porva.crawler.db.DBInputStream, long)
   */
  public DBValue newDBValue(final DBInputStream tupleInput, final long classCode)
      throws DBException
  {
    if (tupleInput == null)
      throw new NullArgumentException("tupleInput");

    DBValue instance = null;
    if (instances.get(classCode) != null) {
      DBValue dbValue = instances.get(classCode);
      instance = dbValue.newInstance(tupleInput);
    }
    return instance;
  }

  /* (non-Javadoc)
   * @see com.porva.crawler.db.DBValueFactory#readObject(com.porva.crawler.db.DBInputStream)
   */
  public DBValue readObject(final DBInputStream tupleInput) throws DBException
  {
    if (tupleInput == null)
      throw new NullArgumentException("tupleInput");

    long classID = tupleInput.readLong();
    DBValue instance = instances.get(classID);
    if (instance == null)
      throw new DBException("Class with this code does not registered: " + classID);

    return instance.newInstance(tupleInput);
  }

  /* (non-Javadoc)
   * @see com.porva.crawler.db.DBValueFactory#newDBValueInstance(com.porva.crawler.db.DBInputStream, java.lang.Class)
   */
  public DBValue newDBValueInstance(final DBInputStream tupleInput,
                                                        final Class cl) throws DBException
  {
    Long classCode = clLong.get(cl);
    if (classCode == null)
      throw new IllegalArgumentException("This class is not registered: " + cl);

    return newDBValue(tupleInput, classCode);
  }
  
  /* (non-Javadoc)
   * @see com.porva.crawler.db.DBValueFactory#getClassCode(java.lang.Class)
   */
  public long getClassCode(final Class cl)
  {
    if (cl == null)
      throw new NullArgumentException("cl");

    Long classCode = clLong.get(cl);
    if (classCode == null)
      throw new IllegalArgumentException("This class is not registered: " + cl);
     
    return classCode;
  }

  // /////////////////////////////////////////////////////////////
  /**
   * Registers given class <code>cl</code> in value factory.
   * 
   * @param cl class to register.
   * @throws IllegalArgumentException
   * @throws IllegalAccessException
   * @throws SecurityException
   * @throws NoSuchMethodException
   * @throws InstantiationException
   * @throws InvocationTargetException
   */
  protected void registerClass(final Class cl) throws IllegalArgumentException,
      IllegalAccessException, SecurityException, NoSuchMethodException, InstantiationException,
      InvocationTargetException
  {
    if (cl == null)
      throw new NullArgumentException("cl");
    
    if (clLong.containsKey(cl))
      return;

    Constructor constructor = cl.getDeclaredConstructor(new Class[] {});
    constructor.setAccessible(true);
    DBValue instance = (DBValue) constructor.newInstance();

    long uid = instance.getClassCode();

    classes.put(uid, cl);
    clLong.put(cl, uid);
    instances.put(uid, instance);

    if (logger.isDebugEnabled())
      logger.debug("Class registered in DBValueFactory: serialVersionUID=" + uid + " class=" + cl);
  }

}
