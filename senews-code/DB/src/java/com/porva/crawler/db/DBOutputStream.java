/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.db;

import java.util.Map;

/**
 * Output stream that allows to write primitives as well as some complex objects to the database
 * tuple. 
 * 
 * @author Poroshin V.
 * @date Sep 25, 2005
 */
public interface DBOutputStream
{

  // ///////////////////////////////////////////// writing primitives

  /**
   * Writes boolean value to this DBOutputStream.<br>
   * Written value can be readed using {@link DBInputStream#readBoolean()}.
   * 
   * @param val value to store.
   */
  public void writeBoolean(boolean val);

  /**
   * Writes single byte to this DBOutputStream.<br>
   * Written value can be readed using {@link DBInputStream#readByte()}.
   * 
   * @param val value to store.
   */
  public void writeByte(byte val);

  /**
   * Writes single char to this DBOutputStream.<br>
   * Written value can be readed using {@link DBInputStream#readChar()}.
   * 
   * @param val value to store.
   */
  public void writeChar(char val);

  /**
   * Writes int to this DBOutputStream.<br>
   * Written value can be readed using {@link DBInputStream#readInt()}.
   * 
   * @param val value to store.
   */
  public void writeInt(int val);

  /**
   * Writes long to this DBOutputStream.<br>
   * Written value can be readed using {@link DBInputStream#readLong()}.
   * 
   * @param val value to store.
   */
  public void writeLong(long val);

  /**
   * Writes float to this DBOutputStream.<br>
   * Written value can be readed using {@link DBInputStream#readFloat()}.
   * 
   * @param val value to store.
   */
  public void writeFloat(float val);

  /**
   * Writes double to this DBOutputStream.<br>
   * Written value can be readed using {@link DBInputStream#readDouble()}.
   * 
   * @param val value to store.
   */
  public void writeDouble(double val);

  // //////////////////////////////////////////// writing more complex objects

  /**
   * Writes all bytes form <code>fromBuf</code> to this DBOutputStream.<br>
   * Written value can be readed using {@link DBInputStream#readBytes()}.
   * 
   * @param fromBuf bytes to store or <code>null</code> to store <code>null</code> bytes.
   */
  public void writeBytes(byte[] fromBuf);

  /**
   * Compresses and writes all bytes form <code>fromBuf</code> to this DBOutputStream.<br>
   * Written value can be readed using {@link DBInputStream#readUncompressBytes()}.
   * 
   * @param fromBuf bytes to store or <code>null</code> to store <code>null</code> bytes.
   */
  public void writeCompressBytes(byte[] fromBuf);

  /**
   * Writes given string to this DBOutputStream.<br>
   * Written value can be readed using {@link DBInputStream#readString()}.
   * 
   * @param val string to store or <code>null</code> to store <code>null</code> string.
   */
  public void writeString(String val);

  /**
   * Writes given map of strings to this DBOutputStream.<br>
   * Written value can be readed using {@link DBInputStream#readMapOfStrings(Map)}.
   * 
   * @param map map of strings to store or <code>null</code> to store <code>null</code> map of
   *          strings.
   */
  public void writeMapOfStrings(final Map<String, String> map);

}
