/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.db;


/**
 * A database cursor. Cursors are used for iterating over a databases records.
 * 
 * @author Poroshin V.
 * @date Sep 24, 2005
 * @param <T>
 */
public interface DBCursor<T extends DBValue>
{
  /**
   * Moves the cursor to the first record of the database, and returns it.<br>
   * If the first record is not exist, i.e. database is empty, then <code>null</code> will be
   * returned.<br>
   * If this method fails for any reason, the position of the cursor will be unchanged.
   * 
   * @return first recored of the database or <code>null</code> if database is empty.
   * @throws DBException if a database failure occurs.
   */
  public DBRecord<T> getFirst() throws DBException;

  /**
   * Moves the cursor to the last record of the database, and returns it.<br>
   * If the last record is not exist, i.e. database is empty, then <code>null</code> will be
   * returned.<br>
   * If this method fails for any reason, the position of the cursor will be unchanged.
   * 
   * @return last recored of the database or <code>null</code> if database is empty.
   * @throws DBException if a database failure occurs.
   */
  DBRecord<T> getLast() throws DBException;

  /**
   * Moves the cursor to the next record of the database, and returns it.<br>
   * If the next record is a duplicate for the current record then the cursor moves to this next
   * duplicate record and returns is. If there is no next record in the database, i.e. current
   * record is the last one then <code>null</code> will be returned.<br>
   * If this method fails for any reason, the position of the cursor will be unchanged.
   * 
   * @return next record of the database even if it is duplicate with currect record or
   *         <code>null</code> if currect record is last one.
   * @throws DBException if a database failure occurs.
   */
  public DBRecord<T> getNext() throws DBException;

  /**
   * Moves the cursor to the previous record of the database, and returns it.<br>
   * If the previous record is a duplicate for the current record then the cursor moves to this
   * previous duplicate record and returns is. If there is no previous record in the database, i.e.
   * current record is the first one then <code>null</code> will be returned.<br>
   * If this method fails for any reason, the position of the cursor will be unchanged.
   * 
   * @return previous record of the database even if it is duplicate with currect record or
   *         <code>null</code> if current record is first one.
   * @throws DBException if a database failure occurs.
   */
  public DBRecord<T> getPrev() throws DBException;

  /**
   * Closes the cursor.<br>
   * The cursor handle may not be used again after this method has been called, regardless of the
   * method's success or failure.
   * 
   * @throws DBException if a database failure occurs.
   */
  public void close() throws DBException;
}
