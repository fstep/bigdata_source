/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.db;

import java.util.List;

import org.apache.commons.lang.NullArgumentException;

/**
 * Interface of database that can store key-value pairs with or without duplicates keys.
 * 
 * @author Poroshin V.
 * @param < T > type of values stored in database.
 * @uml.dependency supplier="com.porva.crawler.db.DBCursor" stereotypes="Basic::Create"
 * @uml.dependency supplier="com.porva.crawler.db.DBFunctor" stereotypes="Basic::Call"
 */
public interface DB<T extends DBValue>
{
  /**
   * Opens database. If it does not exist it will be created. <br>
   * If you try to open already opened database then an old link to already opened database will be
   * returned.
   * 
   * @throws DBException if a database failure occurs.
   */
  public void open() throws DBException;

  /**
   * Closes <b>opened</b> database.
   * 
   * @throws DBException if a database failure occurs.
   * @throws IllegalStateException if database is not opened or it is already closed.
   */
  public void close() throws DBException;

  /**
   * Stores the key-value pair into the database.<br>
   * If the key already appears in the database and duplicates are not configured, the existing
   * key/data pair will be replaced. If the key already appears in the database and sorted
   * duplicates are configured, the new data value is inserted at the correct sorted location.
   * 
   * @param key key string to store into the database.
   * @param value {@link T} object to store into the database.
   * @return status of operation.
   * @throws NullArgumentException if <code>key</code> or <code>value</code> is
   *           <code>null</code>.
   * @throws IllegalStateException if database is not opened or it is already closed.
   */
  public DBOperationStatus put(String key, T value);

  /**
   * Stores the key-value pair into the database.<br>
   * This method is the same as {@link #put(String, DBValue)}.
   * 
   * @param record key-value pair to store.
   * @return status of operation.
   * @throws NullArgumentException if <code>record</code> is <code>null</code>.
   */
  public DBOperationStatus put(DBRecord<T> record);

  /**
   * Stores the key-value pair into the database.<br>
   * Whether or not duplicates are confiured if key already appears in the database this method will
   * fail and returns {@link DBOperationStatus#KEYEXIST}. Otherwise it will store given key-value
   * pair and return {@link DBOperationStatus#SUCCESS}.
   * 
   * @param key key string to store into the database.
   * @param value {@link T} object to store into the database.
   * @return {@link DBOperationStatus} status of operation.
   * @throws NullArgumentException if <code>key</code> or <code>value</code> is
   *           <code>null</code>.
   * @throws IllegalStateException if database is not opened or it is already closed.
   */
  public DBOperationStatus putNoOverwrite(String key, T value);

  /**
   * Stores the key-value pair into the database.<br>
   * This method is the same as {@link #putNoOverwrite(String, DBValue)}.
   * 
   * @param record key-value pair to store.
   * @return status of operation.
   * @throws NullArgumentException if <code>record</code> is <code>null</code>.
   */
  public DBOperationStatus putNoOverwrite(DBRecord<T> record);

  /**
   * Returns {@link T} object of the given key.<br>
   * If the matching key has duplicate values, the first data item in the set of duplicates is
   * returned.
   * 
   * @param key not <code>null</code> key to retrieve value from database.
   * @return {@link T} object retrieved from database for given key or <code>null</code> if there
   *         is no such record in database or some error occured.
   * @throws NullArgumentException if <code>key</code> is <code>null</code>.
   * @throws IllegalStateException if database is not opened or it is already closed.
   */
  public T get(String key);

  /**
   * Populates <code>listForValues</code> by all {@link T} values in database for given key.<br>
   * If duplicates are configured then all values for given key will be returned. If duplicates are
   * not configured then only one value for given key will be returned if it exists.
   * 
   * @param key not <code>null</code> key to retrieve all its values from database.
   * @param listForValues list to store found valus.
   * @return {@link DBOperationStatus} status of operation: {@link DBOperationStatus#SUCCESS} if at
   *         least one value is found; {@link DBOperationStatus#NOTFOUND} if no values found;
   *         {@link DBOperationStatus#ERROR} if a failure occurs.
   * @throws NullArgumentException if <code>key</code> or <code>listForValues</code> is
   *           <code>null</code>.
   * @throws IllegalStateException if database is not opened or it is already closed.
   */
  public DBOperationStatus getAllDup(String key, List<T> listForValues);
  
  /**
   * Returns opened status of database.
   * 
   * @return <code>true</code> if database is opened; <code>false</code> otherwise.
   */
  public boolean isOpened();

  /**
   * Deletes key-value pair from opened database.
   * 
   * @param key
   * @param value
   * @return status of operation.
   * @throws NullArgumentException if <code>key</code> or <code>value</code> is
   *           <code>null</code>.
   * @throws IllegalStateException if database is not opened or it is already closed.
   */
  public DBOperationStatus delete(String key, T value);
  
  /**
   * Deletes all key-value pairs (or one pair if dublicate record are not configured) 
   * for specified <code>key</code> if specified <code>value</code> is equal to the values
   * of records.<br>Equality is check by <code>value.equals()</code> method.
   * 
   * @param key key to search database record.
   * @param value value to check against records values.
   * @return status of the operation.
   * @throws NullArgumentException if <code>key</code> is <code>null</code>.
   */
  public DBOperationStatus deleteByEquals(String key, T value);
  
  /**
   * Deletes all records with this given <code>key</code>.
   * 
   * @param key
   * @return status of operation.
   * @throws NullArgumentException if <code>key</code> is <code>null</code>.
   */
  public DBOperationStatus delete(String key);

  /**
   * Deletes key-value pair from opened database.
   * 
   * @param record to delete.
   * @return status of operation.
   * @throws NullArgumentException if <code>record</code> is <code>null</code>.
   */
  public DBOperationStatus delete(DBRecord<T> record);

  /**
   * Iterates over all tuples in database and performs <code>functor.process(key, value)</code>
   * for each found key-value pair.
   * 
   * @param functor function object to process key-value pairs.
   * @throws NullArgumentException if functor is <code>null</code>.
   * @throws IllegalStateException if database is not opened or it is already closed.
   */
  public void iterateOverAll(DBFunctor<T> functor);

  /**
   * Returns number of tuples (key-value pairs) in the database.
   * 
   * @return number of tuples (key-value pairs) in the database.
   * @throws IllegalStateException if database is not opened or it is already closed.
   */
  public int getRecordsNum();

  /**
   * Returns name of database.
   * 
   * @return name of database.
   */
  public String getDBName();

  /**
   * Returns database cursor.
   * 
   * @return database cursor.
   * @throws DBException if a database failure occurs.
   * @throws IllegalStateException if database is not opened or it is already closed.
   */
  public DBCursor<T> getCursor() throws DBException;

}
