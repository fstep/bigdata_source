/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.db.berkley;

import java.io.File;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.crawler.db.DB;
import com.porva.crawler.db.DBException;
import com.porva.crawler.db.DBFactory;
import com.porva.crawler.db.DBValue;
import com.porva.util.FioUtils;
import com.sleepycat.bind.tuple.TupleBinding;
import com.sleepycat.je.CheckpointConfig;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;

/**
 * Implementation of {@link DBFactory} with berkley je library.
 *
 * @author Poroshin V.
 * @date Nov 3, 2005
 */
public class BerkleyDBFactory implements DBFactory
{
  private Environment enviroment = null; // one db enviroment for all databases

  private File envHomeDir;

  private static Log logger = LogFactory.getLog(BerkleyDBFactory.class);

  /**
   * Constructs new berkley database factory with specified home directory.
   * 
   * @param isAllowCreate <code>true</code> to allow create databases; <code>false</code>
   *          otherwise.
   * @param envHomeDir the database environment's home directory.
   * @param isEnvTransactional <code>true</code> to create transactional enviroment;
   *          <code>false</code> otherwise.
   * @throws DBException if a failure occurs during opening of enviroment or <code>envHomeDir</code>
   *           cannot be located or created.
   * @throws NullArgumentException if <code>envHomeDir</code> is <code>null</code>.
   */
  public BerkleyDBFactory(boolean isAllowCreate, final File envHomeDir, boolean isEnvTransactional)
      throws DBException
  {
    this.envHomeDir = envHomeDir;
    if (this.envHomeDir == null)
      throw new NullArgumentException("envHomeDir");

    if (!envHomeDir.exists() && !envHomeDir.mkdirs())
      throw new DBException("Cannot create nessesary directories for enviroment: " + envHomeDir);

    EnvironmentConfig environmentConfig = new EnvironmentConfig();
    environmentConfig.setTransactional(isEnvTransactional);
    environmentConfig.setAllowCreate(isAllowCreate);
    // environmentConfig.setCacheSize(10000000);

    try {
      enviroment = new Environment(envHomeDir, environmentConfig);
    } catch (DatabaseException e) {
      throw new DBException(e);
    }
  }

  /**
   * Closes {@link Environment} object.<br>
   * Once environment is closed, regardless of whether or not it throws an exception, all databases
   * produced by will not be accessable. The Environment handle should not be closed while any other
   * handle that refers to it is not yet closed.
   * 
   * @throws DBException if a failure occurs.
   * @throws IllegalStateException if <code>enviromnt</code> is <code>null</code>, i.e. it is
   *           already closed or was not ever opened.
   */
  public void close() throws DBException
  {
    if (isClosed())
      throw new IllegalStateException("databaseEnviroment is alredy closed.");

    try {
      optimize();
      enviroment.close();
      enviroment = null;
    } catch (DatabaseException e) {
      throw new DBException(e);
    }
  }
  
  /**
   * Optimize enviroment.
   * 
   * @throws DatabaseException if some database exception occures.
   * @see {@link Environment#cleanLog()}
   */
  private void optimize() throws DatabaseException
  {
    if (logger.isDebugEnabled())
      logger.debug("Database enviroment is optimizing ... ");
    int cleaned = 0;
    while (enviroment.cleanLog() > 0)
      cleaned++;
    if (logger.isDebugEnabled())
      logger.debug("There were " + cleaned + " logs cleaned.");
    if (cleaned != 0) {
      CheckpointConfig force = new CheckpointConfig();
      force.setForce(true);
      enviroment.checkpoint(force);
    }
    enviroment.compress();
  }

  /**
   * Deletes <b>closed</b> factory by deleting all its files and home directory.
   * 
   * @return <code>true</code> if factory was successfully deleted; <code>false</code>
   *         otherwise.
   * @throws IllegalStateException if <code>enviroment</code> is not closed.
   */
  public boolean removeAll()
  {
    if (!isClosed())
      throw new IllegalStateException("Enviroment is not close. Close it before delting.");

    return FioUtils.deleteDir(envHomeDir);
  }

  /**
   * Removes database of given dbName. Applications should never remove databases with opened
   * Database handles.
   * 
   * @param dbName type of database to remove.
   * @throws DBException if a failure occurs.
   * @throws NullArgumentException if dbType is <code>null</code>.
   * @throws IllegalStateException if <code>enviroment</code> is <code>null</code>, i.e.
   *           enviroment is not opened or it is closed.
   */
  public void removeDatabase(String dbName) throws DBException
  {
    if (dbName == null)
      throw new NullArgumentException("dbType");
    if (isClosed())
      throw new IllegalStateException("Cannot remove database from closed enviroment.");

    try {
      enviroment.removeDatabase(null, dbName);
    } catch (Exception e) {
      // may be also a LockTimeOutException exceptions that means
      // that there are unclosed databases in this enviroment.
      throw new DBException(e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.AbstractDBFactory#newGenericDatabaseHandle(boolean)
   */
  public DB<DBValue> newGenericDatabaseHandle(boolean isStoreDup, TupleBinding tupleBinder)
  {
    checkFactoryNotClosed();
    DatabaseConfig dbConfig = makeDatabaseConfig();
    dbConfig.setSortedDuplicates(isStoreDup);
    return new BerkleyDBImpl<DBValue>("Generic", tupleBinder, enviroment, dbConfig);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.AbstractDBFactory#isClosed()
   */
  public boolean isClosed()
  {
    return enviroment == null;
  }

  protected void checkFactoryNotClosed()
  {
    if (isClosed()) {
      logger.fatal("Database enviroment is null! Open/create it before using any database.");
      throw new IllegalStateException(
          "Database enviroment is null! Open/create it before using any database.");
    }
  }

  protected DatabaseConfig makeDatabaseConfig()
  {
    DatabaseConfig dbConfig = new DatabaseConfig();
    dbConfig.setAllowCreate(true);
    dbConfig.setTransactional(true);
    return dbConfig;
  }

  protected Environment getEnvironment()
  {
    return enviroment;
  }

}
