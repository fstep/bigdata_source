/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.db;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Default implementaion of databse record that simply wraps key-value pair.
 * 
 * @author Poroshin V.
 * @date Sep 25, 2005
 * @param <T>
 */
public class DefaulDBRecord<T extends DBValue> implements DBRecord<T>
{
  private String key;

  private T value;

  /**
   * Allocates a new {@link DefaulDBRecord} object with specified <code>key</code> and
   * <code>value</code>.
   * 
   * @param key record's key.
   * @param value record's value.
   * @throws NullArgumentException if <code>key</code> or <code>value</code> is <code>null</code>.
   */
  public DefaulDBRecord(String key, T value)
  {
    this.key = key;
    this.value = value;
    if (this.key == null)
      throw new NullArgumentException("key");
    if (this.value == null)
      throw new NullArgumentException("value");
  }

  /* (non-Javadoc)
   * @see com.porva.crawler.db.DBRecord#getDBKey()
   */
  public String getDBKey()
  {
    return key;
  }

  /* (non-Javadoc)
   * @see com.porva.crawler.db.DBRecord#getDBValue()
   */
  public T getDBValue()
  {
    return value;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#equals(java.lang.Object)
   */
  public boolean equals(final Object other)
  {
    if (!(other instanceof DefaulDBRecord))
      return false;
    DefaulDBRecord castOther = (DefaulDBRecord) other;
    return new EqualsBuilder().append(key, castOther.key).append(value, castOther.value).isEquals();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("key", key)
        .append("value", value).toString();
  }

}
