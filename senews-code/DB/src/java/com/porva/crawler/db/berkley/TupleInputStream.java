/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.db.berkley;

import java.util.Map;

import com.porva.util.GZIPUtils;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.db.DBInputStream;
import com.sleepycat.bind.tuple.TupleInput;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Wrapper of {@link TupleInput} to implement {@link DBInputStream} interface.
 * 
 * @author Poroshin V.
 * @date Sep 25, 2005
 */
class TupleInputStream implements DBInputStream
{
  private TupleInput tupleInput;

  /**
   * Creates a new {@link TupleInputStream} by wrapping <code>tupleInput</code>.
   * 
   * @param tupleInput <code>tupleInput</code> to wrap.
   */
  public TupleInputStream(TupleInput tupleInput)
  {
    this.tupleInput = tupleInput;
    if (this.tupleInput == null)
      throw new NullArgumentException("tupleInput");
  }

  public char readChar()
  {
    return tupleInput.readChar();
  }

  public boolean readBoolean()
  {
    return tupleInput.readBoolean();
  }

  public byte readByte()
  {
    return tupleInput.readByte();
  }

  public int readInt()
  {
    return tupleInput.readInt();
  }

  public long readLong()
  {
    return tupleInput.readLong();
  }

  public float readFloat()
  {
    return tupleInput.readFloat();
  }

  public double readDouble()
  {
    return tupleInput.readDouble();
  }

  // /////////////////////////////////////////////////// reading more complex objects

  public byte[] readBytes()
  {
    byte[] buff = null;
    if (tupleInput.readBoolean()) {
      buff = new byte[tupleInput.readInt()];
      tupleInput.readFast(buff);
    }
    return buff;
  }

  public byte[] readUncompressBytes()
  {
    byte[] buff = null;
    if (tupleInput.readBoolean()) {
      buff = new byte[tupleInput.readInt()];
      tupleInput.readFast(buff);
      buff = GZIPUtils.unzipBestEffort(buff); // todo change GZIPUtils to non-static class -- avoid
      // synch
    }
    return buff;
  }

  public String readString()
  {
    return tupleInput.readString();
  }

  public void readMapOfStrings(final Map<String, String> mapBuffer)
  {
    if (mapBuffer == null)
      throw new NullArgumentException("mapBuffer");

    if (tupleInput.readBoolean()) {
      int length = tupleInput.readInt();
      for (int i = 0; i < length; i++) {
        String key = tupleInput.readString();
        String value = tupleInput.readString();
        mapBuffer.put(key, value);
      }
    }
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("tupleInput",
                                                                              tupleInput)
        .toString();
  }
  
  
}
