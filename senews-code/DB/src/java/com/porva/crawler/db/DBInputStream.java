/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.db;

import java.util.Map;

import org.apache.commons.lang.NullArgumentException;

/**
 * Input stream that allows to read primitives as well as some complex objects from the database
 * tuple. 
 * 
 * @author Poroshin V.
 * @date Sep 25, 2005
 */
public interface DBInputStream
{
  ///////////////////////////////////////////////////// reading primitives
  
  /**
   * Reads values that were written using {@link DBOutputStream#writeChar(char)}.
   * 
   * @return readed char.
   */
  public char readChar();

  /**
   * Reads values that were written using {@link DBOutputStream#writeBoolean(boolean)}.
   * 
   * @return readed boolean.
   */
  public boolean readBoolean();

  /**
   * Reads values that were written using {@link DBOutputStream#writeByte(byte)}.
   * 
   * @return readed byte.
   */
  public byte readByte();

  /**
   * Reads values that were written using {@link DBOutputStream#writeInt(int)}.
   * 
   * @return readed int.
   */
  public int readInt();

  /**
   * Reads values that were written using {@link DBOutputStream#writeLong(long)}.
   * 
   * @return readed long.
   */
  public long readLong();

  /**
   * Reads values that were written using {@link DBOutputStream#writeFloat(float)}.
   * 
   * @return readed float.
   */
  public float readFloat();

  /**
   * Reads values that were written using {@link DBOutputStream#writeDouble(double)}.
   * 
   * @return readed double.
   */
  public double readDouble();
  
  ///////////////////////////////////////////////////// reading more complex objects

  /**
   * Reads values that were written using {@link DBOutputStream#writeBytes(byte[])}.
   * 
   * @return readed bytes or <code>null</code> if <code>null</code> was written.
   */
  public byte[] readBytes();
  
  /**
   * Reads values that were written using {@link DBOutputStream#writeString(String)}.
   * 
   * @return readed string.
   */
  public String readString();

  /**
   * Reads compressed bytes and returns them as uncompressed.<br>
   * This fuction reads bytes written by {@link DBOutputStream#writeCompressBytes(byte[])}. It
   * may return also <code>null</code> if <code>null</code> bytes was written by
   * {@link DBOutputStream#writeCompressBytes(byte[])}.
   * 
   * @return uncompressed readed bytes or <code>null</code> if <code>null</code> was written
   */
  public byte[] readUncompressBytes();

  /**
   * Reads key-value pairs and populates <code>mapByffer</code> by them.
   * 
   * @param mapBuffer map instance to add readed key-value pairs.
   * @throws NullArgumentException if <code>mapByffer</code> is <code>null</code>.
   */
  public void readMapOfStrings(Map<String, String> mapBuffer);

}
