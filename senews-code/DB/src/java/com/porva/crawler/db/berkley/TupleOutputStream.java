/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.db.berkley;

import java.util.Map;

import com.porva.util.GZIPUtils;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.db.DBOutputStream;
import com.sleepycat.bind.tuple.TupleOutput;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Wrapper of {@link TupleOutput} to implement {@link TupleOutputStream} interface.
 * 
 * @author Poroshin V.
 * @date Sep 25, 2005
 */
class TupleOutputStream implements DBOutputStream
{
  private TupleOutput tupleOutput;

  /**
   * Creates a new {@link TupleInputStream} by wrapping <code>tupleOutput</code>.
   * 
   * @param tupleOutput <code>tupleOutput</code> to wrap.
   */
  public TupleOutputStream(TupleOutput tupleOutput)
  {
    this.tupleOutput = tupleOutput;
    if (this.tupleOutput == null)
      throw new NullArgumentException("tupleOutput");
  }

  // ///////////////////////////////////////////// writing primitives

  public void writeChar(char val)
  {
    tupleOutput.writeChar(val);
  }

  public void writeBoolean(boolean val)
  {
    tupleOutput.writeBoolean(val);
  }

  public void writeByte(byte val)
  {
    tupleOutput.writeByte(val);
  }

  public void writeInt(int val)
  {
    tupleOutput.writeInt(val);
  }

  public void writeLong(long val)
  {
    tupleOutput.writeLong(val);
  }

  public void writeFloat(float val)
  {
    tupleOutput.writeFloat(val);
  }

  public void writeDouble(double val)
  {
    tupleOutput.writeDouble(val);
  }

  // //////////////////////////////////////////// writing more complex objects

  public void writeBytes(byte[] buff)
  {
    tupleOutput.writeBoolean(buff != null);
    if (buff != null) {
      tupleOutput.writeInt(buff.length);
      tupleOutput.writeFast(buff);
    }
  }

  public void writeCompressBytes(final byte[] buff)
  {
    tupleOutput.writeBoolean(buff != null);
    if (buff != null) {
      byte[] zippedContent = GZIPUtils.zip(buff);
      tupleOutput.writeInt(zippedContent.length);
      tupleOutput.writeFast(zippedContent);
    }
  }

  public void writeString(String val)
  {
    tupleOutput.writeString(val);
  }

  public void writeMapOfStrings(final Map<String, String> map)
  {
    tupleOutput.writeBoolean(map != null);
    if (map != null) {
      tupleOutput.writeInt(map.entrySet().size());
      for (Map.Entry<String, String> entry : map.entrySet()) {
        tupleOutput.writeString(new String(entry.getKey()));
        tupleOutput.writeString(new String(entry.getValue()));
      }
    }
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("tupleOutput",
                                                                              tupleOutput)
        .toString();
  }

  
}
