package com.porva.crawler.db;

import org.apache.commons.lang.NullArgumentException;

import com.sleepycat.je.OperationStatus;

import junit.framework.TestCase;

public class DBOperationStatusTest extends TestCase
{
  public void testERROR()
  {
    DBOperationStatus error = DBOperationStatus.ERROR(new Exception("test"));
    assertTrue(error == DBOperationStatus.ERROR);
    assertTrue(error == DBOperationStatus.ERROR(new Exception("new test")));
  }

  public void testGetErrorCause()
  {
    DBOperationStatus error = DBOperationStatus.ERROR(new Exception("test"));
    assertEquals("test", error.getErrorCause().getMessage());
    
    error = DBOperationStatus.ERROR(null);
    assertNull(error.getErrorCause());
  }

  public void testMap()
  {
    assertTrue(DBOperationStatus.getFittedInstance(OperationStatus.KEYEMPTY) == DBOperationStatus.KEYEMPTY);
    assertTrue(DBOperationStatus.getFittedInstance(OperationStatus.KEYEXIST) == DBOperationStatus.KEYEXIST);
    assertTrue(DBOperationStatus.getFittedInstance(OperationStatus.NOTFOUND) == DBOperationStatus.NOTFOUND);
    assertTrue(DBOperationStatus.getFittedInstance(OperationStatus.SUCCESS) == DBOperationStatus.SUCCESS);
    
    try {
      DBOperationStatus.getFittedInstance(null);
      fail();
    } catch (NullArgumentException e) {
      assertTrue(true);
    }
  }

}
