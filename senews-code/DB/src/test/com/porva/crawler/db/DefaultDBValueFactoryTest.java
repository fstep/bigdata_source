/*package com.porva.crawler.db;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.lang.NullArgumentException;

import junit.framework.TestCase;

public class DefaultDBValueFactoryTest extends TestCase
{

  DefaultDBValueFactory dbValueFactory = new DefaultDBValueFactory();
  
  protected void setUp() throws Exception
  {
    dbValueFactory.registerClass(TestDBValue.class);
  }
  
  
   * Test method for 'com.porva.crawler.db.DefaultDBValueFactory.newDBValue(DBInputStream, long)'
   
  public void testNewDBValue() throws Exception
  {
    try {
      dbValueFactory.newDBValue(null, 12345);
      fail();
    } catch (NullArgumentException e) {
    }
  }

  
   * Test method for 'com.porva.crawler.db.DefaultDBValueFactory.readObject(DBInputStream)'
   
  public void testReadObject()
  {

  }

  
   * Test method for 'com.porva.crawler.db.DefaultDBValueFactory.newDBValueInstance(DBInputStream, Class)'
   
  public void testNewDBValueInstance()
  {

  }

  
   * Test method for 'com.porva.crawler.db.DefaultDBValueFactory.getClassCode(Class)'
   
  public void testGetClassCode()
  {

  }

  
   * Test method for 'com.porva.crawler.db.DefaultDBValueFactory.registerClass(Class)'
   
  public void testRegisterClass()
  {

  }
  
  /////////////////////////////////////////////////
  
  public static class TestDBValue implements DBValue
  {
    private String test;
    
    private TestDBValue() {}
    
    public TestDBValue(String test)
    {
      this.test = test;
    }

    public void writeTo(DBOutputStream tupleOutput)
    {
      tupleOutput.writeString(test);
    }

    public DBValue newInstance(DBInputStream tupleInput) throws DBException
    {
      return new TestDBValue(tupleInput.readString());
    }

    public long getClassCode()
    {
      return -13453245000132L;
    }
    
  }

}
*/