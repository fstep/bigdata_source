/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.html.simpledom;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.regex.Pattern;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * Class to render {@link SimpleDOM} model to other formats.
 * 
 * @author poroshin
 */
public class SimpleDOMRender
{

  private Stack<Node> stack = new Stack<org.w3c.dom.Node>();

  private final static Set<String> allowedTags = new HashSet<String>();

  /**
   * These tags will appear in a rendered xml. Other tags will be excluded.
   */
  static {
    allowedTags.add("H1");
    allowedTags.add("H2");
    allowedTags.add("H3");
    allowedTags.add("H4");
    allowedTags.add("H5");
    allowedTags.add("P");
    allowedTags.add("BR");
    allowedTags.add("TABLE");
    allowedTags.add("TD");
    allowedTags.add("TH");
    allowedTags.add("TR");
    allowedTags.add("BLOCKQUOTE");
    allowedTags.add("B");
    allowedTags.add("I");
    allowedTags.add("STRONG");
    allowedTags.add("UL");
    allowedTags.add("LI");

    allowedTags.add("A");
    allowedTags.add("IMG");
  }

  /**
   * Render a specified <code>model</code> as an xml.
   * 
   * @param model {@link SimpleDOM} model to render.
   * @param xmlNodeName top xml element name.
   * @param document w3c document to create elements.
   * @param start start position in the model.
   * @param end end position in the model.
   * @return an xml as its top w3c node.
   */
  public Node renderAsXML(final SimpleDOM model,
                          final String xmlNodeName,
                          final Document document,
                          int start,
                          int end)
  {
    Element el = document.createElement(xmlNodeName);
    stack.push(el);

    for (int i = start; i <= end; i++) {
      SimpleElement node = model.getElement(i);
      if (node.getName().equals("STYLE") || node.getName().equals("SCRIPT")) {
        i = model.getEndElementNum(i);
        continue;
      }

      switch (node.getType()) {
      case ELEMENT_START:
        if (allowedTags.contains(node.getName())) {
          Element xmlNode = document.createElement(node.getName());
          // if (node.getAttributes() != null)
          // for (SimpleAttribute attr : node.getAttributes())
          // xmlNode.setAttribute(attr.getName(), attr.getValue());
          if (node.getName().equalsIgnoreCase("a") || 
              node.getName().equalsIgnoreCase("img")) {
            if (node.getAttributes() != null)
              for (SimpleAttribute attr : node.getAttributes())
                xmlNode.setAttribute(attr.getName(), attr.getValue());
          }
          stack.push(xmlNode);
        } else {
          // This is the beginning of an uknown tag. Replace it with " " if it is not
          // a "a" tag
          if (!node.getName().equalsIgnoreCase("a"))
            stack.peek().appendChild(document.createTextNode(" "));
        }
        break;
      case ELEMENT_END:
        if (allowedTags.contains(node.getName()) && hasStartEl(node)) {
          Node xmlNode = null;
          List<Node> nodes = new ArrayList<Node>();
          do {
            xmlNode = stack.pop();
            nodes.add(xmlNode);
          } while (!xmlNode.getNodeName().equals(node.getName()));
          makeNode(nodes, stack);
        } else {
          // This is the end of an uknown tag. Replace it with " " if it is not
          // a "a" tag
          if (!node.getName().equalsIgnoreCase("a"))
            stack.peek().appendChild(document.createTextNode(" "));
        }
        break;
      case TEXT:
        if (node.getValue() != null) {
          stack.peek().appendChild(document.createTextNode(node.getValue()));
        }
        break;
      default:
        break;
      }
    }

    // while (stack.size() >= 2) {
    // stack.peek().appendChild(stack.pop());
    // }
    if (stack.size() >= 2) {
      for (int i = end + 1; i < model.size(); i++) {
        if (stack.size() == 1)
          break;

        SimpleElement node = model.getElement(i);
        switch (node.getType()) {
        case ELEMENT_START:
          if (allowedTags.contains(node.getName())) {
            Element xmlNode = document.createElement(node.getName());
            if (node.getAttributes() != null)
              for (SimpleAttribute attr : node.getAttributes())
                xmlNode.setAttribute(attr.getName(), attr.getValue());
            stack.push(xmlNode);
          }
          break;
        case ELEMENT_END:
          if (allowedTags.contains(node.getName()) && hasStartEl(node)) {
            Node xmlNode = null;
            List<Node> nodes = new ArrayList<Node>();
            do {
              xmlNode = stack.pop();
              nodes.add(xmlNode);
            } while (!xmlNode.getNodeName().equals(node.getName()));
            makeNode(nodes, stack);
          }
          break;
        }
      }
    }

    return el;
  }

  private boolean hasStartEl(SimpleElement node)
  {
    for (Node w3node : stack) {
      if (w3node.getNodeName().equals(node.getName()))
        return true;
    }
    return false;
  }

  public Node renderAsXML(final SimpleDOM model, final String xmlNodeName, final Document document)
  {
    return renderAsXML(model, xmlNodeName, document, 0, model.size() - 1);
  }

  private void makeNode(List<org.w3c.dom.Node> nodes, Stack<org.w3c.dom.Node> stack)
  {
    org.w3c.dom.Node prev = stack.peek();
    prev.appendChild(nodes.get(nodes.size() - 1));
  }

  // ////////////////////////////////////////////////////////////////////
  /**
   * Render a specified <code>model</code> as a visible text.
   * 
   * @param model {@link SimpleDOM} model to render.
   * @param start start position in the model.
   * @param end end position in the model.
   * @return a rendered <code>model</code> as a text.
   */
  public String renderAsVisibleText(final SimpleDOM model, int start, int end)
  {
    StringBuffer sb = new StringBuffer();
    for (int i = start; i <= end; i++) {
      SimpleElement node = model.getElement(i);
      if (node.getName().equals("STYLE") || node.getName().equals("SCRIPT")) {
        i = model.getEndElementNum(i);
        continue;
      }

      switch (node.getType()) {
      case ELEMENT_START:
        renderElementStart(node, sb);
        break;
      case ELEMENT_END:
        renderElementEnd(node, sb);
        break;
      case TEXT:
        renderText(node, sb);
      default:
        break;
      }
    }

    String res = trim(sb.toString()).trim();
    res = Pattern.compile("[ \\t]+\n", Pattern.MULTILINE).matcher(res).replaceAll("\n");
    return res;
  }

  public String renderAsVisibleText(final SimpleDOM model)
  {
    return renderAsVisibleText(model, 0, model.size() - 1);
  }

  private String trim(String str)
  {
    int i = 0;
    for (; i < str.length(); i++) {
      char ch = str.charAt(i);
      if (ch != 160 && ch > 32)
        break;
    }

    int j = str.length();
    for (; j > str.length(); j--) {
      char ch = str.charAt(j);
      if (ch != 160 && ch > 32)
        break;
    }

    return str.substring(i, j);
  }

  private void renderText(final SimpleElement node, final StringBuffer sb)
  {
    String text = node.getValue();
    if (text != null) {
      text = text.replace("\n", " ");
      sb.append(text);
    }
  }

  private void renderElementEnd(final SimpleElement node, final StringBuffer sb)
  {
    if (node.getName().matches("H\\d+"))
      sb.append("\n");
    else if (node.getName().equals("TD"))
      sb.append("\t");
    else if (node.getName().equals("TH"))
      sb.append("\t");
    else if (node.getName().equals("TABLE"))
      sb.append("\n");
  }

  private void renderElementStart(final SimpleElement node, final StringBuffer sb)
  {
    if (node.getName().equals("H1"))
      sb.append("\n\n\n\n\t");
    else if (node.getName().equals("H2"))
      sb.append("\n\n\n\t\t");
    else if (node.getName().equals("H3"))
      sb.append("\n\n\t\t\t");
    else if (node.getName().equals("H4"))
      sb.append("\n\t\t\t\t");
    else if (node.getName().equals("BR"))
      sb.append("\n");
    else if (node.getName().equals("P"))
      sb.append("\n\t");
    else if (node.getName().equals("UL"))
      sb.append("\n");
    else if (node.getName().equals("LI"))
      sb.append("\n\t*\t");
    else if (node.getName().equals("TR"))
      sb.append("\n");
    else if (node.getName().equals("TABLE"))
      sb.append("\n");
  }
}
