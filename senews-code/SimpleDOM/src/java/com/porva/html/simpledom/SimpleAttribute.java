/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.html.simpledom;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Representation of an attribute of some {@link com.porva.html.simpledom.SimpleElement} element.
 * 
 * @author Poroshin V.
 * @date Feb 19, 2006
 */
public class SimpleAttribute
{
  private final String name;

  private final String value;

  /**
   * Allocates a new attribute.
   * 
   * @param name name of attribute.
   * @param value value of attribute.
   */
  SimpleAttribute(final String name, final String value)
  {
    this.name = name;
    this.value = value;
    if (this.name == null)
      throw new NullArgumentException("name");
    if (this.value == null)
      throw new NullArgumentException("value");
  }

  /**
   * Returns name of this attribute.
   * 
   * @return name of this attribute.
   */
  public String getName()
  {
    return name;
  }

  /**
   * Returns value of this attribute.
   * 
   * @return value of this attribute.
   */
  public String getValue()
  {
    return value;
  }

  private transient String toString;

  public String toString()
  {
    if (toString == null) {
      toString = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("name", name)
          .append("value", value).toString();
    }
    return toString;
  }
}
