/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.html.simpledom;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.helpers.DefaultHandler;

public class SimpleDOMParser
{
  protected static final String LEXICAL_HANDLER_PROPERTY_ID = "http://xml.org/sax/properties/lexical-handler";
  
  /**
   * @param htmlContentReader
   * @throws SAXException
   * @throws IOException if <code>htmlContentReader</code> is closed or other IO Exception occurs.
   * @throws NullArgumentException if <code>htmlContentReader</code> is <code>null</code>.
   */
  public SimpleDOM parse(final Reader htmlContentReader) throws SAXException, IOException
  {
    if (htmlContentReader == null)
      throw new NullArgumentException("htmlContentReader");
    
    List<SimpleElement> nodes = new ArrayList<SimpleElement>();
    HTMLSAXHandler handler = new HTMLSAXHandler(nodes);
    HTMLSAXParser saxParser = new HTMLSAXParser();
    saxParser.setContentHandler(handler);
    saxParser.setProperty(LEXICAL_HANDLER_PROPERTY_ID, handler);
    saxParser.parse(new InputSource(htmlContentReader));
    
    return new SimpleDOM(nodes);
  }

  private static class HTMLSAXHandler extends DefaultHandler implements LexicalHandler
  {

    private final List<SimpleElement> nodes;
    
    private boolean isPrevEmptyText = false;

    public HTMLSAXHandler(final List<SimpleElement> nodes)
    {
      this.nodes = nodes;
      if (this.nodes == null)
        throw new NullArgumentException("nodes");
    }

    // SAX DocumentHandler methods

    public void startDocument() throws SAXException
    {
    }

    public void endDocument() throws SAXException
    {
    }

    public void startElement(String namespaceURI, String lName, // local name
                             String qName, // qualified name
                             Attributes attrs) throws SAXException
    {
      String eName = lName; // element name
      if ("".equals(eName))
        eName = qName; // namespaceAware = false

      SimpleElement node = new SimpleElement(eName, SimpleElement.Type.ELEMENT_START, null);
      nodes.add(node);
      if (attrs != null) {
        for (int i = 0; i < attrs.getLength(); i++) {
          String aName = attrs.getLocalName(i); // Attr name
          if ("".equals(aName))
            aName = attrs.getQName(i);
          node.addAttribute(aName, attrs.getValue(i));
        }
      }
    }

    public void endElement(String namespaceURI, String sName, // simple name
                           String qName // qualified name
    ) throws SAXException
    {
      nodes.add(new SimpleElement(sName.toUpperCase(), SimpleElement.Type.ELEMENT_END, null));
    }

    public void characters(char buf[], int offset, int len) throws SAXException
    {
      String s = new String(buf, offset, len);
      
      boolean isEmpty = s.matches("^\\s*$");
      if (isPrevEmptyText && isEmpty)
      	return;
      if (isEmpty)
      	isPrevEmptyText = true;
      else 
      	isPrevEmptyText = false;
      
      nodes.add(new SimpleElement(SimpleDOM.TEXT_NAME, SimpleElement.Type.TEXT, s));
    }

    // LexicalHandler interface

    public void comment(char[] ch, int start, int length) throws SAXException
    {
      String s = new String(ch, start, length);
      nodes.add(new SimpleElement(SimpleDOM.COMMENT_NAME, SimpleElement.Type.COMMENT, s));
    }

    public void startDTD(String name, String publicId, String systemId) throws SAXException
    {
    }

    public void endDTD() throws SAXException
    {
    }

    public void startEntity(String name) throws SAXException
    {
    }

    public void endEntity(String name) throws SAXException
    {
    }

    public void startCDATA() throws SAXException
    {
    }

    public void endCDATA() throws SAXException
    {
    }
  }

}
