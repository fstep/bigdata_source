/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.html.simpledom;

import java.util.List;

import org.apache.commons.lang.NullArgumentException;

/**
 * Simple object model for html/xml like documents.
 * 
 * @author Poroshin V.
 * @date Feb 19, 2006
 */
public class SimpleDOM
{
  /**
   * Name of any comment element {@link SimpleElement}.
   */
  public final static String COMMENT_NAME = "#comment";

  /**
   * Name of any text element {@link SimpleElement}.
   */
  public final static String TEXT_NAME = "#text";

  private List<SimpleElement> nodes;

  /**
   * @param nodes
   * @throws NullArgumentException if <code>nodes</code> is <code>null</code>.
   */
  public SimpleDOM(List<SimpleElement> nodes)
  {
    this.nodes = nodes;
    if (this.nodes == null)
      throw new NullArgumentException("nodes");
  }

  public SimpleElement getElement(int num)
  {
    return nodes.get(num);
  }

  /**
   * Returns number of nodes in this model.
   * 
   * @return number of nodes.
   */
  public int size()
  {
    return nodes.size();
  }

  public int getEndElementNum(int startNodeNum)
  {
    SimpleElement startNode = nodes.get(startNodeNum);
    if (startNode.getType() == SimpleElement.Type.COMMENT
        || startNode.getType() == SimpleElement.Type.TEXT)
      return startNodeNum;

    int depth = 0;
    for (int i = startNodeNum + 1; i < nodes.size(); i++) {
      if (nodes.get(i).getName().equals(startNode.getName()))
        if (nodes.get(i).getType() == SimpleElement.Type.ELEMENT_START) {
          depth++;
        } else if (nodes.get(i).getType() == SimpleElement.Type.ELEMENT_END) {
          if (depth == 0)
            return i;
          else
            depth--;
        }
    }
    return -1;
  }

  // todo remove this i.e. it generates invalid html ???
  @Deprecated
  public String toHTMLString()
  {
    StringBuffer sb = new StringBuffer();
    int depth = 0;
    for (SimpleElement node : nodes) {

      switch (node.getType()) {
      case ELEMENT_START:
        writeTabs(depth++, sb);
        sb.append("<").append(node.getName()).append(writeAttrs(node)).append(">\n");
        break;
      case ELEMENT_END:
        writeTabs(--depth, sb);
        sb.append("</").append(node.getName()).append(">\n");
        break;
      case TEXT:
        writeTabs(depth, sb);
        sb.append(node.getValue()).append("\n");
        break;
      case COMMENT:
        sb.append("<!--").append(node.getValue()).append("-->");
        break;
      default:
        break;
      }
    }
    return sb.toString();
  }

  private void writeTabs(int depth, final StringBuffer sb)
  {
    for (int i = 0; i < depth; i++)
      sb.append("\t");
  }

  private String writeAttrs(final SimpleElement node)
  {
    StringBuffer sb = new StringBuffer();
    if (node.getAttributes() == null)
      return "";
    for (SimpleAttribute attr : node.getAttributes())
      sb.append(" ").append(attr.getName()).append("=\"").append(attr.getValue()).append("\"");
    return sb.toString();
  }

}
