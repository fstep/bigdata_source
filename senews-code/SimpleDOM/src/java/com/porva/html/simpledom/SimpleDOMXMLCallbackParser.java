//package com.porva.html.simpledom;
//
//import java.io.IOException;
//import java.io.Reader;
//import java.util.ArrayList;
//import java.util.List;
//
//import org.apache.commons.lang.NullArgumentException;
//import org.xml.sax.Attributes;
//import org.xml.sax.InputSource;
//import org.xml.sax.SAXException;
//import org.xml.sax.XMLReader;
//import org.xml.sax.ext.LexicalHandler;
//import org.xml.sax.helpers.DefaultHandler;
//import org.xml.sax.helpers.XMLReaderFactory;
//
//public class SimpleDOMXMLCallbackParser
//{
//  protected static final String LEXICAL_HANDLER_PROPERTY_ID = "http://xml.org/sax/properties/lexical-handler";
//
//  public void parse(final Reader htmlContentReader, ParserWatcher watcher) throws SAXException,
//      IOException
//  {
//    if (htmlContentReader == null)
//      throw new NullArgumentException("htmlContentReader");
//    if (watcher == null)
//      throw new NullArgumentException("watcher");
//
//    XMLReader parser = XMLReaderFactory.createXMLReader();
//    parser.setContentHandler(new CallbackHandler(watcher));
//    // parser.setProperty(LEXICAL_HANDLER_PROPERTY_ID, handler);
//    parser.parse(new InputSource(htmlContentReader));
//  }
//
//  // //////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  private static class CallbackHandler extends DefaultHandler implements LexicalHandler
//  {
//
//    private final List<SimpleElement> nodes = new ArrayList<SimpleElement>();
//
//    private ParserWatcher watcher;
//
//    public CallbackHandler(ParserWatcher watcher)
//    {
//      this.watcher = watcher;
//      if (this.watcher == null)
//        throw new NullArgumentException("watcher");
//    }
//
//    // SAX DocumentHandler methods
//
//    public void startDocument() throws SAXException
//    {
//    }
//
//    public void endDocument() throws SAXException
//    {
//    }
//
//    public void startElement(String namespaceURI, String lName, String qName, Attributes attrs)
//        throws SAXException
//    {
//      String eName = lName;
//      if ("".equals(eName))
//        eName = qName;
//      
//      SimpleElement node = new SimpleElement(eName, SimpleElement.Type.ELEMENT_START, null);
//      nodes.add(node);
//      if (attrs != null) {
//        for (int i = 0; i < attrs.getLength(); i++) {
//          String aName = attrs.getLocalName(i); // Attr name
//          if ("".equals(aName))
//            aName = attrs.getQName(i);
//          node.addAttribute(aName, attrs.getValue(i));
//        }
//      }
//      
//      if (watcher.isCallback(node))
//        watcher.callback(nodes);
//    }
//
//    public void endElement(String namespaceURI, String sName, String qName) throws SAXException
//    {
//      SimpleElement node = new SimpleElement(sName, SimpleElement.Type.ELEMENT_END, null);
//      nodes.add(node);
//      
//      if (watcher.isCallback(node))
//        watcher.callback(nodes);
//    }
//
//    public void characters(char buf[], int offset, int len) throws SAXException
//    {
//      String s = new String(buf, offset, len);
//      if (s.trim().equals(""))
//        return;
//      
//      SimpleElement node = new SimpleElement(SimpleDOM.TEXT_NAME, SimpleElement.Type.TEXT, s); 
//      nodes.add(node);
//      
//      if (watcher.isCallback(node))
//        watcher.callback(nodes);
//    }
//
//    // LexicalHandler interface
//
//    public void comment(char[] ch, int start, int length) throws SAXException
//    {
//      String s = new String(ch, start, length);
//      SimpleElement node = new SimpleElement(SimpleDOM.COMMENT_NAME, SimpleElement.Type.COMMENT, s); 
//      nodes.add(node);
//
//      if (watcher.isCallback(node))
//        watcher.callback(nodes);
//    }
//
//    public void startDTD(String name, String publicId, String systemId) throws SAXException
//    {
//    }
//
//    public void endDTD() throws SAXException
//    {
//    }
//
//    public void startEntity(String name) throws SAXException
//    {
//    }
//
//    public void endEntity(String name) throws SAXException
//    {
//    }
//
//    public void startCDATA() throws SAXException
//    {
//    }
//
//    public void endCDATA() throws SAXException
//    {
//    }
//  }
//
//}
