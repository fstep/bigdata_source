/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.html.simpledom;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * Node for simple DOM model.
 * 
 * @author Poroshin V.
 * @date Feb 19, 2006
 */
public class SimpleElement
{
  /**
   * Type of element.
   * 
   * @author Poroshin V.
   * @date Feb 19, 2006
   */
  public static enum Type
  {
    /**
     * Open tag element.
     */
    ELEMENT_START,
    /**
     * Close tag element.
     */
    ELEMENT_END,
    /**
     * Text element.
     */
    TEXT,
    /**
     * Comment element.
     */
    COMMENT
  }

  private final String name;

  private final Type type;

  private final String value;

  private List<SimpleAttribute> attrs;

  /**
   * Allocates a new {@link SimpleElement} object.
   * 
   * @param name name of element.
   * @param type type of element.
   * @param value value of element. If <code>null</code> then this element does not have a value.
   * @throws NullArgumentException if <code>name</code> or <code>type</code> is
   *           <code>null</code>.
   */
  SimpleElement(final String name, final Type type, final String value)
  {
    this.name = name;
    this.type = type;
    this.value = value;
    check();
  }

  private void check()
  {
    if (name == null)
      throw new NullArgumentException("name");
    if (type == null)
      throw new NullArgumentException("type");

    if (type == Type.COMMENT && name != SimpleDOM.COMMENT_NAME)
      throw new IllegalArgumentException("comment element must have name " + SimpleDOM.COMMENT_NAME
          + ": " + name);
    if (type == Type.TEXT && name != SimpleDOM.TEXT_NAME)
      throw new IllegalArgumentException("test element must have name " + SimpleDOM.TEXT_NAME
          + ": " + name);
    if (type == Type.ELEMENT_END && attrs != null)
      throw new IllegalArgumentException("end element cannot have any attributes");
  }

  /**
   * Adds attribute to this element.
   * 
   * @param attrName name of attribute to add.
   * @param attrValue value of attribute to add.
   * @throws NullArgumentException if <code>attrName</code> or <code>attrValue</code> is
   *           <code>null</code>.
   */
  void addAttribute(final String attrName, final String attrValue)
  {
    if (attrName == null)
      throw new NullArgumentException("attrName");
    if (attrValue == null)
      throw new NullArgumentException("attrValue");
//    if (type != Type.ELEMENT_START)
//      throw new IllegalArgumentException("");

    if (attrs == null)
      attrs = new ArrayList<SimpleAttribute>();
    attrs.add(new SimpleAttribute(attrName, attrValue));
  }

  /**
   * Returns value of this element or <code>null</code> if this element does not have a value.<br>
   * Value for TEXT element is its text; value for COMMENT element is textual comment; value for
   * ELEMENT_START or ELEMENT_END is <code>null</code>.
   * 
   * @return value of this element or <code>null</code> if this element does not have a value.
   */
  public String getValue()
  {
    return value;
  }

  /**
   * Returns name of the element.<br>
   * Name for ELEMENT_START or ELEMENT_END is the name of the tag. Name for TEXT is #text. Name for
   * COMMENT is #comment.
   * 
   * @return Returns name of the element.
   */
  public String getName()
  {
    return name;
  }

  /**
   * Returns type of this element.
   * 
   * @return type of this element.
   */
  public Type getType()
  {
    return type;
  }

  /**
   * Returns list of {@link SimpleAttribute} attribute of this element or <code>null</code> if the
   * element does not have attributes.
   * 
   * @return list of attributes of this element or <code>null</code> if the element does not have
   *         attributes.
   */
  public List<SimpleAttribute> getAttributes()
  {
    return attrs;
  }

  /**
   * Returns {@link SimpleAttribute} attribute that has specified <code>attrName</code> or
   * <code>null</code> if such attribute does not exist for this element.
   * 
   * @param attrName name of attribute.
   * @param isCaseSensitive <code>true</code> to case sensitive match of the <code>name</code>;
   *          <code>false</code> to case insensitive match.
   * @return attribute that has specified <code>attrName</code> or <code>null</code> if such
   *         attribute does not exist for this element.
   * @throws NullArgumentException if <code>attrName</code> is <code>null</code>.
   */
  public SimpleAttribute getAttribute(final String attrName, boolean isCaseSensitive)
  {
    if (attrName == null)
      throw new NullArgumentException("attrName");

    if (attrs == null)
      return null;
    for (SimpleAttribute at : attrs)
      if (isCaseSensitive) {
        if (at.getName().equals(attrName))
          return at;
      } else if (at.getName().equalsIgnoreCase(attrName))
        return at;
    return null;
  }

  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("name", name)
        .append("value", value).append("type", type).append("attrs", attrs).toString();
  }
}
