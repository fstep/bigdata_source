package com.porva.html.simpledom;

import org.apache.commons.lang.NullArgumentException;

import junit.framework.TestCase;

public class SimpleAttributeTest extends TestCase
{

  /*
   * Test method for 'com.porva.html.simpledom.SimpleAttribute.SimpleAttribute(String, String)'
   */
  public void testSimpleAttribute()
  {
    try {
      new SimpleAttribute(null, "value");
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      new SimpleAttribute("name", null);
      fail();
    } catch (NullArgumentException e) {
    }
    
    new SimpleAttribute("", ""); // todo: is this ok?
  }

  /*
   * Test method for 'com.porva.html.simpledom.SimpleAttribute.getName()'
   */
  public void testGetName()
  {
    SimpleAttribute attr = new SimpleAttribute("name", "value");
    assertEquals("name", attr.getName());
  }

  /*
   * Test method for 'com.porva.html.simpledom.SimpleAttribute.getValue()'
   */
  public void testGetValue()
  {
    SimpleAttribute attr = new SimpleAttribute("name", "value");
    assertEquals("value", attr.getValue());
  }

}
