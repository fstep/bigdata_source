package com.porva.html.simpledom;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import junit.framework.TestCase;

import org.w3c.dom.Document;

public class SimpleDOMRenderTest extends TestCase
{

	private File realHtmlFile = new File("test/google.blognewschannel.com~1.html");

	// private File htmlFile = new File(
	// "/home/fs/poroshin/src/java/JCrawler/export-1140617724285/searchenginewatch.com/5565.original");

	private File testHtmlFile = new File("test/test.html");

	private Document resultDoc;

	protected void setUp() throws Exception
	{
		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		resultDoc = builder.newDocument();
	}

	public void testRenderAsXML() throws Exception
	{
		SimpleDOM model = new SimpleDOMParser().parse(new FileReader(realHtmlFile));
		SimpleDOMRender render = new SimpleDOMRender();

		org.w3c.dom.Node el = render.renderAsXML(model, "CONTENT", resultDoc);
		resultDoc.appendChild(el);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		prettyPrint(resultDoc, "iso-8859-1", out);
		String res = new String(out.toByteArray(), "iso-8859-1");
		assertFalse(res.contains("studentnewspaper"));
		//System.out.println(res);
	}

//	public void testRenderAsXMLLong() throws Exception
//	{
//		List<File> files = getFilesRecursively(new File(
//				"/home/fs/poroshin/storage/news-crawl/data/arhives.news"));
//		for (File file : files) {
//			if (file.getAbsolutePath().endsWith(".original")) {
//				System.out.println("Processing " + file);
//
//				byte[] content = FioUtils.readFileAsBytes(file.getAbsolutePath());
//				String contentStr = new String(content, "UTF8");
//				SimpleDOM model = new SimpleDOMParser().parse(new StringReader(contentStr));
//				SimpleDOMRender render = new SimpleDOMRender();
//				render.renderAsXML(model, "CONTENT", resultDoc);
//			}
//		}
//	}

	static public List<File> getFilesRecursively(File startingDir)
			throws IOException
	{
		if (!startingDir.isDirectory())
			throw new IllegalArgumentException("Not a directory to list files: "
					+ startingDir);

		List<File> result = new ArrayList<File>();
		for (File file : startingDir.listFiles()) {
			if (file.isFile())
				result.add(file);
			if (file.isDirectory()) {
				List<File> deeperList = getFilesRecursively(file);
				result.addAll(deeperList);
			}
		}
		return result;
	}

	/*
	 * Test method for
	 * 'com.porva.html.HTMLDocumentModelRender.renderAsText(HTMLDocumentModel)'
	 */
	public void testRenderAsVisibleText() throws Exception
	{
		SimpleDOM model = new SimpleDOMParser().parse(new FileReader(realHtmlFile));
		SimpleDOMRender render = new SimpleDOMRender();
		System.out.println(render.renderAsVisibleText(model));
	}

	private void prettyPrint(final Document iNode, final String charset,
			final OutputStream out) throws TransformerFactoryConfigurationError,
			TransformerException
	{
		assert iNode != null;
		assert charset != null;
		assert out != null;

		Source source = new DOMSource(iNode);
		Result result = new StreamResult(out);
		Transformer xformer = TransformerFactory.newInstance().newTransformer();
		xformer.setOutputProperty(OutputKeys.ENCODING, charset);
		xformer.transform(source, result);
	}

}
