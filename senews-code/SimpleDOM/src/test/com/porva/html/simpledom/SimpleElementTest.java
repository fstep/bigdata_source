package com.porva.html.simpledom;

import junit.framework.TestCase;

import org.apache.commons.lang.NullArgumentException;

public class SimpleElementTest extends TestCase
{

  /*
   * Test method for 'com.porva.html.simpledom.SimpleElement.SimpleElement(String, Type, String)'
   */
  public void testSimpleElement()
  {
    try {
      new SimpleElement(null, SimpleElement.Type.COMMENT, null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      new SimpleElement("name", null, null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      new SimpleElement("name", SimpleElement.Type.COMMENT, null);
      fail();
    } catch (IllegalArgumentException e) {
    }
    try {
      new SimpleElement("name", SimpleElement.Type.TEXT, null);
      fail();
    } catch (IllegalArgumentException e) {
    }

    new SimpleElement("name", SimpleElement.Type.ELEMENT_START, "comment");
  }

  /*
   * Test method for 'com.porva.html.simpledom.SimpleElement.getValue()'
   */
  public void testGetValue()
  {
    SimpleElement el = new SimpleElement("name", SimpleElement.Type.ELEMENT_START, "value");
    assertEquals("value", el.getValue());
  }

  /*
   * Test method for 'com.porva.html.simpledom.SimpleElement.getName()'
   */
  public void testGetName()
  {
    SimpleElement el = new SimpleElement("name", SimpleElement.Type.ELEMENT_START, "comment");
    assertEquals("name", el.getName());
  }

  /*
   * Test method for 'com.porva.html.simpledom.SimpleElement.getType()'
   */
  public void testGetType()
  {

  }

  /*
   * Test method for 'com.porva.html.simpledom.SimpleElement.addAttribute(String, String)'
   */
  public void testAddAttribute()
  {
    SimpleElement el = new SimpleElement("name", SimpleElement.Type.ELEMENT_START, "comment");
    try {
      el.addAttribute(null, "value");
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      el.addAttribute("name", null);
      fail();
    } catch (NullArgumentException e) {
    }
  }

  /*
   * Test method for 'com.porva.html.simpledom.SimpleElement.getAttributes()'
   */
  public void testGetAttributes()
  {
    SimpleElement el = new SimpleElement("name", SimpleElement.Type.ELEMENT_START, "comment");
    el.addAttribute("a", "b");
    assertEquals(1, el.getAttributes().size());
    el.getAttributes();
    assertEquals(1, el.getAttributes().size());
  }

  /*
   * Test method for 'com.porva.html.simpledom.SimpleElement.getAttribute(String)'
   */
  public void testGetAttribute()
  {
    SimpleElement el = new SimpleElement("name", SimpleElement.Type.ELEMENT_START, "comment");
    el.addAttribute("a", "b");
    assertEquals("b", el.getAttribute("a", false).getValue());
    assertEquals("b", el.getAttribute("a", true).getValue());
    assertNull(el.getAttribute("A", true));
    assertEquals("b", el.getAttribute("A", false).getValue());
    
    el.getAttribute("a", false);
    new SimpleAttribute("a", "c");
    assertEquals("b", el.getAttribute("a", false).getValue());
    assertEquals("b", el.getAttribute("a", false).getValue());
  }

  /*
   * Test method for 'com.porva.html.simpledom.SimpleElement.toString()'
   */
  public void testToString()
  {
    SimpleElement el = new SimpleElement("name", SimpleElement.Type.ELEMENT_START, "comment");
    el.addAttribute("a", "b");
    System.out.println(el);
  }

}
