package com.porva.html.simpledom;
//package com.porva.html;
//
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileReader;
//import java.io.IOException;
//import java.io.Reader;
//
//import junit.framework.TestCase;
//
//import org.apache.commons.lang.NullArgumentException;
//import org.xml.sax.SAXException;
//
//public class HTMLDocumentModelTest extends TestCase
//{
//  private File testHtmlFile = new File("test/test.html");
//
//  private File realHtmlFile = new File("test/google.blognewschannel.com~1.html");
//
//  private File textFile = new File("test/non-html.file");
//
//  public void testHTMLDocumentModel() throws Exception
//  {
//    try {
//      new HTMLDocumentModel(null);
//      fail();
//    } catch (NullArgumentException e) {
//    }
//
//    try {
//      Reader reader = new FileReader(realHtmlFile);
//      reader.close();
//      new HTMLDocumentModel(reader);
//      fail();
//    } catch (IOException e) {
//    }
//
//    // can parse text files by wrapping it to HTML
//    new HTMLDocumentModel(new FileReader(textFile));
//    new HTMLDocumentModel(new FileReader(testHtmlFile));
//    new HTMLDocumentModel(new FileReader(realHtmlFile));
//  }
//
//  public void testGetNode() throws Exception
//  {
//    HTMLDocumentModel model = new HTMLDocumentModel(new FileReader(testHtmlFile));
//    assertEquals("HTML", model.getNode(0).getName());
//    assertEquals(SimpleElement.Type.ELEMENT_START, model.getNode(0).getType());
//    assertEquals("comment", model.getNode(1).getName());
//    assertEquals(SimpleElement.Type.COMMENT, model.getNode(1).getType());
//    
//    assertEquals("HTML", model.getNode(model.size() - 1).getName());
//    assertEquals(SimpleElement.Type.ELEMENT_END, model.getNode(model.size() - 1).getType());
//  }
//  
//  public void testGetEndNodeNum() throws Exception
//  {
//    HTMLDocumentModel model = new HTMLDocumentModel(new FileReader(testHtmlFile));
//    assertEquals(model.size() - 1, model.getEndNodeNum(0));
//    System.out.println(model.toHTMLString());    
//    assertEquals(1, model.getEndNodeNum(1)); // comments element 
//    assertEquals(4, model.getEndNodeNum(3));
//    assertEquals(5, model.getEndNodeNum(5)); // text element
//  }
//
//}
