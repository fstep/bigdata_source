define pattern year="[0-9]+/[0-9]+/[0-9][0-9][0-9][0-9]"
do (a)year-.-(b)content(c)-(d)$until(-,$startwith"Posted by")(e);(f)-(e);$l("title",(b),(c)).$l("_new",(a),(f)).$l("text",(d),(f))
do (a).$to.text.(b).(c)link(d).$l("li",(d))$l("_li",(a),(d))
set article=_new[title!,text,link?=_li.li]
output article