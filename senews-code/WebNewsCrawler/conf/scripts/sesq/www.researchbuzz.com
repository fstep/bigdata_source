define pattern digit="[0-9][0-9][0-9][0-9]"
do (a)--(b)digit-.-(c)-.-.(d)$until[-,$startwith"Posted in the following"](e);(f)-(e).$l("time",(a),(b)).$l("title",(c))$l("text",(d),(f)).$l("_new",(a),(f))
do (a).$to.text.(b).(c)link(d).$l("li",(d))$l("_li",(a),(d))
set article=_new[time,title!,text,link?=_li.li]
output article
