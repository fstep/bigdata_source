package com.porva.crawler;

import static com.porva.crawler.db.DBTestSettings.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import junit.framework.TestCase;

import com.porva.crawler.Crawler.CrawlerStatus;
import com.porva.crawler.db.DBExporter;
import com.porva.crawler.filter.FilterFactory;
import com.porva.crawler.manager.DBManager;
import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.net.HttpClient;
import com.porva.crawler.net.apache.ApacheHttpService;
import com.porva.crawler.service.FilterService;
import com.porva.crawler.service.ServiceProvider;
import com.porva.crawler.task.InitialTaskReader;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskFactory;
import com.porva.crawler.workload.DBWorkload;
import com.porva.html.CharsetDetectorService;
import com.porva.html.HTML2XMLParser;
import com.porva.html.HTMLSAXLinksExtractor;
import com.porva.html.KCEParser;

public class DefaultCrawlerTest extends TestCase
{
  
  
  public void testStartWork() throws Exception
  {
    createFactory();

    HttpClient httpClient = new ApacheHttpService();
    ServiceProvider filterServices = new ServiceProvider();
    filterServices.register(FilterFactory.newAllowDomainFilter(new String[] { "org" }));
    filterServices.register(FilterFactory.newDepthLimitFilter(0));
    filterServices.register(FilterFactory.newRobotRulesFilter(86400000/* 24h */, dbFactory, httpClient));
    filterServices.register(FilterFactory.newProtocolFilter(new String[] { "http" }));
    filterServices.register(FilterFactory.newRedirectsLimitFilter(1));

    ServiceProvider crawlerServices = new ServiceProvider();
    crawlerServices.register(new FilterService(filterServices));
    crawlerServices.register(new DBManager(new TaskReader(), dbFactory));
    crawlerServices.register(new DBWorkload(dbFactory));
    crawlerServices.register(new DefaultFrontier(crawlerServices));
    crawlerServices.register(httpClient);
    crawlerServices.register(new HTMLSAXLinksExtractor());
    crawlerServices.register(new HTML2XMLParser(new File("conf/html2xml.properties"), new File("conf/scripts")));
    crawlerServices.register(new CharsetDetectorService());
    crawlerServices.register(new KCEParser());    

    Crawler crawler = new DefaultCrawler(500, crawlerServices, 1);

    crawler.startWork();
    crawler.waitUntil(CrawlerStatus.DONE);

    DBExporter exporter = new DBExporter();

    File f = new File("export-tostring");
    f.createNewFile();
    FileWriter fw = new FileWriter(f);
    exporter.exportAllToString(dbFactory, fw);
    fw.close();

    crawlerServices.stopAllServices();
    closeAndRemoveFactory();
    //closeFactory();
  }

  // //////////////////////////////////////////////////////////////////
  class TaskReader implements InitialTaskReader
  {

    private List<Task> initialTasks = new ArrayList<Task>();

    private Iterator<Task> it;

    private boolean isClosed = false;

    public TaskReader() throws Exception
    {
      initialTasks.add(TaskFactory.newFetchURL(new CrawlerURL(0,
          "http://localhost/tmp/redirect.html")));

      // initialTasks.add(TaskFactory.newFetchURL(new CrawlerURL(
      // "http://jakarta.apache.org/commons/lang/userguide.html#Description", 0)));

//      initialTasks.add(TaskFactory
//          .newFetchURL(new CrawlerURL(0, "http://localhost/tmp/links.html")));
      // initialTasks.add(TaskFactory.newFetchURL(new CrawlerURL(
      // "http://www.w3.org", 0)));
      it = initialTasks.iterator();
    }

    public Task getNextTask()
    {
      Task task = null;
      try {
        task = it.next();
      } catch (Exception e) {
      }

      return task;
    }

    public void close() throws IOException
    {
      isClosed = true;
    }

    public boolean isClosed()
    {
      return isClosed;
    }

  }
}
