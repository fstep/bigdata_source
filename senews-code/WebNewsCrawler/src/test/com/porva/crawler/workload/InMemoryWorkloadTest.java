//package com.porva.crawler.workload;
//
//import junit.framework.TestCase;
//
//import org.apache.commons.lang.NullArgumentException;
//
//import com.porva.crawler.task.DefaultFetchResult;
//import com.porva.crawler.task.Task;
//import com.porva.crawler.task.TaskFactory;
//import com.porva.crawler.net.CrawlerURL;
//
//public class InMemoryWorkloadTest extends TestCase
//{
//  Task fetchTask1;
//
//  Task fetchTask2;
//
//  DefaultFetchResult failedResult1;
//
//  InMemoryWorkload workload;
//
//  protected void setUp() throws Exception
//  {
//    fetchTask1 = TaskFactory.FETCH_URL.newTask(new CrawlerURL("http://test.url", 0));
//    fetchTask2 = TaskFactory.FETCH_URL.newTask(new CrawlerURL("http://test.url2", 1));
//    failedResult1 = DefaultFetchResult.newFailedResult(new CrawlerURL("http://test.url", 1),
//                                                TaskFactory.FETCH_URL,
//                                                DefaultFetchResult.Status.TASK_FILTERED);
//    workload = new InMemoryWorkload();
//  }
//
//  public void testGetTask()
//  {
//    assertTrue(workload.getTask().getTaskType() == TaskFactory.LAST);
//    assertTrue(workload.getTask().getTaskType() == TaskFactory.LAST);
//    assertTrue(workload.getWaitingNum() == 0);
//    assertTrue(workload.getRunningNum() == 0);
//
//    workload.addTask(fetchTask1);
//    assertTrue(workload.getWaitingNum() == 1);
//    assertTrue(workload.getRunningNum() == 0);
//    assertEquals(workload.getTask(), fetchTask1);
//    assertTrue(workload.getWaitingNum() == 0);
//    assertTrue(workload.getRunningNum() == 1);
//    assertTrue(workload.getTask().getTaskType() == TaskFactory.EMPTY);
//  }
//
//  public void testAddTask()
//  {
//    try {
//      workload.addTask(null);
//      fail();
//    } catch (NullArgumentException e) {
//    }
//
//    assertTrue(workload.getWaitingNum() == 0);
//    assertTrue(workload.addTask(fetchTask1));
//    assertTrue(workload.getTaskStatus(fetchTask1) == WorkloadEntryStatus.WAITING);
//    assertTrue(workload.getWaitingNum() == 1);
//
//    assertFalse(workload.addTask(fetchTask1));
//
//    assertTrue(workload.addTask(fetchTask2));
//    assertTrue(workload.getWaitingNum() == 2);
//    assertTrue(workload.getTaskStatus(fetchTask2) == WorkloadEntryStatus.WAITING);
//  }
//
//  public void testAddTaskWithUpdation()
//  {
//    try {
//      workload.addTask(null, false);
//      fail();
//    } catch (NullArgumentException e) {
//    }
//
//    assertTrue(workload.getWaitingNum() == 0);
//    assertTrue(workload.addTask(fetchTask1, false));
//    assertTrue(workload.getTaskStatus(fetchTask1) == WorkloadEntryStatus.WAITING);
//    assertTrue(workload.getWaitingNum() == 1);
//
//    assertFalse(workload.addTask(fetchTask1, false));
//    assertTrue(workload.addTask(fetchTask1, true));
//    assertFalse(workload.addTask(fetchTask1, false));
//    assertTrue(workload.addTask(fetchTask1, true));
//    assertTrue(workload.getWaitingNum() == 1);
//
//    assertTrue(workload.addTask(fetchTask2));
//    assertTrue(workload.getWaitingNum() == 2);
//    assertTrue(workload.getTaskStatus(fetchTask2) == WorkloadEntryStatus.WAITING);
//  }
//
//  public void testCompleteTask()
//  {
//    try {
//      workload.completeTask(fetchTask1, null);
//      fail();
//    } catch (NullArgumentException e) {
//    }
//    try {
//      workload.completeTask(null, failedResult1);
//      fail();
//    } catch (NullArgumentException e) {
//    }
//
//    assertFalse(workload.completeTask(fetchTask1, failedResult1));
//
//    workload.addTask(fetchTask1);
//    Task task = workload.getTask();
//    assertEquals(fetchTask1, task);
//    assertTrue(workload.completeTask(task, failedResult1));
//    assertTrue(workload.getTaskStatus(fetchTask1) == WorkloadEntryStatus.FAILED);
//    assertFalse(workload.completeTask(task, failedResult1));
//    assertTrue(workload.getFailedNum() == 1);
//    assertTrue(workload.getWaitingNum() == 0);
//    assertTrue(workload.getRunningNum() == 0);
//    assertTrue(workload.getCompleteNum() == 0);
//  }
//
//  public void testGetTaskStatus()
//  {
//    try {
//      workload.getTaskStatus(null);
//      fail();
//    } catch (NullArgumentException e) {
//    }
//
//    assertTrue(workload.getTaskStatus(fetchTask1) == WorkloadEntryStatus.UNKNOWN);
//    workload.addTask(fetchTask1);
//    assertTrue(workload.getTaskStatus(fetchTask1) == WorkloadEntryStatus.WAITING);
//    Task task = workload.getTask();
//    assertEquals(fetchTask1, task);
//    assertTrue(workload.getTaskStatus(task) == WorkloadEntryStatus.RUNNING);
//    workload.completeTask(task, failedResult1);
//    assertTrue(workload.getTaskStatus(task) == WorkloadEntryStatus.FAILED);
//  }
//
//  public void testGetRunningNum()
//  {
//    assertTrue(workload.getRunningNum() == 0);
//    workload.addTask(fetchTask1);
//    assertTrue(workload.getRunningNum() == 0);
//    workload.getTask();
//    assertTrue(workload.getRunningNum() == 1);
//    workload.completeTask(fetchTask1, failedResult1);
//    assertTrue(workload.getRunningNum() == 0);
//  }
//
//  public void testGetWaitingNum()
//  {
//    assertTrue(workload.getWaitingNum() == 0);
//    workload.addTask(fetchTask1);
//    assertTrue(workload.getWaitingNum() == 1);
//    workload.getTask();
//    assertTrue(workload.getWaitingNum() == 0);
//    workload.completeTask(fetchTask1, failedResult1);
//    assertTrue(workload.getWaitingNum() == 0);
//  }
//
//  public void testGetCompleteNum()
//  {
//    assertTrue(workload.getCompleteNum() == 0);
//    workload.addTask(fetchTask1);
//    assertTrue(workload.getCompleteNum() == 0);
//    workload.getTask();
//    assertTrue(workload.getCompleteNum() == 0);
//    workload.completeTask(fetchTask1, failedResult1);
//    assertTrue(workload.getCompleteNum() == 0);
//  }
//
//  public void testGetFailedNum()
//  {
//    assertTrue(workload.getFailedNum() == 0);
//    workload.addTask(fetchTask1);
//    assertTrue(workload.getFailedNum() == 0);
//    workload.getTask();
//    assertTrue(workload.getFailedNum() == 0);
//    workload.completeTask(fetchTask1, failedResult1);
//    assertTrue(workload.getFailedNum() == 1);
//  }
//
//  public void testGetStat()
//  {
//    // TODO Implement getStat().
//  }
//
//}
