package com.porva.crawler.workload;

import java.io.File;

import junit.framework.TestCase;

import org.apache.commons.lang.NullArgumentException;

import static com.porva.crawler.db.DBTestSettings.*;
import com.porva.crawler.db.DB;
import com.porva.crawler.db.DBException;
import com.porva.crawler.db.berkley.BerkleyCrawlerDBFactory;
import com.porva.crawler.task.FetchTask;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskFactory;
import com.porva.crawler.net.CrawlerURL;

public class HostAccessTest extends TestCase
{
  static final File testDBFile = new File("test/testDb");

  BerkleyCrawlerDBFactory fac;

  DB<Task> hostTaskDB;

  FetchTask task1;

  protected void setUp() throws Exception
  {
    createFactory();
    closeAndRemoveFactory();

    fac = new BerkleyCrawlerDBFactory(testDBFile);
    hostTaskDB = fac.newTaskDatabaseHandle();

    task1 = (FetchTask) TaskFactory.newFetchURL(new CrawlerURL(0, "http://localhost"));
  }

  protected void tearDown() throws DBException
  {
    fac.close();
    assertTrue(fac.removeAll());
  }

  public void testHostAccess() throws Exception
  {
    // incorrect
    try {
      new HostAccess(null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      new HostAccess(hostTaskDB);
      fail("hostTaskDB is not opened");
    } catch (IllegalArgumentException e) {
    }

    // correct
    hostTaskDB.open();
    hostTaskDB.putNoOverwrite("localhost", task1);
    HostAccess hostAccess = new HostAccess(hostTaskDB);
    assertEquals("localhost", hostAccess.getFreeHost());
    assertNull(hostAccess.getFreeHost());
    hostTaskDB.close();
  }

  public void testAddHost() throws Exception
  {
    hostTaskDB.open();
    HostAccess hostAccess = new HostAccess(hostTaskDB);

    // invalid
    try {
      hostAccess.addHost(null);
      fail();
    } catch (NullArgumentException e) {
    }

    // correct
    assertTrue(hostAccess.getBusyHostsNum() == 0);
    assertTrue(hostAccess.getFreeHostsNum() == 0);
    hostTaskDB.put("localhost", task1);
    hostAccess.addHost("localhost");
    assertTrue(hostAccess.getFreeHostsNum() == 1);
    assertTrue(hostAccess.getBusyHostsNum() == 0);
    hostAccess.addHost("localhost");
    assertTrue(hostAccess.getFreeHostsNum() == 1);
    assertTrue(hostAccess.getBusyHostsNum() == 0);
    assertEquals("localhost", hostAccess.getFreeHost());
    assertTrue(hostAccess.getFreeHostsNum() == 0);
    assertTrue(hostAccess.getBusyHostsNum() == 1);
    hostAccess.addHost("localhost");
    assertTrue(hostAccess.getFreeHostsNum() == 0);
    assertTrue(hostAccess.getBusyHostsNum() == 1);

    hostTaskDB.close();
  }

  public void testGetFreeHost() throws Exception
  {
    hostTaskDB.open();
    HostAccess hostAccess = new HostAccess(hostTaskDB);

    assertNull(hostAccess.getFreeHost());
    assertNull(hostAccess.getFreeHost());
    hostTaskDB.put("localhost", task1);
    hostAccess.addHost("localhost");
    assertEquals("localhost", hostAccess.getFreeHost());
    assertNull(hostAccess.getFreeHost());
    assertTrue(hostAccess.getBusyHostsNum() == 1);

    hostAccess.addAccessedHost("localhost", System.currentTimeMillis());
    hostTaskDB.putNoOverwrite("localhost", task1);
    assertNull(hostAccess.getFreeHost());
    // Thread.sleep(hostAccess.getServerDelay());
    // assertEquals("host1", hostAccess.getFreeHost());

    hostTaskDB.close();
  }

  public void testAddBusyHost() throws Exception
  {
    hostTaskDB.open();
    HostAccess hostAccess = new HostAccess(hostTaskDB);

    // illegal
    try {
      hostAccess.addAccessedHost(null, System.currentTimeMillis());
      fail();
    } catch (NullArgumentException e) {
    }

    // correct
    hostAccess.addAccessedHost("test1", System.currentTimeMillis());
    assertTrue(hostAccess.getBusyHostsNum() == 1);

    hostTaskDB.close();
  }

}
