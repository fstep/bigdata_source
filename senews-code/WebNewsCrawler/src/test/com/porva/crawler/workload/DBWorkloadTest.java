package com.porva.crawler.workload;

import static com.porva.crawler.db.DBTestSettings.closeAndRemoveFactory;
import static com.porva.crawler.db.DBTestSettings.createFactory;
import static com.porva.crawler.db.DBTestSettings.dbFactory;
import static com.porva.crawler.db.DBTestSettings.statusDB;
import junit.framework.TestCase;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.UnhandledException;

import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.net.Response;
import com.porva.crawler.net.apache.ApacheHttpService;
import com.porva.crawler.task.EmptyTask;
import com.porva.crawler.task.DefaultFetchResult;
import com.porva.crawler.task.FetchResult;
import com.porva.crawler.task.FetchTask;
import com.porva.crawler.task.LastTask;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskFactory;

public class DBWorkloadTest extends TestCase
{
  Task fetchTask1;

  Task fetchTask2;

  FetchResult failedResult1;

  FetchResult redirectRes;

  static Response response;

  static FetchTask redirTask;

  static Response redirResponse;

  static {
    ApacheHttpService apacheHttpClient = new ApacheHttpService();
    try {
      redirTask = (FetchTask) TaskFactory.newFetchURL(new CrawlerURL(
          1, "http://localhost/tmp/redirect.html"));
      response = apacheHttpClient.get(new CrawlerURL(1, "http://localhost/"));
      redirResponse = apacheHttpClient.get(new CrawlerURL(1, "http://localhost/tmp/redirect.html"));
    } catch (Exception e) {
      throw new UnhandledException(e);
    }
  }

  protected void setUp() throws Exception
  {
    fetchTask1 = TaskFactory.newFetchURL(new CrawlerURL(0, "http://test.url"));
    fetchTask2 = TaskFactory.newFetchURL(new CrawlerURL(1, "http://test.url2"));
    failedResult1 = DefaultFetchResult.newFailedResult((FetchTask)TaskFactory.newFetchURL(new CrawlerURL(1, "http://test.url")),
                                                DefaultFetchResult.Status.TASK_FILTERED);
    redirectRes = DefaultFetchResult.newCompleteResult((FetchTask) TaskFactory.newFetchURL(new CrawlerURL(1, "http://localhost/tmp/redirect.html")), redirResponse);

    createFactory();
  }

  protected void tearDown() throws Exception
  {
    closeAndRemoveFactory();
  }

  public void testStop() throws Exception
  {
    DBWorkload dbWorkload = new DBWorkload(dbFactory);
    dbWorkload.stop();
    dbWorkload.stop(); // warning log should be written, but it is ok;
  }

  public void testDBWorkload() throws Exception
  {
    try {
      new DBWorkload(null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      dbFactory.close();
      new DBWorkload(dbFactory);
      fail();
    } catch (IllegalStateException e) {
    }
    createFactory();

    DBWorkload dbWorkload = new DBWorkload(dbFactory);
    assertTrue(dbWorkload.getNumberOf(WorkloadEntryStatus.COMPLETE) == 0);
    assertTrue(dbWorkload.getNumberOf(WorkloadEntryStatus.RUNNING) == 0);
    assertTrue(dbWorkload.getNumberOf(WorkloadEntryStatus.FAILED) == 0);
    assertTrue(dbWorkload.getNumberOf(WorkloadEntryStatus.WAITING) == 0);
    assertTrue(dbWorkload.getNumberOf(WorkloadEntryStatus.UNKNOWN) == 0);
    dbWorkload.stop();

    statusDB.open();
    statusDB.put(fetchTask1.getDBKey(), WorkloadEntryStatus.RUNNING);
    statusDB.put(fetchTask1.getDBKey(), WorkloadEntryStatus.COMPLETE);
    statusDB.put(fetchTask2.getDBKey(), WorkloadEntryStatus.RUNNING);
    statusDB.put(fetchTask2.getDBKey(), WorkloadEntryStatus.FAILED);
    statusDB.put("x", WorkloadEntryStatus.FAILED);
    statusDB.close();
    statusDB.open();
    dbWorkload = new DBWorkload(dbFactory);
    assertTrue(dbWorkload.getNumberOf(WorkloadEntryStatus.COMPLETE) == 1);
    assertTrue(dbWorkload.getNumberOf(WorkloadEntryStatus.RUNNING) == 0);
    assertTrue(dbWorkload.getNumberOf(WorkloadEntryStatus.FAILED) == 2);
    assertTrue(dbWorkload.getNumberOf(WorkloadEntryStatus.WAITING) == 0);
    assertTrue(dbWorkload.getNumberOf(WorkloadEntryStatus.UNKNOWN) == 0);
    statusDB.close();
    dbWorkload.stop();
    closeAndRemoveFactory();

    createFactory();
    dbWorkload = new DBWorkload(dbFactory);
    dbWorkload.addTask(redirTask);
    dbWorkload.completeTask(redirTask, redirectRes);
    System.out.println(dbWorkload);
    dbWorkload.stop();
  }

  public void testGetTask() throws Exception
  {
    DBWorkload workload = new DBWorkload(dbFactory);

    assertTrue(workload.getTask() instanceof LastTask);
    assertTrue(workload.getTask() instanceof LastTask);
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.WAITING) == 0);
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.RUNNING) == 0);

    workload.addTask(fetchTask1);
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.WAITING) == 1);
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.RUNNING) == 0);
    assertEquals(workload.getTask(), fetchTask1);
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.WAITING) == 0);
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.RUNNING) == 1);
    assertTrue(workload.getTask() instanceof EmptyTask);

    workload.stop();
  }

  public void testAddTask() throws Exception
  {
    DBWorkload workload = new DBWorkload(dbFactory);
    try {
      workload.addTask(null);
      fail();
    } catch (NullArgumentException e) {
    }

    assertTrue(workload.getNumberOf(WorkloadEntryStatus.WAITING) == 0);
    assertTrue(workload.addTask(fetchTask1));
    assertTrue(workload.getTaskStatus(fetchTask1) == WorkloadEntryStatus.WAITING);
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.WAITING) == 1);

    assertFalse(workload.addTask(fetchTask1));

    assertTrue(workload.addTask(fetchTask2));
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.WAITING) == 2);
    assertTrue(workload.getTaskStatus(fetchTask2) == WorkloadEntryStatus.WAITING);

    workload.stop();
  }

  public void testAddTaskUpdated() throws Exception
  {
    DBWorkload workload = new DBWorkload(dbFactory);
    try {
      workload.addTask(null, true);
      fail();
    } catch (NullArgumentException e) {
    }

    assertTrue(workload.getNumberOf(WorkloadEntryStatus.WAITING) == 0);
    assertTrue(workload.addTask(fetchTask1, false));
    assertTrue(workload.getTaskStatus(fetchTask1) == WorkloadEntryStatus.WAITING);
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.WAITING) == 1);

    assertFalse(workload.addTask(fetchTask1, false));
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.WAITING) == 1);
    assertTrue(workload.addTask(fetchTask1, true));
    assertFalse(workload.addTask(fetchTask1, false));
    assertTrue(workload.addTask(fetchTask1, true));
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.WAITING) == 1);

    assertTrue(workload.addTask(fetchTask2, false));
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.WAITING) == 2);
    assertTrue(workload.getTaskStatus(fetchTask2) == WorkloadEntryStatus.WAITING);

    workload.stop();
  }

  public void testCompleteTask() throws Exception
  {
    DBWorkload workload = new DBWorkload(dbFactory);

    try {
      workload.completeTask(null, failedResult1);
      fail();
    } catch (NullArgumentException e) {
    }

    workload.addTask(fetchTask1);
    Task task = workload.getTask();
    assertEquals(fetchTask1, task);
    assertTrue(workload.completeTask(task, failedResult1));
    assertTrue(workload.getTaskStatus(fetchTask1) == WorkloadEntryStatus.FAILED);
    assertFalse(workload.completeTask(task, failedResult1));
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.FAILED) == 1);
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.WAITING) == 0);
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.RUNNING) == 0);
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.COMPLETE) == 0);

    workload.stop();
  }

  public void testGetTaskStatus() throws Exception
  {
    DBWorkload workload = new DBWorkload(dbFactory);
    try {
      workload.getTaskStatus(null);
      fail();
    } catch (NullArgumentException e) {
    }

    assertTrue(workload.getTaskStatus(fetchTask1) == WorkloadEntryStatus.UNKNOWN);
    workload.addTask(fetchTask1);
    assertTrue(workload.getTaskStatus(fetchTask1) == WorkloadEntryStatus.WAITING);
    Task task = workload.getTask();
    assertEquals(fetchTask1, task);
    assertTrue(workload.getTaskStatus(task) == WorkloadEntryStatus.RUNNING);
    workload.completeTask(task, failedResult1);
    assertTrue(workload.getTaskStatus(task) == WorkloadEntryStatus.FAILED);

    workload.stop();
  }

  public void testGetRunningNum() throws Exception
  {
    DBWorkload workload = new DBWorkload(dbFactory);
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.RUNNING) == 0);
    workload.addTask(fetchTask1);
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.RUNNING) == 0);
    workload.getTask();
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.RUNNING) == 1);
    workload.completeTask(fetchTask1, failedResult1);
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.RUNNING) == 0);
    workload.stop();
  }

  public void testGetWaitingNum() throws Exception
  {
    DBWorkload workload = new DBWorkload(dbFactory);
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.WAITING) == 0);
    workload.addTask(fetchTask1);
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.WAITING) == 1);
    workload.getTask();
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.WAITING) == 0);
    workload.completeTask(fetchTask1, failedResult1);
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.WAITING) == 0);
    workload.stop();
  }

  public void testGetCompleteNum() throws Exception
  {
    DBWorkload workload = new DBWorkload(dbFactory);
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.COMPLETE) == 0);
    workload.addTask(fetchTask1);
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.COMPLETE) == 0);
    workload.getTask();
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.COMPLETE) == 0);
    workload.completeTask(fetchTask1, failedResult1);
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.COMPLETE) == 0);
    workload.stop();
  }

  public void testGetFailedNum() throws Exception
  {
    DBWorkload workload = new DBWorkload(dbFactory);
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.FAILED) == 0);
    workload.addTask(fetchTask1);
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.FAILED) == 0);
    workload.getTask();
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.FAILED) == 0);
    workload.completeTask(fetchTask1, failedResult1);
    assertTrue(workload.getNumberOf(WorkloadEntryStatus.FAILED) == 1);
    workload.stop();
  }

  public void testGetStat()
  {
    // TODO Implement getStat().
  }

  public void testGetServiceName()
  {
    // TODO Implement getServiceName().
  }

  public void testIsStopped()
  {
    // TODO Implement isStopped().
  }

}
