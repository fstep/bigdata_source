package com.porva.crawler.workload;

import static com.porva.crawler.db.DBTestSettings.closeAndRemoveFactory;
import static com.porva.crawler.db.DBTestSettings.createFactory;
import static com.porva.crawler.db.DBTestSettings.genericDBWithDups;
import junit.framework.TestCase;

public class WorkloadEntryStatusTest extends TestCase
{

  public void testDBSerialization() throws Exception
  {
    createFactory();
    genericDBWithDups.open();
    
    genericDBWithDups.put("x", WorkloadEntryStatus.COMPLETE);
    genericDBWithDups.put("x", WorkloadEntryStatus.COMPLETE);
    genericDBWithDups.put("y", WorkloadEntryStatus.RUNNING);
    assertEquals(WorkloadEntryStatus.COMPLETE, genericDBWithDups.get("x"));
    assertTrue(WorkloadEntryStatus.COMPLETE == genericDBWithDups.get("x"));
    assertEquals(WorkloadEntryStatus.RUNNING, genericDBWithDups.get("y"));
    assertTrue(WorkloadEntryStatus.RUNNING == genericDBWithDups.get("y"));
    
    genericDBWithDups.close();
    closeAndRemoveFactory();
    
    // equals
    assertEquals(WorkloadEntryStatus.COMPLETE, WorkloadEntryStatus.COMPLETE);
  }

}
