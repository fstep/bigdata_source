package com.porva.crawler.workload;

import static com.porva.crawler.db.DBTestSettings.closeAndRemoveFactory;
import static com.porva.crawler.db.DBTestSettings.createFactory;
import static com.porva.crawler.db.DBTestSettings.dbFactory;
import static com.porva.crawler.workload.WorkloadEntryStatus.COMPLETE;
import static com.porva.crawler.workload.WorkloadEntryStatus.FAILED;
import static com.porva.crawler.workload.WorkloadEntryStatus.RUNNING;
import static com.porva.crawler.workload.WorkloadEntryStatus.UNKNOWN;
import static com.porva.crawler.workload.WorkloadEntryStatus.WAITING;
import junit.framework.TestCase;

import org.apache.commons.lang.NullArgumentException;

public class URLStatusDBTest extends TestCase
{

  protected void setUp() throws Exception
  {
    createFactory();
  }

  protected void tearDown() throws Exception
  {
    closeAndRemoveFactory();
  }

  public void testURLStatusDB() throws Exception
  {
    try {
      new URLStatusDB(null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      dbFactory.close();
      new URLStatusDB(dbFactory);
      fail("factory is closed");
    } catch (IllegalStateException e) {
    }
    createFactory();

    new URLStatusDB(dbFactory);
  }

  public void testOpen() throws Exception
  {
    try {
      dbFactory.close();
      (new URLStatusDB(dbFactory)).open();
      fail("factory is closed");
    } catch (IllegalStateException e) {
    }

    createFactory();
    URLStatusDB statusDB = new URLStatusDB(dbFactory);
    statusDB.open();
    assertTrue(statusDB.getNumberOf(COMPLETE) == 0);
    assertTrue(statusDB.getNumberOf(FAILED) == 0);
    assertTrue(statusDB.getNumberOf(WAITING) == 0);
    assertTrue(statusDB.getNumberOf(RUNNING) == 0);
    assertTrue(statusDB.getNumberOf(UNKNOWN) == 0);

    statusDB.put("c1", COMPLETE);
    statusDB.put("c2", COMPLETE);
    statusDB.put("f1", FAILED);
    statusDB.put("f1", FAILED);
    statusDB.put("r1", RUNNING);
    statusDB.put("w1", WAITING);
    statusDB.close();

    statusDB = new URLStatusDB(dbFactory);
    statusDB.open();
    assertTrue(statusDB.getNumberOf(COMPLETE) == 2);
    assertTrue(statusDB.getNumberOf(FAILED) == 1);
    assertTrue(statusDB.getNumberOf(WAITING) == 1);
    assertTrue(statusDB.getNumberOf(RUNNING) == 1);
    assertTrue(statusDB.getNumberOf(UNKNOWN) == 0);
    statusDB.close();
  }

  public void testPut() throws Exception
  {
    URLStatusDB statusDB = new URLStatusDB(dbFactory);
    statusDB.open();

    assertTrue(statusDB.put("c1", COMPLETE));
    assertTrue("put one COMPLETE", statusDB.getNumberOf(COMPLETE) == 1);

    assertTrue(statusDB.put("c1", COMPLETE));
    assertTrue("put the same COMPLETE", statusDB.getNumberOf(COMPLETE) == 1);

    assertTrue(statusDB.put("c1", FAILED));
    assertTrue("put FAILED on the place of COMPLETE", 
               statusDB.getNumberOf(COMPLETE) == 0);
    assertTrue("put FAILED on the place of COMPLETE", 
               statusDB.getNumberOf(FAILED) == 1);

    statusDB.close();
  }

}
