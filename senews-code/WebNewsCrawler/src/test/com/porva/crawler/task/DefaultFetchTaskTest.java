//package com.porva.crawler.task;
//
//import static com.porva.crawler.db.DBTestSettings.closeAndRemoveFactory;
//import static com.porva.crawler.db.DBTestSettings.createFactory;
//import static com.porva.crawler.db.DBTestSettings.taskDB;
//import junit.framework.TestCase;
//
//import org.apache.commons.lang.NullArgumentException;
//
//import com.porva.crawler.net.CrawlerURL;
//
//public class DefaultFetchTaskTest extends TestCase
//{
//  CrawlerURL url;
//  DefaultFetchTask dft;
//  
//  protected void setUp() throws Exception
//  {
//    url = new CrawlerURL("http://localhost/", 0);
//    dft = new DefaultFetchTask(url);
//  }
//
//  public void testGetDBKey() throws Exception
//  {
//    assertEquals("http://localhost", dft.getDBKey());
//  }
//
//  /*
//   * Class under test for String toString()
//   */
//  public void testToString()
//  {
//    System.out.println(dft.toString());
//    System.out.println(new DefaultFetchTask());
//  }
//
//  /*
//   * Class under test for boolean equals(Object)
//   */
//  public void testEqualsObject() throws Exception
//  {
//    assertEquals(dft, dft);
//    assertEquals(dft, new DefaultFetchTask(url));
//    assertEquals(dft, new DefaultFetchTask(new CrawlerURL("http://localhost/", 0)));
//  }
//
//  /*
//   * Class under test for void DefaultFetchTask()
//   */
//  public void testDefaultFetchTask() throws Exception
//  {
//    try {
//      new DefaultFetchTask(TaskFactory.FETCH_URL, null);
//    } catch (NullArgumentException e) {
//    }
//    try {
//      new DefaultFetchTask(null, new CrawlerURL("http://localhost", 0));
//    } catch (NullArgumentException e) {
//    }
//    try {
//      new DefaultFetchTask(null);
//    } catch (NullArgumentException e) {
//    }
//
//    DefaultFetchTask dft = new DefaultFetchTask(new CrawlerURL("http://localhost", 0));
//    assertTrue(dft.getTaskType() == TaskFactory.DEFAULT_FETCH_TASK);
//  }
//
//  public void testGetCrawlerURL()
//  {
//    assertEquals(dft.getCrawlerURL().getURLString(), url.getURLString());
//  }
//
//  public void testPerform()
//  {
//    try {
//      dft.perform(null);
//    } catch (NullArgumentException e) {
//    }
//    // TODO: add more test here
//  }
//
//  public void testWriteToAndNewInstance() throws Exception
//  {
//    createFactory();
//    taskDB.open();
//    
//    taskDB.put(dft);
//    Task task = taskDB.get(dft.getDBKey());
//    assertEquals(dft, task);
//    
//    taskDB.close();
//    closeAndRemoveFactory();
//  }
//
//}
