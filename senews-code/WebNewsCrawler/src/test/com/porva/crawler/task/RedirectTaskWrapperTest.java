//package com.porva.crawler.task;
//
//import static com.porva.crawler.db.DBTestSettings.closeAndRemoveFactory;
//import static com.porva.crawler.db.DBTestSettings.createFactory;
//import static com.porva.crawler.db.DBTestSettings.taskDB;
//import junit.framework.TestCase;
//
//import org.apache.commons.lang.NullArgumentException;
//
//import com.porva.crawler.net.CrawlerURL;
//
//public class RedirectTaskWrapperTest extends TestCase
//{
//  FetchTask fetchTask;
//  RedirectTaskWrapper taskWrapper;
//  
//  protected void setUp() throws Exception
//  {
//    fetchTask = (FetchTask) TaskFactory.newFetchURL(new CrawlerURL("http://localhost", 1));
//    taskWrapper = new RedirectTaskWrapper(fetchTask);
//  }
//
//  /*
//   * Class under test for void RedirectTaskWrapper()
//   */
//  public void testRedirectTaskWrapper() throws Exception
//  {
//    try {
//      new RedirectTaskWrapper(null);
//    } catch (NullArgumentException e) {
//    }
//
//    assertTrue(taskWrapper.getRedirNum() == 0);
//    assertTrue(taskWrapper.getWrappedTask() == fetchTask);
//    
//    RedirectTaskWrapper rtw2 = new RedirectTaskWrapper(taskWrapper);
//    assertTrue(rtw2.getRedirNum() == 1);
//    assertTrue(rtw2.getWrappedTask() == fetchTask);
//    
//    RedirectTaskWrapper rtw3 = new RedirectTaskWrapper(rtw2);
//    assertTrue(rtw3.getRedirNum() == 2);
//    assertTrue(rtw2.getWrappedTask() == fetchTask);
//  }
//
//  public void testDBSerialization() throws Exception
//  {
//    createFactory();
//    taskDB.open();
//    
//    taskDB.put(taskWrapper);
//    assertEquals(taskWrapper, taskDB.get(taskWrapper.getDBKey()));
//    
//    RedirectTaskWrapper rtw2 = new RedirectTaskWrapper(taskWrapper);
//    taskDB.put("rtw2", rtw2);
////    System.out.println(rtw2);
////    System.out.println(taskDB.get("rtw2"));
//    assertEquals(rtw2, taskDB.get("rtw2"));
//    
//    taskDB.close();
//    closeAndRemoveFactory();
//  }
//
//}
