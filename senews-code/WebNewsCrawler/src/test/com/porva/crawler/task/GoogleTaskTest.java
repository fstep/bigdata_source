package com.porva.crawler.task;

import junit.framework.TestCase;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.net.apache.ApacheHttpService;
import com.porva.crawler.service.ServiceProvider;

public class GoogleTaskTest extends TestCase
{
  static ApacheHttpService apacheHttpClient;
  
  ServiceProvider provider;
  
  static {
    apacheHttpClient = new ApacheHttpService();
//    try {
//      response = apacheHttpClient.get(new CrawlerURL(1, "http://localhost/"));
//      redirResponse = apacheHttpClient.get(new CrawlerURL(1, "http://localhost/tmp/redirect.html"));
//    } catch (Exception e) {
//      throw new UnhandledException(e);
//    }
  }
  
  protected void setUp()
  {
    provider = new ServiceProvider();
    provider.register(apacheHttpClient);
  }
  
  protected void tearDown() throws Exception
  {
    provider.stopAllServices();
  }

  /**
   * Test method for {@link GoogleTask#GoogleTask(int, String, int)}
   */
  public void testGoogleTask()
  {
    try {
      new GoogleTask(1, null, 1);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      new GoogleTask(-1, "query string", 1);
      fail();
    } catch (IllegalArgumentException e) {
    }
    try {
      new GoogleTask(1, "query string", -1);
      fail();
    } catch (IllegalArgumentException e) {
    }
    try {
      new GoogleTask(0, "query string", 0);
      fail();
    } catch (IllegalArgumentException e) {
    }
    try {
      new GoogleTask(0, "\t ", 0);
      fail();
    } catch (IllegalArgumentException e) {
    }
    
    new GoogleTask(1, "query string", 0);
  }

}
