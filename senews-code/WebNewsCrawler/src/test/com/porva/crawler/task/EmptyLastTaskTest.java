package com.porva.crawler.task;

import static com.porva.crawler.db.DBTestSettings.closeAndRemoveFactory;
import static com.porva.crawler.db.DBTestSettings.createFactory;
import static com.porva.crawler.db.DBTestSettings.taskDB;
import junit.framework.TestCase;

public class EmptyLastTaskTest extends TestCase
{

  Task emptyTask = EmptyTask.getInstance();
  Task lastTask = EmptyTask.getInstance();

  /*
   * Class under test for String toString()
   */
  public void testToString()
  {
    System.out.println(emptyTask.toString());
    System.out.println(lastTask.toString());
  }

  public void testGetInstance()
  {
    assertTrue(EmptyTask.getInstance() == EmptyTask.getInstance());
    assertTrue(LastTask.getInstance() == LastTask.getInstance());
  }

  public void testPerform()
  {
    try {
      emptyTask.perform(null);
    } catch (IllegalStateException e) {
    }
    try {
      lastTask.perform(null);
    } catch (IllegalStateException e) {
    }

  }

  public void testWriteToAndNewInstance() throws Exception
  {
    createFactory();
    taskDB.open();
    
    taskDB.put(emptyTask);
    Task task = taskDB.get(emptyTask.getDBKey());
    assertEquals(emptyTask, task);
    assertTrue(emptyTask == task);
    
    taskDB.put(lastTask);
    task = taskDB.get(lastTask.getDBKey());
    assertEquals(lastTask, task);
    assertTrue(lastTask == task);
    
    taskDB.close();
    closeAndRemoveFactory();
  }

  public void testGetDBValue()
  {
    assertTrue(emptyTask.getDBValue() == emptyTask); 
    assertTrue(lastTask.getDBValue() == lastTask);
  }

}
