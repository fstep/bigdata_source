package com.porva.crawler.task;

import static com.porva.crawler.db.DBTestSettings.closeAndRemoveFactory;
import static com.porva.crawler.db.DBTestSettings.createFactory;
import static com.porva.crawler.db.DBTestSettings.resultDB;
import junit.framework.TestCase;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.UnhandledException;

import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.net.Response;
import com.porva.crawler.net.apache.ApacheHttpService;

public class FetchResultTest extends TestCase
{
  static Response response;

  static Response redirResponse;

  FetchTask task1;

  FetchResult completeRes;

  FetchResult redirectRes;

  FetchResult failedRes;

  protected void setUp() throws Exception
  {
    task1 = (FetchTask) TaskFactory.newFetchURL(new CrawlerURL(1, "http://localhost"));
    completeRes = DefaultFetchResult.newCompleteResult(task1, response);
    redirectRes = DefaultFetchResult.newCompleteResult((FetchTask) TaskFactory
        .newFetchURL(new CrawlerURL(1, "http://localhost/tmp/redirect.html")), redirResponse);
    failedRes = DefaultFetchResult.newFailedResult(task1, DefaultFetchResult.Status.TASK_FILTERED);
  }

  static {
    ApacheHttpService apacheHttpClient = new ApacheHttpService();
    try {
      response = apacheHttpClient.get(new CrawlerURL(1, "http://localhost/"));
      redirResponse = apacheHttpClient.get(new CrawlerURL(1, "http://localhost/tmp/redirect.html"));
    } catch (Exception e) {
      throw new UnhandledException(e);
    }
  }

  public void testNewCompleteResult() throws Exception
  {
    try {
      DefaultFetchResult.newCompleteResult(null, null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      DefaultFetchResult.newCompleteResult(new FetchURL(new CrawlerURL(1, "http://localhost")), null);
      fail();
    } catch (NullArgumentException e) {
    }

    DefaultFetchResult.newCompleteResult(new FetchURL(new CrawlerURL(1, "http://localhost")), response);

  }

  public void testNewCompleteResultt() throws Exception
  {
    try {
      DefaultFetchResult.newCompleteResult(null, null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      DefaultFetchResult.newCompleteResult(new FetchURL(new CrawlerURL(1, "http://localhost")), null);
      fail();
    } catch (NullArgumentException e) {
    }

    DefaultFetchResult.newCompleteResult(new FetchURL(new CrawlerURL(1, "http://localhost")), response);

  }

  public void testNewFailedResult() throws Exception
  {
    try {
      DefaultFetchResult.newFailedResult(null, DefaultFetchResult.Status.TASK_FILTERED);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      DefaultFetchResult.newFailedResult(new FetchURL(new CrawlerURL(1, "http://localhost")), null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      DefaultFetchResult.newFailedResult(new FetchURL(new CrawlerURL(1, "http://localhost")),
                                  DefaultFetchResult.Status.COMPLETE);
      fail();
    } catch (IllegalArgumentException e) {
    }

    DefaultFetchResult.newFailedResult(new FetchURL(new CrawlerURL(1, "http://localhost")),
                                DefaultFetchResult.Status.TASK_FILTERED);
    DefaultFetchResult.newFailedResult(new FetchURL(new CrawlerURL(1, "http://localhost")),
                                DefaultFetchResult.Status.HTTP_CLIENT_EXCEPTION);
  }

  public void testNewFailedFetchURLResult() throws Exception
  {
    try {
      DefaultFetchResult.newFailedResult(null, DefaultFetchResult.Status.TASK_FILTERED);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      DefaultFetchResult.newFailedResult(new FetchURL(new CrawlerURL(1, "http://localhost")), null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      DefaultFetchResult.newFailedResult(new FetchURL(new CrawlerURL(1, "http://localhost")),
                                  DefaultFetchResult.Status.COMPLETE);
      fail();
    } catch (IllegalArgumentException e) {
    }

    DefaultFetchResult.newFailedResult(new FetchURL(new CrawlerURL(1, "http://localhost")),
                                DefaultFetchResult.Status.TASK_FILTERED);
    DefaultFetchResult.newFailedResult(new FetchURL(new CrawlerURL(1, "http://localhost")),
                                DefaultFetchResult.Status.HTTP_CLIENT_EXCEPTION);
  }

  public void testGetTask() throws Exception
  {
    FetchTask task1 = (FetchTask) TaskFactory.newFetchURL(new CrawlerURL(
        1, "http://localhost"));
    FetchResult completeRes = DefaultFetchResult.newCompleteResult(task1, response);
    assertEquals(task1, completeRes.getTask());

    FetchResult failedRes = DefaultFetchResult.newFailedResult(task1, DefaultFetchResult.Status.TASK_FILTERED);
    assertEquals(task1, failedRes.getTask());
  }

  public void testGetStatus() throws Exception
  {
    assertTrue(completeRes.getStatus() == DefaultFetchResult.Status.COMPLETE);
    assertTrue(failedRes.getStatus() == DefaultFetchResult.Status.TASK_FILTERED);
  }

  public void testGetResponse() throws Exception
  {
    assertNotNull(completeRes.getResponse());
    assertNull(failedRes.getResponse());
  }

  public void testDBSerialization() throws Exception
  {
    createFactory();
    resultDB.open();

    resultDB.put(completeRes);
    resultDB.put("xxx", failedRes);
    assertEquals(completeRes, resultDB.get(completeRes.getDBKey()));
    assertEquals(failedRes, resultDB.get("xxx"));

    resultDB.close();
    closeAndRemoveFactory();
  }

  public void testGetDBKey()
  {
    assertEquals("http://localhost/tmp/redirect.html", redirectRes.getDBKey());
    assertEquals("http://localhost/", completeRes.getDBKey());
    assertEquals("http://localhost/", failedRes.getDBKey());
  }

}
