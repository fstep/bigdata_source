package com.porva.crawler.task;

import java.io.File;

import junit.framework.TestCase;

import org.apache.commons.lang.NullArgumentException;

public class CITFormatReaderTest extends TestCase
{

  public void testCITFormatReader()
  {
    try {
      new CITFormatReader(null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      new CITFormatReader(new File("test/no_such_file"));
      fail("Cannot read file exception");
    } catch (IllegalArgumentException e) {
    }

    new CITFormatReader(new File("test/correct.cit"));
  }

  public void testGetNextTask() throws Exception
  {
    CITFormatReader citReader = new CITFormatReader(new File("test/incorrect.cit"));
    for (int i = 0; i < 3; i++) {
      try {
        citReader.getNextTask();
        fail();
      } catch (TaskFormatException e) {}
    }
    
    // read correct tasks
    citReader = new CITFormatReader(new File("test/correct.cit"));
    FetchURL task = (FetchURL) citReader.getNextTask();
    assertTrue(task instanceof FetchURL);
    assertEquals("http://google.weblogsinc.com/entry/0264041061650251/", task.getCrawlerURL()
        .getURLString());
    assertTrue(task.getCrawlerURL().getDepth() == 1);
    assertTrue(task.getCrawlerURL().getRedirNum() == 0);
    
    task = (FetchURL) citReader.getNextTask();
    assertTrue(task instanceof FetchURL);
    assertEquals("http://localhost/", task.getCrawlerURL()
        .getURLString());
    assertTrue(task.getCrawlerURL().getDepth() == 2);
    assertTrue(task.getCrawlerURL().getRedirNum() == 0);

  }

}
