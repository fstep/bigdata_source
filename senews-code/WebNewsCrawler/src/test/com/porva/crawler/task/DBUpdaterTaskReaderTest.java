package com.porva.crawler.task;

import static com.porva.crawler.db.DBTestSettings.closeAndRemoveFactory;
import static com.porva.crawler.db.DBTestSettings.createFactory;
import static com.porva.crawler.db.DBTestSettings.dbFactory;
import static com.porva.crawler.db.DBTestSettings.resultDB;

import java.util.HashMap;

import junit.framework.TestCase;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.UnhandledException;

import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.net.Response;
import com.porva.crawler.net.apache.ApacheHttpService;

public class DBUpdaterTaskReaderTest extends TestCase
{
  static Response response;

  static Response redirResponse;

  FetchTask task1;

  FetchResult completeRes;

  FetchResult redirectRes;

  FetchResult failedRes;
  
  static {
    ApacheHttpService apacheHttpClient = new ApacheHttpService();
    try {
      response = apacheHttpClient.get(new CrawlerURL(1, "http://localhost/"));
      redirResponse = apacheHttpClient.get(new CrawlerURL(1, "http://localhost/tmp/redirect.html"));
    } catch (Exception e) {
      throw new UnhandledException(e);
    }
  }

  protected void setUp() throws Exception
  {
    task1 = (FetchTask) TaskFactory.newFetchURL(new CrawlerURL(
        1, "http://localhost"));
    completeRes = DefaultFetchResult.newCompleteResult(task1, response);
    redirectRes = DefaultFetchResult.newCompleteResult((FetchTask) TaskFactory.newFetchURL(new CrawlerURL(1, "http://localhost/tmp/redirect.html")), redirResponse);
    failedRes = DefaultFetchResult.newFailedResult(task1, DefaultFetchResult.Status.TASK_FILTERED);

    createFactory();
  }

  protected void tearDown() throws Exception
  {
    assertTrue(closeAndRemoveFactory());
  }

  public void testDBUpdaterTaskReader() throws Exception
  {
    try {
      new DBUpdaterTaskReader(null, null);
      fail();
    } catch (NullArgumentException e) {
    }

    try {
      dbFactory.close();
      new DBUpdaterTaskReader(dbFactory, null);
      fail("dbFactory is closed");
    } catch (IllegalStateException e) {
    }

    createFactory();
    DBUpdaterTaskReader reader = new DBUpdaterTaskReader(dbFactory, null);
    reader.close();
  }

  public void testGetNextTask() throws Exception
  {
    DBUpdaterTaskReader reader = new DBUpdaterTaskReader(dbFactory, null);
    assertNull("Empty result db has no next tasks", reader.getNextTask());

    resultDB.open();
    resultDB.put(completeRes);
    resultDB.put(failedRes);
    assertNull("No expired task in result db", reader.getNextTask());
    reader.close();
    
    HashMap<Class, Long> newLifetimes = new HashMap<Class, Long>();
    newLifetimes.put(FetchURL.class, 1L);
    reader = new DBUpdaterTaskReader(dbFactory, newLifetimes);
    assertEquals(completeRes.getTask(), reader.getNextTask());
    assertNull(reader.getNextTask());
    
    resultDB.close();

    reader.close();
  }

  public void testClose() throws Exception
  {
    DBUpdaterTaskReader reader = new DBUpdaterTaskReader(dbFactory, null);
    assertFalse(reader.isClosed());
    reader.close();
    assertTrue(reader.isClosed());
    try {
      reader.close();
      fail("already closed");
    } catch (IllegalStateException e) {
      assertTrue(reader.isClosed());
    }
  }

}
