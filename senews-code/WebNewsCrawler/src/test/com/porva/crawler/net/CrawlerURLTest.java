package com.porva.crawler.net;

import static com.porva.crawler.db.DBTestSettings.closeAndRemoveFactory;
import static com.porva.crawler.db.DBTestSettings.createFactory;
import static com.porva.crawler.db.DBTestSettings.genericDBWithDups;
import junit.framework.TestCase;

import org.apache.commons.lang.NullArgumentException;

public class CrawlerURLTest extends TestCase
{

  /*
   * Class under test for void CrawlerURL(String, int)
   */
  public void testCrawlerURLStringint() throws Exception
  {
    try {
      new CrawlerURL(1, null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      new CrawlerURL(1, "localhost");
      fail("No protocol");
    } catch (MalformedCrawlerURLException e) {
    }
    try {
      new CrawlerURL(1, "unknown://localhost");
      fail("unknown protocol");
    } catch (MalformedCrawlerURLException e) {
    }
    try {
      new CrawlerURL(1, "http//localhost");
      fail("no protocol");
    } catch (MalformedCrawlerURLException e) {
    }
    try {
      new CrawlerURL(1, "http://localhost:-2");
      fail("invalid port number");
    } catch (MalformedCrawlerURLException e) {
    }
    try {
      new CrawlerURL(1, "http://x .com");
      fail("illegal char");
    } catch (MalformedCrawlerURLException e) {
    }

    CrawlerURL crawlerURL = new CrawlerURL(1, "http://localhost");
    assertTrue(crawlerURL.getDepth() == 1);
    assertEquals("http://localhost/", crawlerURL.getURLString());
    
    crawlerURL = new CrawlerURL(0, "http://local%20host");
    assertTrue(crawlerURL.getDepth() == 0);
    assertEquals("http://local%20host/", crawlerURL.getURLString());
    
    crawlerURL = new CrawlerURL(-10, "http://localhost/");    // also ok
    assertTrue(crawlerURL.getDepth() == -10);
    assertEquals("http://localhost/", crawlerURL.getURLString());
    
    crawlerURL = new CrawlerURL(1, "mailto:cmusciano@aol.com");
    assertEquals("mailto:cmusciano@aol.com", crawlerURL.getURLString());
    
    crawlerURL = new CrawlerURL(1, "ftp://test.com");
    assertEquals("ftp://test.com/", crawlerURL.getURLString());
    
    crawlerURL = new CrawlerURL(0, "http://local%20host/test#fragment");
    assertTrue(crawlerURL.getDepth() == 0);
    assertEquals("http://local%20host/test", crawlerURL.getURLString());
  }

  /*
   * Class under test for void CrawlerURL(CrawlerURL)
   */
  public void testCrawlerURLCrawlerURL() throws Exception
  {
    try {
      new CrawlerURL(null);
      fail();
    } catch (NullArgumentException e) {
    }
    
    CrawlerURL crawlerURL1 = new CrawlerURL(1, "http://localhost/xxx/../");
    CrawlerURL crawlerURL2 = new CrawlerURL(crawlerURL1);
    assertEquals(crawlerURL1, crawlerURL2);
  }

  /*
   * Class under test for void CrawlerURL(String, int, int)
   */
  public void testCrawlerURLStringintint() throws Exception
  {
    try {
      new CrawlerURL(1, null, 1);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      new CrawlerURL(1, "http://localhost", -1);
      fail("redirNum must be >= 0");
    } catch (IllegalArgumentException e) {
    }
    try {
      new CrawlerURL(1, "localhost", 1);
      fail("No protocol");
    } catch (MalformedCrawlerURLException e) {
    }
    try {
      new CrawlerURL(1, "unknown://localhost", 1);
      fail("unknown protocol");
    } catch (MalformedCrawlerURLException e) {
    }
    try {
      new CrawlerURL(1, "http//localhost", 1);
      fail("no protocol");
    } catch (MalformedCrawlerURLException e) {
    }
    try {
      new CrawlerURL(1, "http://x .com", 1);
      fail("illegal char");
    } catch (MalformedCrawlerURLException e) {
    }

    CrawlerURL crawlerURL = new CrawlerURL(1, "http://localhost", 1);
    assertTrue(crawlerURL.getDepth() == 1);
    assertTrue(crawlerURL.getRedirNum() == 1);
    assertEquals("http://localhost/", crawlerURL.getURLString());
    
    crawlerURL = new CrawlerURL(-1, "http://local%20host");
    assertTrue(crawlerURL.getDepth() == -1);
    assertTrue(crawlerURL.getRedirNum() == 0);
    assertEquals("http://local%20host/", crawlerURL.getURLString());

    crawlerURL = new CrawlerURL(-10, "http://localhost");
    assertTrue(crawlerURL.getDepth() == -10);
    assertTrue(crawlerURL.getRedirNum() == 0);
    assertEquals("http://localhost/", crawlerURL.getURLString());
  }

  public void testGetURLString() throws Exception
  {
    assertEquals("http://localhost/", (new CrawlerURL(1, "http://localhost")).getURLString());
    assertEquals("http://localhost/", (new CrawlerURL(1, "http://localhost/")).getURLString());
    assertEquals("http://localhost/", (new CrawlerURL(1, "http://localhost/xxx/./../")).getURLString());
  }

  public void testGetURL() throws Exception
  {
    CrawlerURL crawlerURL = new CrawlerURL(1, "http://localhost");
    assertEquals("http://localhost/", crawlerURL.getURL().toString());
    
    crawlerURL = new CrawlerURL(1, "http://localhost/xxx/././../yyy/");
    assertEquals("http://localhost/yyy/", crawlerURL.getURL().toString());
  }

  public void testGetURI() throws Exception
  {
    CrawlerURL crawlerURL = new CrawlerURL(1, "http://localhost");
    assertEquals("http://localhost/", crawlerURL.getURI().toString());
    
    crawlerURL = new CrawlerURL(1, "http://localhost/xxx/././../yyy/");
    assertEquals("http://localhost/yyy/", crawlerURL.getURI().toString());

  }

  public void testGetDepthLimit() throws Exception
  {
    CrawlerURL crawlerURL = new CrawlerURL(1, "http://localhost");
    
    crawlerURL = new CrawlerURL(1, "http://localhost");
  }

  public void testGetHost() throws Exception
  {
    CrawlerURL crawlerURL = new CrawlerURL(1, "http://localhost");
    assertEquals("localhost", crawlerURL.getHost());
  }

  public void testGetPath() throws Exception
  {
    CrawlerURL crawlerURL = new CrawlerURL(1, "http://localhost/");
    assertEquals("/", crawlerURL.getPath());
    
    crawlerURL = new CrawlerURL(1, "http://localhost");
    assertEquals("/", crawlerURL.getPath());
    
    crawlerURL = new CrawlerURL(1, "http://localhost/some/path/");
    assertEquals("/some/path/", crawlerURL.getPath());
    
    crawlerURL = new CrawlerURL(1, "http://localhost/some/../path/");
    assertEquals("/path/", crawlerURL.getPath());

  }

  public void testGetProtocol() throws Exception
  {
    assertEquals("http", (new CrawlerURL(0, "http://localhost/")).getProtocol());
    assertEquals("http", (new CrawlerURL(0, "hTtp://localhost/")).getProtocol());
  }

  public void testGetPort() throws Exception
  {
    assertEquals(-1, (new CrawlerURL(0, "http://localhost/")).getPort());
    assertEquals(8080, (new CrawlerURL(0, "http://localhost:8080/")).getPort());
  }

  public void testResolveAsChild() throws Exception
  {
    CrawlerURL crawlerURL = new CrawlerURL(1, "http://localhost");
    
    try {
      crawlerURL.resolveAsChild(null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      crawlerURL.resolveAsChild("invalid relative uri");
      fail();
    } catch (MalformedCrawlerURLException e) {
    }
    
    CrawlerURL resolved = crawlerURL.resolveAsChild("././some/../path/.././.");
    assertEquals("http://localhost/", resolved.getURLString());    
  }

  public void testDBSerialization() throws Exception
  {
    CrawlerURL crawlerURL = new CrawlerURL(1, "http://localhost/xxx/..");
    
    createFactory();
    genericDBWithDups.open();
    
    genericDBWithDups.put("xxx", crawlerURL);
    CrawlerURL deser = (CrawlerURL)genericDBWithDups.get("xxx");
//    System.out.println(deser);
//    System.out.println(crawlerURL);
    assertEquals(crawlerURL, deser);
    
    genericDBWithDups.close();
    closeAndRemoveFactory();
  }
  
  public void testRemoveFragment() throws Exception
  {
    CrawlerURL crawlerURL = new CrawlerURL(1, "http://localhost#fragment1");
    assertEquals("http://localhost", crawlerURL.removeFragment("http://localhost#fragment1"));
    
    assertEquals("http://localhost", crawlerURL.removeFragment("http://localhost"));
  }

  /*
   * Class under test for boolean equals(Object)
   */
  public void testEqualsObject()
  {
    // tested in other tests
  }

}
