package com.porva.crawler.net.apache;

import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.net.HttpClientException;
import com.porva.crawler.net.Response;

public class ApacheHttpClientServiceTest extends TestCase
{
  Map<String, String> settings = new HashMap<String, String>();

  ApacheHttpService httpClient = new ApacheHttpService();

  public void testApacheHttpClientService()
  {
    try {
      new ApacheHttpService(null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      settings.put("http.responsebody-limit", "10xy0");
      new ApacheHttpService(settings);
      fail("Invalid settings: http.responsebody-limit key shoud have Integer value.");
    } catch (NumberFormatException e) {
    }

    settings.put("http.responsebody-limit", "1000");
    settings.put("http.useragent", "My Crawler");
    settings.put("http.protocol.max-redirects", "2");
    settings.put("http.socket.timeout", "1000");
    settings.put("http.connection-manager.max-total", "10");

    new ApacheHttpService(settings);
  }

  public void testGet() throws Exception
  {
    // invalid params
    try {
      httpClient.get(null);
    } catch (NullArgumentException e) {
    }
    try {
      httpClient.get(new CrawlerURL(1, "ftp://localhost"));
      fail("ftp protocol is unsupported by http client");
    } catch (HttpClientException e) {
    }
    // get from dead host
    try {
      httpClient.get(new CrawlerURL(1, "http://unknown.host"));
      fail("Cannot get from dead host.");
    } catch (HttpClientException e) {
    }

    // get "page not found"
    Response response = httpClient.get(new CrawlerURL(1, "http://localhost/unknown.page"));
    assertTrue(response.getCode() == 404);
    assertNotNull("This should be 'page not found message' from server", response.getBody());

    // get available page
    response = httpClient.get(new CrawlerURL(1, "http://localhost/tmp/links.html"));
    assertTrue(response.getCode() == 200);
  }

  public void testNewCopyInstance() throws Exception
  {
    settings.put("http.responsebody-limit", "121");
    settings.put("http.useragent", "My Crawler");
    settings.put("http.protocol.max-redirects", "2");
    settings.put("http.socket.timeout", "1000");
    settings.put("http.connection-manager.max-total", "10");

    httpClient = new ApacheHttpService(settings);
    ApacheHttpService copyService = (ApacheHttpService) httpClient.newCopyInstance();
    Response response = copyService.get(new CrawlerURL(1, "http://localhost/tmp/links.html"));
    assertTrue(response.getBody().length == 121);
  }

  public void testStop() throws Exception
  {
    httpClient.stop();
    httpClient.stop();
  }

  public void testGetServiceName()
  {
    assertEquals("workerservice.httpClient", httpClient.getServiceName());
  }

  public void testIsStopped() throws Exception
  {
    assertFalse(httpClient.isStopped());
    httpClient.stop();
    assertTrue(httpClient.isStopped());
    httpClient.stop();
    assertTrue(httpClient.isStopped());
  }

}
