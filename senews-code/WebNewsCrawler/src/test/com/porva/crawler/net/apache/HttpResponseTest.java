package com.porva.crawler.net.apache;

import junit.framework.TestCase;

import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.net.Response;

import static com.porva.crawler.db.DBTestSettings.*;

public class HttpResponseTest extends TestCase
{
  private static org.apache.commons.httpclient.HttpClient httpClient;

  protected void setUp()
  {
    httpClient = new org.apache.commons.httpclient.HttpClient();
  }

  public void testHttpResponse() throws Exception
  {
    try {
      new HttpResponse(null, 200, 10000, null);
      fail();
    } catch (NullArgumentException e) {}

    CrawlerURL crawlerURL = new CrawlerURL(1, "http://localhost/tmp/links.html");
    GetMethod getMethod = new GetMethod("http://localhost/tmp/links.html");
    int statusCode = httpClient.executeMethod(getMethod);
    HttpResponse response = new HttpResponse(getMethod, statusCode, 1000, crawlerURL);

    assertNotNull(response);
    assertTrue(response.getCode() == 200);
    assertTrue(response.getHeaderValue("content-type").startsWith("text/html"));
    assertTrue(response.getBody().length == 1000);
    assertTrue(response.getHeaders().size() > 0);
    
    getMethod.releaseConnection();
    assertTrue(response.getTimeWhenDisconnected() <= System.currentTimeMillis());
    
    // test as DBValue
    createFactory();
    genericDBWithDups.open();
    genericDBWithDups.put("xxx", response);
    Response resFromDB = (Response)genericDBWithDups.get("xxx");
    assertEquals(response, resFromDB);
    genericDBWithDups.close();
    closeAndRemoveFactory();
  }

}
