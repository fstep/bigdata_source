//package com.porva.crawler.net.nutch;
//
//import static com.porva.crawler.db.DBTestSettings.closeAndRemoveFactory;
//import static com.porva.crawler.db.DBTestSettings.createFactory;
//import static com.porva.crawler.db.DBTestSettings.genericDBWithDups;
//
//import java.net.URL;
//
//import junit.framework.TestCase;
//import net.nutch.net.protocols.http.Http;
//
//import org.apache.commons.lang.NullArgumentException;
//
//import com.porva.crawler.net.CrawlerURL;
//import com.porva.crawler.net.Response;
//
//public class HttpResponseTest extends TestCase
//{
//
//  public void testHttpResponse() throws Exception
//  {
//    try {
//      HttpResponse response = new HttpResponse(null, null);
//      fail();
//    } catch (NullArgumentException e) {}
//    
//    Http http = new Http();
//    CrawlerURL crawlerURL = new CrawlerURL(1, "http://localhost/tmp/links.html"); 
//    net.nutch.net.protocols.Response nutchResponse = http
//        .getResponse(new URL("http://localhost/tmp/links.html"));     
//    HttpResponse response = new HttpResponse(nutchResponse, crawlerURL);
//    
//    assertNotNull(response);
//    System.out.println(response);
//    assertTrue(response.getCode() == 200);
//    assertTrue(response.getHeaderValue("content-type").startsWith("text/html"));
//    assertTrue(response.getBody().length > 100);
//    assertTrue(response.getTimeWhenDisconnected() <= System.currentTimeMillis());
//    
//    // test as DBValue
//    createFactory();
//    genericDBWithDups.open();
//    genericDBWithDups.put("xxx", response);
//    Response resFromDB = (Response)genericDBWithDups.get("xxx");
//    assertEquals(response, resFromDB);
//    genericDBWithDups.close();
//    closeAndRemoveFactory();
//  }
//
//}
