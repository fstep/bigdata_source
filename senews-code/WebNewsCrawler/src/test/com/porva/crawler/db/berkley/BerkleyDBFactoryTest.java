package com.porva.crawler.db.berkley;

import static com.porva.crawler.db.DBTestSettings.dbFactory;
import static com.porva.crawler.db.DBTestSettings.failedFetchResult;
import static com.porva.crawler.db.DBTestSettings.failedFetchResult2;
import static com.porva.crawler.db.DBTestSettings.invalidDBFile;
import static com.porva.crawler.db.DBTestSettings.resultDB;
import static com.porva.crawler.db.DBTestSettings.robotsDB;
import static com.porva.crawler.db.DBTestSettings.robotsFailedFetchResult;
import static com.porva.crawler.db.DBTestSettings.robotsFailedFetchResult2;
import static com.porva.crawler.db.DBTestSettings.statusDB;
import static com.porva.crawler.db.DBTestSettings.taskDB;
import static com.porva.crawler.db.DBTestSettings.testDBFile;
import junit.framework.TestCase;

import com.porva.crawler.db.DB;
import com.porva.crawler.db.DBException;
import com.porva.crawler.db.DBValue;
import com.porva.crawler.task.FetchRobotxtResult;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskResult;
import com.porva.crawler.workload.WorkloadEntryStatus;

public class BerkleyDBFactoryTest extends TestCase
{
  // public static final File newDBFile = new File("test/newDb");
  // public static final File testDBFile = new File("test/testDb");

  public void testBerkleyDBFactory() throws Exception
  {
    try {
      new BerkleyCrawlerDBFactory(invalidDBFile);
      fail("Cannot create factory with invalid home directory");
    } catch (DBException e) {
    }

    BerkleyCrawlerDBFactory fac1 = new BerkleyCrawlerDBFactory(testDBFile);
    assertFalse("Newly created factory is opened", fac1.isClosed());
    BerkleyCrawlerDBFactory fac2 = new BerkleyCrawlerDBFactory(testDBFile);
    assertFalse("Newly created factory is opened", fac2.isClosed());

    fac1.close();
    assertTrue(fac1.isClosed());
    fac2.close();
    assertTrue(fac2.isClosed());

    assertTrue("removes home dir of factory", fac1.removeAll());
    assertFalse("home dir was already removed", fac2.removeAll());
  }

  public void testNewResultDatabaseHandle() throws Exception
  {
    dbFactory = new BerkleyCrawlerDBFactory(testDBFile);
    dbFactory.close();
    try {
      resultDB = dbFactory.newResultDatabaseHandle();
      fail("Cannot create db handle from closed factory.");
    } catch (IllegalStateException e) {
    }

    dbFactory = new BerkleyCrawlerDBFactory(testDBFile);
    resultDB = dbFactory.newResultDatabaseHandle();
    assertNotNull(resultDB);
    assertFalse(resultDB.isOpened());
    resultDB.open();
    assertTrue(resultDB.isOpened());

    // lets populate out db with 2 duplicates records
    resultDB.put(failedFetchResult.getDBKey(), failedFetchResult);
    resultDB.put(failedFetchResult.getDBKey(), failedFetchResult2);
    assertTrue("result db can have duplicate records", resultDB.getRecordsNum() == 2);

    // open another db handle for the same db
    DB<TaskResult> theSameDb = dbFactory.newResultDatabaseHandle();
    assertFalse(theSameDb.isOpened());
    theSameDb.open();
    assertTrue(theSameDb.isOpened());
    assertTrue("another handle points to out db and have 2 records", theSameDb.getRecordsNum() == 2);

    theSameDb.close();
    resultDB.close();
    dbFactory.close();
  }

  public void testNewRobotxtDatabaseHandle() throws Exception
  {
    dbFactory = new BerkleyCrawlerDBFactory(testDBFile);
    dbFactory.close();
    try {
      robotsDB = dbFactory.newRobotxtDatabaseHandle();
      fail("Cannot create db handle from closed factory.");
    } catch (IllegalStateException e) {
    }

    dbFactory = new BerkleyCrawlerDBFactory(testDBFile);
    robotsDB = dbFactory.newRobotxtDatabaseHandle();
    assertNotNull(robotsDB);
    assertFalse(robotsDB.isOpened());
    robotsDB.open();
    assertTrue(robotsDB.isOpened());

    // lets populate out db with 2 duplicates records
    robotsDB.put(robotsFailedFetchResult.getDBKey(), robotsFailedFetchResult);
    robotsDB.put(robotsFailedFetchResult.getDBKey(), robotsFailedFetchResult2);
    assertTrue("robots db cannot have duplicate records", robotsDB.getRecordsNum() == 1);

    // open another db handle for the same db
    DB<FetchRobotxtResult> theSameDb = dbFactory.newRobotxtDatabaseHandle();
    assertFalse(theSameDb.isOpened());
    theSameDb.open();
    assertTrue(theSameDb.isOpened());
    assertTrue("another handle points to out db and have 1 record", theSameDb.getRecordsNum() == 1);

    theSameDb.close();
    robotsDB.close();
    dbFactory.close();
  }

  public void testNewTaskDatabaseHandle() throws Exception
  {
    dbFactory = new BerkleyCrawlerDBFactory(testDBFile);
    dbFactory.close();
    try {
      taskDB = dbFactory.newTaskDatabaseHandle();
      fail("Cannot create db handle from closed factory.");
    } catch (IllegalStateException e) {
    }

    dbFactory = new BerkleyCrawlerDBFactory(testDBFile);
    taskDB = dbFactory.newTaskDatabaseHandle();
    assertNotNull(taskDB);
    assertFalse(taskDB.isOpened());
    taskDB.open();
    assertTrue(taskDB.isOpened());

    // lets populate out db with 2 duplicates records
    taskDB.put("x", failedFetchResult.getTask());
    taskDB.put("x", failedFetchResult.getTask()); // this is the same record -- it wont be added
    taskDB.put("x", failedFetchResult2.getTask());
    assertTrue("task db may have duplicate records", taskDB.getRecordsNum() == 2);

    // open another db handle for the same db
    DB<Task> theSameDb = dbFactory.newTaskDatabaseHandle();
    assertFalse(theSameDb.isOpened());
    theSameDb.open();
    assertTrue(theSameDb.isOpened());
    assertTrue("another handle points to out db and have 2 records", theSameDb.getRecordsNum() == 2);

    theSameDb.close();
    taskDB.close();
    dbFactory.close();
  }

  public void testNewStatusDatabaseHandle() throws Exception
  {
    dbFactory = new BerkleyCrawlerDBFactory(testDBFile);
    dbFactory.close();
    try {
      statusDB = dbFactory.newStatusDatabaseHandle();
      fail("Cannot create db handle from closed factory.");
    } catch (IllegalStateException e) {
    }

    dbFactory = new BerkleyCrawlerDBFactory(testDBFile);
    statusDB = dbFactory.newStatusDatabaseHandle();
    assertNotNull(statusDB);
    assertFalse(statusDB.isOpened());
    statusDB.open();
    assertTrue(statusDB.isOpened());

    // lets populate out db with 2 duplicates records
    statusDB.put("x", WorkloadEntryStatus.COMPLETE);
    statusDB.put("x", WorkloadEntryStatus.COMPLETE); // this is the same record -- it wont be added
    statusDB.put("x", WorkloadEntryStatus.FAILED);
    assertTrue("status db cannot have duplicate records", statusDB.getRecordsNum() == 1);

    // open another db handle for the same db
    DB<WorkloadEntryStatus> theSameDb = dbFactory.newStatusDatabaseHandle();
    assertFalse(theSameDb.isOpened());
    theSameDb.open();
    assertTrue(theSameDb.isOpened());
    assertTrue("another handle points to out db and have 2 records", theSameDb.getRecordsNum() == 1);
    assertEquals(WorkloadEntryStatus.FAILED, theSameDb.get("x"));

    theSameDb.close();
    statusDB.close();
    dbFactory.close();
  }
  
  public void testClose() throws Exception
  {
    dbFactory = new BerkleyCrawlerDBFactory(testDBFile);
    DB<TaskResult> db = dbFactory.newResultDatabaseHandle();
    dbFactory.close();
    try {
      dbFactory.close();
      fail("cannot close already closed factory");
    } catch (IllegalStateException e) {
    }
    assertTrue(dbFactory.removeAll());
    assertTrue(dbFactory.isClosed());

    // now out factory is closed and we cannot operata with db handles
    try {
      db.open();
      fail("Cannot open database after enviroment has been closed.");
    } catch (DBException e) {
    }

    try {
      db.close();
      fail("Cannot close enviroment after is has been closed.");
    } catch (IllegalStateException e) {
    }
  }

  public void testRemoveAll() throws DBException
  {
    dbFactory = new BerkleyCrawlerDBFactory(testDBFile);
    try {
      dbFactory.removeAll();
      fail("Cannot delete opened factory.");
    } catch (IllegalStateException e) {
    }
    dbFactory.close();

    assertTrue(dbFactory.removeAll());
    assertFalse(dbFactory.removeAll());
  }

  public void testRemoveDatabase() throws Exception
  {
    dbFactory = new BerkleyCrawlerDBFactory(testDBFile);
    DB<TaskResult> db = dbFactory.newResultDatabaseHandle();
    db.open();

    try {
      dbFactory.removeDatabase(db.getDBName());
      fail("Cannot remove database from becase there are its opened handles.");
    } catch (DBException e) {}

    db.close();
    dbFactory.removeDatabase(db.getDBName());
    dbFactory.close();
    dbFactory.removeAll();
  }
  
  public void testIsClosed() throws Exception
  {
    dbFactory = new BerkleyCrawlerDBFactory(testDBFile);
    assertFalse("created factory is opened", dbFactory.isClosed());
    
    dbFactory.close();
    assertTrue(dbFactory.isClosed());
    assertTrue(dbFactory.isClosed());
  }

}
