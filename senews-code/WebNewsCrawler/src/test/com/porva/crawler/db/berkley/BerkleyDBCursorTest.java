package com.porva.crawler.db.berkley;

import com.porva.crawler.db.DBCursor;
import com.porva.crawler.db.DBException;
import com.porva.crawler.db.DBRecord;
import com.porva.crawler.task.TaskResult;

import junit.framework.TestCase;

import static com.porva.crawler.db.DBTestSettings.*;

public class BerkleyDBCursorTest extends TestCase
{
  protected void setUp() throws Exception
  {
    createFactory();
  }

  protected void tearDown() throws Exception
  {
    assertTrue(closeAndRemoveFactory());
  }

  public void testBerkleyDBCursor() throws Exception
  {
    try {
      resultDB.getCursor();
      fail("Cannot get cursor from closed or unopened database.");
    } catch (IllegalStateException e) {
    }

    resultDB.open();
    DBCursor<TaskResult> cursor = resultDB.getCursor();
    cursor.close();
    
    resultDB.open();
    cursor = resultDB.getCursor();
    cursor.close();

    resultDB.close();
  }

  public void testClose() throws Exception
  {
    resultDB.open();
    DBCursor<TaskResult> cursor = resultDB.getCursor();
    cursor.close();

    try {
      cursor.getNext();
      fail("Cursor has been closed.");
    } catch (DBException e) {}
    
    try {
      cursor.getFirst();
      fail("Cursor has been closed.");
    } catch (DBException e) {}

    try {
      cursor.getLast();
      fail("Cursor has been closed.");
    } catch (DBException e) {}
    
    try {
      cursor.getPrev();
      fail("Cursor has been closed.");
    } catch (DBException e) {}

    try {
      cursor.close();
      fail("Cursor has been closed.");
    } catch (DBException e) {}
    
    resultDB.close();
  }

  public void testGetNext() throws Exception
  {
    resultDB.open();
    DBCursor<TaskResult> cursor = resultDB.getCursor();
    
    assertNull("next record of empty db should be null", cursor.getNext());
    
    resultDB.put(failedFetchResult.getDBKey(), failedFetchResult);
    DBRecord<TaskResult> first = cursor.getFirst();
    assertEquals(failedFetchResult, first.getDBValue());
    assertNull(cursor.getNext());
    
    assertTrue(resultDB.getRecordsNum() == 1);
    resultDB.put(failedFetchResult2.getDBKey(), failedFetchResult2);
    assertTrue(resultDB.getRecordsNum() == 2);
    first = cursor.getFirst();
    assertEquals(failedFetchResult, first.getDBValue());
    assertEquals(failedFetchResult2, cursor.getNext().getDBValue());
    assertNull(cursor.getNext());
    
    cursor.close();
    resultDB.close();
  }

  public void testGetFirst() throws Exception
  {
    resultDB.open();
    DBCursor<TaskResult> cursor = resultDB.getCursor();
    
    assertNull("First record of empty db should be null", cursor.getFirst());
    
    resultDB.put(failedFetchResult.getDBKey(), failedFetchResult);
    assertTrue("At this point we have one record in databse", resultDB.getRecordsNum() == 1);
    assertEquals(failedFetchResult, cursor.getFirst().getDBValue());
    assertEquals(failedFetchResult, cursor.getFirst().getDBValue());
    
    cursor.close();
    resultDB.close();
  }

  public void testGetLast() throws Exception
  {
    resultDB.open();
    DBCursor<TaskResult> cursor = resultDB.getCursor();
    
    assertNull("Last record of empty db should be null", cursor.getLast());
    
    resultDB.put(failedFetchResult.getDBKey(), failedFetchResult);
    assertTrue("At this point we have one record in databse", resultDB.getRecordsNum() == 1);
    assertEquals(failedFetchResult, cursor.getLast().getDBValue());
    assertEquals(failedFetchResult, cursor.getLast().getDBValue());
    
    cursor.close();
    resultDB.close();
  }

  public void testGetPrev() throws Exception
  {
    resultDB.open();
    DBCursor<TaskResult> cursor = resultDB.getCursor();
    
    assertNull("Prev record of empty db should be null", cursor.getPrev());
    
    resultDB.put(failedFetchResult.getDBKey(), failedFetchResult);
    DBRecord<TaskResult> first = cursor.getFirst();
    assertEquals(failedFetchResult, first.getDBValue());
    assertNull("prev record of first record should be null", cursor.getPrev());
    
    assertTrue("At this point we have one record in databse", resultDB.getRecordsNum() == 1);
    resultDB.put(failedFetchResult2.getDBKey(), failedFetchResult2);
    assertTrue("Now we have two records in db", resultDB.getRecordsNum() == 2);
    assertEquals("Move to first record", failedFetchResult, cursor.getFirst().getDBValue());
    assertEquals("Move to seconds record", failedFetchResult2, cursor.getNext().getDBValue());
    assertEquals("Move back to first record", failedFetchResult, cursor.getPrev().getDBValue());
    assertNull("Prev record of empty db should be null", cursor.getPrev());
    
    cursor.close();
    resultDB.close();
  }

}
