package com.porva.crawler.db;

import java.io.File;

import org.apache.commons.lang.UnhandledException;

import com.porva.crawler.db.berkley.BerkleyCrawlerDBFactory;
import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.net.MalformedCrawlerURLException;
import com.porva.crawler.task.DefaultFetchResult;
import com.porva.crawler.task.FetchResult;
import com.porva.crawler.task.FetchRobotxtResult;
import com.porva.crawler.task.FetchTask;
import com.porva.crawler.task.*;
import com.porva.crawler.task.TaskResult;
import static com.porva.crawler.task.FetchResult.Status.*;
import com.porva.crawler.task.TaskFactory;
import com.porva.crawler.workload.WorkloadEntryStatus;

public class DBTestSettings
{
  public static final File testDBFile = new File("test/testDb");

  public static final File invalidDBFile = new File("test/dont.delete");

  public static BerkleyCrawlerDBFactory dbFactory;

  public static DB<TaskResult> resultDB;

  public static DB<FetchRobotxtResult> robotsDB;

  public static DB<Task> taskDB;

  public static DB<WorkloadEntryStatus> statusDB;

  public static DB<DBValue> genericDBWithDups;

  public String testDbTupleKey = "http://test.url";

  public static FetchResult failedFetchResult;

  public static FetchResult failedFetchResult2;

  public static FetchRobotxtResult robotsFailedFetchResult;

  public static FetchRobotxtResult robotsFailedFetchResult2;

  static {
    try {
      // clean up factory first
      createFactory();
      closeAndRemoveFactory();

      // create some values
      failedFetchResult = DefaultFetchResult.newFailedResult((FetchTask) TaskFactory
          .newFetchURL(new CrawlerURL(1, "http://localhost")), TASK_FILTERED);

      failedFetchResult2 = DefaultFetchResult.newFailedResult((FetchTask) TaskFactory
          .newFetchURL(new CrawlerURL(1, "http://localhost/new")), TASK_FILTERED);

      robotsFailedFetchResult = new FetchRobotxtResult((FetchTask) TaskFactory
          .newFetchRobotxt(new CrawlerURL(1, "http://localhost/robots.txt")), TASK_FILTERED, null);

      robotsFailedFetchResult2 = new FetchRobotxtResult((FetchTask) TaskFactory
          .newFetchURL(new CrawlerURL(1, "http://google.com/robots.txt")), TASK_FILTERED, null);

    } catch (MalformedCrawlerURLException e) {
      throw new UnhandledException(e);
    } catch (DBException e) {
      throw new UnhandledException(e);
    }
  }

  public static void createFactory() throws DBException
  {
    dbFactory = new BerkleyCrawlerDBFactory(testDBFile);
    resultDB = dbFactory.newResultDatabaseHandle();
    robotsDB = dbFactory.newRobotxtDatabaseHandle();
    taskDB = dbFactory.newTaskDatabaseHandle();
    statusDB = dbFactory.newStatusDatabaseHandle();
    genericDBWithDups = dbFactory.newGenericDatabaseHandle(true);
  }
  
  public static void closeFactory() throws DBException
  {
    dbFactory.close();
    dbFactory = null;
    resultDB = null;
    robotsDB = null;
    taskDB = null;
    statusDB = null;
    genericDBWithDups = null;
  }

  public static boolean closeAndRemoveFactory() throws DBException
  {
    dbFactory.close();
    boolean isRemoved = dbFactory.removeAll();
    dbFactory = null;
    resultDB = null;
    robotsDB = null;
    taskDB = null;
    statusDB = null;
    genericDBWithDups = null;
    return isRemoved;
  }

  // DefaultFetchResult testDbTuple = DefaultFetchResult.newFailedResult(TaskFactory.FETCH_URL,
  // testDbTupleKey,
  // 12345,
  // null, null);
  //
  // DefaultFetchResult testDbTupleDup = DefaultFetchResult.newFailedResult(TaskFactory.FETCH_URL,
  // testDbTupleKey,
  // 12346, null, null);

}
