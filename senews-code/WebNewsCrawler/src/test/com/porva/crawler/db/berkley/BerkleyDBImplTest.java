package com.porva.crawler.db.berkley;

import static com.porva.crawler.db.DBTestSettings.closeAndRemoveFactory;
import static com.porva.crawler.db.DBTestSettings.createFactory;
import static com.porva.crawler.db.DBTestSettings.dbFactory;
import static com.porva.crawler.db.DBTestSettings.failedFetchResult;
import static com.porva.crawler.db.DBTestSettings.failedFetchResult2;
import static com.porva.crawler.db.DBTestSettings.resultDB;
import static com.porva.crawler.db.DBTestSettings.robotsFailedFetchResult;
import static com.porva.crawler.db.DBTestSettings.robotsFailedFetchResult2;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.db.DBException;
import com.porva.crawler.db.DBFunctor;
import com.porva.crawler.db.DBOperationStatus;
import com.porva.crawler.task.DefaultFetchResult;
import com.porva.crawler.task.TaskResult;

public class BerkleyDBImplTest extends TestCase
{
  protected void setUp() throws Exception
  {
    createFactory();
  }

  protected void tearDown() throws Exception
  {
    closeAndRemoveFactory();
  }

  public void testOpen() throws Exception
  {
    dbFactory.close();
    try {
      resultDB.open();
      fail("Cannot open database with closed envirment.");
    } catch (DBException e) {
    }

    createFactory();
    resultDB.open();
    assertTrue("newly created database is empty", resultDB.getRecordsNum() == 0);
    assertTrue(resultDB.isOpened());

    // try to open again
    resultDB.open(); // should produce warnings but it is still ok
    resultDB.close();
    assertFalse(resultDB.isOpened());

    // try normal open-close operations
    resultDB.open();
    assertTrue(resultDB.isOpened());
    resultDB.close();
  }

  public void testClose() throws Exception
  {
    try {
      resultDB.close();
      fail("Cannot close database which is not opened.");
    } catch (IllegalStateException e) {
    }

    // try normal open-close operations
    resultDB.open();
    resultDB.close();
    assertFalse(resultDB.isOpened());
  }

  public void testPut() throws Exception
  {
    // invalid constructions
    try {
      resultDB.put(null, null);
      fail("null params not allowed");
    } catch (NullArgumentException e) {
    }

    try {
      resultDB.put(failedFetchResult.getDBKey(), failedFetchResult);
      fail("Cannot put recored to unopened database");
    } catch (IllegalStateException e) {
    }

    resultDB.open();
    assertTrue(
               "Adding one record to database",
               resultDB.put(failedFetchResult.getDBKey(), failedFetchResult) == DBOperationStatus.SUCCESS);
    assertTrue("Now we should have one record in db", resultDB.getRecordsNum() == 1);

    assertTrue(
               "we can store the same record",
               resultDB.put(failedFetchResult.getDBKey(), failedFetchResult) == DBOperationStatus.SUCCESS);
    assertTrue("but still have one unique record", resultDB.getRecordsNum() == 1);

    assertTrue(
               "we can store records with the same key",
               resultDB.put(failedFetchResult.getDBKey(), failedFetchResult2) == DBOperationStatus.SUCCESS);
    assertTrue("and have 2 records new", resultDB.getRecordsNum() == 2);

    resultDB.close();
  }

  public void testPutNoOverwrite() throws Exception
  {
    try {
      resultDB.putNoOverwrite(null, null);
      fail("null params not allowed");
    } catch (NullArgumentException e) {
    }

    try {
      resultDB.put(failedFetchResult.getDBKey(), failedFetchResult);
      fail("Cannot put recored to unopened database");
    } catch (IllegalStateException e) {
    }

    resultDB.open();
    assertTrue("add first record should be SUCCESS", resultDB.putNoOverwrite(failedFetchResult
        .getDBKey(), failedFetchResult) == DBOperationStatus.SUCCESS);
    assertTrue(resultDB.getRecordsNum() == 1);

    assertTrue(
               "adding record with the same key should return KEYEXIST",
               resultDB.putNoOverwrite(failedFetchResult.getDBKey(), failedFetchResult2) == DBOperationStatus.KEYEXIST);
    assertTrue("we still have one record", resultDB.getRecordsNum() == 1);
    resultDB.close();
  }

  public void testGet() throws Exception
  {
    try {
      resultDB.get(null);
      fail("Cannot get null key");
    } catch (NullArgumentException e) {
    }

    try {
      resultDB.get(failedFetchResult.getDBKey());
      fail("Cannot get from unopened database");
    } catch (IllegalStateException e) {
    }

    resultDB.open();
    assertNull((DefaultFetchResult) resultDB.get(failedFetchResult.getDBKey()));
    resultDB.put(failedFetchResult.getDBKey(), failedFetchResult);
    System.out.println(failedFetchResult);
    System.out.println(resultDB.get(failedFetchResult.getDBKey()));
    assertEquals(failedFetchResult, resultDB.get(failedFetchResult.getDBKey()));
    resultDB.close();
  }

  public void testGetAllDup() throws Exception
  {
    try {
      resultDB.getAllDup(null, new ArrayList<TaskResult>());
      fail("Cannot use null key to getAllDup");
    } catch (NullArgumentException e) {
    }
    try {
      resultDB.getAllDup("test", null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      resultDB.getAllDup("test", new ArrayList<TaskResult>());
      fail("Cannot getAllDUp from unopened database");
    } catch (IllegalStateException e) {
    }

    resultDB.open();
    List<TaskResult> res = new ArrayList<TaskResult>();
    assertTrue("getting all duplicates from empty db", resultDB.getAllDup(failedFetchResult
        .getDBKey(), res) == DBOperationStatus.NOTFOUND);
    assertTrue("found nothing", res.size() == 0);
    assertTrue(
               "lets add some value to db",
               resultDB.put(failedFetchResult.getDBKey(), failedFetchResult) == DBOperationStatus.SUCCESS);
    res.clear();
    resultDB.getAllDup(failedFetchResult.getDBKey(), res);
    assertTrue("how we should have one record", res.size() == 1);
    assertTrue(
               "adding duplicate value",
               resultDB.put(failedFetchResult.getDBKey(), failedFetchResult2) == DBOperationStatus.SUCCESS);
    res.clear();
    resultDB.getAllDup(failedFetchResult.getDBKey(), res);
    assertTrue("now it shoud be 2 values", resultDB.getRecordsNum() == 2);
    assertTrue(res.size() == 2);
    assertEquals("check value 1", failedFetchResult, res.get(0));
    assertEquals("check value 2", failedFetchResult2, res.get(1));
    resultDB.close();
  }

  public void testIsOpened() throws Exception
  {
    assertFalse(resultDB.isOpened());
    resultDB.open();
    assertTrue(resultDB.isOpened());
    resultDB.close();
    assertFalse(resultDB.isOpened());
  }

  private class CounterFunctor implements DBFunctor<TaskResult>
  {
    public int num = 0;

    public void process(String key, TaskResult value)
    {
      num++;
    }
  }

  public void testIterateOverAll() throws Exception
  {
    try {
      resultDB.iterateOverAll(null);
      fail("null functor is not allowed");
    } catch (NullArgumentException e) {
    }

    CounterFunctor counterFunctor = new CounterFunctor();
    try {
      resultDB.iterateOverAll(counterFunctor);
      fail("Cannot iterete over unopened database");
    } catch (IllegalStateException e) {
    }

    resultDB.open();
    resultDB.iterateOverAll(counterFunctor);
    assertTrue("functor should found 0 records in empty db", counterFunctor.num == 0);

    assertTrue(
               "adding one record",
               resultDB.put(failedFetchResult.getDBKey(), failedFetchResult) == DBOperationStatus.SUCCESS);
    assertTrue(
               "adding duplicate record",
               resultDB.put(failedFetchResult.getDBKey(), failedFetchResult2) == DBOperationStatus.SUCCESS);
    resultDB.iterateOverAll(counterFunctor);
    assertTrue("now functor should found 2 records", counterFunctor.num == 2);
    resultDB.close();
  }

  public void testDelete() throws Exception
  {
    try {
      resultDB.delete(null, null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      resultDB.delete(failedFetchResult.getDBKey(), failedFetchResult);
      fail("Cannot delete record from unopened database");
    } catch (IllegalStateException e) {
    }

    resultDB.open();
    assertTrue(resultDB.getRecordsNum() == 0);
    assertTrue(resultDB.delete(failedFetchResult.getDBKey(), failedFetchResult) == DBOperationStatus.NOTFOUND);
    assertTrue(resultDB.put(failedFetchResult.getDBKey(), failedFetchResult) == DBOperationStatus.SUCCESS);
    assertTrue(resultDB.getRecordsNum() == 1);
    assertTrue(resultDB.delete(failedFetchResult.getDBKey(), failedFetchResult) == DBOperationStatus.SUCCESS);
    assertTrue(resultDB.getRecordsNum() == 0);

    assertTrue(resultDB.put(failedFetchResult2.getDBKey(), failedFetchResult2) == DBOperationStatus.SUCCESS);
    assertNotNull(resultDB.get(failedFetchResult2.getDBKey()));
    assertTrue(resultDB.delete(failedFetchResult2.getDBKey(), failedFetchResult2) == DBOperationStatus.SUCCESS);
    assertNull(resultDB.get(failedFetchResult2.getDBKey()));

    //
    assertTrue(resultDB.put(robotsFailedFetchResult.getDBKey(), robotsFailedFetchResult) == DBOperationStatus.SUCCESS);
    assertTrue(resultDB.put(robotsFailedFetchResult.getDBKey(), robotsFailedFetchResult2) == DBOperationStatus.SUCCESS);
    assertNotNull(resultDB.get(robotsFailedFetchResult.getDBKey()));
    assertTrue(resultDB.delete(robotsFailedFetchResult.getDBKey()) == DBOperationStatus.SUCCESS);
    assertNull(resultDB.get(robotsFailedFetchResult.getDBKey()));
    assertNull(resultDB.get(robotsFailedFetchResult2.getDBKey()));

    resultDB.close();
  }
  
  public void testDeleteByEquals() throws Exception
  {
    try {
      resultDB.deleteByEquals(null, null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      resultDB.delete(failedFetchResult.getDBKey(), failedFetchResult);
      fail("Cannot delete record from unopened database");
    } catch (IllegalStateException e) {
    }

    resultDB.open();
    assertTrue(resultDB.put(robotsFailedFetchResult.getDBKey(), robotsFailedFetchResult) == DBOperationStatus.SUCCESS);
    assertTrue(resultDB.put(robotsFailedFetchResult.getDBKey(), robotsFailedFetchResult2) == DBOperationStatus.SUCCESS);
    assertNotNull(resultDB.get(robotsFailedFetchResult.getDBKey()));
    assertTrue(resultDB.deleteByEquals(robotsFailedFetchResult.getDBKey(), robotsFailedFetchResult) == DBOperationStatus.SUCCESS);
    assertNotNull(resultDB.get(robotsFailedFetchResult.getDBKey()));
    assertTrue(resultDB.deleteByEquals(robotsFailedFetchResult.getDBKey(), robotsFailedFetchResult) == DBOperationStatus.NOTFOUND);
    resultDB.close();
  }

  public void testGetRecordsNum() throws Exception
  {
    try {
      resultDB.getRecordsNum();
      fail("Cannot get number of records from unopened database");
    } catch (IllegalStateException e) {
    }

    resultDB.open();
    assertTrue(resultDB.getRecordsNum() == 0);
    resultDB.close();
  }

}
