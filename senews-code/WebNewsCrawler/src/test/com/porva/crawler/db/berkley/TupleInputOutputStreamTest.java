package com.porva.crawler.db.berkley;

import static com.porva.crawler.db.DBTestSettings.closeAndRemoveFactory;
import static com.porva.crawler.db.DBTestSettings.createFactory;
import static com.porva.crawler.db.DBTestSettings.taskDB;

import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import org.apache.commons.lang.NotImplementedException;

import com.porva.crawler.db.DBException;
import com.porva.crawler.db.DBInputStream;
import com.porva.crawler.db.DBOutputStream;
import com.porva.crawler.db.DBValue;
import com.porva.crawler.service.ServiceProvider;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskResult;

public class TupleInputOutputStreamTest extends TestCase
{
  // ////////////////////////////////////////////////////////////////

  public static class TestDBValue implements Task
  {
    protected TestDBValue()
    {
    }

    public void writeTo(DBOutputStream tupleOutput)
    {
      assertNotNull(tupleOutput);

      tupleOutput.writeBoolean(true);
      tupleOutput.writeBoolean(false);

      tupleOutput.writeByte(Byte.MIN_VALUE);
      tupleOutput.writeByte((byte) 0);
      tupleOutput.writeByte(Byte.MAX_VALUE);

      tupleOutput.writeChar('x');
      tupleOutput.writeChar(' ');

      tupleOutput.writeInt(Integer.MIN_VALUE);
      tupleOutput.writeInt(0);
      tupleOutput.writeInt(Integer.MAX_VALUE);

      tupleOutput.writeFloat(Float.MIN_VALUE);
      tupleOutput.writeFloat(0.00000F);
      tupleOutput.writeFloat(Float.MAX_VALUE);

      tupleOutput.writeDouble(Double.MIN_VALUE);
      tupleOutput.writeDouble(0.00000D);
      tupleOutput.writeDouble(Double.MAX_VALUE);

      tupleOutput.writeLong(Long.MIN_VALUE);
      tupleOutput.writeLong(000000L);
      tupleOutput.writeLong(Long.MAX_VALUE);

      tupleOutput.writeBytes(null);
      tupleOutput.writeBytes(new byte[] {});
      tupleOutput.writeBytes("xyz".getBytes());

      tupleOutput.writeCompressBytes(null);
      tupleOutput.writeCompressBytes(new byte[] {});
      tupleOutput.writeCompressBytes("xyz".getBytes());

      tupleOutput.writeString(null);
      tupleOutput.writeString("");
      tupleOutput.writeString("string");

      tupleOutput.writeMapOfStrings(null);
      Map<String, String> empty = new HashMap<String, String>();
      tupleOutput.writeMapOfStrings(empty);
      Map<String, String> notEmpty = new HashMap<String, String>();
      notEmpty.put("", "");
      notEmpty.put("x", "");
      notEmpty.put("yy", "zzz");
      tupleOutput.writeMapOfStrings(notEmpty);

    }

    public DBValue newInstance(DBInputStream tupleInput) throws DBException
    {
      assertNotNull(tupleInput);

      assertTrue(tupleInput.readBoolean() == true);
      assertTrue(tupleInput.readBoolean() == false);

      assertTrue(tupleInput.readByte() == Byte.MIN_VALUE);
      assertTrue(tupleInput.readByte() == (byte) 0);
      assertTrue(tupleInput.readByte() == Byte.MAX_VALUE);

      assertTrue(tupleInput.readChar() == 'x');
      assertTrue(tupleInput.readChar() == ' ');

      assertTrue(tupleInput.readInt() == Integer.MIN_VALUE);
      assertTrue(tupleInput.readInt() == 0);
      assertTrue(tupleInput.readInt() == Integer.MAX_VALUE);

      assertTrue(tupleInput.readFloat() == Float.MIN_VALUE);
      assertTrue(tupleInput.readFloat() == 0.00000F);
      assertTrue(tupleInput.readFloat() == Float.MAX_VALUE);

      assertTrue(tupleInput.readDouble() == Double.MIN_VALUE);
      assertTrue(tupleInput.readDouble() == 0.00000D);
      assertTrue(tupleInput.readDouble() == Double.MAX_VALUE);

      assertTrue(tupleInput.readLong() == Long.MIN_VALUE);
      assertTrue(tupleInput.readLong() == 000000L);
      assertTrue(tupleInput.readLong() == Long.MAX_VALUE);

      assertTrue(tupleInput.readBytes() == null);
      assertTrue(tupleInput.readBytes().length == 0);
      assertEquals("xyz", new String(tupleInput.readBytes()));

      assertTrue(tupleInput.readUncompressBytes() == null);
      assertTrue(tupleInput.readUncompressBytes().length == 0);
      assertEquals("xyz", new String(tupleInput.readUncompressBytes()));

      assertTrue(tupleInput.readString() == null);
      assertEquals("", tupleInput.readString());
      assertEquals("string", tupleInput.readString());

      Map<String, String> result = new HashMap<String, String>();
      tupleInput.readMapOfStrings(result);
      assertTrue(result.size() == 0);
      result.clear();
      tupleInput.readMapOfStrings(result);
      assertTrue(result.size() == 0);
      result.clear();
      tupleInput.readMapOfStrings(result);
      assertTrue(result.size() == 3);
      assertEquals(result.get(""), "");
      assertEquals(result.get("x"), "");
      assertEquals(result.get("yy"), "zzz");
      
      return this;
    }

    public long getClassCode()
    {
      return 2355324545032L;
    }

    public TaskResult perform(ServiceProvider serviceProvider)
    {
      throw new NotImplementedException();
    }

    public String getDBKey()
    {
      return "test-task";
    }

    public DBValue getDBValue()
    {
      return this;
    }

  }

  // ////////////////////////////////////////////////////////////////
  protected void setUp() throws Exception
  {
    createFactory();
  }

  protected void tearDown() throws Exception
  {
    closeAndRemoveFactory();
  }

  public void testAll() throws DBException
  {
    TestDBValue testDBValue = new TestDBValue();
    taskDB.open();

    taskDB.put("x", testDBValue);
    System.out.println(taskDB.get("x"));
//    assertEquals(testDBValue, (TestDBValue) taskDB.get("x"));

    taskDB.close();
  }

}
