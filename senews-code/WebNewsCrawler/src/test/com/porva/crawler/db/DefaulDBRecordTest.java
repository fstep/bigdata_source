package com.porva.crawler.db;

import static com.porva.crawler.db.DBTestSettings.failedFetchResult;
import junit.framework.TestCase;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.task.FetchResult;

public class DefaulDBRecordTest extends TestCase
{
  
  public void testDefaulDBRecord()
  {
    try {
      new DefaulDBRecord<FetchResult>(null, failedFetchResult);
      fail();
    } catch (NullArgumentException e) {}
    try {
      new DefaulDBRecord<FetchResult>("xxx", null);
      fail();
    } catch (NullArgumentException e) {}
    
    new DefaulDBRecord<FetchResult>("xxx", failedFetchResult);
  }

  public void testGetDBKey()
  {
    DefaulDBRecord<FetchResult> dr = new DefaulDBRecord<FetchResult>("xxx", failedFetchResult);
    assertEquals("xxx", dr.getDBKey());
  }

  public void testGetDBValue()
  {
    DefaulDBRecord<FetchResult> dr = new DefaulDBRecord<FetchResult>("xxx", failedFetchResult);
    assertEquals(failedFetchResult, dr.getDBValue());
  }

}
