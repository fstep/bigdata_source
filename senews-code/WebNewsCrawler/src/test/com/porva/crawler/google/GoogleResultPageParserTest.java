package com.porva.crawler.google;

import junit.framework.*;
import com.porva.crawler.google.GoogleResultPageParser;
import com.porva.crawler.google.WebSearchResult;

import java.io.File;
import java.io.FileInputStream;

/**
 * Test case for {@link com.porva.crawler.google.GoogleResultPageParser}
 *
 * @author Poroshin V.
 * @date Jan 26, 2006
 */
public class GoogleResultPageParserTest extends TestCase
{
  GoogleResultPageParser googleResultPageParser;

  String googlePage;

  String googlePageFile = System.getProperty("user.dir") + File.separator + "test"
      + File.separator + "google-result.html";

  protected void setUp()
  {
    try {
      File file = new File(googlePageFile);
      FileInputStream in = new FileInputStream(file);
      byte[] buff = new byte[(int) file.length()];
      in.read(buff);

      googlePage = new String(buff);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /**
   * Test for {@link GoogleResultPageParser#parseWebResultPage(String, String)}
   * 
   * @throws Exception
   */
  public void testWebResultPageParser() throws Exception
  {
    googleResultPageParser = new GoogleResultPageParser();
    WebSearchResult webSearchResult = googleResultPageParser.parseWebResultPage(googlePage,
                                                                                "data mining");
    System.out.println(webSearchResult);
    assertEquals(10, webSearchResult.getResults().size());
    assertEquals(121000000, webSearchResult.getTotalResultsNum());
    assertEquals(10, webSearchResult.getResults().size());
    assertEquals("http://www.the-data-mine.com/", webSearchResult.getResults().get(0).getURLString());
  }
}