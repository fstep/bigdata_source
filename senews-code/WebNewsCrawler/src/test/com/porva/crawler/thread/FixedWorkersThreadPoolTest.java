//package com.porva.crawler.thread;
//
//import java.util.List;
//import java.util.Map;
//
//import junit.framework.TestCase;
//
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//
//import com.porva.crawler.DefaultCrawler;
//import com.porva.crawler.DefaultFrontier;
//import com.porva.crawler.service.AbstractWorkerService;
//import com.porva.crawler.service.ServiceException;
//import com.porva.crawler.service.ServiceProvider;
//import com.porva.crawler.service.WorkerService;
//import com.porva.crawler.task.Task;
//import com.porva.crawler.task.TaskResult;
//import com.porva.crawler.task.TaskFactory;
//import com.porva.crawler.task.TestWorkerTask;
//import com.porva.crawler.thread.WorkersFixedThreadPool;
//
//public class FixedWorkersThreadPoolTest extends TestCase
//{
//
//  private static final Log logger = LogFactory.getLog(FixedWorkersThreadPoolTest.class);
//
//  WorkersFixedThreadPool threadPool;
//
//  Task[] tasks = new Task[10];
//
//  protected void setUp() throws ServiceException
//  {
//    ServiceProvider services = new ServiceProvider();
//    services.register(new TestService());
//
//    threadPool = new WorkersFixedThreadPool(5, new DefaultCrawler(), new DefaultFrontier(),
//        services);
//  }
//
//  // public void testAssign() throws InterruptedException
//  // {
//  // WorkerServiceProvider services = new WorkerServiceProvider();
//  // services.addService(new TestService());
//  //    
//  // WorkersFixedThreadPool threadPool = new WorkersFixedThreadPool(5, new TestCrawlerImpl(),
//  // services);
//  // TestWorkerTask[] tasks = new TestWorkerTask[10];
//  // logger.info("Pausing ...");
//  // threadPool.pause();
//  // for (int i = 0; i < 10; i++) {
//  // tasks[i] = new TestWorkerTask();
//  // threadPool.assign(tasks[i]);
//  // }
//  // logger.info("Tasks added");
//  // Thread.sleep(500);
//  // logger.info("Resuming ...");
//  // threadPool.resume();
//  //
//  // Thread.sleep(500);
//  // logger.info("Pausing ...");
//  // threadPool.pause();
//  //    
//  // Thread.sleep(1000);
//  // logger.info("Resuming ...");
//  // threadPool.resume();
//  //
//  // // threadPool.stop();
//  // threadPool.complete();
//  // TestWorkerTask newTask = new TestWorkerTask();
//  // threadPool.assign(newTask);
//  // threadPool.complete();
//  //    
//  // for (int i = 0; i < tasks.length; i++) {
//  // assertEquals("0 1 2 3 4 5 6 7 8 9 ", tasks[i].getTaskResult());
//  // }
//  // assertEquals("0 1 2 3 4 5 6 7 8 9 ", newTask.getTaskResult());
//  //    
//  // // threadPool.stop();
//  // }
//  public void testSD() throws InterruptedException
//  {
//    logger.info("%%%%%%%%%%%%%%%%%%% testSPD ");
//    threadPool.waitUntil(WorkersFixedThreadPool.State.SLEEPING);
//    threadPool.stop();
//    threadPool.waitUntil(WorkersFixedThreadPool.State.DONE);
//  }
//
//  public void testSPD() throws InterruptedException
//  {
//    logger.info("%%%%%%%%%%%%%%%%%%% testSPD ");
//    threadPool.waitUntil(WorkersFixedThreadPool.State.SLEEPING);
//    threadPool.pause();
//    threadPool.waitUntil(WorkersFixedThreadPool.State.PAUSED);
//    threadPool.stop();
//    threadPool.waitUntil(WorkersFixedThreadPool.State.DONE);
//  }
//
//  public void testSRD() throws InterruptedException
//  {
//    logger.info("%%%%%%%%%%%%%%%%%%% testSRD ");
//    threadPool.waitUntil(WorkersFixedThreadPool.State.SLEEPING);
//    for (int i = 0; i < 10; i++) {
//      tasks[i] = new TestWorkerTask();
//      threadPool.assign(tasks[i]);
//    }
//    threadPool.waitUntil(WorkersFixedThreadPool.State.RUNNING);
//    threadPool.stop();
//    threadPool.waitUntil(WorkersFixedThreadPool.State.DONE);
//  }
//
//  public void testSRPD() throws InterruptedException
//  {
//    logger.info("%%%%%%%%%%%%%%%%%%% testSRPD ");
//    threadPool.waitUntil(WorkersFixedThreadPool.State.SLEEPING);
//    for (int i = 0; i < 10; i++) {
//      tasks[i] = new TestWorkerTask();
//      threadPool.assign(tasks[i]);
//    }
//    threadPool.waitUntil(WorkersFixedThreadPool.State.RUNNING);
//    threadPool.pause();
//    threadPool.waitUntil(WorkersFixedThreadPool.State.PAUSED);
//    threadPool.stop();
//    threadPool.waitUntil(WorkersFixedThreadPool.State.DONE);
//  }
//
//  public void testSRPRS() throws InterruptedException
//  {
//    logger.info("%%%%%%%%%%%%%%%%%%% testSRPRS ");
//    threadPool.waitUntil(WorkersFixedThreadPool.State.SLEEPING);
//    for (int i = 0; i < 10; i++) {
//      tasks[i] = new TestWorkerTask();
//      threadPool.assign(tasks[i]);
//    }
//    threadPool.waitUntil(WorkersFixedThreadPool.State.RUNNING);
//    threadPool.pause();
//    threadPool.waitUntil(WorkersFixedThreadPool.State.PAUSED);
//    threadPool.resume();
//    threadPool.waitUntil(WorkersFixedThreadPool.State.RUNNING);
//    threadPool.waitUntil(WorkersFixedThreadPool.State.SLEEPING);
//
//    checkTasksResults();
//  }
//
//  public void testSPRS() throws InterruptedException
//  {
//    logger.info("%%%%%%%%%%%%%%%%%%% testSPRS ");
//    threadPool.waitUntil(WorkersFixedThreadPool.State.SLEEPING);
//    threadPool.pause();
//    threadPool.waitUntil(WorkersFixedThreadPool.State.PAUSED);
//    for (int i = 0; i < 10; i++) {
//      tasks[i] = new TestWorkerTask();
//      threadPool.assign(tasks[i]);
//    }
//    threadPool.resume();
//    threadPool.waitUntil(WorkersFixedThreadPool.State.RUNNING);
//    threadPool.waitUntil(WorkersFixedThreadPool.State.SLEEPING);
//
//    checkTasksResults();
//  }
//
//  public void testSRPRPRS() throws InterruptedException
//  {
//    logger.info("%%%%%%%%%%%%%%%%%%% testSRPRPRS ");
//    threadPool.waitUntil(WorkersFixedThreadPool.State.SLEEPING);
//    for (int i = 0; i < 10; i++) {
//      tasks[i] = new TestWorkerTask();
//      threadPool.assign(tasks[i]);
//    }
//    threadPool.waitUntil(WorkersFixedThreadPool.State.RUNNING);
//    threadPool.pause();
//    threadPool.waitUntil(WorkersFixedThreadPool.State.PAUSED);
//    threadPool.resume();
//    threadPool.waitUntil(WorkersFixedThreadPool.State.RUNNING);
//    threadPool.pause();
//    threadPool.waitUntil(WorkersFixedThreadPool.State.PAUSED);
//    threadPool.resume();
//    threadPool.waitUntil(WorkersFixedThreadPool.State.RUNNING);
//    threadPool.waitUntil(WorkersFixedThreadPool.State.SLEEPING);
//
//    checkTasksResults();
//  }
//
//  public void testSRS() throws InterruptedException
//  {
//    logger.info("%%%%%%%%%%%%%%%%%%% testSRS ");
//    threadPool.waitUntil(WorkersFixedThreadPool.State.SLEEPING);
//    for (int i = 0; i < 10; i++) {
//      tasks[i] = new TestWorkerTask();
//      threadPool.assign(tasks[i]);
//    }
//    threadPool.waitUntil(WorkersFixedThreadPool.State.RUNNING);
//    threadPool.waitUntil(WorkersFixedThreadPool.State.SLEEPING);
//
//    checkTasksResults();
//  }
//
//  void checkTasksResults()
//  {
//    for (int i = 0; i < tasks.length; i++) {
//      assertEquals("0 1 2 3 4 5 6 7 8 9 ", tasks[i].getTaskResult());
//    }
//  }
//
//  // //////////////////////////////
//  public class TestService extends AbstractWorkerService
//  {
//    public TestService()
//    {
//      super("testService");
//    }
//
//    public String doTestTask()
//    {
//      StringBuffer res = new StringBuffer();
//      for (int i = 0; i < 10; i++) {
//        res.append(i).append(' ');
//        try {
//          Thread.sleep(20);
//        } catch (InterruptedException e) {
//          // TODO Auto-generated catch block
//          e.printStackTrace();
//        }
//      }
//      return res.toString();
//    }
//
//    public WorkerService newCopyInstance()
//    {
//      return new TestService();
//    }
//
//  }
//
//  // //////////////////////////////
//  class TestCrawlerImpl implements Crawler
//  {
//
//    public synchronized void completeTask(Task task)
//    {
//      logger.info("Task completed: " + task.toString());
//    }
//
//    public synchronized void workersPoolStateChanged(final WorkersFixedThreadPool.State newState)
//    {
//      logger.info("WorkersFixedThreadPool state changed: " + newState);
//    }
//
//    public void startWork()
//    {
//      // TODO Auto-generated method stub
//
//    }
//
//    public void pauseWork()
//    {
//      // TODO Auto-generated method stub
//
//    }
//
//    public void resumeWork()
//    {
//      // TODO Auto-generated method stub
//
//    }
//
//    public void suspendWork()
//    {
//      // TODO Auto-generated method stub
//
//    }
//
//    public void interruptWork()
//    {
//      // TODO Auto-generated method stub
//
//    }
//
//    public CrawlerStatus getStatus()
//    {
//      // TODO Auto-generated method stub
//      return null;
//    }
//
//    public Map<String, String> getStat()
//    {
//      // TODO Auto-generated method stub
//      return null;
//    }
//
//    public void waitUntil(CrawlerStatus status) throws InterruptedException
//    {
//      // TODO Auto-generated method stub
//
//    }
//
//  }
//
//  static class TestFrontier extends AbstractWorkerService implements Frontier
//  {
//
//    public TestFrontier()
//    {
//      super("testFrontier");
//    }
//
//    public Task getTask()
//    {
//      // TODO Auto-generated method stub
//      return null;
//    }
//
//    public void completeTask(Task task, TaskResult result)
//    {
//      // TODO Auto-generated method stub
//
//    }
//
//    public boolean addTask(Task task)
//    {
//      // TODO Auto-generated method stub
//      return false;
//    }
//
//    public int addTaskList(List<Task> tasks)
//    {
//      // TODO Auto-generated method stub
//      return 0;
//    }
//
//    public WorkerService newCopyInstance()
//    {
//      return new TestFrontier();
//    }
//  }
//
//}
