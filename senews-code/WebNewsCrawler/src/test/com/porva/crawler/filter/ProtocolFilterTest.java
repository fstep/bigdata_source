package com.porva.crawler.filter;

import junit.framework.TestCase;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.task.FetchTask;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskFactory;
import com.porva.crawler.net.CrawlerURL;

/**
 * Test case of {@link ProtocolFilter}.
 *
 * @author Poroshin V.
 * @date Jan 24, 2006
 */
public class ProtocolFilterTest extends TestCase
{
  FetchTask fetchHttp;

  FetchTask fetchHttps;

  FetchTask fetchFtp;

  protected void setUp() throws Exception
  {
    CrawlerURL httpUrl = new CrawlerURL(0, "http://localhost");
    CrawlerURL httpsUrl = new CrawlerURL(0, "https://localhost");
    CrawlerURL ftpUrl = new CrawlerURL(0, "ftp://localhost");

    fetchHttp = (FetchTask) TaskFactory.newFetchURL(httpUrl);
    fetchHttps = (FetchTask) TaskFactory.newFetchURL(httpsUrl);
    fetchFtp = (FetchTask) TaskFactory.newFetchURL(ftpUrl);
  }

  /**
   * test of {@link ProtocolFilter#ProtocolFilter(String[])}
   */
  public void testProtocolFilter()
  {
    // incorrect initialization
    try {
      new ProtocolFilter(null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      new ProtocolFilter(new String[] {});
      fail();
    } catch (IllegalArgumentException e) {
    }

    // correct initialization
    new ProtocolFilter(new String[] { "http" });

  }

  /**
   * test of {@link ProtocolFilter#isValid(Task)}
   * 
   * @throws Exception
   */
  public void testIsValid() throws Exception
  {
    ProtocolFilter filter = new ProtocolFilter(new String[] { "http", "https" });
    try {
      filter.isValid(null);
      fail("Should not process null argments.");
    } catch (NullArgumentException e) {
    }
    assertTrue(filter.isValid(fetchHttp));
    assertTrue(filter.isValid(fetchHttps));
    assertFalse(filter.isValid(fetchFtp));

    assertTrue("Not FetchTask is always valid", filter.isValid(TaskFactory.newEmptyTask()));
  }

  /**
   * test of {@link ProtocolFilter#newCopyInstance()}
   * 
   * @throws Exception
   */
  public void testNewCopyInstance() throws Exception
  {
    ProtocolFilter filter = new ProtocolFilter(new String[] { "http", "https" });
    ProtocolFilter filterCopy = (ProtocolFilter) filter.newCopyInstance();
    try {
      filterCopy.isValid(null);
      fail("Should not process null argments.");
    } catch (NullArgumentException e) {
    }
    assertTrue(filterCopy.isValid(fetchHttp));
    assertTrue(filterCopy.isValid(fetchHttps));
    assertFalse(filterCopy.isValid(fetchFtp));
  }
  
  /**
   * test of {@link ProtocolFilter#getConfigStr()}
   */
  public void testGetConfigStr()
  {
    ProtocolFilter filter = new ProtocolFilter(new String[] { "http", "https" });
    assertEquals("http,https", filter.getConfigStr());
    
    filter = new ProtocolFilter(new String[] { "http" });
    assertEquals("http", filter.getConfigStr());
  }

}
