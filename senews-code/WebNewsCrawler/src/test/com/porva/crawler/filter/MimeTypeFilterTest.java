package com.porva.crawler.filter;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.UnhandledException;

import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.net.Response;
import com.porva.crawler.net.apache.ApacheHttpService;
import com.porva.crawler.task.DefaultFetchResult;
import com.porva.crawler.task.FetchResult;
import com.porva.crawler.task.FetchTask;
import com.porva.crawler.task.TaskFactory;
import com.porva.crawler.task.TaskResult;

import junit.framework.TestCase;

/**
 * Test case for {@link com.porva.crawler.filter.MimeTypeFilter}.
 *
 * @author Poroshin V.
 * @date Jan 24, 2006
 */
public class MimeTypeFilterTest extends TestCase
{
  static Response response;

  static Response redirResponse;
  
  static Response zipRes;

  FetchTask task1;
  
  FetchTask zipTask;

  FetchResult completeRes;
  
  FetchResult completeZipRes;

  FetchResult redirectRes;

  FetchResult failedRes;
  
  static {
    ApacheHttpService apacheHttpClient = new ApacheHttpService();
    try {
      response = apacheHttpClient.get(new CrawlerURL(1, "http://localhost/"));
      redirResponse = apacheHttpClient.get(new CrawlerURL(1, "http://localhost/tmp/redirect.html"));
      zipRes = apacheHttpClient.get(new CrawlerURL(1, "http://localhost/tmp/bill.zip"));
    } catch (Exception e) {
      throw new UnhandledException(e);
    }
  }

  protected void setUp() throws Exception
  {
    task1 = (FetchTask) TaskFactory.newFetchURL(new CrawlerURL(1, "http://localhost"));
    zipTask = (FetchTask) TaskFactory.newFetchURL(new CrawlerURL(1, "http://localhost/tmp/bill.zip"));
    completeRes = DefaultFetchResult.newCompleteResult(task1, response);
    completeZipRes = DefaultFetchResult.newCompleteResult(zipTask, zipRes);
    redirectRes = DefaultFetchResult.newCompleteResult((FetchTask) TaskFactory
        .newFetchURL(new CrawlerURL(1, "http://localhost/tmp/redirect.html")), redirResponse);
    failedRes = DefaultFetchResult.newFailedResult(task1, DefaultFetchResult.Status.TASK_FILTERED);
  }

  /**
   * test of {@link MimeTypeFilter#MimeTypeFilter(String[])}
   */
  public void testMimeTypeFilter()
  {
    try {
      new MimeTypeFilter(null);
      fail();
    } catch (NullArgumentException e) {}
    try {
      new MimeTypeFilter(new String[] {});
      fail("allowedMimeTypes must contain at least one element");
    } catch (IllegalArgumentException e) {
    }

    new MimeTypeFilter(new String[] { "text/plain", "text/html" });
  }

  /**
   * test of {@link MimeTypeFilter#isValid(TaskResult)}.
   */
  public void testIsValid()
  {
    MimeTypeFilter filter = new MimeTypeFilter(new String[] { "text/plain", "text/html" });
    try {
      filter.isValid(null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      assertTrue(filter.isValid(failedRes));
      fail("response is null");
    } catch (IllegalArgumentException e) {
    }
    
    assertTrue(filter.isValid(completeRes));
    assertTrue(filter.isValid(redirectRes));
    assertFalse(filter.isValid(completeZipRes));
    
    filter = new MimeTypeFilter(new String[] { "application/x-zip-compressed", "application/zip"});
    assertTrue(filter.isValid(completeZipRes));
    assertFalse(filter.isValid(completeRes));
    assertFalse(filter.isValid(redirectRes));
  }
  
  /**
   * test of {@link MimeTypeFilter#getConfigStr()}.
   */
  public void testGetConfigStr()
  {
    MimeTypeFilter filter = new MimeTypeFilter(new String[] { "text/plain", "text/html" });
    assertEquals("text/plain,text/html", filter.getConfigStr());
    
    filter = new MimeTypeFilter(new String[] {"text/plain"});
    assertEquals("text/plain", filter.getConfigStr());
  }

}
