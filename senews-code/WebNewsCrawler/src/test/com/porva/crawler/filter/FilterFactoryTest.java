package com.porva.crawler.filter;

import static com.porva.crawler.db.DBTestSettings.closeAndRemoveFactory;
import static com.porva.crawler.db.DBTestSettings.createFactory;
import static com.porva.crawler.db.DBTestSettings.dbFactory;
import junit.framework.TestCase;

import com.porva.crawler.net.HttpClient;
import com.porva.crawler.net.apache.ApacheHttpService;

public class FilterFactoryTest extends TestCase
{
  public void testNewFilter() throws Exception
  {
    Filter filter = FilterFactory.newFilter(Filter.DEPTH_LIMIT, "1", null, null); 
    assertTrue(filter instanceof DepthLimitFilter);
    
    filter = FilterFactory.newFilter(Filter.DOMAINS, "allow:org,net", null, null);
    assertTrue(filter instanceof DomainFilter);
    assertEquals("allow:org,net", filter.getConfigStr());
    
    filter = FilterFactory.newFilter(Filter.FILE_TYPE, "disallow:html,htm", null, null);
    assertTrue(filter instanceof FileTypeFilter);
    assertEquals("disallow:html,htm", filter.getConfigStr());
    
    filter = FilterFactory.newFilter(Filter.HOST, "java.sun.com", null, null);
    assertTrue(filter instanceof HostFilter);
    assertEquals("java.sun.com", filter.getConfigStr());
    
    filter = FilterFactory.newFilter(Filter.MIME_TYPE, "text/plain,text/html", null, null);
    assertTrue(filter instanceof MimeTypeFilter);
    assertEquals("text/plain,text/html", filter.getConfigStr());
    
    filter = FilterFactory.newFilter(Filter.PROTOCOLS, "http", null, null);
    assertTrue(filter instanceof ProtocolFilter);
    assertEquals("http", filter.getConfigStr());
    
    filter = FilterFactory.newFilter(Filter.REDIRECTS_LIMIT, "4", null, null);
    assertTrue(filter instanceof RedirectsLimitFilter);
    assertEquals("4", filter.getConfigStr());
    
    //filter = FilterFactory.newFilter(Filter.ROBOTS_RULES, "86400001", null);

  }

  public void testNewAllowDomainFilter()
  {
    FilterFactory.newAllowDomainFilter(null);
    FilterFactory.newAllowDomainFilter(new String[] {});
    FilterFactory.newAllowDomainFilter(new String[] { "com", "net" });
    // see more tests in DomainFilterTest class
  }

  public void testNewDisallowDomainFilter() throws Exception
  {
    Filter filter = FilterFactory.newDisallowDomainFilter(null);
    filter.stop();
    filter = FilterFactory.newDisallowDomainFilter(new String[] {});
    filter.stop();
    filter = FilterFactory.newDisallowDomainFilter(new String[] { "com", "net" });
    filter.stop();
    // see more tests in DomainFilterTest class
  }

//  public void testNewDomainFilter()
//  {
//    try {
//      factory.newDomainFilter(null);
//      fail();
//    } catch (IllegalArgumentException e) {
//    }
//    try {
//      factory.newDomainFilter("");
//      fail();
//    } catch (IllegalArgumentException e) {
//    }
//    try {
//      factory.newDomainFilter("com,net");
//      fail("allow or disallow should be specified");
//    } catch (IllegalArgumentException e) {
//    }
//    try {
//      factory.newDomainFilter("allow:c om,net");
//      fail("illegal char in domain");
//    } catch (IllegalArgumentException e) {
//    }
//
//    factory.newDomainFilter("allow:");
//    factory.newDomainFilter("allow:com");
//    factory.newDomainFilter("disallow:");
//    factory.newDomainFilter("disallow:org,com");
//  }

  public void testNewRobotRulesFilter() throws Exception
  {
    HttpClient httpClient = new ApacheHttpService(); 
    createFactory();
    Filter filter = FilterFactory.newRobotRulesFilter(1000, dbFactory, httpClient);
    filter.stop();
    closeAndRemoveFactory();
    // see more test in RobotRulesFilterTest class
  }

  public void testNewDepthLimitFilter()
  {
    FilterFactory.newDepthLimitFilter(1);
    // see more test in DepthLimitFilterTest class
  }

//  public void testNewDepthLimitFilterString()
//  {
//    try {
//      factory.newDepthLimitFilter(null);
//      fail();
//    } catch (NullArgumentException e) {
//    }
//    try {
//      factory.newDepthLimitFilter("x");
//      fail();
//    } catch (NumberFormatException e) {
//    }
//
//    factory.newDepthLimitFilter("1");
//    factory.newDepthLimitFilter("-1");
//  }

  public void testNewProtocolFilter()
  {
    FilterFactory.newProtocolFilter(new String[] {"com"});
  }

}
