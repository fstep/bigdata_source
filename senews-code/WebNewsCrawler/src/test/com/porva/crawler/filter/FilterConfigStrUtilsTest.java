package com.porva.crawler.filter;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.filter.FilterConfigStrUtils.ConfigParam;

import junit.framework.TestCase;

public class FilterConfigStrUtilsTest extends TestCase
{

  String[] emptyStrs = new String[] { "", " ", "\t \n" };

  /*
   * Test method for
   * 'com.porva.crawler.filter.FilterConfigStrUtils.parseBooleanAndStringListStr(String)'
   */
  public void testParseBooleanAndStringListStr()
  {
    try {
      FilterConfigStrUtils.parseBooleanAndStringListStr(null, false, null);
      fail();
    } catch (NullArgumentException e) {
    }

    for (String invalidStr : emptyStrs) {
      try {
        FilterConfigStrUtils.parseBooleanAndStringListStr(invalidStr, false, null);
        fail();
      } catch (IllegalArgumentException e) {
      }
    }
    
    ConfigParam cp = FilterConfigStrUtils.parseBooleanAndStringListStr("A", true, null);
    assertTrue(cp.isAllow());
    assertEquals("a", cp.getParams()[0]);
    
    cp = FilterConfigStrUtils.parseBooleanAndStringListStr("101", true, null);
    assertTrue(cp.isAllow());
    assertEquals(101, Integer.parseInt(cp.getParams()[0]));
    
    cp = FilterConfigStrUtils.parseBooleanAndStringListStr("allow:A,B,c", true, null);
    assertTrue(cp.isAllow());
    assertEquals("a", cp.getParams()[0]);
    assertEquals("b", cp.getParams()[1]);
    assertEquals("c", cp.getParams()[2]);
    
    cp = FilterConfigStrUtils.parseBooleanAndStringListStr("disallow:", true, null);
    assertFalse(cp.isAllow());
    assertEquals(null, cp.getParams());
    
    cp = FilterConfigStrUtils.parseBooleanAndStringListStr("disallow:aBc", false, null);
    assertFalse(cp.isAllow());
    assertEquals("aBc", cp.getParams()[0]);
    
    cp = FilterConfigStrUtils.parseBooleanAndStringListStr("disallow:aBc", true, "abc");
    assertFalse(cp.isAllow());
    assertEquals("abc", cp.getParams()[0]);
    
    try {
      FilterConfigStrUtils.parseBooleanAndStringListStr("disallow:aBc", true, "acb");
      fail();
    } catch (IllegalArgumentException e) {
    }

  }

}
