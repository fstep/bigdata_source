package com.porva.crawler.filter;

import static com.porva.crawler.db.DBTestSettings.closeAndRemoveFactory;
import static com.porva.crawler.db.DBTestSettings.createFactory;
import static com.porva.crawler.db.DBTestSettings.dbFactory;
import junit.framework.TestCase;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.db.CrawlerDBFactory;
import com.porva.crawler.db.DBException;
import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.net.HttpClient;
import com.porva.crawler.net.apache.ApacheHttpService;
import com.porva.crawler.task.FetchTask;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskFactory;

/**
 * Test case for {@link com.porva.crawler.filter.RobotRulesFilter}
 *
 */
public class RobotRulesFilterTest extends TestCase
{
  FetchTask fetchForbidden;

  FetchTask fetchForbidden2;

  FetchTask fetchAllowed;
  
  FetchTask fetchInvalid;
  
  HttpClient httpClient = new ApacheHttpService();

  protected void setUp() throws Exception
  {
    createFactory();
    fetchForbidden = (FetchTask) TaskFactory.newFetchURL(new CrawlerURL(0,
        "http://localhost/ITProj/list.txt"));
    fetchForbidden2 = (FetchTask) TaskFactory.newFetchURL(new CrawlerURL(0,
        "http://localhost/ITProj/some/path"));
    fetchAllowed = (FetchTask) TaskFactory.newFetchURL(new CrawlerURL(0,
        "http://localhost/tmp/links.html"));
    fetchInvalid = (FetchTask) TaskFactory.newFetchURL(new CrawlerURL(0,
        "http://unknown-host/"));
  }

  protected void tearDown() throws DBException
  {
    closeAndRemoveFactory();
  }

  /**
   * test of {@link RobotRulesFilter#RobotRulesFilter(int, CrawlerDBFactory)}
   * 
   * @throws Exception
   */
  public void testRobotRulesFilter() throws Exception
  {
    try {
      new RobotRulesFilter(-1, dbFactory, httpClient);
      fail("Rules lifetime must be >= 0");
    } catch (IllegalArgumentException e) {
    }

    RobotRulesFilter filter = new RobotRulesFilter(1000, dbFactory, httpClient);
    filter.stop();
  }

  /**
   * test of {@link RobotRulesFilter#stop()}
   * 
   * @throws Exception
   */
  public void testStop() throws Exception
  {
    RobotRulesFilter filter = new RobotRulesFilter(1000, dbFactory, httpClient);
    filter.stop();
    filter.stop(); // should produce only warning
  }

  /**
   * test of {@link RobotRulesFilter#isValid(Task)}
   * 
   * @throws Exception
   */
  public void testIsValidURI() throws Exception
  {
    RobotRulesFilter filter = new RobotRulesFilter(100000, dbFactory, httpClient);

    try {
      filter.isValid(null);
      fail();
    } catch (NullArgumentException e) {
    }

    assertFalse(filter.isValid(fetchForbidden));
    assertFalse(filter.isValid(fetchForbidden));
    assertFalse(filter.isValid(fetchForbidden2));
    assertTrue(filter.isValid(fetchAllowed));
    assertTrue(filter.isValid(fetchInvalid));
    filter.stop();

    filter = new RobotRulesFilter(1000, dbFactory, httpClient);
    assertFalse(filter.isValid(fetchForbidden));
    assertFalse(filter.isValid(fetchForbidden));
    assertFalse(filter.isValid(fetchForbidden2));
    assertTrue(filter.isValid(fetchAllowed));
    filter.stop();
  }

  /**
   * test of {@link RobotRulesFilter#newCopyInstance()}
   * 
   * @throws Exception
   */
  public void testNewCopyInstance() throws Exception
  {
    RobotRulesFilter filter = new RobotRulesFilter(500, dbFactory, httpClient);
    assertFalse(filter.isValid(fetchForbidden));

    RobotRulesFilter copyFilter = (RobotRulesFilter) filter.newCopyInstance();
    assertFalse(copyFilter.isValid(fetchForbidden));

    copyFilter.stop();
    filter.stop();
  }
  
  /**
   * test of {@link RobotRulesFilter#getConfigStr()}
   * 
   * @throws Exception
   */
  public void testGetConfigStr() throws Exception
  {
    RobotRulesFilter filter = new RobotRulesFilter(100000, dbFactory, httpClient);
    assertEquals("100000", filter.getConfigStr());
    filter.stop();
  }

}
