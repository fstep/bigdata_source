//package com.porva.crawler.filter;
//
//import static com.porva.crawler.task.FetchResult.Status.TASK_FILTERED;
//
//import org.apache.commons.lang.NullArgumentException;
//
//import com.porva.crawler.net.CrawlerURL;
//import com.porva.crawler.net.MalformedCrawlerURLException;
//import com.porva.crawler.task.FetchTask;
//import com.porva.crawler.task.FetchURLResult;
//import com.porva.crawler.task.TaskFactory;
//
//import junit.framework.TestCase;
//
//public class MimeFilterTest extends TestCase
//{
//  public void testMimeFilter()
//  {
//    try {
//      new MimeFilter(null);
//      fail();
//    } catch (NullArgumentException e) {
//    }
//    try {
//      new MimeFilter(new String[] {});
//      fail("allowedMimeTypes has no elements");
//    } catch (IllegalArgumentException e) {
//    }
//    try {
//      new MimeFilter(new String[] {"text/html", "text xml"});
//      fail("invalid mime type");
//    } catch (IllegalArgumentException e) {
//    }
//    
//    TaskResultFilter filter = new MimeFilter(new String[] {"text/html", "text/xml"});
//    assertNotNull(filter);
//    //System.out.println(filter.toString());
//  }
//  
//  public void testIsValid() throws Exception
//  {
//    TaskResultFilter filter = new MimeFilter(new String[] {"text/html", "text/xml"});
//    
//    CrawlerURL httpUrl = new CrawlerURL(0, "http://localhost");
//    FetchTask fetchHttp = (FetchTask) TaskFactory.newFetchURL(httpUrl);
//    FetchURLResult failedFetchResult = new FetchURLResult(fetchHttp, TASK_FILTERED, null);
//    assertTrue("failed task is always valid", filter.isValid(failedFetchResult)); // ???
//  }
//  
//}
