package com.porva.crawler.filter;

import junit.framework.TestCase;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskFactory;

/**
 * Test case for {@link FileTypeFilter}
 * 
 * @author Poroshin V.
 * @date Jan 26, 2006
 */
public class FileTypeFilterTest extends TestCase
{

  /**
   * Test method for {@link FileTypeFilter#FileTypeFilter(boolean, String[])}
   */
  public void testFileTypeFilter()
  {
    try {
      new FileTypeFilter(true, null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      new FileTypeFilter(true, new String[] {});
      fail();
    } catch (IllegalArgumentException e) {
    }
    try {
      new FileTypeFilter(true, new String[] {"  "});
      fail();
    } catch (IllegalArgumentException e) {
    }
    
    new FileTypeFilter(true, new String[] {"html"});
    new FileTypeFilter(true, new String[] {"html", "htm"});
    new FileTypeFilter(true, new String[] {"x_!", "th ml"}); // also valid
  }


  /**
   * Test method for {@link FileTypeFilter#isValid(Task)}
   * 
   * @throws Exception
   */
  public void testIsValid() throws Exception
  {
    TaskFilter filter = new FileTypeFilter(true, new String[] {"html", "pdf"});
    try {
      filter.isValid(null); 
      fail();
    } catch (NullArgumentException e) {
    }
    
    assertTrue(filter.isValid(TaskFactory.newEmptyTask()));
    assertTrue(filter.isValid(TaskFactory.newFetchURL(new CrawlerURL(0, "http://test/index.html"))));
    assertTrue(filter.isValid(TaskFactory.newFetchURL(new CrawlerURL(0, "http://test/"))));
    assertTrue(filter.isValid(TaskFactory.newFetchURL(new CrawlerURL(0, "http://test"))));
    assertFalse(filter.isValid(TaskFactory.newFetchURL(new CrawlerURL(0, "http://test/index.htm"))));
    assertTrue(filter.isValid(TaskFactory.newFetchURL(new CrawlerURL(0, "http://test/some.PdF"))));
  }

  /**
   * Test method for {@link FileTypeFilter#isValidToProduce(Task)}
   * 
   * @throws Exception
   */
  public void testIsValidToProduce() throws Exception
  {
    TaskFilter filter = new FileTypeFilter(true, new String[] {"html", "pdf"});
    assertTrue(filter.isValidToProduce(TaskFactory.newFetchURL(new CrawlerURL(0, "http://any.url"))));
  }

  /**
   * Test method for {@link FileTypeFilter#getConfigStr()}
   */
  public void testGetConfigStr()
  {
    Filter filter = new FileTypeFilter(true, new String[] {"html", "pdf"});
    assertEquals("allow:html,pdf", filter.getConfigStr());
    
    filter = new FileTypeFilter(false, new String[] {"html"});
    assertEquals("disallow:html", filter.getConfigStr());
  }

  /**
   * Test method for {@link FileTypeFilter#newCopyInstance()}
   * 
   * @throws Exception
   */
  public void testNewCopyInstance() throws Exception
  {
    Filter filter = new FileTypeFilter(true, new String[] {"html", "pdf"});
    Filter copyFilter = (Filter)filter.newCopyInstance();
    assertEquals(filter.getConfigStr(), copyFilter.getConfigStr());
  }

}
