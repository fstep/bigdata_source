package com.porva.crawler.filter;

import junit.framework.TestCase;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskFactory;

/**
 * Test case for {@link com.porva.crawler.filter.RedirectsLimitFilter}
 */
public class RedirectsLimitFilterTest extends TestCase
{

  /**
   * test of {@link RedirectsLimitFilter#RedirectsLimitFilter(int)}
   */
  public void testRedirectsLimitFilter()
  {
    try {
      new RedirectsLimitFilter(-1);
    } catch (IllegalArgumentException e) {
    }
    
    new RedirectsLimitFilter(0);
    new RedirectsLimitFilter(1);
  }

  /**
   * test of {@link RedirectsLimitFilter#isValid(Task)}
   * 
   * @throws Exception
   */
  public void testIsValid() throws Exception
  {
    try {
      (new RedirectsLimitFilter(0)).isValid(null);
      fail();
    } catch (NullArgumentException e) {
    }
    
    RedirectsLimitFilter filter = new RedirectsLimitFilter(0);
    assertTrue("Not fetch task is always valid", filter.isValid(TaskFactory.newLastTask()));
    assertTrue(filter.isValid(TaskFactory.newFetchURL(new CrawlerURL(0, "http://localhohst", 0))));
    assertFalse(filter.isValid(TaskFactory.newFetchURL(new CrawlerURL(0, "http://localhohst", 1))));
  }

  /**
   * test of {@link RedirectsLimitFilter#newCopyInstance()}
   * 
   * @throws Exception
   */
  public void testNewCopyInstance() throws Exception
  {
    TaskFilter copyFilter = (TaskFilter)(new RedirectsLimitFilter(0)).newCopyInstance();
    assertTrue("Not fetch task is always valid", copyFilter.isValid(TaskFactory.newLastTask()));
    assertTrue(copyFilter.isValid(TaskFactory.newFetchURL(new CrawlerURL(0, "http://localhohst", 0))));
    assertFalse(copyFilter.isValid(TaskFactory.newFetchURL(new CrawlerURL(0, "http://localhohst", 1))));
  }
  
  /**
   * test of {@link RedirectsLimitFilter#getConfigStr()}
   */
  public void testGetConfigStr()
  {
    RedirectsLimitFilter filter = new RedirectsLimitFilter(0);
    assertEquals("0", filter.getConfigStr());
    
    filter = new RedirectsLimitFilter(3);
    assertEquals("3", filter.getConfigStr());
  }

}
