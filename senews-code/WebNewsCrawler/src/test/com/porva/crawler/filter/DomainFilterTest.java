package com.porva.crawler.filter;

import junit.framework.TestCase;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.task.FetchTask;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskFactory;

/**
 * Test case of {@link DomainFilter}.
 * 
 * @author Poroshin V.
 * @date Oct 16, 2005
 */
public class DomainFilterTest extends TestCase
{
  CrawlerURL url1;

  CrawlerURL url2;

  CrawlerURL url3;

  CrawlerURL url4;

  CrawlerURL url5;

  protected void setUp() throws Exception
  {
    url1 = new CrawlerURL(0, "http://test.com");
    url2 = new CrawlerURL(0, "http://test");
    url3 = new CrawlerURL(0, "http://test.com/test.org");
    url4 = new CrawlerURL(0, "http://test.org/test.net");
    url5 = new CrawlerURL(0, "http://test.net/test.org");
  }

  /**
   * test: {@link DomainFilter#newAllowDomainFilter(String[])}.
   * 
   * @throws Exception
   */
  public void testNewAllowDomains() throws Exception
  {
    DomainFilter dissallowAllNotNullDomainsFilter = DomainFilter.newAllowDomainFilter(null);
    assertFalse(dissallowAllNotNullDomainsFilter.isValid(TaskFactory.newFetchURL(url1)));
    assertTrue(dissallowAllNotNullDomainsFilter.isValid(TaskFactory.newFetchURL(url2)));

    dissallowAllNotNullDomainsFilter = DomainFilter.newAllowDomainFilter(new String[] {});
    assertFalse(dissallowAllNotNullDomainsFilter.isValid(TaskFactory.newFetchURL(url1)));
    assertTrue(dissallowAllNotNullDomainsFilter.isValid(TaskFactory.newFetchURL(url2)));

    DomainFilter allowComNet = DomainFilter.newAllowDomainFilter(new String[] { "com", "net" });
    assertTrue(allowComNet.isValid(TaskFactory.newFetchURL(url1)));
    assertTrue(allowComNet.isValid(TaskFactory.newFetchURL(url2)));
    assertTrue(allowComNet.isValid((FetchTask) TaskFactory.newFetchURL(url3)));
    assertFalse(allowComNet.isValid(TaskFactory.newFetchURL(url4)));
    assertTrue(allowComNet.isValid(TaskFactory.newFetchURL(url5)));
  }

  /**
   * test: {@link DomainFilter#newDisallowDomainFilter(String[])}.
   * 
   * @throws Exception
   */
  public void testNewDisallowDomainFilter() throws Exception
  {
    DomainFilter allowAll = DomainFilter.newDisallowDomainFilter(null);
    assertTrue(allowAll.isValid(TaskFactory.newFetchURL(url1)));
    assertTrue(allowAll.isValid(TaskFactory.newFetchURL(url2)));
    assertTrue(allowAll.isValid(TaskFactory.newFetchURL(url3)));

    allowAll = DomainFilter.newDisallowDomainFilter(new String[] {});
    assertTrue(allowAll.isValid(TaskFactory.newFetchURL(url1)));
    assertTrue(allowAll.isValid(TaskFactory.newFetchURL(url2)));
    assertTrue(allowAll.isValid(TaskFactory.newFetchURL(url3)));

    DomainFilter disallowComNet = DomainFilter
        .newDisallowDomainFilter(new String[] { "com", "net" });
    assertFalse(disallowComNet.isValid(TaskFactory.newFetchURL(url1)));
    assertTrue(disallowComNet.isValid(TaskFactory.newFetchURL(url2)));
    assertFalse(disallowComNet.isValid(TaskFactory.newFetchURL(url3)));
    assertTrue(disallowComNet.isValid(TaskFactory.newFetchURL(url4)));
    assertFalse(disallowComNet.isValid(TaskFactory.newFetchURL(url5)));
  }

  /**
   * test: {@link TaskFilter#isValid(Task)}
   */
  public void testIsValid()
  {
    DomainFilter disallowComNet = DomainFilter
        .newDisallowDomainFilter(new String[] { "com", "net" });
    try {
      assertFalse(disallowComNet.isValid(null));
      fail();
    } catch (NullArgumentException e) {
    }

    assertTrue("Not FetchTask tasks are always true", disallowComNet.isValid(TaskFactory
        .newEmptyTask()));
  }

  /**
   * test of {@link DomainFilter#newCopyInstance()}.
   * @throws Exception
   */
  public void testNewCopyInstance() throws Exception
  {
    TaskFilter dissallowAllNotNullDomainsFilter = DomainFilter.newAllowDomainFilter(null);
    dissallowAllNotNullDomainsFilter = (TaskFilter) dissallowAllNotNullDomainsFilter
        .newCopyInstance();
    assertFalse(dissallowAllNotNullDomainsFilter.isValid(TaskFactory.newFetchURL(url1)));
    assertTrue(dissallowAllNotNullDomainsFilter.isValid(TaskFactory.newFetchURL(url2)));

    dissallowAllNotNullDomainsFilter = DomainFilter.newAllowDomainFilter(new String[] {});
    assertFalse(dissallowAllNotNullDomainsFilter.isValid(TaskFactory.newFetchURL(url1)));
    assertTrue(dissallowAllNotNullDomainsFilter.isValid(TaskFactory.newFetchURL(url2)));

    DomainFilter allowComNet = DomainFilter.newAllowDomainFilter(new String[] { "com", "net" });
    assertTrue(allowComNet.isValid(TaskFactory.newFetchURL(url1)));
    assertTrue(allowComNet.isValid(TaskFactory.newFetchURL(url2)));
    assertTrue(allowComNet.isValid((FetchTask) TaskFactory.newFetchURL(url3)));
    assertFalse(allowComNet.isValid(TaskFactory.newFetchURL(url4)));
    assertTrue(allowComNet.isValid(TaskFactory.newFetchURL(url5)));

    TaskFilter allowAll = DomainFilter.newDisallowDomainFilter(null);
    allowAll = (TaskFilter) allowAll.newCopyInstance();
    assertTrue(allowAll.isValid(TaskFactory.newFetchURL(url1)));
    assertTrue(allowAll.isValid(TaskFactory.newFetchURL(url2)));
    assertTrue(allowAll.isValid(TaskFactory.newFetchURL(url3)));

    allowAll = DomainFilter.newDisallowDomainFilter(new String[] {});
    assertTrue(allowAll.isValid(TaskFactory.newFetchURL(url1)));
    assertTrue(allowAll.isValid(TaskFactory.newFetchURL(url2)));
    assertTrue(allowAll.isValid(TaskFactory.newFetchURL(url3)));

    DomainFilter disallowComNet = DomainFilter
        .newDisallowDomainFilter(new String[] { "com", "net" });
    assertFalse(disallowComNet.isValid(TaskFactory.newFetchURL(url1)));
    assertTrue(disallowComNet.isValid(TaskFactory.newFetchURL(url2)));
    assertFalse(disallowComNet.isValid(TaskFactory.newFetchURL(url3)));
    assertTrue(disallowComNet.isValid(TaskFactory.newFetchURL(url4)));
    assertFalse(disallowComNet.isValid(TaskFactory.newFetchURL(url5)));
  }

  /**
   * test of {@link DomainFilter#getConfigStr()}.
   */
  public void testGetConfigStr()
  {
    DomainFilter disallowComNet = DomainFilter
        .newDisallowDomainFilter(new String[] { "com", "net" });
    assertEquals("disallow:com,net", disallowComNet.getConfigStr());

    disallowComNet = DomainFilter.newDisallowDomainFilter(new String[] {"com"});
    assertEquals("disallow:com", disallowComNet.getConfigStr());

    disallowComNet = DomainFilter.newDisallowDomainFilter(new String[] {});
    assertEquals("disallow:", disallowComNet.getConfigStr());
    
    disallowComNet = DomainFilter.newDisallowDomainFilter(null);
    assertEquals("disallow:", disallowComNet.getConfigStr());

    DomainFilter allowComNet = DomainFilter.newAllowDomainFilter(new String[] { "com", "net" });
    assertEquals("allow:com,net", allowComNet.getConfigStr());
    
    allowComNet = DomainFilter.newAllowDomainFilter(new String[] { "com"});
    assertEquals("allow:com", allowComNet.getConfigStr());

    DomainFilter dissallowAllNotNullDomainsFilter = DomainFilter
        .newAllowDomainFilter(new String[] {});
    assertEquals("allow:", dissallowAllNotNullDomainsFilter.getConfigStr());

    dissallowAllNotNullDomainsFilter = DomainFilter.newAllowDomainFilter(null);
    assertEquals("allow:", dissallowAllNotNullDomainsFilter.getConfigStr());

  }

}
