package com.porva.crawler.filter;

import junit.framework.TestCase;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.filter.Filter.ProcessingSpeed;
import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskFactory;

/**
 * Test case of {@link DepthLimitFilter}.
 * 
 * @author Poroshin V.
 * @date Oct 16, 2005
 */
public class DepthLimitFilterTest extends TestCase
{
  TaskFilter filter = new DepthLimitFilter(1);

  /**
   * test: {@link DepthLimitFilter#DepthLimitFilter(int)}
   */
  public void testDepthLimitFilter()
  {
    filter = new DepthLimitFilter(-10); // also valid
    assertNotNull(filter);
    assertEquals(filter.getServiceName(), Filter.DEPTH_LIMIT);
    assertEquals(filter.getProcessingSpeed(), ProcessingSpeed.VERY_FAST);
  }
  
  /**
   * test: {@link TaskFilter#isValid(Task)}
   * @throws Exception 
   */
  public void testIsValid() throws Exception
  {
    testIsValid(filter);
  }

  private void testIsValid(TaskFilter filter) throws Exception
  {
    try {
      filter.isValid(null);
      fail();
    } catch (NullArgumentException e) {
    }

    // test Not a fetch task
    assertTrue("taks of other type is always true", filter.isValid(TaskFactory.newEmptyTask()));

    // test this specified depth limit
    assertTrue(filter.isValid(TaskFactory.newFetchURL(new CrawlerURL(0,
        "http://test.com", 1))));

    assertTrue(filter.isValid(TaskFactory.newFetchURL(new CrawlerURL(1,
        "http://test.com", 1))));

    assertFalse(filter.isValid(TaskFactory.newFetchURL(new CrawlerURL(2,
        "http://test.com", 1))));

    // test default depth limit
    assertTrue(filter.isValid(TaskFactory.newFetchURL(new CrawlerURL(1, "http://test.com"))));
    assertFalse(filter.isValid(TaskFactory.newFetchURL(new CrawlerURL(2,
        "http://test.com"))));
  }
  
  /**
   * test: {@link com.porva.crawler.service.WorkerService#newCopyInstance()}
   * @throws Exception
   */
  public void testNewCopyInstance() throws Exception
  {
    DepthLimitFilter copyFilter = (DepthLimitFilter)filter.newCopyInstance();
    assertFalse(copyFilter == filter);
    testIsValid(copyFilter);
  }
  
  /**
   * test: Comparable interface of the Filter.
   * @throws Exception
   */
  public void testCompareTo() throws Exception
  {
    Filter veryFastFilter1 = new DepthLimitFilter(1);
    Filter veryFastFilter2 = new DepthLimitFilter(2);
    Filter copyFilter = (Filter)veryFastFilter1.newCopyInstance();
    
    assertTrue(veryFastFilter1.compareTo(veryFastFilter1) == 0);
    assertTrue(veryFastFilter1.compareTo(veryFastFilter2) == 0);
    assertTrue(veryFastFilter1.compareTo(copyFilter) == 0);
    assertTrue(veryFastFilter2.compareTo(copyFilter) == 0);
  }
  
  /**
   * 
   */
  public void testGetConfigStr()
  {
    Filter f1 = new DepthLimitFilter(1);
    assertEquals("1", f1.getConfigStr());
    Filter f2 = new DepthLimitFilter(2);
    assertEquals("2", f2.getConfigStr());
  }
}
