package com.porva.crawler.filter;

import junit.framework.TestCase;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.task.FetchTask;
import com.porva.crawler.task.TaskFactory;

public class HostFilterTest extends TestCase
{
  
  FetchTask fetchGoogle;

  FetchTask fetchJava;

  FetchTask fetchMicrosoft;

  protected void setUp() throws Exception
  {
    CrawlerURL httpUrl = new CrawlerURL(0, "http://google.com");
    CrawlerURL httpsUrl = new CrawlerURL(0, "http://java.sun.com/something");
    CrawlerURL ftpUrl = new CrawlerURL(0, "http://microsoft.com/x/y/x.html");

    fetchGoogle = (FetchTask) TaskFactory.newFetchURL(httpUrl);
    fetchJava = (FetchTask) TaskFactory.newFetchURL(httpsUrl);
    fetchMicrosoft = (FetchTask) TaskFactory.newFetchURL(ftpUrl);
  }


  /*
   * Test method for 'com.porva.crawler.filter.HostFilter.HostFilter(String[])'
   */
  public void testHostFilter()
  {
    // incorrect initialization
    try {
      new HostFilter(null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      new HostFilter(new String[] {});
      fail();
    } catch (IllegalArgumentException e) {
    }

    // correct initialization
    new HostFilter(new String[] { "google.com" });
    new HostFilter(new String[] { "google.com", "java.sun.com" });
  }


  /*
   * Test method for 'com.porva.crawler.filter.HostFilter.isValid(Task)'
   */
  public void testIsValid()
  {
    HostFilter filter = new HostFilter(new String[] { "google.com" });
    assertTrue(filter.isValid(fetchGoogle));
    assertFalse(filter.isValid(fetchJava));
    assertFalse(filter.isValid(fetchMicrosoft));
    
    filter = new HostFilter(new String[] { "google.com", "java.sun.com" });
    assertTrue(filter.isValid(fetchGoogle));
    assertTrue(filter.isValid(fetchJava));
    assertFalse(filter.isValid(fetchMicrosoft));
    
    filter = new HostFilter(new String[] { "google.fi", "java.sun.fi" });
    assertFalse(filter.isValid(fetchGoogle));
    assertFalse(filter.isValid(fetchJava));
    assertFalse(filter.isValid(fetchMicrosoft));
  }

  /*
   * Test method for 'com.porva.crawler.filter.HostFilter.newCopyInstance()'
   */
  public void testNewCopyInstance() throws Exception
  {
    HostFilter filter = new HostFilter(new String[] { "google.com" });
    HostFilter copyFilter = (HostFilter)filter.newCopyInstance(); 
    assertTrue(copyFilter.isValid(fetchGoogle));
    assertFalse(copyFilter.isValid(fetchJava));
    assertFalse(copyFilter.isValid(fetchMicrosoft));
  }

  /*
   * Test method for 'com.porva.crawler.filter.HostFilter.isValidToProduce(Task)'
   */
  public void testIsValidToProduce()
  {
    HostFilter filter = new HostFilter(new String[] { "google.com" });
    assertTrue(filter.isValidToProduce(null));
  }

  /*
   * Test method for 'com.porva.crawler.filter.HostFilter.getConfigStr()'
   */
  public void testGetConfigStr()
  {
    HostFilter filter = new HostFilter(new String[] { "google.com" });
    assertEquals("google.com", filter.getConfigStr());
    
    filter = new HostFilter(new String[] { "google.fi", "java.sun.fi" });
    assertEquals("google.fi,java.sun.fi", filter.getConfigStr());
  }

}
