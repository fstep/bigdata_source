package com.porva.crawler.service;

import org.apache.commons.lang.NullArgumentException;

import junit.framework.TestCase;

public class ServiceProviderTest extends TestCase
{
  ServiceProvider sp;

  protected void setUp()
  {
    sp = new ServiceProvider();
  }

  /*
   * Class under test for void ServiceProvider()
   */
  public void testServiceProvider()
  {
    assertNotNull(sp);
    assertTrue(sp.getAllServices().size() == 0);
  }

  /*
   * Class under test for void ServiceProvider(ServiceProvider)
   */
  public void testServiceProviderServiceProvider() throws Exception
  {
    try {
      new ServiceProvider(null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      ServiceProvider sp = new ServiceProvider();
      sp.register(new FailedTestWorkerService());
      new ServiceProvider(sp);
      fail("Creation of FailedTestWorkerService should fail.");
    } catch (ServiceException e) {
    }

    sp.register(new TestWorkerService());
    sp.register(new TestWorkerService()); // this will just replace previous service
    sp.register(new TestCommonService());
    sp.getService("testWorkerService");
    sp.getService("testCommonService");
    assertTrue(TestWorkerService.num == 1);
    assertTrue(TestCommonService.num == 1);

    ServiceProvider copySp = new ServiceProvider(sp);
    copySp.getService("testWorkerService");
    copySp.getService("testCommonService");
    assertTrue(TestWorkerService.num == 2);
    assertTrue(TestCommonService.num == 1);
  }

  public void testGetService() throws Exception
  {
    try {
      sp.getService(null);
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      sp.getService("unknown service");
      fail();
    } catch (NoSuchServiceException e) {
    }

    sp.register(new TestCommonService());
    assertNotNull(sp.getService("testCommonService"));
  }

  public void testRegister()
  {
    try {
      sp.register(null);
      fail();
    } catch (NullArgumentException e) {
    }

    sp.register(new TestCommonService());
    sp.register(new TestWorkerService());
    assertTrue(sp.getAllServices().size() == 2);
  }

  public void testStopAllServices()
  {
    sp.register(new TestCommonService());
    sp.register(new FailedTestWorkerService());
    try {
      sp.stopAllServices();
      fail();
    } catch (ServiceException e) {
    }
  }

  public void testStopAllWorkerServices()
  {
    sp.register(new TestCommonService());
    sp.register(new FailedTestWorkerService());
    try {
      sp.stopAllWorkerServices();
      fail();
    } catch (ServiceException e) {
    }
  }

  public void testStopAllCommonServices() throws Exception
  {
    sp.register(new TestCommonService());
    sp.register(new FailedTestWorkerService());
    sp.stopAllCommonServices();
  }

  public void testGetWorkerServices()
  {
    assertTrue(sp.getWorkerServices().size() == 0);
    assertTrue(sp.getCommonServices().size() == 0);
    
    WorkerService ws1 = new TestWorkerService();
    WorkerService ws2 = new FailedTestWorkerService();
    sp.register(ws1);
    sp.register(ws2);
    
    assertTrue(sp.getCommonServices().size() == 0);
    assertTrue(sp.getWorkerServices().size() == 2);
    assertEquals(ws1, sp.getWorkerServices().get("testWorkerService"));
    assertEquals(ws2, sp.getWorkerServices().get("failedTestWorkerService"));
  }

  public void testGetCommonServices()
  {
    assertTrue(sp.getWorkerServices().size() == 0);
    assertTrue(sp.getWorkerServices().size() == 0);
    
    CommonService cs = new TestCommonService();
    sp.register(cs);
    assertTrue(sp.getWorkerServices().size() == 0);
    assertTrue(sp.getCommonServices().size() == 1);
    assertEquals(cs, sp.getCommonServices().get("testCommonService"));
  }

  public void testGetAllServices()
  {
    assertTrue(sp.getWorkerServices().size() == 0);
    assertTrue(sp.getCommonServices().size() == 0);
    
    CommonService cs = new TestCommonService();
    WorkerService ws1 = new TestWorkerService();
    WorkerService ws2 = new FailedTestWorkerService();
    sp.register(ws1);
    sp.register(ws2);
    sp.register(cs);
    
    assertTrue(sp.getWorkerServices().size() == 2);
    assertTrue(sp.getCommonServices().size() == 1);
  }

  // //////////////////////////////////////////////////////////////////////////////////////
  static class TestWorkerService extends AbstractWorkerService
  {
    public static int num = 1;

    public TestWorkerService()
    {
      super("testWorkerService");
    }

    public WorkerService newCopyInstance() throws ServiceException
    {
      num++;
      return new TestWorkerService();
    }

  }

  static class FailedTestWorkerService extends AbstractWorkerService
  {
    public FailedTestWorkerService()
    {
      super("failedTestWorkerService");
    }

    public WorkerService newCopyInstance() throws ServiceException
    {
      throw new ServiceException("Failed to create instance of FailedTestWorkerService.");
    }

    public void stop() throws ServiceException
    {
      throw new ServiceException("Failed stop FailedTestWorkerService.");
    }

  }

  static class TestCommonService extends AbstractService implements CommonService
  {
    public static int num = 1;

    public TestCommonService()
    {
      super("testCommonService");
    }
  }

}
