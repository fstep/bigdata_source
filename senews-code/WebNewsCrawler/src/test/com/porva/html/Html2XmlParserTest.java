package com.porva.html;

import java.io.File;
import java.io.FileInputStream;
import java.net.URI;

import junit.framework.TestCase;

import org.apache.commons.lang.NullArgumentException;

import com.porva.html2xml.ScriptTester;

public class Html2XmlParserTest extends TestCase
{
  static byte[] content = null;

  static byte[] contentInvalid = null;

  static URI uri = null;

  static {
    try {
      FileInputStream fin = new FileInputStream("test/selowdown.html");
      content = new byte[34037];
      fin.read(content);
      uri = new URI(
          "http://www.searchenginelowdown.com/2005/10/googles-q3-earnings-pundit-roundup.html");
      fin.close();

      FileInputStream fin2 = new FileInputStream("test/manual-mptfi.html");
      contentInvalid = new byte[718];
      fin2.read(contentInvalid);
      fin2.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Test for {@link HTML2XMLParser#process(String, URI)}.
   */
  public void testProcess() throws Exception
  {
    HTML2XMLParser parser = new HTML2XMLParser(new File("conf/html2xml.properties"), new File("conf/scripts/"));
    try {
      parser.process(null, uri);
      fail();
    } catch (NullArgumentException e) {
    }

    // process with xml result
    String page = new String(content, "iso-8859-1");
    String xml = parser.process(page, uri);
    //System.out.println(xml);
    String resultPattern = "article ==   1;"
        + "title[1]   eq    'Google's Q3 Earnings Pundit Roundup';"
        + "iso-date[1] eq    '2005-10-21';" + "content[1] =~    /^My favorite Google earnings.*/s;";
    assertTrue(ScriptTester.testResult(xml, resultPattern));

    assertNull("Cannot process without uri", parser.process(page, null));
    assertNull("Cannot parse this page", parser.process("xxx", uri));
  }
}
