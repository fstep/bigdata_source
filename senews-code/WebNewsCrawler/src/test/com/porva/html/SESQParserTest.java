package com.porva.html;

import java.io.File;
import java.io.FileInputStream;
import java.net.URI;

import junit.framework.TestCase;

import org.apache.commons.lang.NullArgumentException;

public class SESQParserTest extends TestCase
{

  /*
   * Test method for 'com.porva.html.SESQParser.SESQParser(File)'
   */
  public void testSESQParser()
  {
    try {
      new SESQParser(null);
      fail();
    } catch (NullArgumentException e) {
    }
    
    try {
      new SESQParser(new File("does/not/exist"));
      fail();
    } catch (IllegalArgumentException e) {
    }
    
    new SESQParser(new File("conf/scripts"));
  }

  /*
   * Test method for 'com.porva.html.SESQParser.process(String, URI)'
   */
  public void testProcess() throws Exception
  {
    SESQParser parser = new SESQParser(new File("conf/scripts/sesq"));
    
    try {
      parser.process(null, null);
      fail();
    } catch (NullArgumentException e) {
    }
    
    String html = readFile(new File("test/pandia-article.html"), "utf-8");
    String xml = parser.process(html, new URI("http://www.pandia.com/sew/159-microsoft-deletes-outspoken-chinese-blog.html"));
    assertTrue(xml != null);    
  }
  
  private String readFile(final File file, final String charset)
  {
    try {
      FileInputStream fis = new FileInputStream(file);
      byte buffer[] = new byte[4096];
      StringBuffer sb = new StringBuffer(512);
      int len;
      while ((len = fis.read(buffer)) >= 0) {
        sb.append(new String(buffer, 0, len, charset));
      }
      fis.close();
      return sb.toString();
    } catch (Exception e) {
      e.printStackTrace();
      return "";
    }
  }


  /*
   * Test method for 'com.porva.html.SESQParser.getTitle()'
   */
  public void testGetTitle()
  {

  }

  /*
   * Test method for 'com.porva.html.SESQParser.getLinks()'
   */
  public void testGetLinks()
  {

  }

  /*
   * Test method for 'com.porva.html.SESQParser.newCopyInstance()'
   */
  public void testNewCopyInstance()
  {

  }

}
