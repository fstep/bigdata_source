package com.porva.html;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.UnhandledException;

import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.net.Response;
import com.porva.crawler.net.apache.ApacheHttpService;
import com.porva.crawler.service.LinksExtractorService;
import com.porva.crawler.task.FetchResult;
import com.porva.crawler.task.DefaultFetchResult;
import com.porva.crawler.task.FetchTask;
import com.porva.crawler.task.TaskFactory;

public class HTMLSAXLinksExtractorTest extends TestCase
{
  static Response response;

  FetchResult failedRes;

  FetchResult completeRes;

  FetchTask task1;

  LinksExtractorService linksExtractor;

  static {
    ApacheHttpService apacheHttpClient = new ApacheHttpService();
    try {
      response = apacheHttpClient.get(new CrawlerURL(1, "http://localhost/tmp/links.html"));
    } catch (Exception e) {
      throw new UnhandledException(e);
    }
  }

  protected void setUp() throws Exception
  {
    linksExtractor = new HTMLSAXLinksExtractor();

    task1 = (FetchTask) TaskFactory.newFetchURL(new CrawlerURL(1, "http://localhost"));
    failedRes = DefaultFetchResult.newFailedResult(task1, DefaultFetchResult.Status.TASK_FILTERED);
    completeRes = DefaultFetchResult.newCompleteResult(task1, response);
  }

  public void testHTMLSAXLinksExtractor()
  {
    LinksExtractorService linksExtractor = new HTMLSAXLinksExtractor();
    assertNotNull(linksExtractor);
  }

  /*
   * Class under test for List<CrawlerURL> extractLinks(DefaultFetchResult)
   */
  public void testExtractLinks1()
  {
    try {
      linksExtractor.extractLinks(null, new ArrayList<CrawlerURL>());
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      linksExtractor.extractLinks(failedRes, null);
      fail();
    } catch (NullArgumentException e) {
    }

    List<CrawlerURL> links = new ArrayList<CrawlerURL>();
    assertTrue("extracting from failed task", linksExtractor.extractLinks(failedRes, links) == 0);
    assertTrue(links.size() == 0);

    assertTrue(linksExtractor.extractLinks(completeRes, links) != 0);
    assertTrue(links.size() == 12);
  }

  /*
   * Class under test for List<CrawlerURL> extractLinks(CrawlerURL, byte[])
   */
  public void testExtractLinks2()
  {
    try {
      linksExtractor.extractLinks(null, "test".getBytes(), new ArrayList<CrawlerURL>());
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      linksExtractor.extractLinks(failedRes.getTask().getCrawlerURL(), null,
                                  new ArrayList<CrawlerURL>());
      fail();
    } catch (NullArgumentException e) {
    }
    try {
      linksExtractor.extractLinks(failedRes.getTask().getCrawlerURL(), "test".getBytes(),
                                  null);
      fail();
    } catch (NullArgumentException e) {
    }

    List<CrawlerURL> links = new ArrayList<CrawlerURL>();
    assertTrue("extracting from failed task", linksExtractor.extractLinks(failedRes, links) == 0);
    assertTrue(links.size() == 0);

    assertTrue(linksExtractor.extractLinks(completeRes, links) != 0);
    assertTrue(links.size() == 12);    
  }

}
