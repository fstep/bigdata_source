#!/usr/bin/perl -w
package TanaSend;

use strict;
use IO::Socket;
use Data::Dumper;

sub sendMsg($ $ $)
{
    my ($host, $port, $str) = @_;
    my $socket = IO::Socket::INET->new(Proto => 'tcp',
                                    PeerPort => $port,
                                    #Type => SOCK_STREAM,
                                    PeerAddr => $host,
				    #Blocking => 1
				    )
        or die "Can't create client for $host:$port: $!\n";

    #$socket->autoflush(1);

    print $socket $str;
    return readMsg($socket);
}

sub error
{
    print '[error] ', shift, "\n";
}

sub readNumber
{
    my $socket = shift;
    my $ch = '';
    my $num = '';
    while (1) {
	read($socket, $ch, 1);
        last if ($ch !~ /\d/);
        $num .= $ch;
    }

    return $num;
}

sub readChar
{
    my $socket = shift;
    my $exp_ch = shift;
    my $ch;
    read($socket, $ch, 1);
    if ($ch ne $exp_ch) {
        error("$exp_ch expected but $ch found");
		return;
    }
    return 1;
}

sub readMsg
{
    my %h = ();
    my $socket = shift;
    my $ch;
    my $buff;
	my $num;

    #read($socket, $buff, 3);
	$buff = <$socket>;
	chomp $buff;
	($buff, $num) =  split(/ /, $buff);
	
    if ($buff ne 'fix') {
        error("only fix messages supported");
		return;
    }
    #return if (!readChar($socket, ' '));
    #my $num = readNumber($socket);

    for (my $i = 0; $i < $num; $i++) {
        my $buff1 = '';
        my $buff2 = '';
        my $keyLength = readNumber($socket);
		read($socket, $buff1, $keyLength);
        return if (!readChar($socket, ':'));
        return if (!readChar($socket, ' '));
        my $valueLength = readNumber($socket);
		read($socket, $buff2, $valueLength);

		if (length($buff2) != $valueLength) {
            error("expected $valueLength length value but readed only ".length($buff2));
			return;
        }
        return if (!readChar($socket, "\n"));

        $h{$buff1} = $buff2;
    }

    return \%h;
}

sub hashToStr
{
    my $str = '';
    my %h = %{$_[0]};

    my $size = keys(%h);
    $str = "fix $size\n";
    
    for my $key (keys %h) {
        $str .= length($key)." $key: ".length($h{$key});
        if (length($h{$key}) != 0) {
            $str .= " $h{$key}\n";
        }
        else {
            $str .= "\n";
        }
    }
    
    return $str;
}

return 1;
###############
# main
#
#my %h = ("function" => "parse", "language" => "english", "format"=>"special", "content"=>"some content");
#my %result = sendMsg("ganges.hiit.fi", 5555, hashToStr(\%h));
#
#print $result{'parsed-content'};
