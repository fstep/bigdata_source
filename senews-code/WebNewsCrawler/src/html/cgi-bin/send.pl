#!/usr/bin/perl -w

use strict;
use TanaSend;
use WebForm;
&WebForm::GetFormInput;

# 1. get script and html source
my $script = $field{textarea};
#my $page_content = $field{pageContent};
my $page_content = read_content();

# 2. send them to html2xml java server via TANA and receive result
my $host = 'localhost';
my $port = 12345;
my %h = ('cmd' => 'test', 'content'=>$page_content, 'script'=>$script);
my $result = TanaSend::sendMsg($host, $port, TanaSend::hashToStr(\%h));

# 3. print result

if (defined($result->{'error'})) {
    print "Content-Type: text/html\n\n";
print <<EOF;
<html>
<head>
<title>Script test result</title>
</head>
<body>
error: $result->{'error'} <br>
</body>
</html>
EOF
} else {
    print "Content-Type: text/xml\n\n";
    print $result->{'xml-content'};
}


sub read_content
{
    open(FP, "_orig.html") or die $!;
    my $content = '';
    while (defined(my $str = <FP>)) {
        $content .= $str."\n";
    }
    close FP;
    return $content;
}