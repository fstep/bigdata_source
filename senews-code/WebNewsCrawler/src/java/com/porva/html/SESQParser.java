/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.html;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import sesq.webextractor.ExtractParser;
import sesq.webextractor.Extractor;

import com.porva.crawler.service.AbstractWorkerService;
import com.porva.crawler.service.ServiceException;
import com.porva.crawler.service.WorkerService;

/**
 * {@link com.porva.html.HTMLParser} that parses given HTML content to XML by SESQ parser.
 * 
 * @author Poroshin V.
 * @date Feb 7, 2006
 */
public class SESQParser extends AbstractWorkerService implements HTMLParser
{
  // private Extractor extractor = new Extractor();
  private File scriptsDir = null;

  private static Map<String, List<Script>> hostScript = new HashMap<String, List<Script>>();

  private Extractor extractor = null;

  private static final Log logger = LogFactory.getLog(SESQParser.class);

  /**
   * Allocates a new {@link SESQParser} object with specified directory of scripts' location.
   * 
   * @param scriptsDir directory of scripts.
   * @throws NullArgumentException if <code>scriptsDir</code> is <code>null</code>.
   * @throws IllegalArgumentException if <code>scriptsDir</code> file does not exist or it is not
   *           a directory.
   */
  public SESQParser(final File scriptsDir)
  {
    super(HTMLParser.SESQ);

    this.scriptsDir = scriptsDir;
    if (this.scriptsDir == null)
      throw new NullArgumentException("scriptsDir");
    if (!this.scriptsDir.exists() || !this.scriptsDir.isDirectory())
      throw new IllegalArgumentException();
  }

  /**
   * Parse input <code>html</code> content to XML output string by SESQ parser.
   * 
   * @return parsing result as XML string or <code>null</code> if parsing failed.
   * @throws NullArgumentException if <code>html</code> or <code>uri</code> is <code>null</code>.
   */
  public String process(final String html, final URI uri)
  {
    extractor = null;
    String xml = null;

    if (html == null)
      throw new NullArgumentException("html");
    if (uri == null)
      throw new NullArgumentException("uri");

    final String uriStr = uri.toString();
    final List<Script> scripts = getScripts(uri.getHost());
    if (scripts.isEmpty())
      return null;

    for (Script script : scripts) {

      if (logger.isInfoEnabled())
        logger.info("Using script file: " + script.getScriptFile());

      extractor = ExtractParser.instance().parse(script.getScriptFile());

      RunnableParser parserRunnable = new RunnableParser(html, uriStr, extractor);
      Thread parserThread = new Thread(parserRunnable);

      try {
        parserThread.start();
        parserThread.join(1000 * 30); // todo: parameter to config file
        if (parserThread.isAlive())
          parserThread.interrupt();
      } catch (InterruptedException e) {
        logger.warn("SESQ parser need to be interrupted due to timeout!");
        // timerThread.join();
      }

      if (!isResultEmpty(parserRunnable.result)) {
        xml = parserRunnable.result;
        break;
      }
    }

    if (xml == null) { // html2xml parser failed to parse even with script
      if (logger.isWarnEnabled() && !scripts.isEmpty())
        logger.warn("Tsinghua parser failed to parse:" + " url=" + uriStr);
    }

    return xml;
  }

  public String getTitle()
  {
    // TODO Auto-generated method stub
    return null;
  }

  public List<String> getLinks()
  {
    // TODO Auto-generated method stub
    return null;
  }

  public WorkerService newCopyInstance() throws ServiceException
  {
    return new SESQParser(scriptsDir);
  }

  private boolean isResultEmpty(final String xml)
  {
    if (xml == null)
      return true;
    String[] line = xml.split("\\n");
    if (line[1].matches("<DOCUMENT>") && line[2].matches("</DOCUMENT>"))
      return true;
    return false;
  }

  private List<Script> getScripts(final String host)
  {
    List<Script> scripts = hostScript.get(host);
    if (scripts == null) {
      scripts = new ArrayList<Script>();
      File[] files = getScriptsFiles(host);
      for (int i = 0; i < files.length; i++) {
        Script scr = getScript(files[i]);
        if (scr != null)
          scripts.add(scr);
      }

      hostScript.put(host, scripts);
    }
    return scripts;
  }

  private File[] getScriptsFiles(final String host)
  {
    RegexpFilenameFilter filter = new RegexpFilenameFilter(".*" + host + ".*");
    return scriptsDir.listFiles(filter);
  }

  private Script getScript(final File f)
  {
    Script script = null;
    if (f.exists() && f.isFile()) {
      String scriptContent = readFile(f, "iso-8859-1");
      script = new Script(scriptContent, f);
    }
    return script;
  }

  private String readFile(final File file, final String charset)
  {
    try {
      FileInputStream fis = new FileInputStream(file);
      byte buffer[] = new byte[4096];
      StringBuffer sb = new StringBuffer(512);
      int len;
      while ((len = fis.read(buffer)) >= 0) {
        sb.append(new String(buffer, 0, len, charset));
      }
      fis.close();
      return sb.toString();
    } catch (Exception e) {
      e.printStackTrace();
      return "";
    }
  }

  // ///////////////////////////////////////////////////////////////////////////////////////////
  private static class RunnableParser implements Runnable
  {
    String html;

    String uriStr;

    String result;

    final String encoding = "iso-8859-1";

    Extractor extractor;

    /**
     * Allocates a new {@link RunnableParser} object.
     * 
     * @param html
     * @param uriStr
     * @param extractor
     * @throws NullArgumentException if <code>html</code> or <code>uriStr</code> or
     *           <code>extractor</code> is <code>null</code>.
     */
    public RunnableParser(final String html, final String uriStr, final Extractor extractor)
    {
      this.html = html;
      this.uriStr = uriStr;
      this.extractor = extractor;

      if (this.extractor == null)
        throw new NullArgumentException("extractor");
      if (this.html == null)
        throw new NullArgumentException("html");
      if (this.uriStr == null)
        throw new NullArgumentException("uriStr");
    }

    public void run()
    {
      try {
        ByteArrayInputStream in = new ByteArrayInputStream(html.getBytes(encoding));
        extractor.setPage(in, encoding, uriStr);
        extractor.process();
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        extractor.writeResult(pw);
        result = sw.toString();
      } catch (UnsupportedEncodingException e) {
        logger.fatal("Encoding is not supported", e);
        e.printStackTrace(); // should not happen
      }
    }
  }

  // ///////////////////////////////////////////////////////////////////////////////////////////
  private static class Script
  {
    private String scriptContent = null;

    private File scriptFile = null;

    /**
     * Allocates a new {@link Script} object.
     * 
     * @param scriptContent
     * @param scriptFile
     * @throws NullArgumentException if <code>scriptContent</code> or <code>scriptFile</code> is
     *           <code>null</code>.
     */
    public Script(final String scriptContent, final File scriptFile)
    {
      this.scriptContent = scriptContent;
      this.scriptFile = scriptFile;

      if (this.scriptContent == null)
        throw new NullArgumentException("scriptContent");
      if (this.scriptFile == null)
        throw new NullArgumentException("scriptFile");
    }

    /**
     * Returns content of the script as string.
     * 
     * @return content of the script as string.
     */
    public String getScriptContent()
    {
      return scriptContent;
    }

    /**
     * Returns file of the script.
     * 
     * @return file of the script.
     */
    public File getScriptFile()
    {
      return scriptFile;
    }
  }

  // ///////////////////////////////////////////////////////////////////////////////////////////
  class RegexpFilenameFilter implements FilenameFilter
  {
    private Pattern pattern;

    public RegexpFilenameFilter(final String regexp)
    {
      this.pattern = Pattern.compile(regexp);
    }

    public boolean accept(File dir, String name)
    {
      if (pattern.matcher(name).matches())
        return true;
      return false;
    }
  }

}
