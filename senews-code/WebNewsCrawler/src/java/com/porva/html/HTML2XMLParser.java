/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.html;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.crawler.CrawlerInit;
import com.porva.crawler.service.AbstractWorkerService;
import com.porva.crawler.service.ServiceException;
import com.porva.crawler.service.WorkerService;
import com.porva.html.keycontent.NewsXMLCleaner;
import com.porva.html2xml.DefaultScriptRunner;
import com.porva.html2xml.NewsXMLResultFormatter;
import com.porva.html2xml.ScriptLoader;
import com.porva.html2xml.StringScriptParser;
import com.porva.html2xml.task.StoreLinks;
import com.porva.html2xml.task.StoreTask;
import com.porva.html2xml.task.Task;

public class HTML2XMLParser extends AbstractWorkerService implements DOMDocumentParser
{
  private static final Log logger = LogFactory.getLog(HTML2XMLParser.class);

  private List<String> links = null;

  private DefaultScriptRunner scriptRunnerImpl;

  private ScriptLoader scriptLoader;

  private File propFile = null;

  private File scriptsDir = null;

  private String title = null;

  private String xmlEncoding = "utf-8";
  
  private NewsXMLCleaner newsXmlCleaner = new NewsXMLCleaner();

  public HTML2XMLParser(final File propFile, final File scriptsDir)
      throws ParserConfigurationException, IOException
  {
    super(DOMDocumentParser.HTML2XML);
    scriptRunnerImpl = new DefaultScriptRunner();

    this.propFile = propFile;
    this.scriptsDir = scriptsDir;
    if (this.propFile == null)
      throw new NullArgumentException("propFile");
    if (this.scriptsDir == null)
      throw new NullArgumentException("scriptsDir");
    if (!this.propFile.exists())
      throw new IllegalArgumentException("properties file does not exist: " + this.propFile);
    if (this.propFile.isDirectory())
      throw new IllegalArgumentException("properties file is a directory: " + this.propFile);
    if (!this.scriptsDir.exists())
      throw new IllegalArgumentException("scripts directory does not exist: " + this.scriptsDir);
    if (!this.scriptsDir.isDirectory())
      throw new IllegalArgumentException("scripts directory is a file: " + this.scriptsDir);

    if (CrawlerInit.getProperties().get("parser.html2xml.output-encoding") != null)
      xmlEncoding = (String) CrawlerInit.getProperties().get("parser.html2xml.output-encoding");

    scriptLoader = new ScriptLoader(propFile, scriptsDir);
  }

  /* (non-Javadoc)
   * @see com.porva.html.DOMDocumentParser#process(java.lang.String, java.net.URI)
   */
  public String process(final String content, final URI uri)
  {
    if (content == null)
      throw new NullArgumentException("content");

    List<String> scriptList = scriptLoader.getScripts(uri);
    if (scriptList == null || scriptList.size() == 0)
      return null;

    String xml = null;
    for (String script : scriptList) {
      try {
        List<Task> tasks = scriptRunnerImpl.performScript(content, uri, new StringScriptParser(script));
        setLinks(tasks);
        setTitle(tasks);
        NewsXMLResultFormatter xmlResultFormatter = new NewsXMLResultFormatter();
        xml = xmlResultFormatter.format(tasks, xmlEncoding, scriptRunnerImpl);
      } catch (Exception e) {
        logger.warn("Html2Xml exception", e); // todo
        xml = "<CONTENT></CONTENT>";
      } 
      
      if (xml.contains("<DOCUMENT/>") || xml.contains("<CONTENT></CONTENT>")
          || xml.contains("<CONTENT/>") || xml.contains("<content/>")
          || xml.contains("<content></content>")) {
        logger.debug(xml);
        xml = null;
      }
      
      if (xml != null)
        break;
    }
    
    //  clean news xml
    if (xml != null) {
      try {
        xml = newsXmlCleaner.clean(xml);
      } catch (Exception e) {
        logger.warn("Failed  to clean news xml", e);
      } 
    }

    return xml;
  }

  public String getTitle()
  {
    return title;
  }

  public List<String> getLinks()
  {
    return links;
  }

  public WorkerService newCopyInstance() throws ServiceException
  {
    try {
      return new HTML2XMLParser(propFile, scriptsDir);
    } catch (ParserConfigurationException e) {
      throw new ServiceException(e);
    } catch (IOException e) {
      throw new ServiceException(e);
    }
  }

  private void setLinks(final List<Task> tasks)
  {
    assert tasks != null;

    for (final Task task : tasks) {
      if (task instanceof StoreLinks) {
        final StoreLinks storeLinks = (StoreLinks) task;
        if (links == null)
          links = new ArrayList<String>();
        links.addAll(storeLinks.getLinks());
      }
    }
  }

  private void setTitle(final List<Task> tasks)
  {
    assert tasks != null;

    for (final Task task : tasks) {
      if (task instanceof StoreTask) {
        final StoreTask storeTask = (StoreTask) task;
        if (storeTask.getLabel().equalsIgnoreCase("title")) {
          title = storeTask.getContent();
          break;
        }
      }
    }
  }

}
