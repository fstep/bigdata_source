/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.html;

import java.net.URL;
import java.nio.charset.Charset;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.net.Response;
import com.porva.crawler.service.AbstractWorkerService;
import com.porva.crawler.service.ServiceException;
import com.porva.crawler.service.WorkerService;

public class CharsetDetectorService extends AbstractWorkerService
{
  public static final Charset DEF_CHARSET = Charset.forName("utf-8");

  private HTMLCpDetector htmlCpDetector = new HTMLCpDetector(new CpDetector(), DEF_CHARSET);

  public CharsetDetectorService()
  {
    super("charset-detector");
  }

  public WorkerService newCopyInstance() throws ServiceException
  {
    return new CharsetDetectorService();
  }

  public Charset detect(final Response response, final URL url)
  {
    if (response == null)
      throw new NullArgumentException("response");

    byte[] content = response.getBody();
    String httpCharset = HTMLCpDetector.getResponseCharset(response.getHeaderValue("content-type"));
    String htmlCharset = HTMLCpDetector.getMetaTagCharset(content);

    return htmlCpDetector.detect(httpCharset, content, htmlCharset, url);
  }
}
