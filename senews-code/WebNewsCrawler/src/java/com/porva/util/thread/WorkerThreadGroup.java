/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.util.thread;

import java.util.Vector;
import java.util.Enumeration;

/**
 * Provides control for the group of <code>WorkerThread</code> objects.
 */

public class WorkerThreadGroup
{
  private Vector<WorkerThread> threads = new Vector<WorkerThread>();

  /**
   * Adds new <code>WorkerThread</code> to current group.
   * @param thread <code>WorkerThread</code> to add.
   */
  public void add(WorkerThread thread)
  {
    if (thread != null)
      threads.add(thread);
  }

  /**
   * Returns threads in current <code>WorkerThread</code> with specified status.
   * @param workerTheadStatus status of threads to be returned.
   * @return WorkerThread threads vector.
   */
  public Vector get(WorkerThreadStatus workerTheadStatus)
  {
    Vector<WorkerThread> v = new Vector<WorkerThread>();
    removeDoneThreads();
    for (Enumeration e = threads.elements(); e.hasMoreElements(); ) {
      WorkerThread workerThread = ((WorkerThread)e.nextElement());
      if (workerThread.getStatus() == workerTheadStatus)
        v.add(workerThread);
    }
    return v;
  }

  /**
   * Forces removing of <code>WorkerThread</code> objects with <code>DONE</code> status
   * form current group.
   */
  public void removeDoneThreads()
  {
    for (Enumeration e = threads.elements(); e.hasMoreElements(); ) {
      WorkerThread workerThread = ((WorkerThread)e.nextElement());
      if (workerThread.getStatus() == WorkerThreadStatus.DONE)
        threads.removeElement(workerThread);
    }
  }

  /**
   * Interrupts <code>WorkerThread</code> threads with
   * specified <code>workerTheadStatus</code> status
   * @param workerTheadStatus status of threads to interrupted.
   */
  public void interrupt(WorkerThreadStatus workerTheadStatus)
  {
    removeDoneThreads();
    for (Enumeration e = threads.elements(); e.hasMoreElements(); ) {
      WorkerThread workerThread = ((WorkerThread)e.nextElement());
      if (workerThread.getStatus() == workerTheadStatus)
        workerThread.interrupt();
    }
  }

  /**
   * Wakes up <code>WorkerThread</code> threads in the group with
   * <code>workerTheadStatus</code> status.
   * @param workerTheadStatus status of threads to wake up.
   */
  public void resume(WorkerThreadStatus workerTheadStatus)
  {
    removeDoneThreads();
    for (Enumeration e = threads.elements(); e.hasMoreElements(); ) {
      WorkerThread workerThread = ((WorkerThread)e.nextElement());
      if (workerThread.getStatus() == workerTheadStatus)
        ((WorkerThread)e.nextElement()).resumeWork();
    }
  }

  /**
   * Starts all <code>WorkerThread</code> threads in the group.
   */
  public void startAll()
  {
    removeDoneThreads();
    for (Enumeration e = threads.elements(); e.hasMoreElements(); )
      ((WorkerThread)e.nextElement()).start();
  }

  /**
   * Wakes up all <code>WorkerThread</code> threads in the group.
   */
  public void resumeAll()
  {
    removeDoneThreads();
    for (Enumeration e = threads.elements(); e.hasMoreElements(); )
      ((WorkerThread)e.nextElement()).resumeWork();
  }

  /**
   * Interrupts all <code>WorkerThread</code> threads in the group.
   */
  public void scheduleInterruptAll()
  {
    removeDoneThreads();
    for (Enumeration e = threads.elements(); e.hasMoreElements(); )
      ((WorkerThread)e.nextElement()).scheduleDone();
  }

  /**
   * Waits to join all <code>WorkerThread</code> threads in the group.
   */
  public void joinAll()
  {
    try {
      for (Enumeration e = threads.elements(); e.hasMoreElements(); )
        ((WorkerThread)e.nextElement()).join();
    } catch (InterruptedException e1) {
      e1.printStackTrace();
    }
  }

  /**
   * Schedule all <code>WorkerThread</code> threads in the group to pauseWork.
   * see also: WorkerThread.schedulePause()
   */
  public void schedulePauseAll()
  {
    removeDoneThreads();
    for (Enumeration e = threads.elements(); e.hasMoreElements(); )
      ((WorkerThread)e.nextElement()).schedulePause();
  }

  /**
   * Schedule all <code>WorkerThread</code> threads in the group to sleep specified sleepInMs.
   * @param ms sleepInMs in milliseconds to sleep.
   * see also: WorkerThread.scheduleSleep(long ms)
   */
  public void scheduleSleepAll(long ms)
  {
    removeDoneThreads();
    for (Enumeration e = threads.elements(); e.hasMoreElements(); )
      ((WorkerThread)e.nextElement()).scheduleSleep(ms);
  }

  /**
   * Checkes if all threads have the same status.
   * @param status status to check.
   * @return <code>true</code> if all threads have the same status; <code>false</code> otherwise.
   */
  public boolean isAllHaveOneStatus(WorkerThreadStatus status)
  {
    for (Enumeration e = threads.elements(); e.hasMoreElements(); )
      if ( ((WorkerThread)e.nextElement()).getInfo().getStatus() != status)
        return false;

    return true;
  }

}
