/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.util.thread;

/**
 * Extends standart <code>Thread</code> class to provide additional thread control
 * and status information.
 * New methods like <code>schedulePause</code> and <code>scheduleSleep</code>
 * are provided for saveConfFile thread control. Additional status information is represented in
 * class <code>WorkerThreadInfo</code>.
 */


abstract public class WorkerThread extends Thread
{
  private long sleepTime = 0;
  private WorkerThreadInfo info;

  /**
   * Returns information <code>WorkerThreadInfo</code> for this <code>WorkerThread</code>.
   * @return this thread's information.
   */
  public WorkerThreadInfo getInfo() { return info; }

  /**
   * Returns the status of the thread.
   * Redirects call <code>getStatus()</code> to current <code>WorkerThreadInfo</code> of the <code>WorkerThread</code>.
   * @return {@link WorkerThreadStatus} status of the current worker thread.
   */
  public WorkerThreadStatus getStatus() { return info.getStatus(); }

  /**
   * Allocated a new <code>WorkerThread</code> object.
   */
  public WorkerThread() { info = new WorkerThreadInfo(this); }

  /**
   * Wakes up <code>PAUSED</code> <code>WorkerThread</code> of current object and changes it status to  <code>RUNNING</code>.
   * A <code>WorkerThread</code> can be <code>PAUSED</code> by <code>schedulePause</code> function.
   */
  public synchronized void resumeWork()
  {
    notifyAll();
    info.setStatus(WorkerThreadStatus.RUNNING);
  }

  /**
   * Changes status of current <code>WorkerThread</code> to <code>DONE</code>.
   * This method should be called by <code>WorkerThread</code> childs
   * then work is finished.
   */
  public void scheduleDone() { info.setStatus(WorkerThreadStatus.GONNA_DONE); }

  /**
   * Schedules current <code>WorkerThread</code> object to pauseWork its work and
   * changes its status to <code>PAUSING</code>.
   * Actual pausing will be done then <code>update()</code> method is called.
   * After pausing object's work can be resumed by <code>resumeWork()</code> method.
   */
  public void schedulePause() { info.setStatus(WorkerThreadStatus.GONNA_PAUSE);  }

  /**
   * Schedules current <code>WorkerThread</code> object to sleep for <code>ms<code> milliseconds
   * and changes its status to <code>GONNA_SLEEP</code>.
   * Actual sleeping will be done then <code>update()</code> method is called.
   * @param ms milliseconds to sleep
   */
  public void scheduleSleep(long ms)
  {
    this.sleepTime = ms;
    info.setStatus(WorkerThreadStatus.GONNA_SLEEP);
  }

  /**
   * Locks current thread in object monitor until <code>resumeWork()</code> is called.
   */
  private synchronized void pause()
  {
    try {
      while (info.getStatus() == WorkerThreadStatus.PAUSED)
        wait();
    } catch (InterruptedException e) {}

  }

  /**
   * Causes the currently executing thread to sleep for the specified number of milliseconds.
   * @param ms milliseconds to sleep
   * @return actual slept milliseconds
   */
  private long sleepAndGetSlept(long ms)
  {
    long startTime = 0;
    try {
      startTime = System.currentTimeMillis();
      sleep(ms);
    } catch (InterruptedException e) {}
    return System.currentTimeMillis() - startTime;
  }

  /**
   * Updates status of <code>WorkerThread</code> object and performs
   * scheduled pauseWork and sleep requests.
   */
  protected void update()
  {
    if (info.getStatus() == WorkerThreadStatus.GONNA_PAUSE) {
      info.setStatus(WorkerThreadStatus.PAUSED);
      pause();
      info.setStatus(WorkerThreadStatus.RUNNING);
    }
    else if (info.getStatus() == WorkerThreadStatus.GONNA_SLEEP) {
      info.setStatus(WorkerThreadStatus.SLEEPING);
      info.setSleptTime(sleepAndGetSlept(sleepTime));
      info.setStatus(WorkerThreadStatus.RUNNING);
    }
    else if (info.getStatus() == WorkerThreadStatus.GONNA_DONE) {
      info.setStatus(WorkerThreadStatus.DONE);
      //this.interrupt();
    }
    else if (info.getStatus() == WorkerThreadStatus.READY) {
      info.setStatus(WorkerThreadStatus.RUNNING);
    }
  }

  /**
   * Called then status of <code>WorkerThread</code> object is changed.
   */
  abstract protected void fireStatusChanged();

}
