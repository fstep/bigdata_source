/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.util.thread;

import com.porva.util.IDGenerator;

/**
 * Status {@link WorkerThreadStatus} and other Information about {@link WorkerThread}.
 */

public class WorkerThreadInfo
{
  private WorkerThreadStatus status = WorkerThreadStatus.READY;          // initial status
  private long sleptTime = 0;
  private String id;
  private WorkerThread workerThread = null;

  /**
   * Allocates a new {@link WorkerThreadInfo} object with a
   * specified {@link WorkerThread} owner of it and <code>id</code> label.
   * @param workerThread owner of current {@link WorkerThreadInfo} object.
   * @param id unique id label.
   */
  public WorkerThreadInfo(WorkerThread workerThread, String id)
  {
    this.workerThread = workerThread;
    this.id = id;
  }

  /**
   * Allocates a new {@link WorkerThreadInfo} object with a
   * specified {@link WorkerThread} owner of it.
   * <code>id</code> label is generated automatically by {@link IDGenerator} class.
   * @param workerThread
   */
  public WorkerThreadInfo(WorkerThread workerThread)
  {
    this.workerThread = workerThread;
    this.id = IDGenerator.nextString();
  }

  /**
   * Returns slept sleepInMs for previous sleep of the {@link WorkerThread}  owner of current {@link WorkerThreadInfo} object.
   * @return slept sleepInMs in milliseconds.
   */
  public long getSleptTime() { return sleptTime; }

  /**
   * Set slept sleepInMs for previous sleep of the {@link WorkerThread}  owner of current {@link WorkerThreadInfo} object.
   * This method should be used only by {@link WorkerThread} owner of current {@link WorkerThreadInfo} object.
   * @param ms slept sleepInMs in milliseconds.
   */
  protected void setSleptTime(long ms) { this.sleptTime = ms; }

  /**
   * Returns unique ID of the current  {@link WorkerThreadInfo} object.
   * @return unique id as string.
   */
  public String getID() { return id; }

  /**
   * Changes status of current  {@link WorkerThreadInfo} object.
   * This method should be used only by {@link WorkerThread} owner of current {@link WorkerThreadInfo} object.
   * @param status new status
   */
  protected void setStatus(WorkerThreadStatus status)
  {
    this.status = status;
    workerThread.fireStatusChanged();
  }

  /**
   * Returns status of current  {@link WorkerThreadInfo} object.
   * @return {@link WorkerThreadStatus} status of current worker thread.
   */
  public WorkerThreadStatus getStatus()
  {
    return status;
  }

}
