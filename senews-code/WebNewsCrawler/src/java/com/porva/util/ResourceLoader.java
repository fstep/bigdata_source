/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.util;

import java.awt.Image;
import java.io.IOException;
import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.UIDefaults;

/**
 * Loads resources from jar files.
 */

public class ResourceLoader
{
  private static JarResources jar;
  private static final Icon defImage = new ImageIcon();

  // totally static class
  private ResourceLoader() {}

//  /**
//   * Sets jar archive from where resources will be extracted.
//   * @param jarFile filename of jar file
//   * @return <code>true</code> if jar file is successfully loaded; <code>false</code> otherwise.
//   */
  private static final boolean setJarRepository(String jarFile)
  {
    try {
      URL url = Configuration.searchResouce(jarFile);
      if (url!= null)
        jar = new JarResources(url.getPath());
      else
        return false;
    } catch (IOException e) {
      return false;
    }
    return true;
  }

//  /**
//   * Loads {@link ImageIcon} <code>name</code> from jar repository.
//   * @param name path to resource inside jar file.
//   * @return loaded {@link ImageIcon}.
//   */
//  public static final ImageIcon getImageIcon(String name)
//  {
//    return new ImageIcon(jar.getResource(name));
//  }
//
  public static final Image getImage(String key)
  {
    Icon i = MyDefaults.getIcon(key);
    if (i == null) {
      //no icon associated to the action.
      return null;
    }
    else return ((ImageIcon)MyDefaults.getIcon(key)).getImage();
  }

  protected static UIDefaults MyDefaults;
  protected static ResourceLoader foo = new ResourceLoader();
  public static final String ImagePath = "";

  static {
    setJarRepository("res.jar"); // todo:
    String imgDir = "img/";
    Object[] icons = {
      "Logo",                           makeIcon(foo.getClass(), imgDir + "logo48.png"),

      "Menu.File.NewProject",           makeIcon(foo.getClass(), imgDir + "New16.png"),
      "Menu.File.OpenProject",          makeIcon(foo.getClass(), imgDir + "Open16.png"),
      "Menu.Empty",                     makeIcon(foo.getClass(), imgDir + "Empty16.png"),
      "Menu.File.Export",               makeIcon(foo.getClass(), imgDir + "Export16.png"),
      "Menu.Download.Start",            makeIcon(foo.getClass(), imgDir + "Play16.png"),
      "Menu.Download.Pause",            makeIcon(foo.getClass(), imgDir + "Pause16.png"),
      "Menu.Download.Suspend",          makeIcon(foo.getClass(), imgDir + "Stop16.png"),
      "Menu.Settings.ProjectSettings",  makeIcon(foo.getClass(), imgDir + "Properties16.png"),
      "Menu.Plugins.DBStat",            makeIcon(foo.getClass(), imgDir + "Plugins/DBStat16.png"),
      "Menu.Plugins.SelfRefMap",        makeIcon(foo.getClass(), imgDir + "Plugins/SelfRefMap16.png"),
      "Menu.Plugins.HtmlViewer",        makeIcon(foo.getClass(), imgDir + "Plugins/HtmlViewer16.png"),
      "Menu.Plugins.SourceViewer",      makeIcon(foo.getClass(), imgDir + "Plugins/SourceViewer16.png"),
      "Menu.Help.OnlineTopics",         makeIcon(foo.getClass(), imgDir + "HelpOnline16.png"),
      "Menu.Help.About",                makeIcon(foo.getClass(), imgDir + "About16.png"),

      "Plugins.HtmlViewer.Back",            makeIcon(foo.getClass(), imgDir + "HtmlViewer/back.png"),
      "Plugins.HtmlViewer.Back-active",     makeIcon(foo.getClass(), imgDir + "HtmlViewer/back-active.png"),
      "Plugins.HtmlViewer.Forward",         makeIcon(foo.getClass(), imgDir + "HtmlViewer/forward.png"),
      "Plugins.HtmlViewer.Forward-active",  makeIcon(foo.getClass(), imgDir + "HtmlViewer/forward-active.png"),
      "Plugins.HtmlViewer.Refresh",         makeIcon(foo.getClass(), imgDir + "HtmlViewer/refresh.png"),
      "Plugins.HtmlViewer.Refresh-active",  makeIcon(foo.getClass(), imgDir + "HtmlViewer/refresh-active.png"),
      "Plugins.HtmlViewer.Stop",            makeIcon(foo.getClass(), imgDir + "HtmlViewer/stop.png"),
      "Plugins.HtmlViewer.Stop-active",     makeIcon(foo.getClass(), imgDir + "HtmlViewer/stop-active.png"),
      "Plugins.HtmlViewer.Go",              makeIcon(foo.getClass(), imgDir + "HtmlViewer/go.png"),
      "Plugins.HtmlViewer.Go-active",       makeIcon(foo.getClass(), imgDir + "HtmlViewer/go-active.png"),

      "Misc.HelpBulb",                  makeIcon(foo.getClass(), imgDir + "HelpBulb.png"),

    };

    MyDefaults = new UIDefaults (icons);     // (My)Create a defaults table initialized with the specified key/value pairs.
  }

  private static Icon makeIcon(Class cl, String path)
  {
    //return new ImageIcon(cl.getResource(path));
    //return LookAndFeel.makeIcon(cl, path);

    byte [] imageData = jar.getResource(path);
    if (imageData == null)
      return defImage;
    return new ImageIcon(imageData);
  }

  public static final Icon getIcon(String key)
  {
    Icon i = MyDefaults.getIcon(key);       // Le controlla tutte ?
    if (i == null) {
      //no icon associated to the action.
      return null;
    }
    else return MyDefaults.getIcon(key);
  }

}
