/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.util;

import java.util.List;
import java.util.Vector;
import java.util.Enumeration;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * Supplemental class for grabbing log output created by log4j.
 * It is mostly used for testing purposes. <br>
 *
 * For instance, in your TestCase class you can write:
 * <code>
<p>LogTracer logTracer = new LogTracer();<br>
<br>
protected void setUp()<br>
{<br>
&nbsp;&nbsp; logTracer.initLog4j(Configuration.class);<br>
&nbsp;&nbsp; logTracer.setPrintLog(true);<br>
&nbsp;&nbsp; // ...<br>
}</p>
 * </code>
 * This will grab all created log and print it to stdout.
 */

public class LogTracer
{
  private WatchAppender watchAppender;

  /**
   * Inits given class with <code>DEBUG</code> level of logging.
   * @param class2log class to init log for.
   */
  public void initLog4j(String class2log)
  {
    Logger.getLogger("").setLevel(Level.FINE);

    watchAppender = new WatchAppender();
    Logger.getLogger(class2log).addHandler(watchAppender);
  }

  /**
   * Inits root level with <code>DEBUG</code> level of logging.
   * This will init all classes with <code>DEBUG</code> level of logging.
   */
  public void initLog4j()
  {
    Logger.getLogger("").setLevel(Level.FINE);

    watchAppender = new WatchAppender();
    Logger.getLogger("").addHandler(watchAppender);
  }


  /**
   * Gets {@link WatchAppender} object with grabbed log.
   * @return {@link WatchAppender} object for current logged session.
   */
  public WatchAppender getLogHistory()  { return watchAppender; }

  /**
   * Specifies to print log to stdout or not.
   * @param isPrintLog <code>true</code> to print log to stdout or <code>false</code> otherwise.
   */
  public void setPrintLog(boolean isPrintLog) { watchAppender.isPrintLog = isPrintLog; }
}

class WatchAppender extends Handler
{
  private Vector<String> logData = new Vector<String>();
  private Vector<String> levelData = new Vector<String>();
  boolean isPrintLog = false;

  public void publish(LogRecord record)
  {
    levelData.add(record.getLevel().toString());
    logData.add(record.getMessage());
    if (isPrintLog)
      System.out.println(record.getMillis() + "\t" + record.getThreadID() + "\t" +  record.getLevel().toString() + "\t" +
              record.getMessage());
  }

  public void flush()
  {
  }

  /**
   * Erases all grabbed log data.
   */
  public void close()
  {
    logData.clear();
  }

  /**
   * Returns log message number i starting from the begginng of logging session.
   * @param i number of log message to return.
   * @return log message.
   */
  public String getMessageAt(int i)
  {
    return logData.elementAt(i);
  }

  /**
   * Returns all grabbed messages in one string.
   * Messages are separated by new line characted "\n".
   * @return all messages logged in current session.
   */
  public String getMessages()
  {
    String res = new String("");
    for (Enumeration e = logData.elements(); e.hasMoreElements(); ) {
      res += e.nextElement() + "\n";
    }
    return res;
  }

  /**
   * Returns all indexes of messages with given loggin livel.
   * @param level logging level.
   * @return Array of {@link Integer} indexes.
   */
  public Object[] getMsgIndexesAt(String level)
  {
    List<Integer> res = new Vector<Integer>();
    for (int i = 0; i < levelData.size(); i++) {
      String el = (String)levelData.elementAt(i);
      if (el.equals(level)) {
        res.add(i);
      }
    }
    return res.toArray();
  }

}