/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.util.gui;

import javax.swing.*;

import org.apache.commons.lang.NullArgumentException;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

/**
 * Template class to create standart dialog box with two buttons: Ok and Cancle. Usage: Use in
 * constructor of child class expression:
 * <code>addMainComponent(Component your_central_component);</code> to add your main container
 * with elements to dialog box.
 */
abstract public class JDialogStandart extends JDialog
{
  JPanel jPanelBottom1 = new JPanel();

  JPanel jPanelBottom2 = new JPanel();

  JButton jButtonOk = new JButton();

  JButton jButtonCancel = new JButton();

  public JDialogStandart(final Frame owner, final String title, boolean isModal)
  {
    super(owner, title, isModal);
    try {
      setDefaultCloseOperation(DISPOSE_ON_CLOSE);
      init();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void init() throws Exception
  {
    this.setResizable(true);
    jPanelBottom1.setLayout(new BorderLayout());
    jButtonOk.setMaximumSize(new Dimension(75, 27));
    jButtonOk.setMinimumSize(new Dimension(75, 27));
    jButtonOk.setPreferredSize(new Dimension(75, 27));
    jButtonOk.setText("OK");
    jButtonCancel.setMaximumSize(new Dimension(75, 27));
    jButtonCancel.setMinimumSize(new Dimension(75, 27));
    jButtonCancel.setPreferredSize(new Dimension(75, 27));
    jButtonCancel.setText("Cancel");
    this.getContentPane()
        .add(Box.createRigidArea(new Dimension(10, 0)), java.awt.BorderLayout.EAST);
    this.getContentPane()
        .add(Box.createRigidArea(new Dimension(10, 0)), java.awt.BorderLayout.WEST);
    this.getContentPane().add(Box.createRigidArea(new Dimension(0, 15)),
                              java.awt.BorderLayout.NORTH);
    this.getContentPane().add(jPanelBottom1, java.awt.BorderLayout.SOUTH);

    jPanelBottom1.add(jPanelBottom2, java.awt.BorderLayout.EAST);
    jPanelBottom1.add(Box.createRigidArea(new Dimension(10, 0)), java.awt.BorderLayout.WEST);
    jPanelBottom1.add(Box.createRigidArea(new Dimension(0, 10)), java.awt.BorderLayout.NORTH);
    jPanelBottom1.add(Box.createRigidArea(new Dimension(0, 10)), java.awt.BorderLayout.SOUTH);
    jPanelBottom2.setLayout(new BoxLayout(jPanelBottom2, BoxLayout.X_AXIS));
    jPanelBottom2.add(jButtonOk);
    jPanelBottom2.add(Box.createRigidArea(new Dimension(5, 0)));
    jPanelBottom2.add(jButtonCancel);
    jPanelBottom2.add(Box.createRigidArea(new Dimension(10, 0)));

    jButtonOk.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonOk_actionPerformed(e);
      }
    });

    jButtonCancel.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonCancel_actionPerformed(e);
      }
    });
  }

  public void addMainComponent(final Component component)
  {
    getContentPane().add(component, java.awt.BorderLayout.CENTER);
  }

  abstract public void jButtonOk_actionPerformed(ActionEvent e);

  public void jButtonCancel_actionPerformed(ActionEvent e)
  {
    this.dispose();
  }

  // //////////// static fucntions /////////////////////////////////////////////////////

  /**
   * Centers given dialog in parent component.
   * 
   * @param jDialog dialog to center.
   * @throws NullArgumentException if <code>jDialog</code> is <code>null</code>.
   */
  public static void centerInParent(final JDialog jDialog)
  {
    if (jDialog == null)
      throw new NullArgumentException("jDialog");

    int x;
    int y;
    Container parent = jDialog.getParent();
    Point topLeft = parent.getLocationOnScreen();
    Dimension parentSize = parent.getSize();
    Dimension ownSize = jDialog.getSize();
    if (parentSize.width >= ownSize.width)
      x = ((parentSize.width - ownSize.width) / 2) + topLeft.x;
    else
      x = topLeft.x;
    if (parentSize.height >= ownSize.height)
      y = ((parentSize.height - ownSize.height) / 2) + topLeft.y;
    else
      y = topLeft.y;

    jDialog.setLocation(x, y);
    jDialog.requestFocus();
  }

  /**
   * Force the escape key to call the same action as pressing the Cancel button.
   * 
   * @param jComponent component to add "cancel by escape" functionality.
   * @param cancelAction action to perform for 'escape' key.
   * @throws NullArgumentException if <code>jComponent</code> or <code>cancelAction</code> is
   *           <code>null</code>.
   */
  public static void addCancelByEscapeKey(final JComponent jComponent,
                                          final AbstractAction cancelAction)
  {
    if (jComponent == null)
      throw new NullArgumentException("jComponent");
    if (cancelAction == null)
      throw new NullArgumentException("cancelAction");

    String CANCEL_ACTION_KEY = "CANCEL_ACTION_KEY";
    int noModifiers = 0;
    KeyStroke escapeKey = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, noModifiers, false);
    InputMap inputMap = jComponent.getRootPane()
        .getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    inputMap.put(escapeKey, CANCEL_ACTION_KEY);
    jComponent.getRootPane().getActionMap().put(CANCEL_ACTION_KEY, cancelAction);
  }

  /**
   * @param panel
   * @throws NullArgumentException if <code>panel</code> is <code>null</code>.
   * @throws IllegalStateException if <code>panel</code> do not have {@link BorderLayout} layout.
   */
  public static void addStandartBorder(final JPanel panel)
  {
    if (panel == null)
      throw new NullArgumentException("panel");
    if (!(panel.getLayout() instanceof java.awt.BorderLayout))
      throw new IllegalStateException("Panel must have border layout to add border");

    panel.add(Box.createRigidArea(new Dimension(15, 0)), java.awt.BorderLayout.EAST);
    panel.add(Box.createRigidArea(new Dimension(15, 0)), java.awt.BorderLayout.WEST);
    panel.add(Box.createRigidArea(new Dimension(0, 15)), java.awt.BorderLayout.NORTH);
    panel.add(Box.createRigidArea(new Dimension(0, 20)), java.awt.BorderLayout.SOUTH);
  }

}
