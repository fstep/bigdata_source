/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.util.gui;

import javax.swing.*;

/**
 * JTabbedPane object for making tabs with close buttons.
 * Example of usage:
 * <code>
 *     JTabbedPane pane = new JTabbedPane(); <br>
 *     pane.setUI(new TabbedPaneWithCloseButtonsUI()); <br>
 * </code>
 * <br><br>
 *
 * <img src="../../../../pic/closeTab.jpg">
 *
 */

public class TabbedPaneWithCloseButtons extends JTabbedPane
{

  private static final long serialVersionUID = 9138061073643482205L;

  public TabbedPaneWithCloseButtons()
  {
    this.setUI(new TabbedPaneWithCloseButtonsUI());
  }

//  // for testing
//  public static void main(String[] args)
//  {
//    JTabbedPane pane = new JTabbedPane();
//    pane.setUI(new TabbedPaneWithCloseButtonsUI());
//    pane.add("Panel 1",new JLabel("Content of Panel 1"));
//    pane.add("Panel 2",new JLabel("Content of Panel 2"));
//    pane.add("Panel 3",new JLabel("Content of Panel 3"));
//    pane.add("Panel 4",new JLabel("Content of Panel 4"));
//
//    JFrame frame = new JFrame();
//    frame.getContentPane().add(pane);
//    frame.setSize(400,100);
//    frame.setVisible(true);
//    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//  }

}
