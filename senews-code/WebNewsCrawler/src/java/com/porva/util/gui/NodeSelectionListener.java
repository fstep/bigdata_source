/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.util.gui;

import javax.swing.*;
import javax.swing.tree.TreePath;
import javax.swing.tree.DefaultTreeModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Listener for implementing {@link JTree} with {@link CheckNode} check nodes.
 * @see CheckNode
 * @see CheckRenderer
 */

public class NodeSelectionListener extends MouseAdapter
{
  JTree tree;

  public NodeSelectionListener(JTree tree) {
    this.tree = tree;
  }

  public void mouseClicked(MouseEvent e) {
    int x = e.getX();
    int y = e.getY();
    int row = tree.getRowForLocation(x, y);
    TreePath  path = tree.getPathForRow(row);
    //TreePath  path = tree.getSelectionPath();
    if (path != null) {
      CheckNode node = (CheckNode)path.getLastPathComponent();
      boolean isSelected = ! (node.isSelected());
      node.setSelected(isSelected);
      if (node.getSelectionMode() == CheckNode.DIG_IN_SELECTION) {
        if ( isSelected ) {
          //tree.expandPath(path);
        } else {
          //tree.collapsePath(path);
        }
      }
      ((DefaultTreeModel)tree.getModel()).nodeChanged(node);
      // I need revalidate if node is root.  but why?
      if (row == 0) {
        tree.revalidate();
        tree.repaint();
      }
    }
  }
}

