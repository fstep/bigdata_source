/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.util.gui;

import javax.swing.tree.DefaultMutableTreeNode;
import java.util.Enumeration;

/**
 * {@link DefaultMutableTreeNode} as check box. <br><br>
 * <img src="../../../../pic/checkTree.gif">
 * @see CheckRenderer
 * @see NodeSelectionListener
 */

public class CheckNode extends DefaultMutableTreeNode
{
  private static final long serialVersionUID = -2811552225271098204L;

  /**
   * Select only selected check node.
   */
  public final static int SINGLE_SELECTION = 0;

  /**
   * Mark selected also all child nodes of selected check node.
   */
  public final static int DIG_IN_SELECTION = 4;


  protected int selectionMode;
  protected boolean isSelected;
  
  /**
   * Creates a new {@link CheckNode} object with <code>null</code> user object.
   * Created node can have childrens and it is initially unchecked.
   */
  public CheckNode()
  {
    this(null);
  }

  /**
   * Creates a new {@link CheckNode} object with given user object.
   * Created node can have childrens and it is initially unchecked.
   * @param userObject
   */
  public CheckNode(Object userObject)
  {
    this(userObject, true, false);
  }

  /**
   * Creates a new {@link CheckNode} object with given user object and allowing children parameter.
   * @param userObject
   * @param allowsChildren <code>true</code> if this node supposes to have children nodes; <code>false</code> otherwise.
   * @param isSelected <code>true</code> to mark this node selected; <code>false</code> otherwise.
   */
  public CheckNode(Object userObject, boolean allowsChildren, boolean isSelected)
  {
    super(userObject, allowsChildren);
    this.isSelected = isSelected;
    setSelectionMode(DIG_IN_SELECTION);
  }

  /**
   * Sets selectin mode.
   * {@link #SINGLE_SELECTION} -- select only this node.
   * {@link #DIG_IN_SELECTION} -- mark selected also all child nodes of this node.
   * @param mode selection mode.
   */
  public void setSelectionMode(int mode)
  {
    selectionMode = mode;
  }

  /**
   * Retuns current selection mode.
   * @return selection mode.
   */
  public int getSelectionMode()
  {
    return selectionMode;
  }

  /**
   * Select or deselect node.
   * @param isSelected <code>true</code> to select node; <code>fasle</code> otherwise.
   */
  public void setSelected(boolean isSelected)
  {
    this.isSelected = isSelected;
    
    if ((selectionMode == DIG_IN_SELECTION)
        && (children != null)) {
      Enumeration en = children.elements();
      while (en.hasMoreElements()) {
        CheckNode node = (CheckNode)en.nextElement();
        node.setSelected(isSelected);
      }
    }
  }

  /**
   * Gets selection of node.
   * @return <code>true</code> if node is selected; <code>fasle</code> otherwise.
   */
  public boolean isSelected()
  {
    return isSelected;
  }


  // If you want to change "isSelected" by CellEditor,
  /*
  public void setUserObject(Object obj) {
    if (obj instanceof Boolean) {
      setSelected(((Boolean)obj).booleanValue());
    } else {
      super.setUserObject(obj);
    }
  }
  */

}


