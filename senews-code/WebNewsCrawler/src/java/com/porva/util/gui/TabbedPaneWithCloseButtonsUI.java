/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.util.gui;

import javax.swing.*;
import javax.swing.plaf.basic.*;
import java.awt.*;
import java.awt.event.*;

/**
 * TabbedPaneUI object for making tabs with close buttons.
 * Example of usage:
 * <code>
 *     JTabbedPane pane = new JTabbedPane(); <br>
 *     pane.setUI(new TabbedPaneWithCloseButtonsUI()); <br>
 * </code>
 * <br><br>
 *
 * <img src="../../../../pic/closeTab.jpg">
 *
 */

public class TabbedPaneWithCloseButtonsUI extends BasicTabbedPaneUI
{
  // override to return our layoutmanager
  protected LayoutManager createLayoutManager() {	return new TestPlafLayout(); }

  // add 40 to the tab size to allow room for the close button and 8 to the height
  protected Insets getTabInsets(int tabPlacement, int tabIndex)
  {
    //note that the insets that are returned to us are not copies.
    Insets defaultInsets = (Insets)super.getTabInsets(tabPlacement,tabIndex).clone();
    defaultInsets.right += 35;
    defaultInsets.top += 0;
    defaultInsets.bottom += 6;
    return defaultInsets;
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  class TestPlafLayout extends TabbedPaneLayout
  {
    //a list of our close buttons
    java.util.ArrayList<CloseButton> closeButtons = new java.util.ArrayList<CloseButton>();

    public void layoutContainer(Container parent)
    {
      super.layoutContainer(parent);

      //ensure that there are at least as many close buttons as tabs
      while(tabPane.getTabCount() > closeButtons.size())
        closeButtons.add(new CloseButton(closeButtons.size()));

      Rectangle rect = new Rectangle();
      int i;
      for(i = 0; i < tabPane.getTabCount();i++) {
        rect = getTabBounds(i,rect);
        JButton closeButton = (JButton)closeButtons.get(i);

        //shift the close button 3 down from the top of the pane and 20 to the left
        closeButton.setLocation(rect.x+rect.width-20,rect.y+5);
        closeButton.setSize(15,15);
        tabPane.add(closeButton);
      }

      for(; i < closeButtons.size();i++) //remove any extra close buttons
        tabPane.remove((JButton)closeButtons.get(i));
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // implement UIResource so that when we add this button to the
    // tabbedpane, it doesn't try to make a tab for it!
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    class CloseButton extends JButton implements javax.swing.plaf.UIResource
    {
      private static final long serialVersionUID = -1715519494550356585L;

      public CloseButton(int index)
      {
        super(new CloseButtonAction(index));
        setToolTipText("Close this tab");

        //remove the typical padding for the button
        setMargin(new Insets(0,0,0,0));

        addMouseListener(new MouseAdapter() {
          public void mouseEntered(MouseEvent e) { setForeground(new Color(255,0,0)); }
          public void mouseExited(MouseEvent e)  { setForeground(new Color(0,0,0));   }
        });
      }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    class CloseButtonAction extends AbstractAction
    {
      private static final long serialVersionUID = -3442596659162967497L;
      int index;

      public CloseButtonAction(int index)
      {
        super("x");
        this.index = index;
      }

      public void actionPerformed(ActionEvent e) { tabPane.remove(index);	}

    }	// End of CloseButtonAction
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  }	// End of TestPlafLayout


  // for testing
  public static void main(String[] args)
  {
    JTabbedPane pane = new JTabbedPane();
    pane.setUI(new TabbedPaneWithCloseButtonsUI());
    pane.add("Panel 1",new JLabel("Content of Panel 1"));
    pane.add("Panel 2",new JLabel("Content of Panel 2"));
    pane.add("Panel 3",new JLabel("Content of Panel 3"));
    pane.add("Panel 4",new JLabel("Content of Panel 4"));

    JFrame frame = new JFrame();
    frame.getContentPane().add(pane);
    frame.setSize(400,100);
    frame.setVisible(true);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

}
