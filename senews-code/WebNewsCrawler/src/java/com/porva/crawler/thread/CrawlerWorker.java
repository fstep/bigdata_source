/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.thread;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.crawler.Frontier;
import com.porva.crawler.service.ServiceException;
import com.porva.crawler.service.ServiceProvider;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskResult;

/**
 * Crawler worker thread.<br>
 * Worker thread performs different crawling tasks like fetching urls, extracting links, filtering
 * and so on. Tasks may rely on installed services for this worker thread to do their job.<br>
 * Crawler worker thread is a member of thead pool.  
 * 
 * @author Poroshin V.
 * @date Oct 10, 2005
 */
public class CrawlerWorker extends Thread
{

  /**
   * The thread pool that this object belongs to.
   */
  private WorkersFixedThreadPool threadPoolOwner;

  private Frontier frontier;

  private ServiceProvider services;

  private ServiceProvider serviceProvider;

  private static Log logger = LogFactory.getLog(CrawlerWorker.class);

  CrawlerWorker(final WorkersFixedThreadPool threadPoolOwner,
                final Frontier frontier,
                final ServiceProvider services) throws ServiceException
  {
    this.threadPoolOwner = threadPoolOwner;
    this.frontier = frontier;
    this.services = services;

    if (this.threadPoolOwner == null)
      throw new NullArgumentException("threadPoolOwner");
    if (this.frontier == null)
      throw new NullArgumentException("frontier");
    if (this.services == null)
      throw new NullArgumentException("services");

    serviceProvider = new ServiceProvider(services);
  }

  public void run()
  {
    Task task = null;

    do {
      task = threadPoolOwner.getAssignment();
      if (task != null) {
        TaskResult taskResult = task.perform(serviceProvider);
        frontier.completeTask(task, taskResult);
      }
    } while (task != null);

    try {
      logger.debug("Stopping all working services in CrawlerWorker.");
      serviceProvider.stopAllWorkerServices();
    } catch (ServiceException e) {
      logger.warn("Failed to stop some worker services in CrawlerWorker", e);
      // todo: is this critical ???
    }
    threadPoolOwner.workerDone();
  }

}
