/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.workload;

import static com.porva.crawler.workload.WorkloadEntryStatus.COMPLETE;
import static com.porva.crawler.workload.WorkloadEntryStatus.FAILED;
import static com.porva.crawler.workload.WorkloadEntryStatus.RUNNING;
import static com.porva.crawler.workload.WorkloadEntryStatus.WAITING;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.crawler.db.CrawlerDBFactory;
import com.porva.crawler.db.DB;
import com.porva.crawler.db.DBException;
import com.porva.crawler.db.DBFunctor;
import com.porva.crawler.db.DBOperationStatus;
import com.porva.crawler.service.AbstractService;
import com.porva.crawler.service.ServiceException;
import com.porva.crawler.task.FetchResult;
import com.porva.crawler.task.FetchTask;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskFactory;
import com.porva.crawler.task.TaskResult;

/**
 * Workload for wide crawling. Uses available databases to store its content.
 * 
 * @author Poroshin V.
 * @date Oct 11, 2005
 */
public class DBWorkload extends AbstractService implements Workload
{
  private DB<Task> hostTaskDb = null;

  protected URLStatusDB urlStatusDB = null;

  private HostAccess hostAccess = null;

  private static final Log logger = LogFactory.getLog(DBWorkload.class);
  
  /**
   * Creates a new {@link DBWorkload} object with specified database factory <code>dbFactory</code>.
   * 
   * @param dbFactory not closed database factory.
   * @throws DBException if some database error occures.
   * @throws NullArgumentException if <code>dbFactory</code> is <code>null</code>.
   * @throws IllegalStateException if <code>dbFactory</code> is closed.
   */
  public DBWorkload(final CrawlerDBFactory dbFactory) throws DBException
  {
    super("commonservice.workload");

    if (dbFactory == null)
      throw new NullArgumentException("dbFactory");
    if (dbFactory.isClosed())
      throw new IllegalStateException("dbFactory is closed.");

    hostTaskDb = dbFactory.newTaskDatabaseHandle();
    urlStatusDB = new URLStatusDB(dbFactory);
    hostTaskDb.open();
    urlStatusDB.open();
    hostAccess = new HostAccess(hostTaskDb);

    if (getNumberOf(RUNNING) != 0) {
      logger.warn("urlStatusDB has " + getNumberOf(RUNNING) + " running tasks!");
      fixInconsistans();
    }
  }

  // todo: fix this by moving running to waiting
  private void fixInconsistans()
  {
    // List<String> runningURLs = new ArrayList<String>();
    // urlStatusDB.getAllKeysOf(RUNNING, runningURLs);
    // urlStatusDB.put("")

    int deletedNum = urlStatusDB.deleteAll(RUNNING);
    logger.info("Inconsistance fixed by deleting " + deletedNum + " tasks");
  }

  public Task getTask()
  {
    Task task = TaskFactory.newEmptyTask();
    if (getNumberOf(WAITING) > 0) {
      String freeHost = hostAccess.getFreeHost();
      if (freeHost != null) {
        task = hostTaskDb.get(freeHost);
        if (task == null) {
          logger.warn("There is no tasks for freeHost " + freeHost
              + " in hostTaskDb. Workload is not consistant.");
          return TaskFactory.newEmptyTask(); // FIXME: raise exception ?
        }
        if (hostTaskDb.delete(freeHost, task) != DBOperationStatus.SUCCESS) {
          logger.warn("Cannot delete key-value pair from hostTaskDb: key=" + freeHost + " value="
              + task);
          return TaskFactory.newEmptyTask(); // FIXME: raise exception ?
        }
        if (!urlStatusDB.put(task.getDBKey(), WorkloadEntryStatus.RUNNING)) {
          return TaskFactory.newEmptyTask(); // FIXME: raise exception ?
        }
      }
    } else if (getNumberOf(RUNNING) == 0) {
      task = TaskFactory.newLastTask();
    }
    if (logger.isDebugEnabled())
      logger.debug(this.toString());
    return task;
  }

  public boolean addTask(final Task task)
  {
    return addTask(task, false);
  }

  public boolean addTask(final Task task, boolean isUpdate)
  {
    if (task == null)
      throw new NullArgumentException("task");

    boolean hasValue = (urlStatusDB.get(task.getDBKey()) != null);
    if (!isUpdate && hasValue)
      return false;

    String key = task.getDBKey();
    if (task instanceof FetchTask)
      key = ((FetchTask) task).getCrawlerURL().getHost();

//    boolean newTask = true;
//    if (isUpdate && (hostTaskDb.delete(key, task) == DBOperationStatus.SUCCESS)) {
//      newTask = false;
//    }
    if (hostTaskDb.put(key, task) != DBOperationStatus.SUCCESS) {
      logger.warn("Cannot put task to hostTaskDb: " + task);
      return false;
    }
    if (!urlStatusDB.put(task.getDBKey(), WorkloadEntryStatus.WAITING)) {
      logger.warn("Cannot put task to urlStatusDB: " + task);
      return false;
      // todo: delete this task from hostTaskDb
    }

    if (task instanceof FetchTask) {
      FetchTask fetchTask = (FetchTask) task;
      hostAccess.addHost(fetchTask.getCrawlerURL().getHost());
    }
    // if (newTask)
    // waitingNum++;
    return true;
  }

  public boolean completeTask(final Task task, final TaskResult result)
  {
    if (task == null)
      throw new NullArgumentException("task");
    if (result == null)
      throw new NullArgumentException("result");

    WorkloadEntryStatus status = urlStatusDB.get(task.getDBKey());
    if (status == null || status != WorkloadEntryStatus.RUNNING) {
      // TODO ???
      hostAccess.addAccessedHost(((FetchTask)task).getCrawlerURL().getHost(), 
                                 System.currentTimeMillis());
      return false;
    }

    // completeAllTasks(task, result);
    if (task instanceof FetchTask)
      completeFetchTask((FetchTask) task, (FetchResult) result);
    else
      completeOtherTask(task, result);

    // runningNum--;
    return true;
  }

  private boolean completeOtherTask(final Task task, final TaskResult result)
  {
    // if (urlStatusDB.put(result.getDBKey(), WorkloadEntryStatus.COMPLETE) !=
    // DBOperationStatus.SUCCESS) {
    // logger.warn("Cannot put status COMPLETE to urlStatusDB for key: " + result.getDBKey());
    // return false; // FIXME: raise exception ?
    // }
    // completeNum++;
    // return true;
    throw new NotImplementedException("TODO");
  }

  private boolean completeFetchTask(FetchTask fetchTask, FetchResult fetchResult)
  {
    assert fetchResult != null;
    assert fetchTask != null;

    if (fetchResult.getStatus() != FetchResult.Status.COMPLETE) {
      putURLStatus(fetchResult.getDBKey(), WorkloadEntryStatus.FAILED);
      // TODO ???
      //hostAccess.freeHost(fetchTask.getCrawlerURL().getHost());
      hostAccess.addAccessedHost(fetchTask.getCrawlerURL().getHost(), System.currentTimeMillis());
    } else {
      putURLStatus(fetchResult.getDBKey(), WorkloadEntryStatus.COMPLETE);
      hostAccess.addAccessedHost(fetchTask.getCrawlerURL().getHost(), fetchResult.getResponse()
          .getTimeWhenDisconnected());
    }

    return true;
  }

  private boolean putURLStatus(String url, WorkloadEntryStatus status)
  {
    assert url != null;
    assert status != null;

    if (!urlStatusDB.put(url, status)) {
      logger.warn("Cannot put status " + status + " to urlStatusDB for key: " + url);
      return false; // FIXME: raise exception ?
    }

    return true;
  }

  public WorkloadEntryStatus getTaskStatus(final Task task)
  {
    if (task == null)
      throw new NullArgumentException("task");

    WorkloadEntryStatus status = urlStatusDB.get(task.getDBKey());
    if (status == null)
      return WorkloadEntryStatus.UNKNOWN;
    return status;
  }

  public int getNumberOf(final WorkloadEntryStatus status)
  {
    if (status == null)
      throw new NullArgumentException("status");

    return urlStatusDB.getNumberOf(status);
  }

  public Map<String, String> getStat()
  {
    Map<String, String> stat = new HashMap<String, String>();
    stat.put("workload.num-of-complete", Integer.toString(getNumberOf(WorkloadEntryStatus.COMPLETE)));
    stat.put("workload.num-of-failed", Integer.toString(getNumberOf(WorkloadEntryStatus.FAILED)));
    stat.put("workload.num-of-running", Integer.toString(getNumberOf(WorkloadEntryStatus.RUNNING)));
    stat.put("workload.num-of-waiting", Integer.toString(getNumberOf(WorkloadEntryStatus.WAITING)));
    
    return stat;
  }

  public void stop() throws ServiceException
  {
    if (isStopped()) {
      super.stop();
      return;
    }

    try {
      hostTaskDb.close();
      urlStatusDB.close();
      super.stop();
    } catch (DBException e) {
      logger.warn("Cannot close databases of workload", e);
      throw new ServiceException("Cannot close databases of workload", e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    if (!isStopped() && logger.isDebugEnabled())
      checkIntegrity();

    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("hostAccess",
                                                                              hostAccess)
        .append("runningNum", getNumberOf(RUNNING)).append("waitingNum", getNumberOf(WAITING))
        .append("completeNum", getNumberOf(COMPLETE)).append("failedNum", getNumberOf(FAILED))
        .toString();
  }

  // //////////////////////////////////////////////
  private void checkIntegrity()
  {
    CountFunctor countFunctor = new CountFunctor();
    urlStatusDB.iterateOverAll(countFunctor);
    if (getNumberOf(RUNNING) != countFunctor.runningNum)
      logger.warn("Integrity error - different running num: expected " + getNumberOf(RUNNING)
          + " but actual " + countFunctor.runningNum);
    if (getNumberOf(WAITING) != countFunctor.waitingNum)
      logger.warn("Integrity error - different waiting num: expected " + getNumberOf(WAITING)
          + " but actual " + countFunctor.waitingNum);
    if (getNumberOf(COMPLETE) != countFunctor.completeNum)
      logger.warn("Integrity error - different complete num: expected " + getNumberOf(COMPLETE)
          + " but actual " + countFunctor.completeNum);
    if (getNumberOf(FAILED) != countFunctor.failedNum)
      logger.warn("Integrity error - different failed num: expected " + getNumberOf(FAILED)
          + " but actual " + countFunctor.failedNum);
  }

  static class CountFunctor implements DBFunctor<WorkloadEntryStatus>
  {
    int completeNum = 0;

    int failedNum = 0;

    int waitingNum = 0;

    int runningNum = 0;

    int unknownNum = 0;

    public void process(final String key, final WorkloadEntryStatus value)
    {
      if (key == null)
        throw new NullArgumentException("key");
      if (value == WorkloadEntryStatus.COMPLETE)
        completeNum++;
      else if (value == WorkloadEntryStatus.FAILED)
        failedNum++;
      else if (value == WorkloadEntryStatus.WAITING)
        waitingNum++;
      else if (value == WorkloadEntryStatus.RUNNING)
        runningNum++;
      else if (value == WorkloadEntryStatus.UNKNOWN)
        unknownNum++;
    }
  };

}
