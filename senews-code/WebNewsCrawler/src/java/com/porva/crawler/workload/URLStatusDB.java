/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.workload;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.crawler.db.CrawlerDBFactory;
import com.porva.crawler.db.DB;
import com.porva.crawler.db.DBException;
import com.porva.crawler.db.DBFunctor;
import com.porva.crawler.db.DBOperationStatus;

/**
 * DB<WorkloadEntryStatus> wrapper that tracks number of values for each type.
 * 
 * @author Poroshin V.
 * @date Oct 13, 2005
 */
public class URLStatusDB
{

  private DB<WorkloadEntryStatus> db;

  private int runningNum = 0;

  private int waitingNum = 0;

  private int completeNum = 0;

  private int failedNum = 0;

  private static final Log logger = LogFactory.getLog(URLStatusDB.class);

  /**
   * Creates a new URLStatusDB database handle.
   * 
   * @param dbFactory not closed database factory.
   * @throws NullArgumentException if <code>dbFactory</code> is <code>null</code>.
   * @throws IllegalStateException if <code>dbFactory</code> is closed.
   */
  public URLStatusDB(final CrawlerDBFactory dbFactory)
  {
    if (dbFactory == null)
      throw new NullArgumentException("dbFactory");
    if (dbFactory.isClosed())
      throw new IllegalStateException("dbFactory is closed");

    db = dbFactory.newStatusDatabaseHandle();
  }

  /**
   * Opens database.
   * 
   * @throws DBException if some database failure occurs.
   * @throws IllegalStateException if database factory is closed.
   */
  public void open() throws DBException
  {
    db.open();
    CountFunctor countFunctor = new CountFunctor();
    db.iterateOverAll(countFunctor);
    runningNum = countFunctor.runningNum;
    waitingNum = countFunctor.waitingNum;
    completeNum = countFunctor.completeNum;
    failedNum = countFunctor.failedNum;
  }

  /**
   * Closes opened database.
   * 
   * @throws DBException if some database failure occurs.
   */
  public void close() throws DBException
  {
    db.close();
  }

  /**
   * Puts given key-value pair into database.
   * 
   * @param key
   * @param value
   * @return <code>true</code> if operation succeeded; <code>false</code> otherwise.
   * @throws NullArgumentException if <code>key</code> or <code>value</code> is
   *           <code>null</code>.
   */
  public boolean put(final String key, final WorkloadEntryStatus value)
  {
    if (key == null)
      throw new NullArgumentException("key");
    if (value == null)
      throw new NullArgumentException("value");

    WorkloadEntryStatus oldStat = db.get(key);
    if (db.put(key, value) != DBOperationStatus.SUCCESS) {
      logger.warn("Failed to put key-value pair to URLStatusDB: key=" + key + " value=" + value);
      return false;
    }

    if (oldStat != null)
      changeNum(oldStat, -1);
    changeNum(value, 1);

    return true;
  }

  /**
   * Returns WorkloadEntryStatus object of the given key.<br>
   * If the matching key has duplicate values, the first data item in the set of duplicates is
   * returned.
   * 
   * @param key not <code>null</code> key to retrieve value from database.
   * @return WorkloadEntryStatus object retrieved from database for given key or <code>null</code>
   *         if there is no such record in database or some error occured.
   * @throws NullArgumentException if <code>key</code> is <code>null</code>.
   * @throws IllegalStateException if database is not opened or it is already closed.
   */
  public WorkloadEntryStatus get(final String key)
  {
    return db.get(key);
  }

  /**
   * Returns opened status of database.
   * 
   * @return <code>true</code> if database is opened; <code>false</code> otherwise.
   */
  public boolean isOpened()
  {
    return db.isOpened();
  }

  /**
   * Iterates over all tuples in database and performs <code>functor.process(key, value)</code>
   * for each found key-value pair.
   * 
   * @param functor function object to process key-value pairs.
   * @throws NullArgumentException if functor is <code>null</code>.
   * @throws IllegalStateException if database is not opened or it is already closed.
   */
  public void iterateOverAll(DBFunctor<WorkloadEntryStatus> functor)
  {
    db.iterateOverAll(functor);
  }

  /**
   * Returns number of workload entries with given <code>status</code>.
   * 
   * @param status of entry in workload.
   * @return number of workload entries with given <code>status</code>.
   * @throws NullArgumentException if <code>status</code> is <code>null</code>.
   */
  public int getNumberOf(final WorkloadEntryStatus status)
  {
    if (status == null)
      throw new NullArgumentException("status");

    if (status == WorkloadEntryStatus.COMPLETE)
      return completeNum;
    if (status == WorkloadEntryStatus.FAILED)
      return failedNum;
    if (status == WorkloadEntryStatus.RUNNING)
      return runningNum;
    if (status == WorkloadEntryStatus.WAITING)
      return waitingNum;
    return 0;
  }

  /**
   * Populates list <code>buff</code> with all keys for given <code>status</code>. <br>
   * <b>Warning: list of all keys can be quite big! Be sure it can fit into memory!</b>
   * 
   * @param status
   * @param buff
   * @throws NullArgumentException if <code>status</code> or <code>buff</code> is
   *           <code>null</code>.
   */
  public void getAllKeysOf(final WorkloadEntryStatus status, final List<String> buff)
  {
    if (status == null)
      throw new NullArgumentException("status");
    if (buff == null)
      throw new NullArgumentException("buff");

    KeysFunctor functor = new KeysFunctor(status, buff);
    db.iterateOverAll(functor);
  }

  /**
   * Removes all database entries with given <code>status</code>.
   * 
   * @param status status to remove all entries with this key.
   * @return number of deleted entries.
   */
  public int deleteAll(final WorkloadEntryStatus status)
  {
    if (status == null)
      throw new NullArgumentException("status");

    final List<String> keys = new ArrayList<String>();
    getAllKeysOf(status, keys);

    int deletedNum = 0;
    for (String key : keys) {
      if (db.delete(key, status) == DBOperationStatus.SUCCESS)
        deletedNum++;
      else
        logger.warn("Failed to delete key-value form URLStatusDB: key=" + key + " value=" + status);
    }
    changeNum(status, -deletedNum);

    return deletedNum;
  }

  // //////////////////////////////////////////////////// private

  private void changeNum(final WorkloadEntryStatus status, int num)
  {
    if (status == WorkloadEntryStatus.COMPLETE)
      completeNum += num;
    else if (status == WorkloadEntryStatus.FAILED)
      failedNum += num;
    else if (status == WorkloadEntryStatus.RUNNING)
      runningNum += num;
    else if (status == WorkloadEntryStatus.WAITING)
      waitingNum += num;
  }

  private static class CountFunctor implements DBFunctor<WorkloadEntryStatus>
  {
    int completeNum = 0;

    int failedNum = 0;

    int waitingNum = 0;

    int runningNum = 0;

    int unknownNum = 0;

    public void process(final String key, final WorkloadEntryStatus value)
    {
      if (key == null)
        throw new NullArgumentException("key");
      if (value == WorkloadEntryStatus.COMPLETE)
        completeNum++;
      else if (value == WorkloadEntryStatus.FAILED)
        failedNum++;
      else if (value == WorkloadEntryStatus.WAITING)
        waitingNum++;
      else if (value == WorkloadEntryStatus.RUNNING)
        runningNum++;
      else if (value == WorkloadEntryStatus.UNKNOWN)
        unknownNum++;
    }
  };

  private static class KeysFunctor implements DBFunctor<WorkloadEntryStatus>
  {
    private WorkloadEntryStatus status;

    private List<String> buff;

    /**
     * Allocates a new instance of {@link KeysFunctor}.
     * 
     * @param status
     * @param buff
     * @throws NullArgumentException if <code>status</code> or <code>buff</code> is
     *           <code>null</code>.
     */
    public KeysFunctor(final WorkloadEntryStatus status, final List<String> buff)
    {
      this.status = status;
      this.buff = buff;

      if (this.status == null)
        throw new NullArgumentException("status");
      if (this.buff == null)
        throw new NullArgumentException("buff");
    }

    public void process(final String key, final WorkloadEntryStatus value)
    {
      if (key == null)
        throw new NullArgumentException("key");
      if (value == status)
        buff.add(key);
    }
  };

}
