///*
// * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
// * redistribute it and/or modify it under the terms of the GNU General Public License as published
// * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
// * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
// * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
// * the GNU General Public License along with this program; if not, write to the Free Software
// * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// */
//package com.porva.crawler.workload;
//
//import java.util.HashMap;
//import java.util.LinkedList;
//import java.util.Map;
//
//import org.apache.commons.lang.NullArgumentException;
//
//import com.porva.crawler.service.AbstractService;
//import com.porva.crawler.task.DefaultFetchResult;
//import com.porva.crawler.task.FetchTask;
//import com.porva.crawler.task.Task;
//import com.porva.crawler.task.TaskResult;
//import com.porva.crawler.task.TaskFactory;
//import com.porva.html2xml.task.EmptyTask;
//
///**
// * Implementation of crawler's workload where all workload etries are stored in main memory. Do not
// * use this wokload for real wide crawler tasks unless you have enough memory for this.
// * 
// * @author Poroshin V.
// * @date Sep 13, 2005
// */
//public class InMemoryWorkload extends AbstractService implements Workload
//{
//
//  public InMemoryWorkload()
//  {
//    super("commonservice.workload");
//  }
//
//  private LinkedList<Task> waitingList = new LinkedList<Task>();
//
//  private Map<Task, WorkloadEntryStatus> statusMap = new HashMap<Task, WorkloadEntryStatus>();
//
//  private int runningNum = 0;
//
//  private int waitingNum = 0;
//
//  private int completeNum = 0;
//
//  private int failedNum = 0;
//
//  /**
//   * Returns next {@link Task} task with status {@link WorkloadEntryStatus#WAITING} and changes its
//   * status to {@link WorkloadEntryStatus#RUNNING}.
//   * 
//   * @return {@link Task} workload entry. If there is no more tasks to process available in workload
//   *         but there are still some running tasks then {@link EmptyTask} will be returned. If
//   *         there is no any tasks to process available and no running tasks then last task instance
//   *         will be returned.
//   */
//  public Task<TaskResult> getTask()
//  {
//    Task task = TaskFactory.EMPTY.newTask(null);
//    if (waitingList.size() > 0) {
//      task = waitingList.removeFirst();
//      statusMap.put(task, WorkloadEntryStatus.RUNNING);
//      runningNum++;
//      waitingNum--;
//    } else if (runningNum == 0) {
//      task = TaskFactory.LAST.newTask(null);
//    }
//    return task;
//  }
//
//  public boolean addTask(Task task)
//  {
//    return addTask(task, false);
//  }
//
//  public boolean addTask(Task task, boolean isUpdate)
//  {
//    if (task == null)
//      throw new NullArgumentException("task");
//
//    if (!isUpdate && statusMap.containsKey(task))
//      return false;
//    if (!waitingList.remove(task))
//      waitingNum++;
//    waitingList.addLast(task);
//    statusMap.put(task, WorkloadEntryStatus.WAITING);
//
//    return true;
//  }
//
//  /**
//   * Completes given task in workload by changing status of task from
//   * {@link WorkloadEntryStatus#RUNNING} to {@link WorkloadEntryStatus#COMPLETE} if result is
//   * complete or {@link WorkloadEntryStatus#COMPLETE} if result is failed.<br>
//   * If given <code>task</code> is not present in workload or its status different from
//   * {@link WorkloadEntryStatus#RUNNING} then this method will fail and return <code>false</code>.
//   * 
//   * @param task task to complete in workload.
//   * @param result task's result.
//   * @return <code>true</code> if task was competed in workload or <code>false</code> otherwise.
//   * @throws NullArgumentException if <code>task</code> is <code>null</code>.
//   * @throws NullArgumentException if <code>result</code> is <code>null</code>.
//   */
//  public boolean completeTask(Task task, TaskResult result)
//  {
//    if (task == null)
//      throw new NullArgumentException("task");
//    if (result == null)
//      throw new NullArgumentException("result");
//
//    WorkloadEntryStatus status = statusMap.get(task);
//    if (status == null || status != WorkloadEntryStatus.RUNNING)
//      return false;
//
//    if (task instanceof FetchTask)
//      completeFetchTask((FetchTask) task, (DefaultFetchResult) result);
//
//    runningNum--;
//    return true;
//  }
//
//  private boolean completeFetchTask(FetchTask task, DefaultFetchResult result)
//  {
//    if (DefaultFetchResult.Status.COMPLETE == result.getStatus()) {
//      statusMap.put(task, WorkloadEntryStatus.FAILED);
//      failedNum++;
//    } else {
//      statusMap.put(task, WorkloadEntryStatus.COMPLETE);
//      completeNum++;
//    }
//    return true;
//  }
//
//  /**
//   * Returns {@link WorkloadEntryStatus} status of the task in workload. If task is not present in
//   * workload then {@link WorkloadEntryStatus#UNKNOWN} will be returned.
//   * 
//   * @param task {@link Task} object to get its status.
//   * @return {@link WorkloadEntryStatus} status of the task
//   * @throws NullArgumentException if <code>task</code> is <code>null</code>.
//   */
//  public WorkloadEntryStatus getTaskStatus(Task task)
//  {
//    if (task == null)
//      throw new NullArgumentException("task");
//
//    WorkloadEntryStatus status = statusMap.get(task);
//    if (status == null)
//      return WorkloadEntryStatus.UNKNOWN;
//    return status;
//  }
//
//  /**
//   * Returns number of workload entries with status {@link WorkloadEntryStatus#RUNNING}.
//   * 
//   * @return number of running entries in workload.
//   */
//  public int getRunningNum()
//  {
//    return runningNum;
//  }
//
//  /**
//   * Returns number of workload entries with status {@link WorkloadEntryStatus#WAITING}.
//   * 
//   * @return number of waiting entries in workload.
//   */
//  public int getWaitingNum()
//  {
//    return waitingNum;
//  }
//
//  /**
//   * Returns number of workload entries with status {@link WorkloadEntryStatus#COMPLETE}.
//   * 
//   * @return number of compete entries in workload.
//   */
//  public int getCompleteNum()
//  {
//    return completeNum;
//  }
//
//  /**
//   * Returns number of workload entries with status {@link WorkloadEntryStatus#FAILED}.
//   * 
//   * @return number of failed entries in workload.
//   */
//  public int getFailedNum()
//  {
//    return failedNum;
//  }
//
//  /**
//   * Returns statistics of the object.
//   * 
//   * @return map of statistic information.
//   */
//  public Map<String, String> getStat()
//  {
//    Map<String, String> statMap = new HashMap<String, String>();
//    statMap.put("InMemoryWorkload", "stat is not implemented!");
//    return statMap;
//  }
//
//}
