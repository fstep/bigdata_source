/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.workload;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.porva.crawler.db.DBInputStream;
import com.porva.crawler.db.DBOutputStream;
import com.porva.crawler.db.DBValue;

/**
 * Status of each workload entry.
 * 
 * @author Poroshin V.
 * @date Sep 13, 2005
 */
public class WorkloadEntryStatus implements DBValue
{
  /**
   * Workload entry is not present in workload.
   */
  public static final WorkloadEntryStatus UNKNOWN = new WorkloadEntryStatus((byte) 0, "UNKNOWN");

  /**
   * Workload entry is waiting for processing.
   */
  public static final WorkloadEntryStatus WAITING = new WorkloadEntryStatus((byte) 1, "WAITING");

  /**
   * Workload entry is processing.
   */
  public static final WorkloadEntryStatus RUNNING = new WorkloadEntryStatus((byte) 2, "RUNNING");

  /**
   * Workload entry was processed and its result has failed status.
   */
  public static final WorkloadEntryStatus FAILED = new WorkloadEntryStatus((byte) 3, "FAILED");

  /**
   * Workload entry was successfuly processed and its result has not failed status.
   */
  public static final WorkloadEntryStatus COMPLETE = new WorkloadEntryStatus((byte) 4, "COMPLETE");

  // REDIRECT((byte)5);

  private byte byteVal;

  private String name;

  protected WorkloadEntryStatus()
  {
  }

  private WorkloadEntryStatus(byte byteVal, final String name)
  {
    this.byteVal = byteVal;
    this.name = name;
  }

  // //////////////////////////////////
  // AbstractDBValue implementation
  // //////////////////////////////////

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DBValue#newInstance(com.porva.crawler.db.DBInputStream)
   */
  public DBValue newInstance(final DBInputStream tupleInput)
  {
    byte byteVal = tupleInput.readByte();
    switch (byteVal) {
    case 0:
      return UNKNOWN;
    case 1:
      return WAITING;
    case 2:
      return RUNNING;
    case 3:
      return FAILED;
    case 4:
      return COMPLETE;
    // case 5: return REDIRECT;
    }
    throw new IllegalArgumentException(
        "byteVal is not representing any WorkloadEntryStatus objects.");
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DBValue#writeTo(com.porva.crawler.db.DBOutputStream)
   */
  public void writeTo(DBOutputStream tupleOutput)
  {
    tupleOutput.writeByte(byteVal);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DBValue#getClassCode()
   */
  public long getClassCode()
  {
    return 283333850901442L;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("name", name)
        .toString();
  }

}
