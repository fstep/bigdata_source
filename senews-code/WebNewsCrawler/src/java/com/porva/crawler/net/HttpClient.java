/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.net;

import com.porva.crawler.service.WorkerService;

/**
 * Service that can execute HTTP get requests.
 * 
 * @author Poroshin V.
 * @date Sep 26, 2005
 */
public interface HttpClient extends WorkerService
{
  /**
   * Executes HTTP get method for give <code>url</code> and returns {@link Response} object as a
   * result.
   * 
   * @param url
   * @return response result of get method execution.
   * @throws org.apache.commons.lang.NullArgumentException if <code>url</code> is
   *           <code>null</code>.
   * @throws HttpClientException if given <code>url</code> has unsupported protocol or if host
   *           cannot be located or if some other I/O failier occurs.
   */
  public Response get(CrawlerURL url) throws HttpClientException;

  /**
   * Returns configuration parameter of this HttpClient.
   * 
   * @param paramName name of configuration parameter to return
   * @return configuration parameter of this HttpClient or <code>null</code> if no such
   *         configuratio parameter exists.
   */
  public Object getParameter(String paramName);

  /**
   * Sets value of configuration parameter with name <code>paramName</code> to
   * <code>paramValue</code>.
   * 
   * @param paramName name of parameter.
   * @param paramValue new value of paramter to set.
   */
  public void setParameter(final String paramName, final Object paramValue);
}
