///*
// * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
// * redistribute it and/or modify it under the terms of the GNU General Public License as published
// * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
// * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
// * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
// * the GNU General Public License along with this program; if not, write to the Free Software
// * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// */
//package com.porva.crawler.net.nutch;
//
//import java.io.IOException;
//
//import net.nutch.net.protocols.http.Http;
//import net.nutch.net.protocols.http.HttpException;
//
//import org.apache.commons.lang.NotImplementedException;
//import org.apache.commons.lang.NullArgumentException;
//
//import com.porva.crawler.net.CrawlerURL;
//import com.porva.crawler.net.HttpClient;
//import com.porva.crawler.net.HttpClientException;
//import com.porva.crawler.net.MalformedCrawlerURLException;
//import com.porva.crawler.net.Response;
//import com.porva.crawler.service.AbstractWorkerService;
//import com.porva.crawler.service.WorkerService;
//
//@Deprecated
//public class NutchHttpService extends AbstractWorkerService implements HttpClient
//{
//  public NutchHttpService()
//  {
//    super("workerservice.httpClient");
//  }
//
//  private Http http = new Http();
//
//  public Response get(CrawlerURL url) throws HttpClientException
//  {
//    if (url == null)
//      throw new NullArgumentException("url");
//
//    HttpResponse response = null;
//
//    if (!"http".equalsIgnoreCase(url.getProtocol())) {
//      throw new HttpClientException("Unsupported protocol:" + " proto=" + url.getProtocol()
//          + " crawlerUri=" + url.toString());
//    }
//
//    try {
//      net.nutch.net.protocols.Response nutchResponse = http.getResponse(url.getURL());
//      CrawlerURL crawlerURL = new CrawlerURL(url
//          .getDepth(), nutchResponse.getUrl().toExternalForm());
//      response = new HttpResponse(nutchResponse, crawlerURL);
//    } catch (HttpException e) {
//      throw new HttpClientException(e);
//    } catch (IOException e) {
//      throw new HttpClientException(e);
//    } catch (MalformedCrawlerURLException e) {
//      throw new HttpClientException(e);
//    }
//
//    if (response == null || response.getBody() == null) {
//      throw new HttpClientException("Failed to fetch:" + " proto=" + url.getProtocol()
//          + " crawlerUri=" + url.toString());
//    }
//
//    return response;
//  }
//
//  public WorkerService newCopyInstance()
//  {
//    return new NutchHttpService();
//  }
//
//  public Object getParameter(String paramName)
//  {
//    throw new NotImplementedException();
//  }
//
//  public void setParameter(String paramName, Object paramValue)
//  {
//    throw new NotImplementedException();
//  }
//}
