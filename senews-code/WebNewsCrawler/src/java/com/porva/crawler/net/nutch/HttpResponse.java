///*
// * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
// * redistribute it and/or modify it under the terms of the GNU General Public License as published
// * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
// * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
// * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
// * the GNU General Public License along with this program; if not, write to the Free Software
// * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// */
//package com.porva.crawler.net.nutch;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import org.apache.commons.lang.NullArgumentException;
//import org.apache.commons.lang.builder.ToStringBuilder;
//import org.apache.commons.lang.builder.ToStringStyle;
//
//import com.porva.crawler.net.CrawlerURL;
//import com.porva.crawler.net.DefaultResponseImpl;
//
///**
// * Response object to present Nutch HTTP client get method results.
// * 
// * @author Poroshin V.
// * @date Sep 26, 2005
// */
//class HttpResponse extends DefaultResponseImpl
//{
//  private net.nutch.net.protocols.Response nutchResponse;
//
//  /**
//   * @param nutchResponse
//   * @param crawlerURL 
//   */
//  public HttpResponse(net.nutch.net.protocols.Response nutchResponse, CrawlerURL crawlerURL)
//  {
//    this.nutchResponse = nutchResponse;
//    if (this.nutchResponse == null)
//      throw new NullArgumentException("nutchResponse");
//    
//    setTimeWhenDisconnected(System.currentTimeMillis());
//    setCode(nutchResponse.getCode());
//    
//    // set headers
//    Map<String,String> headers = new HashMap<String,String>();
//    for (Map.Entry<String,String> header : nutchResponse.getHeaders().entrySet())
//      headers.put(header.getKey().toLowerCase(), header.getValue());
//    setHeaders(headers);
//    
//    setBody(nutchResponse.getContent());
////    setCrawlerURL(crawlerURL);
//  }
//
//  private transient String toString;
//
//  /* (non-Javadoc)
//   * @see java.lang.Object#toString()
//   */
//  public String toString()
//  {
//    if (toString == null) {
//      toString = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
//          .appendSuper(super.toString()).toString();
//    }
//    return toString;
//  }
//
////  public URL getRedirect()
////  {
////    int code = nutchResponse.getCode();
////    if (code >= 300 && code < 400) {
////      try {
////        return new URL(nutchResponse.getHeader("Location"));
////      } catch (MalformedURLException e) {
////        // todo: log warining
////        return null;
////      }
////    }
////    return null;
////  }
//}
