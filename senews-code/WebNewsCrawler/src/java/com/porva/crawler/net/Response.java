/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.net;

import java.util.Map;

import com.porva.crawler.db.DBValue;

/**
 * Generic response interface that can be stored in databse.
 * 
 * @author Poroshin V.
 * @date Sep 26, 2005
 */
public interface Response extends DBValue
{
  /**
   * Gets the response header value associated with the given name.<br>
   * Header name matching is case insensitive. <code>null</code> will be returned if either
   * headerName is null or there is no matching header for headerName.
   * 
   * @param name the header name to match
   * @return the matching header value
   */
  public String getHeaderValue(String name);

  /**
   * Returns a map of the response headers.
   * 
   * @return name-value map of the response headers.
   */
  public Map<String, String> getHeaders();

  /**
   * Returns uncompressed response body as an array of bytes.
   * 
   * @return response body.
   */
  public byte[] getBody();

  /**
   * Returns checksum of response body or -1 if resonse body is <code>null</code>.
   * 
   * @return checksum of response body or -1 if resonse body is <code>null</code>.
   */
  public long getBodyChecksum();

//  /**
//   * Returns Crawler URL of response.
//   * 
//   * @return Crawler URL of response.
//   */
//  public CrawlerURL getCrawlerURL();

  /**
   * Returns response code.
   * 
   * @return response code.
   */
  public int getCode();

  // /**
  // * Returns URL of redirect location if this response is redirect or <code>null</code> otherwise.
  // *
  // * @return URL of redirect location if this response is redirect; <code>null</code> otherwise.
  // */
  // public URL getRedirect();

  /**
   * Returns time in milliseconds when client has disconnected from server.
   * 
   * @return time in milliseconds when client has disconnected from server.
   */
  public long getTimeWhenDisconnected();
}
