/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.IllegalClassException;
import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @author Poroshin V.
 * @date Sep 27, 2005
 */
public class ServiceProvider
{
  private Map<String, Service> allServices = new HashMap<String, Service>();

  private Map<String, CommonService> commonServices = new HashMap<String, CommonService>();

  private Map<String, WorkerService> workerServices = new HashMap<String, WorkerService>();

  private static Log logger = LogFactory.getLog(ServiceProvider.class);

  /**
   * Creates a new empty {@link ServiceProvider} object.
   */
  public ServiceProvider()
  {
  }

  /**
   * Creates a new {@link ServiceProvider} object as a copy of <code>otherServiceProvider</code>.<br>
   * All instances of {@link WorkerService} in <code>otherServiceProvider</code> will be cloned in
   * this provider using {@link WorkerService#newCopyInstance()} function. All instances of
   * {@link CommonService} will be saved without clonning.
   * 
   * @param otherServiceProvider other {@link ServiceProvider} object.
   * @throws ServiceException if creation of some WorkerService has failed.
   * @throws NullArgumentException if <code>otherServiceProvider</code> is <code>null</code>.
   */
  public ServiceProvider(final ServiceProvider otherServiceProvider) throws ServiceException
  {
    if (otherServiceProvider == null)
      throw new NullArgumentException("otherServiceProvider");

    for (Map.Entry<String, CommonService> entry : otherServiceProvider.getCommonServices()
        .entrySet()) {
      allServices.put(entry.getKey(), entry.getValue());
      commonServices.put(entry.getKey(), entry.getValue());
    }
    for (Map.Entry<String, WorkerService> entry : otherServiceProvider.getWorkerServices()
        .entrySet()) {
      WorkerService serviceCopy = entry.getValue().newCopyInstance();
      allServices.put(entry.getKey(), serviceCopy);
      workerServices.put(entry.getKey(), serviceCopy);
    }
  }

  /**
   * Returns service object of specified <code>serviceName</code>.
   * 
   * @param serviceName name of service to get object of.
   * @return service object of specified <code>serviceName</code>.
   * @throws NoSuchServiceException if <code>serviceName</code> does not match any installed
   *           service.
   * @throws NullArgumentException if <code>serviceName</code> is <code>null</code>.
   */
  public Service getService(final String serviceName) throws NoSuchServiceException
  {
    if (serviceName == null)
      throw new NullArgumentException("serviceName");

    Service service = allServices.get(serviceName);
    if (service == null)
      throw new NoSuchServiceException("Unknown service requested: " + serviceName);
    return service;
  }

  /**
   * Registers given service object in this service provider.
   * 
   * @param service to register in this service provider.
   * @throws NullArgumentException if <code>service</code> is <code>null</code>.
   * @throws IllegalClassException TODO
   */
  public void register(Service service)
  {
    if (service == null)
      throw new NullArgumentException("service");

    if (service instanceof WorkerService)
      workerServices.put(service.getServiceName(), (WorkerService) service);
    else if (service instanceof CommonService)
      commonServices.put(service.getServiceName(), (CommonService) service);
    else
      // TODO: handle all services
      throw new IllegalClassException("expected class " + WorkerService.class.toString()
          + " or class " + CommonService.class.toString() + ": " + service.getClass().toString());

    if (logger.isDebugEnabled())
      logger.debug("New service installed: " + service.toString());

    allServices.put(service.getServiceName(), service);
  }

  /**
   * Stops all registered services.
   * 
   * @throws ServiceException if some service has failed to stop.
   */
  public void stopAllServices() throws ServiceException
  {
    for (Map.Entry<String, Service> entry : allServices.entrySet()) {
      entry.getValue().stop();
    }
  }

  /**
   * Stops all registered worker services.
   * 
   * @throws ServiceException if some worker service has failed to stop.
   */
  public void stopAllWorkerServices() throws ServiceException
  {
    for (Map.Entry<String, WorkerService> entry : workerServices.entrySet())
      entry.getValue().stop();
  }

  /**
   * Stops all registered common services.
   * 
   * @throws ServiceException if some common service has failed to stop.
   */
  public void stopAllCommonServices() throws ServiceException
  {
    for (Map.Entry<String, CommonService> entry : commonServices.entrySet()) {
      entry.getValue().stop();
    }
  }

  /**
   * Returns all worker services registered in this service provider. If none of worker services is
   * registered then an empty map will be returned.
   * 
   * @return service-name/service-object map of all registered worker services in this provider.
   */
  Map<String, WorkerService> getWorkerServices()
  {
    return workerServices;
  }

  /**
   * Returns all common services registered in this service provider. If none of common services is
   * registered then an empty map will be returned.
   * 
   * @return service-name/service-object map of all registered common services in this provider.
   */
  Map<String, CommonService> getCommonServices()
  {
    return commonServices;
  }

  /**
   * Returns all services registered in this service provider. If none of services is registered
   * then an empty map will be returned.
   * 
   * @return service-name/service-object map of all registered services in this provider.
   */
  public Map<String, Service> getAllServices()
  {
    return allServices;
  }

  public String toString()
  {
    ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE);
    
    for (Map.Entry<String,Service> entry : allServices.entrySet()) {
      builder.append("\t" + entry.getKey(), "=" + entry.getValue());
    }
    
    return builder.toString();
  }
  
}
