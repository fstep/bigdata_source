/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.service;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Default implementaion of {@link com.porva.crawler.service.Service} interface.
 * <br>
 * 
 * @author Poroshin V.
 * @date Sep 15, 2005
 */
public abstract class AbstractService implements Service
{
  private String name;
  private boolean isStopped = false;
  private static Log logger = LogFactory.getLog(AbstractService.class);
  
  private AbstractService() {}
  
  /**
   * Defines {@link Service} object with specified service name.
   * 
   * @param name name of service.
   * @throws NullArgumentException if <code>name</code> is <code>null</code>.
   */
  public AbstractService(final String name)
  {
    this.name = name;
    if (this.name == null)
      throw new NullArgumentException("name");
  }

  /* (non-Javadoc)
   * @see com.porva.crawler.service.Service#getServiceName()
   */
  public String getServiceName()
  {
    return name;
  }

  /**
   * Marks service as stopped.<br>
   * Method <code>isStopped()</code> of stopped service will return
   * <code>true</code>. If service is already stopped then warning to logger
   * will be written.<br>
   * <br>
   * Consider to overwrite this method in child classes to perform additional
   * finalization operations. Call <code>super.stop()</code> in your child
   * overwritten <code>stop()</code> method to mark service as stopped.
   */
  public void stop() throws ServiceException
  {
    if (isStopped())
      logger.warn("Requested to stop already stopped service: " + this.toString());
    else 
      isStopped = true;
  }

  /* (non-Javadoc)
   * @see com.porva.crawler.service.Service#isStopped()
   */
  public boolean isStopped()
  {
    return isStopped;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("name", name)
        .append("isStopped", isStopped).toString();
  }
}
