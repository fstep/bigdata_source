/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.service;

/**
 * Label interface that marks implemeted classes as common service.<br>
 * Common service is kind of service object that can be used from many different clients
 * simultaniously, i.e. one instance of common service may be available for differect clients at the 
 * same time. Implementation of common service should deals with this issue and probable make it 
 * thread-save.
 * 
 * @author Poroshin V.
 * @date Sep 27, 2005
 */
public interface CommonService extends Service
{

}
