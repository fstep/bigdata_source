/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.crawler.filter.Filter;
import com.porva.crawler.filter.TaskResultFilter;
import com.porva.crawler.filter.TaskFilter;
import com.porva.crawler.task.FetchResult;
import com.porva.crawler.task.FetchTask;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.builder.ToStringBuilder;

public class FilterService extends AbstractWorkerService
{
  private List<TaskFilter> taskFilters = new ArrayList<TaskFilter>();
  
  private TaskFilter robotRulesFilter = null;
  
  private List<TaskResultFilter> fetchResultFilters = new ArrayList<TaskResultFilter>();

  private ServiceProvider servises = new ServiceProvider();

  private static final Log logger = LogFactory.getLog(FilterService.class);

  public FilterService(final List<Filter> filters) throws ServiceException
  {
    super("workerservice.filter");
    ServiceProvider filterServices = new ServiceProvider();
    for (Filter filter : filters)
      filterServices.register(filter);

    init(filterServices);
  }

  public FilterService(final ServiceProvider servises) throws ServiceException
  {
    super("workerservice.filter");
    if (servises == null)
      throw new NullArgumentException("services");

    init(servises);
  }

  private void init(final ServiceProvider servises) throws ServiceException
  {
    for (Map.Entry<String, Service> entry : servises.getAllServices().entrySet()) {
      if (entry.getValue().getServiceName().equals(Filter.ROBOTS_RULES))
        robotRulesFilter = (TaskFilter)entry.getValue(); 
      else if (entry.getValue() instanceof TaskFilter) 
        taskFilters.add((TaskFilter) entry.getValue());
      else if (entry.getValue() instanceof TaskResultFilter)
        fetchResultFilters.add((TaskResultFilter) entry.getValue());
      this.servises.register(entry.getValue());
    }
    Collections.sort(taskFilters);
    Collections.sort(fetchResultFilters);
  }

  public boolean isValidFetchTask(final FetchTask fetchTask)
  {
    if (fetchTask == null)
      throw new NullArgumentException("fetchTask");

    for (TaskFilter filter : taskFilters) {
      if (!filter.isValid(fetchTask)) {
        if (logger.isDebugEnabled())
          logger.debug("Not a valid crawler URL according to filer " + filter + ": " + fetchTask);
        return false;
      }
    }
    return true;
  }
  
  public boolean isValidRobotRules(final FetchTask fetchTask)
  {
    if (robotRulesFilter == null)
      return true;
    
    return robotRulesFilter.isValid(fetchTask);
  }

  public boolean isValidFetchResult(final FetchResult fetchResult)
  {
    if (fetchResult == null)
      throw new NullArgumentException("fetchResult");

    for (TaskResultFilter filter : fetchResultFilters) {
      if (!filter.isValid(fetchResult)) {
        if (logger.isDebugEnabled())
          logger.debug("Not a valid carwler URL according to filer " + filter + ": " + fetchResult);
        return false;
      }
    }
    return true;
  }

  public boolean isValidToProduce(final FetchTask fetchTask)
  {
    if (fetchTask == null)
      throw new NullArgumentException("fetchTask");

    for (TaskFilter filter : taskFilters) {
      if (!filter.isValidToProduce(fetchTask)) {
        if (logger.isDebugEnabled())
          logger
              .debug("Not a valid for processing according to filer " + filter + ": " + fetchTask);
        return false;
      }
    }
    return true;
  }

  public WorkerService newCopyInstance() throws ServiceException
  {
    return new FilterService(new ServiceProvider(servises));
  }

  public void stop() throws ServiceException
  {
    servises.stopAllServices();
    super.stop();
  }

  // todo: add functions for fetchResultFilters

  public String toString()
  {
    ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE);

    for (Filter filter : taskFilters)
      builder.append("\t\t" + filter);
    for (Filter filter : fetchResultFilters)
      builder.append("\t\t" + filter);

    return builder.toString();
  }

}
