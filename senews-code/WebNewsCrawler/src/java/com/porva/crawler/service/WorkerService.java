package com.porva.crawler.service;

/**
 * Service interface that should be implemented by any services that suppose to have separate
 * instances in each worker thread.<br>
 * 
 * @author Poroshin V.
 * @date Sep 15, 2005
 */
public interface WorkerService extends Service
{

  /**
   * Constructs a new instance of {@link WorkerService} as a copy of this service.<br>
   * Note that copy service should do its job exactly as the original does but it does not nessesary
   * mean that they are equal, i.e. <code>service.equals(service.newCopyInstance())</code> may be
   * <code>false</code>.
   * 
   * @return new instance of {@link WorkerService} as a copy of this service.
   * @throws ServiceException if some failer occures.
   */
  public WorkerService newCopyInstance() throws ServiceException;
}
