/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.service;

import java.net.URI;
import java.util.List;

public class ParserService extends AbstractWorkerService
{
  
  private String charset = null;
  
  private String title = null;
  
  private List<String> links = null;

  public ParserService()
  {
    super("workerservice.parser");
  }
  
  public WorkerService newCopyInstance() throws ServiceException
  {
    return new ParserService();
  }

  public String parse(byte[] content, URI uri)
  {
    // 1. detect charset 
    // 2. apply html2xml parser
    // 3. apply kce parser
    // 4. apply jtidy
    return null;
  }

  public String getTitle()
  {
    return title;
  }

  public List<String> getLinks()
  {
    return links;
  }
  
  public String getCharset()
  {
    return charset;
  }

}
