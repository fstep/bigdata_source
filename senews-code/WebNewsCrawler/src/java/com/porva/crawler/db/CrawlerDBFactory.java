/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.db;

import com.porva.crawler.task.FetchRobotxtResult;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskResult;
import com.porva.crawler.workload.WorkloadEntryStatus;


/**
 * Abstract database factory with methods to create special databases for crawler.
 * 
 * @author Poroshin V.
 * @date Sep 17, 2005
 */
public interface CrawlerDBFactory extends DBFactory
{
  
  /**
   * Name of the database to store crawled data.
   */
  public static final String DB_RESULTS = "Result";
  
  /**
   * Name of the database to store robots.txt files.  
   */
  public static final String DB_ROBOTXT = "Robotxt";
  
  /**
   * Name of the database to store tasks in workload.
   */
  public static final String DB_TASK = "Task";
  
  /**
   * Name of the database to store status of tasks in workload.
   */
  public static final String DB_STATUS = "Status";
  
  /**
   * Name of the generic database.
   */
  public static final String DB_GENERIC = "Generic";

  /**
   * Returns new {@link DB} database handle to store {@link TaskResult} objects. If database does
   * not exist it will be created. If the same type of database was alredy requested a <b>new</b>
   * {@link DB} handle will be returned.
   * 
   * @return database to store {@link TaskResult} objects.
   * @throws IllegalStateException if factory is already closed.
   */
  public DB<TaskResult> newResultDatabaseHandle();

  /**
   * Returns new {@link DB} database handle to store {@link FetchRobotxtResult} objects. If database does
   * not exist it will be created. If the same type of database was alredy requested a <b>new</b>
   * {@link DB} handle will be returned.
   * 
   * @return database to store {@link FetchRobotxtResult} objects.
   * @throws IllegalStateException if factory is already closed.
   */
  public DB<FetchRobotxtResult> newRobotxtDatabaseHandle();

  /**
   * Returns new {@link DB} database handle to store {@link Task} objects. If database does not
   * exist it will be created. If the same type of database was alredy requested a <b>new</b>
   * {@link DB} handle will be returned.
   * 
   * @return database to store {@link Task} objects.
   * @throws IllegalStateException if factory is already closed.
   */
  public DB<Task> newTaskDatabaseHandle();

  /**
   * Returns new {@link DB} database handle to store {@link WorkloadEntryStatus} objects. If
   * database does not exist it will be created. If the same type of database was alredy requested a
   * <b>new</b> {@link DB} handle will be returned.
   * 
   * @return database to store {@link WorkloadEntryStatus} objects.
   * @throws IllegalStateException if factory is already closed.
   */
  public DB<WorkloadEntryStatus> newStatusDatabaseHandle();
  
  /**
   * Returns new {@link DB} database handle to store any {@link DBValue} objects. If database does
   * not exist it will be created. If the same type of database was alredy requested a <b>new</b>
   * {@link DB} handle will be returned.
   * 
   * @param isStoreDup <code>true</code> to allow store duplicates; <code>false</code> otherwise.
   * @return database to store any {@link DBValue} objects.
   * @throws IllegalStateException if factory is already closed.
   */
  public DB<DBValue> newGenericDatabaseHandle(boolean isStoreDup);

}
