/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.db.berkley;

import java.io.File;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.porva.crawler.db.CrawlerDBFactory;
import com.porva.crawler.db.CrawlerDBValueFactory;
import com.porva.crawler.db.DB;
import com.porva.crawler.db.DBException;
import com.porva.crawler.db.DBValue;
import com.porva.crawler.task.FetchRobotxtResult;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskResult;
import com.porva.crawler.workload.WorkloadEntryStatus;
import com.sleepycat.bind.tuple.TupleBinding;
import com.sleepycat.je.DatabaseConfig;

/**
 * Implementation of {@link CrawlerDBFactory} using berkley db.<br>
 * 
 * @author Poroshin V.
 * @date Sep 13, 2005
 */
public class BerkleyCrawlerDBFactory extends BerkleyDBFactory implements CrawlerDBFactory
{
  private File envHomeDir;

  private TupleBinding tupleBinder = new DBValueTupleBinder(new CrawlerDBValueFactory());
  
  /**
   * Allocates a new database factory.
   * 
   * @param envHomeDir home directory of database enviroment.
   * @throws DBException if some database exception occures.
   */
  public BerkleyCrawlerDBFactory(final File envHomeDir) throws DBException
  {
    super(true, envHomeDir, true);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.AbstractDBFactory#newResultDatabaseHandle()
   */
  public DB<TaskResult> newResultDatabaseHandle()
  {
    checkFactoryNotClosed();
    DatabaseConfig dbConfig = makeDatabaseConfig();
    dbConfig.setSortedDuplicates(true);
    return new BerkleyDBImpl<TaskResult>(DB_RESULTS, tupleBinder, getEnvironment(), dbConfig);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.AbstractDBFactory#newRobotxtDatabaseHandle()
   */
  public DB<FetchRobotxtResult> newRobotxtDatabaseHandle()
  {
    checkFactoryNotClosed();
    DatabaseConfig dbConfig = makeDatabaseConfig();
    dbConfig.setSortedDuplicates(false);
    return new BerkleyDBImpl<FetchRobotxtResult>(DB_ROBOTXT, tupleBinder, getEnvironment(), dbConfig);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.AbstractDBFactory#newTaskDatabaseHandle()
   */
  public DB<Task> newTaskDatabaseHandle()
  {
    checkFactoryNotClosed();
    DatabaseConfig dbConfig = makeDatabaseConfig();
    dbConfig.setSortedDuplicates(true);
    return new BerkleyDBImpl<Task>(DB_TASK, tupleBinder, getEnvironment(), dbConfig);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.AbstractDBFactory#newStatusDatabaseHandle()
   */
  public DB<WorkloadEntryStatus> newStatusDatabaseHandle()
  {
    checkFactoryNotClosed();
    DatabaseConfig dbConfig = makeDatabaseConfig();
    dbConfig.setSortedDuplicates(false);
    return new BerkleyDBImpl<WorkloadEntryStatus>(DB_STATUS, tupleBinder, getEnvironment(), dbConfig);
  }

  public DB<DBValue> newGenericDatabaseHandle(boolean isStoreDup)
  {
    checkFactoryNotClosed();
    DatabaseConfig dbConfig = makeDatabaseConfig();
    dbConfig.setSortedDuplicates(isStoreDup);
    return new BerkleyDBImpl<DBValue>(DB_GENERIC, tupleBinder, getEnvironment(), dbConfig);
  }

  // /////////////////////////////////////////////////////////////////////////////// private

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("envHomeDir",
                                                                              envHomeDir)
        .append("tupleBinder", tupleBinder).toString();
  }

}
