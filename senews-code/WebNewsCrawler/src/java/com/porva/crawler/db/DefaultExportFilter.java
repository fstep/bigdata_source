/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.db;

import com.porva.crawler.net.Response;
import com.porva.crawler.task.FetchRSSResult;
import com.porva.crawler.task.FetchResult;
import com.porva.crawler.task.FetchRobotxtResult;
import com.porva.crawler.task.FetchURLResult;
import com.porva.crawler.task.GoogleTaskResult;
import com.porva.crawler.task.TaskResult;

/**
 * Default implementaion of {@link com.porva.crawler.db.ExportFilter} interface.<br>
 * This filter is used in main cralwer start-up script implemented by
 * {@link com.porva.crawler.CrawlerMain} class.
 * 
 * @author Poroshin V.
 * @date Jun 9, 2006
 */
public class DefaultExportFilter implements ExportFilter
{

  private boolean isExportFetchURL = true;;

  private boolean isExportFetchRSS = true;

  private boolean isExportGoogle = true;

  private boolean isExportRobotxt = true;

  private long timeRangeStart = -1;

  private long timeRangeEnd = Long.MAX_VALUE;

  public DefaultExportFilter(String params)
  {
    if (params == null || params.equals(""))
      return;

    String[] pairs = params.split(";");
    for (String p : pairs) {
      String[] pair = p.split(":");
      if (pair[0].equals("timeRange")) {
        String[] times = pair[1].split("\\.\\.");
        timeRangeStart = Long.parseLong(times[0]);
        timeRangeEnd = Long.parseLong(times[1]);
      } else {
        throw new IllegalArgumentException(
            "DefaultExportFilter expects artgument: [timeRange:start..end]");
      }
    }
  }

  public void setExportFetchURL(boolean isExportFetchURL)
  {
    this.isExportFetchURL = isExportFetchURL;
  }

  public void setExportFetchRSS(boolean isExportFetchRSS)
  {
    this.isExportFetchRSS = isExportFetchRSS;
  }

  public void setExportGoogle(boolean isExportGoogle)
  {
    this.isExportGoogle = isExportGoogle;
  }

  public void setExportRobotxt(boolean isExportRobotxt)
  {
    this.isExportRobotxt = isExportRobotxt;
  }

  public boolean isExport(final String key, final TaskResult value)
  {
    if (value instanceof FetchResult) {
      Response response = ((FetchResult) value).getResponse();
      if (response != null) {
        if (response.getTimeWhenDisconnected() > timeRangeEnd
            || response.getTimeWhenDisconnected() < timeRangeStart)
          return false;
      }
    }

    if (!isExportFetchURL && (value instanceof FetchURLResult)) {
      return false;
    } else if (!isExportFetchRSS && (value instanceof FetchRSSResult)) {
      return false;
    } else if (!isExportRobotxt && (value instanceof FetchRobotxtResult)) {
      return false;
    } else if (!isExportGoogle && (value instanceof GoogleTaskResult)) {
      return false;
    }

    return true;
  }

}
