/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.db;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.crawler.db.berkley.TupleInputOutputStreamTest.TestDBValue;
import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.net.DefaultResponseImpl;
import com.porva.crawler.task.DefaultFetchResult;
import com.porva.crawler.task.FetchRSSResult;
import com.porva.crawler.task.FetchRobotxtResult;
import com.porva.crawler.task.FetchURLResult;
import com.porva.crawler.task.GoogleTaskResult;
import com.porva.crawler.task.TaskFactory;
import com.porva.crawler.workload.WorkloadEntryStatus;

/**
 * Static factory to create any registered DBValue implementations.<br>
 * <b>TODO: add objects pool feature to this factory</b><br>
 * <br>
 * Factory can create particular instance of DBValue if this instance<br>
 * 1) implements interface {@link DBValue}<br>
 * 2) has a parameterless constructor of any visibility type (private, protected, etc.)<br>
 * 3) registered in DBValueFactory
 * 
 * @author Poroshin V.
 * @date Oct 6, 2005
 */
public class CrawlerDBValueFactory extends DefaultDBValueFactory
{
  private static Log logger = LogFactory.getLog(CrawlerDBValueFactory.class);

  public static final CrawlerDBValueFactory instance = new CrawlerDBValueFactory();

  /**
   * Registed all database values.<br>
   * TODO: registed all classes that implements DBValue dynamicly
   * using reflection.
   */
  public CrawlerDBValueFactory()
  {
    try {
      CrawlerURL crawlerURL = new CrawlerURL(0, "http://localhost");

      registerClass(DefaultResponseImpl.class);
      registerClass(TestDBValue.class); // todo remove it

      registerClass(TaskFactory.newEmptyTask().getClass());
      registerClass(TaskFactory.newLastTask().getClass());
      registerClass(TaskFactory.newFetchURL(crawlerURL).getClass());
      registerClass(TaskFactory.newFetchRSS(crawlerURL).getClass());
      registerClass(TaskFactory.newGoogleTask(1, "test", 1).getClass());
      registerClass(TaskFactory.newFetchRobotxt(crawlerURL).getClass());

      registerClass(CrawlerURL.class);

      registerClass(DefaultFetchResult.class);
      registerClass(FetchRobotxtResult.class);
      registerClass(FetchURLResult.class);
      registerClass(FetchRSSResult.class);
      registerClass(GoogleTaskResult.class);

      registerClass(WorkloadEntryStatus.class);
    } catch (Exception e) {
      logger.fatal("DBValueFactory faild to initialize: ", e);
      throw new IllegalStateException("DBValueFactory faild to initialize", e);
    }
  }
}
