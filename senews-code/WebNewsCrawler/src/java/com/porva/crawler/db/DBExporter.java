/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.db;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.crawler.CrawlerInit;
import com.porva.crawler.task.FetchRSSResult;
import com.porva.crawler.task.FetchResult;
import com.porva.crawler.task.FetchRobotxtResult;
import com.porva.crawler.task.FetchURLResult;
import com.porva.crawler.task.GoogleTaskResult;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskResult;
import com.porva.crawler.task.FetchResult.Status;
import com.porva.crawler.workload.WorkloadEntryStatus;

/**
 * Class to export databases created by {@link CrawlerDBFactory} factory into various formats.
 * 
 * @author Poroshin
 */
public class DBExporter
{

  private static String pathSeparator = System.getProperty("file.separator");

  private static Log logger = LogFactory.getLog(DBExporter.class);

  /**
   * Exporting results of a crawling as <i>news</i>.<br>
   * <i>News</i> format means that:<br>
   * * Exported data will be under specified <code>dir</code> directory.<br>
   * * Each database record will be exported into two files: <i>meta</i> and <i>original</i>, 
   * where <i>meta</i> file contains meta information and <i>original</i> holds dump of bytes
   * from inet socket (i.e. downloaded data without any conversions).<br>
   * * Only two databases are exported: resultDb and robotsTxt.   
   * 
   * @param factory factory to open databases.
   * @param dir top directory to export to.
   * @param filters set of filters used during exporting process.
   * @throws DBException if some databse exception occures.
   * @throws NullArgumentException if <code>factory</code> or <code>dir</code> is <code>null</code>.
   */
  public void exportNews(CrawlerDBFactory factory, File dir, List<ExportFilter> filters)
      throws DBException
  {
    if (factory == null)
      throw new NullArgumentException("factory");
    if (dir == null)
      throw new NullArgumentException("dir");
    if (filters == null)
      filters = new ArrayList<ExportFilter>();

    exportNewsDb(factory.newResultDatabaseHandle(), factory, dir, filters);
    exportNewsDb(factory.newRobotxtDatabaseHandle(), factory, dir, filters);
  }
  
  private void exportNewsDb(DB<? extends DBValue> db, CrawlerDBFactory factory, File dir,
                            List<ExportFilter> filters) throws DBException
  {
    DB<DBValue> tempDb = factory.newGenericDatabaseHandle(false);
    tempDb.open();
    db.open();
    db.iterateOverAll(new ResultDbFunctor(dir, filters, tempDb));
    db.close();
    tempDb.close();
    factory.removeDatabase(CrawlerDBFactory.DB_GENERIC);
  }

  /**
   * Exporting given database <code>db</code> using toString method of the values.<br>
   * This type of exporting is probably useful only for testing purposes.
   * 
   * @param db database to export.
   * @param writer writer to write exported data.
   * @throws DBException if some database exception occures.
   * @throws NullArgumentException if <code>db</code> or <code>writer</code> is 
   *  <code>null</code>.
   */
  public void exportToString(DB<? extends DBValue> db, Writer writer) throws DBException
  {
    if (db == null)
      throw new NullArgumentException("db");
    if (!db.isOpened()) {
      db.open();
    }
    if (writer == null)
      throw new NullArgumentException("writer");

    logger.info("Exporting database " + db);

    try {
      writer.write("\n\ndatabase: " + db.toString());
      db.iterateOverAll(new ToStringExportFunctor(writer));
      writer.flush();
    } catch (IOException e) {
      logger.warn(e);
      e.printStackTrace();
    }

    if (db.isOpened())
      db.close();
  }

  /**
   * Exporting all databases created from given <code>factory</code> to <code>writer</code>.
   * Each database is exported by the {@link #exportToString(DB, Writer)} method.<br>
   * This type of exporting is probably useful only for testing purposes. 
   * 
   * @param factory databases factory.
   * @param writer writer to write exported data to.
   * @throws DBException if some database exception occures.
   */
  public void exportAllToString(final CrawlerDBFactory factory, final Writer writer)
      throws DBException
  {
    if (factory == null)
      throw new NullArgumentException("factory");
    if (factory.isClosed())
      throw new IllegalStateException("factory is closed");
    if (writer == null)
      throw new NullArgumentException("writer");

    DB<TaskResult> resDB = factory.newResultDatabaseHandle();
    DB<FetchRobotxtResult> robotsDB = factory.newRobotxtDatabaseHandle();
    DB<WorkloadEntryStatus> statusDB = factory.newStatusDatabaseHandle();
    DB<Task> taskDB = factory.newTaskDatabaseHandle();

    exportToString(resDB, writer);
    exportToString(robotsDB, writer);
    exportToString(statusDB, writer);
    exportToString(taskDB, writer);
  }

  // ////////////////////////////////////////////////////////////////////

  static class ResultDbFunctor<T extends FetchResult> implements DBFunctor<TaskResult>
  {
    
    private File dir;

    private int num = 0;

    private String xmlEncoding = "utf-8";

    private String kceEncoding = "utf-8";
    
    private String metaEncoding = "utf-8";

    private List<ExportFilter> filters;
    
    private DB<DBValue> tempDb;

    public ResultDbFunctor(File dir, List<ExportFilter> filters, DB<DBValue> tempDb)
    {
      this.dir = dir;
      this.filters = filters;
      this.tempDb = tempDb;

      if (this.dir == null)
        throw new NullArgumentException("dir");
      if (!this.tempDb.isOpened())
        throw new IllegalStateException("Database tempDb is not opened.");

      if (CrawlerInit.getProperties().get("parser.html2xml.output-encoding") != null)
        xmlEncoding = (String) CrawlerInit.getProperties().get("parser.html2xml.output-encoding");

      if (CrawlerInit.getProperties().get("parser.kce.output-encoding") != null)
        kceEncoding = (String) CrawlerInit.getProperties().get("parser.kce.output-encoding");
      
      if (CrawlerInit.getProperties().get("export.meta-encoding") != null)
        metaEncoding = (String) CrawlerInit.getProperties().get("export.meta-encoding");
    }
    
    private void exportMeta(final String metaFilename, final FetchResult res) throws IOException
    {
      assert metaFilename != null;
      assert res != null;
      
      File metaFile = new File(metaFilename);
      metaFile.createNewFile();
      Writer writer = new BufferedWriter(new OutputStreamWriter(
         new FileOutputStream(metaFile), metaEncoding));
      writer.write("url\t" + res.getTask().getCrawlerURL().getURLString() + "\n");
      if (res instanceof FetchURLResult) {
        writer.write("title\t" + ((FetchURLResult) res).getTitle() + "\n");
        writer.write("detected-charset\t" + ((FetchURLResult) res).getCharset() + "\n");
      }
      writer.write("date\t" + res.getResponse().getTimeWhenDisconnected() + "\n");

      writer.write("Meta-code\t" + res.getResponse().getCode() + "\n");
      for (Map.Entry<String, String> header : res.getResponse().getHeaders().entrySet()) {
        StringBuffer sb = new StringBuffer();
        writer.write(sb.append("Meta-").append(header.getKey()).append("\t")
            .append(header.getValue()).append("\n").toString());
      }
      writer.flush();
      writer.close();
    }

    private void exportOriginal(final String origFilename, final FetchResult res)
        throws IOException
    {
      assert origFilename != null;
      assert res != null;

      final File origFile = new File(origFilename);
      origFile.createNewFile();
      FileOutputStream fout = new FileOutputStream(origFile);
      fout.write(res.getResponse().getBody());
      fout.flush();
      fout.close();
    }

    private void exportParsed(String parsedFilename, final FetchURLResult res) throws IOException
    {
      assert parsedFilename != null;
      assert res != null;

      if (res.getParsed() == null)
        return;
      
      String encoding = "utf-8";
      if (res.getParsed().startsWith("<?xml")) {
        encoding = xmlEncoding;
        parsedFilename += ".xml";
      } else {
        encoding = kceEncoding; // todo: it might be not kce processed ???
        parsedFilename += ".kce";
      }

      File parsedFile = new File(parsedFilename);
      parsedFile.createNewFile();
      Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(parsedFile),
          encoding));

      if (res.getParsed() != null) {
        writer.write(res.getParsed());
      }
      writer.flush();
      writer.close();
    }

    private String makePath(final FetchResult res)
    {
      assert res != null;

      String hostname = res.getTask().getCrawlerURL().getURL().getHost();
      File hostDir = new File(dir + pathSeparator + hostname);
      hostDir.mkdirs();

      if (res instanceof FetchRobotxtResult)
        return hostDir + pathSeparator + "robots.txt";
      if (res instanceof FetchRSSResult)
        return hostDir + pathSeparator + Integer.toString(num++) + "-rss";
      if (res instanceof GoogleTaskResult)
        return hostDir + pathSeparator + Integer.toString(num++) + "-google";

      return hostDir + pathSeparator + Integer.toString(num++);
    }

    private void exportFetchURLResult(final FetchURLResult res) throws IOException
    {
      assert res != null;
      
      if (tempDb.get(res.getTask().getCrawlerURL().getURLString()) != null)
        return;

      if (res.getStatus() == Status.COMPLETE && res.getResponse().getCode() == 200) {
        String path = makePath(res);
        exportMeta(path + ".meta", res);
        exportOriginal(path + ".original", res);
        exportParsed(path, res);
        
        tempDb.put(res.getTask().getCrawlerURL().getURLString(), new TempDbValue(true));
      }

    }

    private void exportFetchRSSResult(final FetchRSSResult res) throws IOException
    {
      assert res != null;
      if (res.getStatus() == Status.COMPLETE && res.getResponse().getCode() == 200) {
        String path = makePath(res);
        exportMeta(path + ".meta", res);
        exportOriginal(path + ".original", res);
      }
    }

    private void exportRobotxtResult(final FetchRobotxtResult res) throws IOException
    {
      assert res != null;

      if (res.getStatus() == Status.COMPLETE && res.getResponse().getCode() == 200) {
        String path = makePath(res);
        exportMeta(path + ".meta", res);
        exportOriginal(path + ".original", res);
      }
    }

    private void exportGoogleResult(final GoogleTaskResult res) throws IOException
    {
      assert res != null;
      if (res.getStatus() == Status.COMPLETE && res.getResponse().getCode() == 200) {
        String path = makePath(res);
        exportMeta(path + ".meta", res);
        exportOriginal(path + ".original", res);
      }
    }

    private long processedRecordsNum = 0;

    public void process(String key, TaskResult value)
    {
      if (key == null)
        throw new NullArgumentException("key");
      if (value == null)
        throw new NullArgumentException("value");

      processedRecordsNum++;
      if ((processedRecordsNum % 1000) == 0)
        logger.info(processedRecordsNum + " records processed from database");

      for (ExportFilter filter : filters)
        if (!filter.isExport(key, value))
          return;

      try {
        if (value instanceof FetchURLResult) {
          exportFetchURLResult((FetchURLResult) value);
        } else if (value instanceof FetchRSSResult) {
          exportFetchRSSResult((FetchRSSResult) value);
        } else if (value instanceof FetchRobotxtResult) {
          exportRobotxtResult((FetchRobotxtResult) value);
        } else if (value instanceof GoogleTaskResult) {
          exportGoogleResult((GoogleTaskResult) value);
        }
      } catch (IOException e) {
        logger.warn("Failed to export database", e);
        e.printStackTrace();
      }
    }

  }

  // /////////////////////////////////////////////////////////////////////

  static class ToStringExportFunctor implements DBFunctor
  {
    private Writer writer;

    public ToStringExportFunctor(final Writer writer)
    {
      this.writer = writer;

      if (this.writer == null)
        throw new NullArgumentException("writer");
    }

    public void process(final String key, final DBValue value)
    {
      if (key == null)
        throw new NullArgumentException("key");

      StringBuffer sb = new StringBuffer();
      try {
        writer.write(sb.append("\nkey: ").append(key).append("\nvalue: ").append(value.toString())
            .toString());
      } catch (IOException e) {
        logger.warn("Failed to export database", e);
        e.printStackTrace();
      }
    }
  };
  
  // /////////////////////////////////////////////////////////////////////
  /**
   * Values for a temporary database used during exporting.  
   * 
   * @author Poroshin V.
   */
  static class TempDbValue implements DBValue
  {
    private Boolean key;
    
    TempDbValue(Boolean key)
    {
      this.key = key;
      if (this.key == null)
        throw new NullArgumentException("key");
    }

    public long getClassCode()
    {
      return 381471324023483411L;
    }

    public DBValue newInstance(DBInputStream tupleInput) throws DBException
    {
      return new TempDbValue(tupleInput.readBoolean());
    }

    public void writeTo(DBOutputStream tupleOutput)
    {
      tupleOutput.writeBoolean(key);
    }
  }

}
