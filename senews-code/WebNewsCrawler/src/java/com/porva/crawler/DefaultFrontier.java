/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler;

import java.util.List;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.crawler.manager.Manager;
import com.porva.crawler.service.AbstractService;
import com.porva.crawler.service.NoSuchServiceException;
import com.porva.crawler.service.ServiceProvider;
import com.porva.crawler.task.FetchResult;
import com.porva.crawler.task.LastTask;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskResult;
import com.porva.crawler.workload.Workload;
import com.porva.crawler.workload.WorkloadEntryStatus;

/**
 * General purpose frontier implementation. Frontier is a thread-save object where workers threads
 * will report to.
 * 
 * @author Poroshin V.
 * @date Sep 14, 2005
 */
public class DefaultFrontier extends AbstractService implements Frontier
{
  private Workload workload;

  private Manager manager;

  private static Log logger = LogFactory.getLog(DefaultFrontier.class);

  /**
   * Constructs new {@link DefaultFrontier} object with specified workload and manager.
   * 
   * @param services {@link ServiceProvider} object with at least "commonservice.workload" and
   *          "commonservice.manager" servises installed.
   * @throws NullArgumentException if <code>services</code> or is <code>null</code>.
   */
  public DefaultFrontier(final ServiceProvider services)
  {
    super("commonservice.frontier");

    if (services == null)
      throw new NullArgumentException("services");

    try {
      this.workload = (Workload) services.getService("commonservice.workload");
      this.manager = (Manager) services.getService("commonservice.manager");
    } catch (NoSuchServiceException e) {
      logger.fatal(e);
      throw new IllegalArgumentException(e);
    }

    if (this.manager == null)
      throw new NullArgumentException("manager");
  }

  /**
   * Returns next {@link Task} task to perform.
   * 
   * @return next {@link Task} task to perform.<br>
   *         If there is no more tasks to process available in workload but there are still some
   *         running tasks then {@link EmptyTask} will be returned. If there is no any tasks to
   *         process available and no running tasks then {@link LastTask} instance will be returned.
   */
  public synchronized Task getTask()
  {
    return workload.getTask();
  }

  /**
   * Completes task and its result in manager and workload.
   * 
   * @param task processed task to complete.
   * @param result task result to complete.
   * @throws NullArgumentException if <code>task</code> or <code>result</code> is
   *           <code>null</code>.
   */
  public synchronized void completeTask(final Task task,
                                        final TaskResult result)
  {
    if (task == null)
      throw new NullArgumentException("task");
    if (result == null)
      throw new NullArgumentException("result");
    
    manager.completeTask(task, result);
    if (!workload.completeTask(task, result))
      if (logger.isWarnEnabled())
        logger.warn("Workload failed to complete task: " + task + " " + result);

    if (logger.isInfoEnabled())
      if (result instanceof FetchResult) {
        FetchResult fetchResult = (FetchResult)result;
        if (fetchResult.getResponse() != null)
          logger.info("Task completed with code " + fetchResult.getResponse().getCode() + " :" + task);
        else 
          logger.info("Task failed with reason " + fetchResult.getStatus()  + " : " + task);
      }
  }
  
  /**
   * Adds new task to workload as {@link WorkloadEntryStatus#WAITING}.
   * 
   * @param task new task to add to workload.
   * @return <code>true</code> if task was added successfuly; <code>false</code> otherwise.
   */
  public synchronized boolean addTask(final Task task)
  {
    if (task == null)
      throw new NullArgumentException("task");

    return workload.addTask(task, manager.isOldTask(task));
  }

  /**
   * Adds list of tasks to workload as {@link WorkloadEntryStatus#WAITING} tasks.
   * 
   * @param tasks list of tasks to add.
   * @return number of successfuly added tasks to workload.
   */
  public synchronized int addTaskList(final List<Task> tasks)
  {
    if (tasks == null)
      throw new NullArgumentException("tasks");

    int numOfAdded = 0;
    for (Task task : tasks) {
      if (workload.addTask(task, manager.isOldTask(task)))
        numOfAdded++;
    }
    return numOfAdded;
  }

}
