/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.filter;

import com.porva.crawler.service.WorkerService;

/**
 * Interface of general filter. <br>
 * Filter is {@link com.porva.crawler.service.WorkerService} instance with ability to validate given
 * object.<br>
 * Filters are comparable in terms of their processing speed. The slower processing speed is the
 * greater this filter is.
 * 
 * @author Poroshin V.
 * @date Sep 15, 2005
 */
public interface Filter extends WorkerService, Comparable<Filter>
{

  /**
   * URI filter that dissallows all URIs with crawling depth more then depth limit.<br>
   * This filter has {@link com.porva.crawler.filter.Filter.ProcessingSpeed#VERY_FAST} processing
   * speed.
   */
  public static final String DEPTH_LIMIT = "filter.depth-limit";

  /**
   * {@link TaskFilter} implementation that allows or disallows specified domains to process.<br>
   * This filter has {@link com.porva.crawler.filter.Filter.ProcessingSpeed#VERY_FAST} processing
   * speed.
   */
  public static final String DOMAINS = "filter.domains";

  /**
   * {@link TaskFilter} implementation that allows specified protocols to process.<br>
   * Be sure that for every allowed protocol there is an appropriate fetching service, like
   * HttpClient for HTTP.<br>
   * This filter has {@link com.porva.crawler.filter.Filter.ProcessingSpeed#VERY_FAST} processing
   * speed.
   */
  public static final String PROTOCOLS = "filter.protocols";

  /**
   * Filter that limits number of redirects that the crawler can follow.
   */
  public static final String REDIRECTS_LIMIT = "filter.redirects-limit";

  /**
   * Filter to obey robots.txt rules files.<br>
   * This filter downloads robots.txt files and stores them in database.<br>
   * This filter has {@link com.porva.crawler.filter.Filter.ProcessingSpeed#VERY_SLOW} processing
   * speed since it can work with database or/and download files from the Internet.
   */
  public static final String ROBOTS_RULES = "filter.robots-rules";

  /**
   * Filter that allows only resouces with specified mime-types.
   */
  public static final String MIME_TYPE = "filter.mime-type";
  
  
  public static final String FILE_TYPE = "filter.file-type";
  
  
  public static final String HOST = "filter.host";

  /**
   * Processing speed of the filter.
   * 
   * @author Poroshin V.
   * @date Sep 16, 2005
   */
  public static enum ProcessingSpeed
  {
    /**
     * Denotes very fast filters.<br>
     * This filter has the fastest perfomance speed.<br>
     * This kind of filter should not depend on any external resoures like databases or network
     * services.
     */
    VERY_FAST,

    /**
     * Denotes fast filtes.<br>
     * This filter has quite fast performace speed.<br>
     * This kind of filter should not depend on any external resoures like databases or network
     * services.
     */
    FAST,

    /**
     * Denotes normal speed filters.<br>
     */
    NORMAL,

    /**
     * Denotes slow filter. <br>
     * This kind of filter probable communicats with local database.
     */
    SLOW,

    /**
     * Denotes very slow filter.<br>
     * This kind of filter probable uses some network operations like making http requests,
     * connecting to remote tcp sercive, etc.
     */
    VERY_SLOW;
  }

  /**
   * Returns estimated processing speed of this filter.
   * 
   * @return processing speed of the filter.
   */
  public ProcessingSpeed getProcessingSpeed();

  /**
   * Returns configuration string for this filter.
   * 
   * @return configuration string for this filter.
   */
  public String getConfigStr();
  
}
