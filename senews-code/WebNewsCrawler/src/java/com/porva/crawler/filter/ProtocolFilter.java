/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.filter;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.porva.crawler.service.ServiceException;
import com.porva.crawler.service.WorkerService;
import com.porva.crawler.task.FetchTask;
import com.porva.crawler.task.Task;

/**
 * {@link TaskFilter} implementation that allows specified protocols to process.<br>
 * Be sure that for every allowed protocol there is an appropriate fetching service, like HttpClient 
 * for HTTP.<br>
 * This filter has {@link com.porva.crawler.filter.Filter.ProcessingSpeed#VERY_FAST} processing
 * speed.
 * 
 * @author Poroshin V.
 * @date Sep 22, 2005
 */
class ProtocolFilter extends AbstractTaskFilter
{
  private String[] allowedProtocols = null;

  /**
   * Constructs a new protocol filer with specified list of allowed protocols.
   * 
   * @param allowedProtocols list of allowed protocols.
   * @throws NullArgumentException if <code>allowedProtocols</code> is <code>null</code>.
   * @throws IllegalArgumentException if <code>allowedProtocols</code> length is 0.
   */
  ProtocolFilter(final String[] allowedProtocols)
  {
    super(Filter.PROTOCOLS, ProcessingSpeed.VERY_FAST);
    init(allowedProtocols);
  }
  
  void init(final String[] allowedProtocols)
  {
    this.allowedProtocols = (String[]) ArrayUtils.clone(allowedProtocols);
    if (this.allowedProtocols == null)
      throw new NullArgumentException("allowedProtocols");
    if (this.allowedProtocols.length == 0)
      throw new IllegalArgumentException("allowedProtocols length cannot be 0");
  }

  /* (non-Javadoc)
   * @see com.porva.crawler.filter.TaskFilter#isValid(com.porva.crawler.task.Task)
   */
  public boolean isValid(final Task task)
  {
    if (task == null)
      throw new NullArgumentException("task");
    
    if (!(task instanceof FetchTask))
      return true;
    FetchTask fetchTask = (FetchTask)task; 

    String protocol = fetchTask.getCrawlerURL().getProtocol();
    for (String allowedProtocol : allowedProtocols)
      if (allowedProtocol.equalsIgnoreCase(protocol))
        return true;
    return false;
  }

  public WorkerService newCopyInstance() throws ServiceException
  {
    return new ProtocolFilter(allowedProtocols);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
        .appendSuper(super.toString()).append("allowedProtocols", allowedProtocols).toString();
  }

  public boolean isValidToProduce(Task task)
  {
    return true;
  }

  public String getConfigStr()
  {
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < allowedProtocols.length - 1; i++)
      sb.append(allowedProtocols[i]).append(",");
    sb.append(allowedProtocols[allowedProtocols.length - 1]);

    return sb.toString();
  }
}
