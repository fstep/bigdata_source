/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.filter;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.db.CrawlerDBFactory;
import com.porva.crawler.db.DBException;
import com.porva.crawler.filter.Filter.ProcessingSpeed;
import com.porva.crawler.filter.FilterConfigStrUtils.ConfigParam;
import com.porva.crawler.net.HttpClient;
import com.porva.crawler.service.ServiceException;
import com.porva.crawler.task.Task;

/**
 * Factory to create Filter objects.
 * 
 * @author Poroshin V.
 * @date Oct 5, 2005
 */

public class FilterFactory
{

  private FilterFactory()
  {
  }

  public static Filter newFilter(final String filterType,
                                 final String configStr,
                                 final CrawlerDBFactory dbFactory,
                                 final HttpClient httpClient) throws DBException, ServiceException
  {
    ConfigParam cp = FilterConfigStrUtils.parseBooleanAndStringListStr(configStr, true, null);
    Filter filter = null;

    if (filterType.equals(Filter.DEPTH_LIMIT)) {
      filter = new DepthLimitFilter(Integer.parseInt(cp.getParams()[0]));
    } else if (filterType.equals(Filter.DOMAINS)) {
      filter = new DomainFilter(cp.isAllow(), cp.getParams());
    } else if (filterType.equals(Filter.FILE_TYPE)) {
      filter = new FileTypeFilter(cp.isAllow(), cp.getParams());
    } else if (filterType.equals(Filter.HOST)) {
      filter = new HostFilter(cp.getParams());
    } else if (filterType.equals(Filter.MIME_TYPE)) {
      filter = new MimeTypeFilter(cp.getParams());
    } else if (filterType.equals(Filter.PROTOCOLS)) {
      filter = new ProtocolFilter(cp.getParams());
    } else if (filterType.equals(Filter.REDIRECTS_LIMIT)) {
      filter = new RedirectsLimitFilter(Integer.parseInt(cp.getParams()[0]));
    } else if (filterType.equals(Filter.ROBOTS_RULES)) {
      filter = new RobotRulesFilter(Integer.parseInt(cp.getParams()[0]), dbFactory, httpClient);
    }

    return filter;
  }

  public static void initFilter(final Filter filter, final String configStr)
  {
    if (filter == null)
      throw new NullArgumentException("filter");

    ConfigParam cp = FilterConfigStrUtils.parseBooleanAndStringListStr(configStr, true, "");

    if (filter instanceof DepthLimitFilter) {
      // FIXME ((DepthLimitFilter)filter).init(Integer.parseInt(cp.getParams()[0]));
      throw new NotImplementedException();
    } else if (filter instanceof DomainFilter) {
      ((DomainFilter) filter).init(cp.isAllow(), cp.getParams());
    } else if (filter instanceof FileTypeFilter) {
      ((FileTypeFilter) filter).init(cp.isAllow(), cp.getParams());
    } else if (filter instanceof HostFilter) {
      ((HostFilter) filter).init(cp.getParams());
    } else if (filter instanceof HostFilter) {
      ((MimeTypeFilter) filter).init(cp.getParams());
    } else if (filter instanceof ProtocolFilter) {
      ((ProtocolFilter) filter).init(cp.getParams());
    } else if (filter instanceof RedirectsLimitFilter) {
      // FIXME ((RedirectsLimitFilter)filter).init(Integer.parseInt(cp.getParams()[0]));
      throw new NotImplementedException();
    } else if (filter instanceof RobotRulesFilter) {
      // FIXME filter = new RobotRulesFilter(Integer.parseInt(cp.getParams()[0]), dbFactory);
      throw new NotImplementedException();
    }
  }

  /**
   * Creation method that constructs new {@link TaskFilter} object that will pass only allowed
   * domains specified by <code>allowedDomains</code>. If <code>allowedDomains</code> array is
   * <code>null</code> or empty then ALL domains will be disallowed except <code>null</code>
   * domain.<br>
   * <br>
   * URIs with empty domains are denoted as URIs with <code>null</code> domains. For URLs with
   * <code>null</code> domains {@link TaskFilter#isValid(Task)} will always return
   * <code>true</code>.
   * 
   * @param allowedDomains array of allowed domains or <code>null</code> to disallow all domains.
   * @return new allocated {@link DomainFilter} object.
   */
  public static Filter newAllowDomainFilter(String[] allowedDomains)
  {
    return DomainFilter.newAllowDomainFilter(allowedDomains);
  }

  public static void initAllowDomainFilter(Filter filter, String[] allowedDomains)
  {
    if (filter == null)
      throw new NullArgumentException("filter");

    ((DomainFilter) filter).init(true, allowedDomains);
  }

  /**
   * Creation method that constructs new {@link TaskFilter} object that will pass only domains NOT
   * specified by <code>disallowedDomains</code>. If <code>disallowedDomains</code> array is
   * <code>null</code> or empty then ALL domains will be allowed. <br>
   * URIs with empty domains are denoted as URIs with <code>null</code> domains. For URLs with
   * <code>null</code> domains {@link TaskFilter#isValid(Task)} will always return
   * <code>true</code>.
   * 
   * @param disallowedDomains array of disallowed domains or <code>null</code> to allow all
   *          domains.
   * @return new allocated {@link DomainFilter} object.
   */
  public static Filter newDisallowDomainFilter(String[] disallowedDomains)
  {
    return DomainFilter.newDisallowDomainFilter(disallowedDomains);
  }

  public static void initDisallowDomainFilter(Filter filter, String[] disallowedDomains)
  {
    if (filter == null)
      throw new NullArgumentException("filter");

    ((DomainFilter) filter).init(false, disallowedDomains);
  }

  public static Filter newDomainFilter(boolean isAllow, String[] domains)
  {
    return new DomainFilter(isAllow, domains);
  }

  public static void initDomainFilter(Filter filter, boolean isAllow, String[] domains)
  {
    if (filter == null)
      throw new NullArgumentException("filter");

    ((DomainFilter) filter).init(isAllow, domains);
  }

  /**
   * Allocates a new {@link TaskFilter} object with specified lifetime of robots rules.<br>
   * This will also set processing speed to {@link ProcessingSpeed#VERY_SLOW}.
   * 
   * @param rulesLifetime lifetime in milliseconds for robots rules.<br>
   *          After lifetime expires robots.txt file will be refetched again.
   * @param dbFactory <b>not closed</b> database factory.
   * @return a new {@link TaskFilter} object.
   * @throws DBException if database failure occurs.
   * @throws ServiceException
   * @throws IllegalArgumentException if <code>rulesLifetime</code> is less then 0.
   */
  public static Filter newRobotRulesFilter(int rulesLifetime,
                                           final CrawlerDBFactory dbFactory,
                                           final HttpClient httpClient) throws DBException,
      ServiceException
  {
    return new RobotRulesFilter(rulesLifetime, dbFactory, httpClient);
  }

  /**
   * Allocates a new {@link TaskFilter} object that dissallows all URIs with crawling depth more
   * then depth limit.
   * 
   * @param depthLimit default depth limit value.
   * @return a new filter.
   */
  public static Filter newDepthLimitFilter(int depthLimit)
  {
    return new DepthLimitFilter(depthLimit);
  }

  /**
   * Constructs a new protocol filer with specified list of allowed protocols.
   * 
   * @param allowedProtocols list of allowed protocols.
   * @return new instance of protocol filter.
   * @throws NullArgumentException if <code>allowedProtocols</code> is <code>null</code>.
   */
  public static Filter newProtocolFilter(final String[] allowedProtocols)
  {
    return new ProtocolFilter(allowedProtocols);
  }

  public static void initProtocolFilter(Filter filter, final String[] allowedProtocols)
  {
    if (filter == null)
      throw new NullArgumentException("filter");

    ((ProtocolFilter) filter).init(allowedProtocols);
  }

  /**
   * Costructs a new filter that restricts number of redirects.
   * 
   * @param maxRedirects max. number of redirects.
   * @return new constructed filter.
   * @throws IllegalArgumentException if <code>maxRedirects</code> is < 0.
   */
  public static Filter newRedirectsLimitFilter(int maxRedirects)
  {
    return new RedirectsLimitFilter(maxRedirects);
  }

  /**
   * Constructs a new filter that allows only specifined mime types.
   * 
   * @param allowedMimeTypes
   * @return new constructed filter.
   */
  public static Filter newAllowMimeTypesFilter(final String[] allowedMimeTypes)
  {
    return new MimeTypeFilter(allowedMimeTypes);
  }

  public static void initAllowMimeTypesFilter(Filter filter, final String[] allowedMimeTypes)
  {
    if (filter == null)
      throw new NullArgumentException("filter");

    ((MimeTypeFilter) filter).init(allowedMimeTypes);
  }

  public static Filter newFileTypeFilter(boolean isAllow, String[] types)
  {
    return new FileTypeFilter(isAllow, types);
  }

  public static void initFileTypeFilter(Filter filter, boolean isAllow, String[] types)
  {
    if (filter == null)
      throw new NullArgumentException("filter");

    ((FileTypeFilter) filter).init(isAllow, types);
  }

  public static Filter newHostFilter(String[] allowedHost)
  {
    return new HostFilter(allowedHost);
  }

  public static void initHostFilter(Filter filter, String[] allowedHost)
  {
    if (filter == null)
      throw new NullArgumentException("filter");

    ((HostFilter) filter).init(allowedHost);
  }

}
