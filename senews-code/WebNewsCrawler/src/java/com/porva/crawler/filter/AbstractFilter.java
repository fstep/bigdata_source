/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.filter;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.CompareToBuilder;

import com.porva.crawler.service.AbstractWorkerService;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * This class provides default implementaion of {@link com.porva.crawler.filter.Filter} interface.
 * <br>
 * 
 * @author Poroshin V.
 * @date Sep 16, 2005
 */
abstract class AbstractFilter extends AbstractWorkerService implements Filter
{
  private ProcessingSpeed processingSpeed;

  /**
   * Allocates a new filter with specified service name and processing speed.
   * 
   * @param name name of the filter service.
   * @param processingSpeed estimated processing speed of the filter.
   */
  public AbstractFilter(final String name,
                        final ProcessingSpeed processingSpeed
                        )
  {
    super(name);
    this.processingSpeed = processingSpeed;
    if (this.processingSpeed == null)
      throw new NullArgumentException("processingSpeed");
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.filter.Filter#getProcessingSpeed()
   */
  public ProcessingSpeed getProcessingSpeed()
  {
    return processingSpeed;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Comparable#compareTo(java.lang.Object)
   */
  public int compareTo(final Filter other)
  {
    return new CompareToBuilder().append(getProcessingSpeed(), other.getProcessingSpeed())
        .toComparison();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
        .appendSuper(super.toString()).append("processingSpeed", processingSpeed).toString();
  }

}
