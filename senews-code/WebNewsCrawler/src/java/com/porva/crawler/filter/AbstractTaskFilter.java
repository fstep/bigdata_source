/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.filter;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * This class provides default implementaion of {@link com.porva.crawler.filter.TaskFilter}
 * interface. <br>
 * 
 * @author Poroshin V.
 * @date Sep 27, 2005
 */
public abstract class AbstractTaskFilter extends AbstractFilter implements TaskFilter
{

  /**
   * Allocates a new {@link TaskFilter} with specified service name and processing speed.
   * 
   * @param name name of the filter service.
   * @param processingSpeed estimated processing speed of the filter.
   */
  public AbstractTaskFilter(final String name,
                            final ProcessingSpeed processingSpeed)
  {
    super(name, processingSpeed);
  }

  private transient String toString;

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    if (toString == null) {
      toString = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
          .appendSuper(super.toString()).toString();
    }
    return toString;
  }

}
