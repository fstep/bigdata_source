/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.filter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.service.ServiceException;
import com.porva.crawler.service.WorkerService;
import com.porva.crawler.task.FetchResult;
import com.porva.crawler.task.TaskResult;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * Filter that allows only resouces with specified mime-types.
 *
 * @author Poroshin V.
  */
class MimeTypeFilter extends AbstractTaskResultFilter
{

  private static final Pattern pattern = Pattern.compile("^(\\S+/[^\\s;]+);?.*");

  private String[] allowedMimeTypes;

  /**
   * Constructs a new filter that allows tasks results if "content-type" mime type is one of
   * <code>allowedMimeTypes</code>.
   * 
   * @param allowedMimeTypes array of allowed mime-type strings.
   * @throws NullArgumentException if <code>allowedMimeTypes</code> is <code>null</code>.
   * @throws IllegalArgumentException if <code>allowedMimeTypes</code> has length of 0.
   */
  public MimeTypeFilter(final String[] allowedMimeTypes)
  {
    super(Filter.MIME_TYPE, ProcessingSpeed.VERY_FAST);
    init(allowedMimeTypes);
  }
  
  void init(final String[] allowedMimeTypes)
  {
    if (allowedMimeTypes == null)
      throw new NullArgumentException("allowedMimeTypes");
    if (allowedMimeTypes.length == 0)
      throw new IllegalArgumentException("allowedMimeTypes must contain at least one element");

    this.allowedMimeTypes = (String[]) ArrayUtils.clone(allowedMimeTypes);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.service.WorkerService#newCopyInstance()
   */
  public WorkerService newCopyInstance() throws ServiceException
  {
    return new MimeTypeFilter(allowedMimeTypes);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.filter.TaskResultFilter#isValid(com.porva.crawler.task.TaskResult)
   */
  public boolean isValid(final TaskResult taskResult)
  {
    if (taskResult == null)
      throw new NullArgumentException("taskResult");
    if (!(taskResult instanceof FetchResult))
      return true;

    FetchResult fetchResult = (FetchResult) taskResult;
    if (fetchResult.getResponse() == null)
      throw new IllegalArgumentException("response is null");

    String contentType = fetchResult.getResponse().getHeaderValue("content-type");
    if (contentType == null)
      return true;

    Matcher matcher = pattern.matcher(contentType);
    if (matcher.matches()) {
      String responseMimeType = matcher.group(1);
      for (String mimeType : allowedMimeTypes) {
        if (mimeType.equalsIgnoreCase(responseMimeType))
          return true;
      }
    }
    return false;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.filter.TaskResultFilter#isValidToProduce(com.porva.crawler.task.TaskResult)
   */
  public boolean isValidToProduce(TaskResult taskResult)
  {
    return true;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("allowedMimeTypes",
                                                                              allowedMimeTypes)
        .toString();
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.filter.Filter#getConfigStr()
   */
  public String getConfigStr()
  {
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < allowedMimeTypes.length - 1; i++)
      sb.append(allowedMimeTypes[i]).append(",");
    sb.append(allowedMimeTypes[allowedMimeTypes.length - 1]);

    return sb.toString();
  }

}
