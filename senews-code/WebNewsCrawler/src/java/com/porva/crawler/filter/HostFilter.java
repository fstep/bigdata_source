/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.filter;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.porva.crawler.service.ServiceException;
import com.porva.crawler.service.WorkerService;
import com.porva.crawler.task.FetchTask;
import com.porva.crawler.task.Task;

/**
 * Host filter.
 *
 * @author Poroshin V.
 * @date Jan 30, 2006
 */
public class HostFilter extends AbstractTaskFilter
{
  private String[] allowedHosts = null;

  /**
   * Constructs a new host filer with specified list of allowed hosts.
   * 
   * @param allowedHosts list of allowed hosts.
   * @throws NullArgumentException if <code>allowedHosts</code> is <code>null</code>.
   * @throws IllegalArgumentException if <code>allowedHosts</code> length is 0.
   */
  HostFilter(final String[] allowedHosts)
  {
    super(Filter.HOST, ProcessingSpeed.VERY_FAST);
    init(allowedHosts);
  }
  
  void init(final String[] allowedProtocols)
  {
    this.allowedHosts = (String[]) ArrayUtils.clone(allowedProtocols);
    if (this.allowedHosts == null)
      throw new NullArgumentException("allowedHosts");
    if (this.allowedHosts.length == 0)
      throw new IllegalArgumentException("allowedHosts length cannot be 0");
  }

  /* (non-Javadoc)
   * @see com.porva.crawler.filter.TaskFilter#isValid(com.porva.crawler.task.Task)
   */
  public boolean isValid(final Task task)
  {
    if (task == null)
      throw new NullArgumentException("task");
    
    if (!(task instanceof FetchTask))
      return true;
    FetchTask fetchTask = (FetchTask)task; 

    String protocol = fetchTask.getCrawlerURL().getHost();
    for (String allowedHost : allowedHosts)
      if (allowedHost.equalsIgnoreCase(protocol))
        return true;
    return false;
  }

  public WorkerService newCopyInstance() throws ServiceException
  {
    return new HostFilter(allowedHosts);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
        .appendSuper(super.toString()).append("allowedHosts", allowedHosts).toString();
  }

  public boolean isValidToProduce(Task task)
  {
    return true;
  }

  public String getConfigStr()
  {
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < allowedHosts.length - 1; i++)
      sb.append(allowedHosts[i]).append(",");
    sb.append(allowedHosts[allowedHosts.length - 1]);

    return sb.toString();
  }

}
