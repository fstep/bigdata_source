/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.filter;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.crawler.CrawlerInit;
import com.porva.crawler.DefaultCrawler;
import com.porva.crawler.db.CrawlerDBFactory;
import com.porva.crawler.db.DB;
import com.porva.crawler.db.DBException;
import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.net.HttpClient;
import com.porva.crawler.net.MalformedCrawlerURLException;
import com.porva.crawler.net.Response;
import com.porva.crawler.service.ServiceException;
import com.porva.crawler.service.ServiceProvider;
import com.porva.crawler.service.WorkerService;
import com.porva.crawler.task.FetchResult;
import com.porva.crawler.task.FetchRobotxtResult;
import com.porva.crawler.task.FetchTask;
import com.porva.crawler.task.GoogleTask;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskFactory;

/**
 * Filter to obey robots.txt rules files.<br>
 * This filter downloads robots.txt files and stores them in database.<br>
 * This filter has {@link com.porva.crawler.filter.Filter.ProcessingSpeed#VERY_SLOW} processing
 * speed since it can work with database or/and download files from the Internet.
 * 
 * @author Poroshin V.
 * @date Sep 15, 2005
 */
class RobotRulesFilter extends AbstractTaskFilter
{

  // // todo:
  // private static Map<String,String> apacheSettings = new Hashtable<String,String>();
  // static {
  // apacheSettings.put("http.protocol.follow-redirects", "true");
  // }

  // todo: maybe request it from ServiceProvider ?
  private HttpClient httpClient;

  private ServiceProvider serviceProvider = new ServiceProvider();

  private DB<FetchRobotxtResult> db;

  private final CrawlerDBFactory dbFactory;

  private int rulesLifetime;

  private RobotRulesParser robotRulesParser;

  /**
   * Warinig: be sure that this Map permits <code>null</code> values.
   */
  private static Map<String, RobotRulesParser.RobotRuleSet> robotRulesCache = new Hashtable<String, RobotRulesParser.RobotRuleSet>();

  private static Log logger = LogFactory.getLog(DefaultCrawler.class);

  /**
   * Allocates a new {@link RobotRulesFilter} object with specified lifetime of robots rules.<br>
   * This will also set processing speed to {@link ProcessingSpeed#VERY_SLOW}.
   * 
   * @param rulesLifetime lifetime in milliseconds for robots rules.<br>
   *          After lifetime expires robots.txt file will be refetched again.
   * @param dbFactory <b>not closed</b> database factory.
   * @throws DBException if database failure occurs.
   * @throws ServiceException 
   * @throws IllegalArgumentException if <code>rulesLifetime</code> is less then 0.
   * @throws NullArgumentException if <code>httpClient</code> or <code>dbFactory</code> is
   *           <code>null</code>.
   */
  RobotRulesFilter(int rulesLifetime, final CrawlerDBFactory dbFactory, HttpClient httpClient)
      throws DBException, ServiceException
  {
    super(Filter.ROBOTS_RULES, ProcessingSpeed.VERY_SLOW);

    this.rulesLifetime = rulesLifetime;
    this.dbFactory = dbFactory;
    this.httpClient = httpClient;
    if (this.httpClient == null)
      throw new NullArgumentException("httpClient");
    if (this.rulesLifetime < 0)
      throw new IllegalArgumentException("rulesLifetime must be >= 0: " + this.rulesLifetime);
    if (this.dbFactory == null)
      throw new NullArgumentException("dbFactory");
    if (this.dbFactory.isClosed())
      throw new IllegalStateException("dbFactory is closed.");

    db = this.dbFactory.newRobotxtDatabaseHandle();
    db.open();
    if (logger.isDebugEnabled())
      logger.debug("Database handle opened in RobotRulesFilter: " + db);

    //robotRulesParser = new RobotRulesParser(CrawlerSettings.HTTP_ROBOTS_AGENTS.split(","));
    robotRulesParser = new RobotRulesParser(CrawlerInit.getProperties().getProperty("http.robots.agents").split(","));
    serviceProvider.register(httpClient.newCopyInstance());
  }

  /**
   * Returns <code>true</code> if {@link CrawlerURL} object is valid according to rules defined in
   * robots.txt file of this host.<br>
   * If <code>crawlerUri</code> does not define host then <code>true</code> will be returned.<br>
   * If robots.txt file cannot be fetched from host, it is empty or its parsing fails then an empty
   * rules will be applied. Empty rules will allow all requests to host.
   * 
   * @param task object to validate.
   * @return <code>true</code> if task object is valid according to this filter;
   *         <code>false</code> otherwise.
   * @throws NullArgumentException if <code>crawlerUri</code> is <code>null</code>.
   */
  public boolean isValid(final Task task)
  {
    if (task == null)
      throw new NullArgumentException("task");

    if (!(task instanceof FetchTask))
      return true;

    if (task instanceof GoogleTask)
      return true;

    FetchTask fetchTask = (FetchTask) task;

    String host = fetchTask.getCrawlerURL().getHost();
    if (host == null)
      return true;

    RobotRulesParser.RobotRuleSet rules = getRulesFromCache(host);
    if (rules != null && !rules.isExpiredNow())
      return checkRules(rules, fetchTask.getCrawlerURL());

    if (rules == null) {
      rules = getRulesFromDB(host);
      if (rules != null && !rules.isExpiredNow()) {
        robotRulesCache.put(host, rules);
        return checkRules(rules, fetchTask.getCrawlerURL());
      }
    }

    rules = fetchRules(fetchTask.getCrawlerURL());
    if (rules == null) {
      rules = RobotRulesParser.getEmptyRules();
      rules.setExpireTime(System.currentTimeMillis() + rulesLifetime);
    }
    robotRulesCache.put(host, rules);

    return checkRules(rules, fetchTask.getCrawlerURL());
  }

  /**
   * Returns <code>true</code> if given <code>rules</code> allows <code>crawlerUri</code>.
   * 
   * @param rules not <code>null</code> rules
   * @param crawlerUri not <code>null</code> crawlerUri to check it against rules.
   * @return <code>true</code> if given <code>rules</code> allows <code>crawlerUri</code>;
   *         <code>false</code> otherwise.
   */
  boolean checkRules(RobotRulesParser.RobotRuleSet rules, CrawlerURL crawlerUri)
  {
    assert rules != null;
    assert crawlerUri != null;

    String path = crawlerUri.getPath();
    if ((path == null) || "".equals(path))
      path = "/";

    return rules.isAllowed(path);
  }

  /**
   * Returns {@link RobotRulesParser.RobotRuleSet} object for specified host or <code>null</code>
   * if <code>host</code> key is not present in cache or it was explicitly mapped to
   * <code>null</code>.
   * 
   * @param host hostname used as a key to search rules cache.
   * @return {@link RobotRulesParser.RobotRuleSet} object for specified host or <code>null</code>
   *         if <code>host</code> key is not present in cache or it was explicitly mapped to
   *         <code>null</code>.
   */
  RobotRulesParser.RobotRuleSet getRulesFromCache(String host)
  {
    if (robotRulesCache.containsKey(host))
      return robotRulesCache.get(host);
    return null;
  }

  /**
   * Returns robots rules object from database or <code>null</code> if for given <code>host</code>
   * rules are not stored in database.
   * 
   * @param host hostname as a key to seek in database
   * @return rules object for this <code>host</code> or <code>null</code> if no rules are stored
   *         in database for this <code>host</code>.
   */
  RobotRulesParser.RobotRuleSet getRulesFromDB(String host)
  {
    RobotRulesParser.RobotRuleSet rules = null;
    FetchRobotxtResult fetchResult = db.get(host);
    if (fetchResult == null)
      return null;

    if (FetchResult.Status.COMPLETE == fetchResult.getStatus())
      rules = RobotRulesParser.getEmptyRules();
    else if (fetchResult.getResponse().getBody() != null)
      rules = robotRulesParser.parseRules(fetchResult.getResponse().getBody());
    rules.setExpireTime(fetchResult.getResponse().getTimeWhenDisconnected() + rulesLifetime);

    return rules;
  }

  /**
   * Downloads robots.txt file and returns rules based on it.<br>
   * If download fails for some reason then <code>null</code> will be returned.
   * 
   * @param crawlerUri
   * @return {@link RobotRulesParser.RobotRuleSet} if downloading of robots.txt file succeded;
   *         <code>false</code> otherwise.
   */
  RobotRulesParser.RobotRuleSet fetchRules(CrawlerURL crawlerUri)
  {
    assert crawlerUri != null;
    assert crawlerUri.getHost() != null;

    String host = crawlerUri.getHost();
    RobotRulesParser.RobotRuleSet rules = null;
    FetchRobotxtResult fetchResult = null;
    String robotsURLString = "http://" + host + "/robots.txt";
    CrawlerURL crawlerURL = null;

    try {
      crawlerURL = new CrawlerURL(0, robotsURLString);
      Task task = TaskFactory.newFetchRobotxt(crawlerURL);
      fetchResult = (FetchRobotxtResult) task.perform(serviceProvider);
      rules = getRules(fetchResult);
    } catch (MalformedCrawlerURLException e) {
      logger.warn("Error in uri " + robotsURLString, e);
    }

    return rules;
  }

  private RobotRulesParser.RobotRuleSet getRules(final FetchRobotxtResult fetchResult)
  {
    assert fetchResult != null;

    RobotRulesParser.RobotRuleSet rules = null;

    FetchRobotxtResult completeRes = findCompleteResult(fetchResult);

    if (completeRes != null) {
      List<FetchRobotxtResult> redirects = new ArrayList<FetchRobotxtResult>();
      findAllRedirects(fetchResult, redirects);
      for (FetchRobotxtResult res : redirects) {
        db.put(res.getTask().getCrawlerURL().getHost(), completeRes);
      }
      db.put(completeRes.getTask().getCrawlerURL().getHost(), completeRes);

      rules = robotRulesParser.parseRules(completeRes.getResponse().getBody());
      rules.setExpireTime(fetchResult.getResponse().getTimeWhenDisconnected() + rulesLifetime);
    }

    return rules;
  }

  private FetchRobotxtResult findCompleteResult(final FetchRobotxtResult result)
  {
    if (result == null || result.getResponse() == null)
      return null;

    if (isOk(result.getResponse()))
      return result;

    return findCompleteResult(result.getRedirectResult());
  }

  private void findAllRedirects(final FetchRobotxtResult result, List<FetchRobotxtResult> redirects)
  {
    assert redirects != null;

    if (result == null || result.getResponse() == null)
      return;

    if (isRedirect(result.getResponse()))
      redirects.add(result);

    findAllRedirects(result.getRedirectResult(), redirects);
  }

  private boolean isRedirect(final Response response)
  {
    assert response != null;

    return (response.getCode() >= 300 && response.getCode() < 400);
  }

  private boolean isOk(final Response response)
  {
    assert response != null;

    return (response.getCode() == 200);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.service.WorkerService#newCopyInstance()
   */
  public WorkerService newCopyInstance() throws ServiceException
  {
    try {
      return new RobotRulesFilter(rulesLifetime, dbFactory, httpClient);
    } catch (DBException e) {
      throw new ServiceException(e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.service.Service#stop()
   */
  public void stop() throws ServiceException
  {
    if (isStopped())
      return;
    try {
      db.close();
    } catch (DBException e) {
      throw new ServiceException(e);
    } finally {
      if (logger.isDebugEnabled())
        logger.debug("Database handle closed in RobotRulesFilter: " + db);
      super.stop();
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.filter.TaskFilter#isValidToProduce(com.porva.crawler.task.Task)
   */
  public boolean isValidToProduce(Task task)
  {
    return true;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.filter.Filter#getConfigStr()
   */
  public String getConfigStr()
  {
    return String.valueOf(rulesLifetime);
  }
  
  ////////////////////////////////////////////////////////////////////////////////////
  /**
   * This class handles the parsing of <code>robots.txt</code> files.
   * It emits RobotRules objects, which describe the download permissions
   * as described in RobotRulesParser.
   *
   * @author Tom Pierce, modified by Mike Cafarella
   */
  static class RobotRulesParser {

      private HashMap<String,Integer> robotNames;

      private static final String CHARACTER_ENCODING= "UTF-8";
      private static final int NO_PRECEDENCE= Integer.MAX_VALUE;
      private static final RobotRuleSet EMPTY_RULES= new RobotRuleSet();

      /**
       * This class holds the rules which were parsed from a robots.txt
       * file, and can test paths against those rules.
       */
      public static class RobotRuleSet {
          ArrayList<RobotsEntry> tmpEntries;
          RobotsEntry[] entries;
          long expireTime;

          /**
           */
          private class RobotsEntry {
              String prefix;
              boolean allowed;

              RobotsEntry(String prefix, boolean allowed) {
                  this.prefix= prefix;
                  this.allowed= allowed;
              }
          }

          /**
           * should not be instantiated from outside RobotRulesParser
           */
          private RobotRuleSet() {
              tmpEntries= new ArrayList<RobotsEntry>();
              entries= null;
          }

          /**
           */
          private void addPrefix(String prefix, boolean allow) {
              if (tmpEntries == null) {
                  tmpEntries = new ArrayList<RobotsEntry>();
                  if (entries != null) {
                      for (int i= 0; i < entries.length; i++) 
                          tmpEntries.add(entries[i]);
                  }
                  entries= null;
              }

              tmpEntries.add(new RobotsEntry(prefix, allow));
          }

          /**
           */
          private void clearPrefixes() {
              if (tmpEntries == null) {
                  tmpEntries= new ArrayList<RobotsEntry>();
                  entries= null;
              } else {
                  tmpEntries.clear();
              }
          }

          /**
           * Change when the ruleset goes stale.
           */
          public void setExpireTime(long expireTime) {
              this.expireTime = expireTime;
          }

          /**
           * Get expire sleepInMs
           */
          public long getExpireTime() {
              return expireTime;
          }
          
          /**
           * Returns <code>true</code> if rules are expired at the moment of
           * invoking this function.<br>
           * Rules are expired if its {@link RobotRuleSet#getExpireTime()} value 
           * less then current system time.
           * 
           * @return <code>true</code> if rules are expired now;
           *         <code>false</code> otherwise.
           */
          public boolean isExpiredNow()
          {
            return (System.currentTimeMillis() > expireTime);            
          }

          /** 
           *  Returns <code>false</code> if the <code>robots.txt</code> file
           *  prohibits us from accessing the given <code>path</code>, or
           *  <code>true</code> otherwise.
           */ 
          public boolean isAllowed(String path) {
              try {
                  path= URLDecoder.decode(path, CHARACTER_ENCODING);
              } catch (Exception e) {
                  // just ignore it- we can still try to match 
                  // path prefixes
              }

              if (entries == null) {
                  entries= new RobotsEntry[tmpEntries.size()];
                  entries= (RobotsEntry[]) 
                      tmpEntries.toArray(entries);
                  tmpEntries= null;
              }

              int pos= 0;
              int end= entries.length;
              while (pos < end) {
                  if (path.startsWith(entries[pos].prefix))
                      return entries[pos].allowed;
                  pos++;
              }

              return true;
          }

          /**
           */
          public String toString() {
              isAllowed("x");  // force String[] representation
              StringBuffer buf= new StringBuffer();
              for (int i= 0; i < entries.length; i++) 
                  if (entries[i].allowed)
                      buf.append("Allow: " + entries[i].prefix
                                 + System.getProperty("line.separator"));
                  else 
                      buf.append("Disallow: " + entries[i].prefix
                                 + System.getProperty("line.separator"));
              return buf.toString();
          }
      }

      /**
       *  Creates a new <code>RobotRulesParser</code> which will use the
       *  supplied <code>robotNames</code> when choosing which stanza to
       *  follow in <code>robots.txt</code> files.  Any name in the array
       *  may be matched.  The order of the <code>robotNames</code>
       *  determines the precedence- if many names are matched, only the
       *  rules associated with the robot name having the smallest index
       *  will be used.
       */
      public RobotRulesParser(String[] robotNames) {
          this.robotNames= new HashMap<String,Integer>();
          for (int i= 0; i < robotNames.length; i++) {
              this.robotNames.put(robotNames[i].toLowerCase(), new Integer(i));
          }
          // always make sure "*" is included
          if (!this.robotNames.containsKey("*"))
              this.robotNames.put("*", new Integer(robotNames.length));
      }

      /**
       * Returns a {@link RobotRuleSet} object which encapsulates the
       * rules parsed from the supplied <code>robotContent</code>.
       */
      public RobotRuleSet parseRules(byte[] robotContent) {
          if (robotContent == null) 
              return EMPTY_RULES;

          String content= new String (robotContent);

          StringTokenizer lineParser= new StringTokenizer(content, "\n\r");

          RobotRuleSet bestRulesSoFar= null;
          int bestPrecedenceSoFar= NO_PRECEDENCE;

          RobotRuleSet currentRules= new RobotRuleSet();
          int currentPrecedence= NO_PRECEDENCE;

          boolean addRules= false;    // in stanza for our robot
          boolean doneAgents= false;  // detect multiple agent lines

          while (lineParser.hasMoreTokens()) {
              String line= lineParser.nextToken();

              // trim out comments and whitespace
              int hashPos= line.indexOf("#");
              if (hashPos >= 0) 
                  line= line.substring(0, hashPos);
              line= line.trim();

              if ( (line.length() >= 11) 
                   && (line.substring(0, 11).equalsIgnoreCase("User-agent:")) ) {

                  if (doneAgents) {
                      if (currentPrecedence < bestPrecedenceSoFar) {
                          bestPrecedenceSoFar= currentPrecedence;
                          bestRulesSoFar= currentRules;
                          currentPrecedence= NO_PRECEDENCE;
                          currentRules= new RobotRuleSet();
                      }
                      addRules= false;
                  }
                  doneAgents= false;

                  String agentNames= line.substring(line.indexOf(":") + 1);
                  agentNames= agentNames.trim();
                  StringTokenizer agentTokenizer= new StringTokenizer(agentNames);

                  while (agentTokenizer.hasMoreTokens()) {
                      // for each agent listed, see if it's us:
                      String agentName= agentTokenizer.nextToken().toLowerCase();

                      Integer precedenceInt= (Integer) robotNames.get(agentName);

                      if (precedenceInt != null) {
                          int precedence= precedenceInt.intValue();
                          if ( (precedence < currentPrecedence)
                               && (precedence < bestPrecedenceSoFar) )
                              currentPrecedence= precedence;
                      }
                  }

                  if (currentPrecedence < bestPrecedenceSoFar) 
                      addRules= true;

              } else if ( (line.length() >= 9)
                          && (line.substring(0, 9).equalsIgnoreCase("Disallow:")) ) {

                  doneAgents= true;
                  String path= line.substring(line.indexOf(":") + 1);
                  path= path.trim();
                  try {
                      path= URLDecoder.decode(path, CHARACTER_ENCODING);
                  } catch (Exception e) {
                      //logger.warning("error parsing robots rules- can't decode path: "   + path);
                  }

                  if (path.length() == 0) { // "empty rule"
                      if (addRules)
                          currentRules.clearPrefixes();
                  } else {  // rule with path
                      if (addRules)
                          currentRules.addPrefix(path, false);
                  }

              } else if ( (line.length() >= 6)
                          && (line.substring(0, 6).equalsIgnoreCase("Allow:")) ) {

                  doneAgents= true;
                  String path= line.substring(line.indexOf(":") + 1);
                  path= path.trim();

                  if (path.length() == 0) { 
                      // "empty rule"- treat same as empty disallow
                      if (addRules)
                          currentRules.clearPrefixes();
                  } else {  // rule with path
                      if (addRules)
                          currentRules.addPrefix(path, true);
                  }
              }
          }

          if (currentPrecedence < bestPrecedenceSoFar) {
              bestPrecedenceSoFar= currentPrecedence;
              bestRulesSoFar= currentRules;
          }

          if (bestPrecedenceSoFar == NO_PRECEDENCE) 
              return EMPTY_RULES;
          return bestRulesSoFar;
      }

      /**
       *  Returns a <code>RobotRuleSet</code> object appropriate for use
       *  when the <code>robots.txt</code> file is empty or missing; all
       *  requests are allowed.
       */
      public static RobotRuleSet getEmptyRules() {
          return EMPTY_RULES;
      }

      /**
       *  Returns a <code>RobotRuleSet</code> object appropriate for use
       *  when the <code>robots.txt</code> file is not fetched due to a
       *  <code>403/Forbidden</code> response; all requests are
       *  disallowed.
       */
      static RobotRuleSet getForbidAllRules() {
          RobotRuleSet rules= new RobotRuleSet();
          rules.addPrefix("", false);
          return rules;
      }

      private final static int BUFSIZE= 2048;

      /** command-line main for testing */
      public static void main(String[] argv) {
          if (argv.length != 3) {
              System.out.println("Usage:");
              System.out.println("   java <robots-file> <url-file> <agent-name>+");
              System.out.println("");
              System.out.println("The <robots-file> will be parsed as a robots.txt file,");
              System.out.println("using the given <agent-name> to select rules.  URLs ");
              System.out.println("will be read (one per line) from <url-file>, and tested");
              System.out.println("against the rules.");
              System.exit(-1);
          }
          try { 
              FileInputStream robotsIn= new FileInputStream(argv[0]);
              LineNumberReader testsIn= new LineNumberReader(new FileReader(argv[1]));
              String[] robotNames= new String[argv.length - 1];

              for (int i= 0; i < argv.length - 2; i++) 
                  robotNames[i]= argv[i+2];

              ArrayList<byte[]> bufs= new ArrayList<byte[]>();
              byte[] buf= new byte[BUFSIZE];
              int totBytes= 0;

              int rsize= robotsIn.read(buf);
              while (rsize >= 0) {
                  totBytes+= rsize;
                  if (rsize != BUFSIZE) {
                      byte[] tmp= new byte[rsize];
                      System.arraycopy(buf, 0, tmp, 0, rsize);
                      bufs.add(tmp);
                  } else {
                      bufs.add(buf);
                      buf= new byte[BUFSIZE];
                  }
                  rsize= robotsIn.read(buf);
              }

              byte[] robotsBytes= new byte[totBytes];
              int pos= 0;

              for (int i= 0; i < bufs.size(); i++) {
                  byte[] currBuf= (byte[]) bufs.get(i);
                  int currBufLen= currBuf.length;
                  System.arraycopy(currBuf, 0, robotsBytes, pos, currBufLen);
                  pos+= currBufLen;
              }

              RobotRulesParser parser= 
                  new RobotRulesParser(robotNames);
              RobotRuleSet rules= parser.parseRules(robotsBytes);
              System.out.println("Rules:");
              System.out.println(rules);
              System.out.println();

              String testPath= testsIn.readLine().trim();
              while (testPath != null) {
                  System.out.println( (rules.isAllowed(testPath) ? 
                                       "allowed" : "not allowed")
                                      + ":\t" + testPath);
                  testPath= testsIn.readLine();
              }

          } catch (Exception e) {
              e.printStackTrace();
          }
      }

  }


}
