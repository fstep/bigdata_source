/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.manager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.crawler.CrawlerInit;
import com.porva.crawler.Crawler.CrawlerStatus;
import com.porva.crawler.db.CrawlerDBFactory;
import com.porva.crawler.db.DB;
import com.porva.crawler.db.DBException;
import com.porva.crawler.db.DBOperationStatus;
import com.porva.crawler.net.Response;
import com.porva.crawler.service.AbstractService;
import com.porva.crawler.service.ServiceException;
import com.porva.crawler.task.FetchRSSTask;
import com.porva.crawler.task.FetchResult;
import com.porva.crawler.task.FetchTask;
import com.porva.crawler.task.FetchURL;
import com.porva.crawler.task.InitialTaskReader;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskFormatException;
import com.porva.crawler.task.TaskResult;

/**
 * Manager of the crawle based on databases.
 * 
 * @author Poroshin V.
 * @date Jan 22, 2006
 */
public class DBManager extends AbstractService implements Manager
{
  private InitialTaskReader taskReader;

  private CrawlerDBFactory dbFactory;

  private DB<TaskResult> db;

  private static Log logger = LogFactory.getLog(DBManager.class);

  private static Map<Class, Long> lifetime = new HashMap<Class, Long>();

  /**
   * Constructs a new {@link DBManager}.
   * 
   * @param taskReader reader of initial tasks.
   * @param dbFactory factory of databases.
   * @throws DBException if some database error occured.
   * @throws NullArgumentException if <code>taskReader</code> or <code>dbFactory</code> is
   *           <code>null</code>.
   */
  public DBManager(final InitialTaskReader taskReader, final CrawlerDBFactory dbFactory)
      throws DBException
  {
    super("commonservice.manager");

    this.taskReader = taskReader;
    this.dbFactory = dbFactory;
    if (this.taskReader == null)
      throw new NullArgumentException("taskReader");
    if (this.dbFactory == null)
      throw new NullArgumentException("dbFactory");

    db = dbFactory.newResultDatabaseHandle();
    db.open();
    if (logger.isDebugEnabled())
      logger.debug("Database handle opened in DBManager: " + db);

    initLifetimes();
  }

  private void initLifetimes()
  {
    long time = Long.parseLong(CrawlerInit.getProperties().getProperty("task.FetchTask.lifetime"));
    lifetime.put(FetchURL.class, time);

    time = Long.parseLong(CrawlerInit.getProperties().getProperty("task.FetchRssTask.lifetime"));
    lifetime.put(FetchRSSTask.class, time);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.manager.Manager#getNextInitialTask()
   */
  public Task getNextInitialTask() throws IOException, TaskFormatException
  {
    Task task = null;
    if (!taskReader.isClosed()) {
      task = taskReader.getNextTask();
      if (task == null)
        taskReader.close();
    }
    return task;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.manager.Manager#completeTask(com.porva.crawler.task.Task,
   *      com.porva.crawler.task.TaskResult)
   */
  public void completeTask(final Task task, final TaskResult result)
  {
    if (task == null)
      throw new NullArgumentException("task");
    if (result == null)
      throw new NullArgumentException("result");

    // todo: check if result is valid to store according to TaskResultFilters
    // todo: check if this task is already present in db and if it is check if
    // it is a new version
    if (task instanceof FetchTask) {
      List<TaskResult> dups = new ArrayList<TaskResult>();
      FetchResult curResult = (FetchResult) result;
      db.getAllDup(task.getDBKey(), dups);
      for (TaskResult prevTaskResult : dups) {
        FetchResult prevResult = (FetchResult) prevTaskResult;

        if (prevResult != null) {
          if (prevResult.getResponse() == null)
            if (db.deleteByEquals(task.getDBKey(), prevResult) != DBOperationStatus.SUCCESS)
              logger.warn("Failed to delete recored");

          if (prevResult.getResponse() != null && curResult.getResponse() != null) {
            if (prevResult.getResponse().getBodyChecksum() == curResult.getResponse()
                .getBodyChecksum()) {
              if (logger.isInfoEnabled())
                logger.info("ResultDB already has this result: " + result);
              if (db.deleteByEquals(task.getDBKey(), prevResult) != DBOperationStatus.SUCCESS)
                logger.warn("Failed to delete recored");
              else
                break;
            }
          }
        }
      }

      db.put(curResult);
    }

  }

  public void crawlerStatusChanged(CrawlerStatus newStatus)
  {
  }

  public Map<String, String> getStat()
  {
    Map<String, String> stat = new HashMap<String, String>();
    return stat;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.service.Service#stop()
   */
  public void stop() throws ServiceException
  {
    try {
      db.close();
    } catch (DBException e) {
      throw new ServiceException("Manager failed to close its database handle: " + db, e);
    }
    if (logger.isDebugEnabled())
      logger.debug("Database handle closed in manager: " + db);
    super.stop();
  }

  public boolean isOldTask(final Task task)
  {
    if (task == null)
      throw new NullArgumentException("task");

    Long time = lifetime.get(task.getClass());
    if (time == null)
      return false;

    if (task instanceof FetchTask) {
      List<TaskResult> dups = new ArrayList<TaskResult>();
      db.getAllDup(task.getDBKey(), dups);
      if (dups.size() == 0)
        return false;

      long lastDisconnectedTime = 0;
      for (TaskResult dup : dups) {
        if (dup instanceof FetchResult) {
          Response response = ((FetchResult) dup).getResponse();
          if (response != null && response.getTimeWhenDisconnected() > lastDisconnectedTime)
            lastDisconnectedTime = response.getTimeWhenDisconnected();
        }
      }

      if (lastDisconnectedTime + time < System.currentTimeMillis()) {
        return true;
      }
    }

    return false;
  }

}
