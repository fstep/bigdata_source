/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler.manager;

import java.io.IOException;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.Stat;
import com.porva.crawler.Crawler.CrawlerStatus;
import com.porva.crawler.service.CommonService;
import com.porva.crawler.service.ServiceException;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskFormatException;
import com.porva.crawler.task.TaskResult;

/**
 * Interface of crawler manager.
 * 
 * @author Poroshin V.
 * @date Sep 14, 2005
 */
public interface Manager extends Stat, CommonService
{

  /**
   * Returns next initial {@link Task} task or <code>null</code> if no more initial tasks
   * available.
   * 
   * @return next initial {@link Task} task or <code>null</code> if no more initial tasks
   *         available.
   * @throws TaskFormatException 
   * @throws IOException 
   */
  public Task getNextInitialTask() throws IOException, TaskFormatException;

  /**
   * Completes the task.<br>
   * This method should be called only from {@link com.porva.crawler.Crawler} object to complete
   * task after its processing.
   * 
   * @param task {@link Task} task to complete.
   * @param result task result.
   * @throws NullArgumentException if <code>task</code> is <code>null</code>.
   * @throws NullArgumentException if <code>result</code> is <code>null</code>.
   */
  public void completeTask(final Task task, final TaskResult result);

  /**
   * Closes the manager.
   */
  public void stop() throws ServiceException;

  /**
   * Returns <code>true</code> if namager is closed; <code>false</code> otherwise.
   * 
   * @return <code>true</code> if namager is closed; <code>false</code> otherwise.
   */
  public boolean isStopped();

  /**
   * This method should be called only by {@link com.porva.crawler.Crawler} object to report to
   * manager that its status has changed.
   * 
   * @param newStatus new status {@link CrawlerStatus} of {@link com.porva.crawler.Crawler} object.
   */
  public void crawlerStatusChanged(final CrawlerStatus newStatus);
  
  
  public boolean isOldTask(final Task task);

}
