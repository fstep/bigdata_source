/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.task;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.db.*;
import com.porva.crawler.net.Response;
import com.sleepycat.util.IOExceptionWrapper;

/**
 * {@link InitialTaskReader} implementation that reads tasks results from database and returns
 * update tasks.
 * 
 * @author Poroshin V.
 * @date Oct 10, 2005
 */
public class DBUpdaterTaskReader implements InitialTaskReader
{
  private CrawlerDBFactory dbFactory;

  private DB<TaskResult> resultDB;

  private DBCursor<TaskResult> cursor;

  private boolean isClosed = false;

  private Map<Class, Long> typeLifetime;

  /**
   * Creates a new {@link DBUpdaterTaskReader} object with specified database factory and map of
   * lifetimes.
   * 
   * @param dbFactory not closed factory to get result database handle.
   * @param typeLifetime map of lifetime values for tasks types; can be <code>null</code> that
   *          means that all default lifetime values will be used.
   * @throws DBException if a database failure occures.
   * @throws NullArgumentException if <code>dbFactory</code> is <code>null</code>.
   * @throws IllegalStateException if <code>dbFactory</code> is closed.
   */
  public DBUpdaterTaskReader(final CrawlerDBFactory dbFactory, final Map<Class, Long> typeLifetime)
      throws DBException
  {
    this.dbFactory = dbFactory;
    this.typeLifetime = typeLifetime;

    if (this.dbFactory == null)
      throw new NullArgumentException("dbFactory");
    if (this.dbFactory.isClosed())
      throw new IllegalStateException("dbFactory is closed");

    resultDB = this.dbFactory.newResultDatabaseHandle();
    resultDB.open();
    cursor = resultDB.getCursor();
    isClosed = false;
  }

  /**
   * Returns next update task or <code>null</code> if no more update tasks available.
   */
  public Task getNextTask() throws IOException, TaskFormatException
  {
    Task nextTask = null;
    DBRecord<TaskResult> record = null;
    try {
      while ((record = cursor.getNext()) != null) {
        record.getDBKey();
        TaskResult taskResult = record.getDBValue();
        if (taskResult instanceof DefaultFetchResult)
          nextTask = updateTask((DefaultFetchResult) taskResult);
        if (nextTask != null)
          return nextTask;
      }
    } catch (DBException e) {
      throw new IOExceptionWrapper(e);
    }
    return null;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.task.InitialTaskReader#close()
   */
  public void close() throws IOException
  {
    isClosed = true;
    try {
      cursor.close();
    } catch (DBException e) {
      throw new IOExceptionWrapper(e);
    } finally {
      try {
        resultDB.close();
      } catch (DBException e) {
        throw new IOExceptionWrapper(e);
      }
    }

  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.task.InitialTaskReader#isClosed()
   */
  public boolean isClosed()
  {
    return isClosed;
  }

  // ////////////////////////////////////////////////////////////// private

  private Task updateTask(final DefaultFetchResult result)
  {
    assert result != null;

    Task<FetchResult> task = result.getTask();
    Response response = result.getResponse();
    if (response != null) {
      long lifetime = getLifetime(task);
      if (lifetime != Long.MAX_VALUE) {
        long timeWhenExpired = response.getTimeWhenDisconnected() + lifetime;
        if (timeWhenExpired < System.currentTimeMillis()) {
          return result.getTask(); // todo:
        }
      }
    }

    return null;
  }

  private long getLifetime(final Task<?> task)
  {
    assert task != null;

    if (typeLifetime != null) {
      Long lifetime = typeLifetime.get(task.getClass());
      if (lifetime != null)
        return lifetime;
    }

    // return task.getTaskType().getTaskLifetime();
    return Long.MAX_VALUE; // FIXME
  }

}
