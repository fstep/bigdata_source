/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
//package com.porva.crawler.task;
//
//import org.apache.commons.lang.NullArgumentException;
//
//import com.porva.crawler.db.DBException;
//import com.porva.crawler.db.DBInputStream;
//import com.porva.crawler.db.DBOutputStream;
//import com.porva.crawler.db.DBValue;
//import com.porva.crawler.db.DBValueFactory;
//import com.porva.crawler.net.CrawlerURL;
//import com.porva.crawler.service.ServiceProvider;
//import org.apache.commons.lang.builder.ToStringStyle;
//import org.apache.commons.lang.builder.ToStringBuilder;
//import org.apache.commons.lang.builder.EqualsBuilder;
//
///**
// * {@link FetchTask} implementation that wraps another FetchTask as redirect. 
// * 
// * @author Poroshin V.
// * @date Oct 13, 2005
// */
//public class RedirectTaskWrapper implements FetchTask
//{
//  private FetchTask wrappedTask;
//
//  private int redirNum = 0;
//
//  // ////////////////////////////////////////// constructors
//
//  protected RedirectTaskWrapper()
//  {
//  }
//
//  /**
//   * Creates a new RedirectTaskWrapper by wrapping given <code>taskToWrap</code>.<br>
//   * If <code>taskToWrap</code> is instance of {@link RedirectTaskWrapper} then number of
//   * redirectes will be increased by one in this object and wrapped task of <code>taskToWrap</code>
//   * will be wrapped insted of wrapping <code>taskToWrap</code> itself.
//   * 
//   * @param taskToWrap task to wrap.
//   * @throws NullArgumentException if <code>taskToWrap</code> is <code>null</code>.
//   */
//  public RedirectTaskWrapper(final FetchTask taskToWrap)
//  {
//    if (taskToWrap == null)
//      throw new NullArgumentException("taskToWrap");
//
//    if (taskToWrap instanceof RedirectTaskWrapper) {
//      RedirectTaskWrapper redirTaskToWrap = (RedirectTaskWrapper) taskToWrap;
//      this.wrappedTask = redirTaskToWrap.getWrappedTask();
//      this.redirNum = redirTaskToWrap.getRedirNum() + 1;
//    } else {
//      this.wrappedTask = taskToWrap;
//      this.redirNum = 0;
//    }
//  }
//
//  private RedirectTaskWrapper(FetchTask taskToWrap, int redirNum)
//  {
//    this.wrappedTask = taskToWrap;
//    this.redirNum = redirNum;
//
//    if (this.wrappedTask == null)
//      throw new NullArgumentException("taskToWrap");
//    if (this.wrappedTask instanceof RedirectTaskWrapper)
//      throw new IllegalArgumentException("taskToWrap cannot be instance of RedirectTaskWrapper");
//    if (this.redirNum < 0)
//      throw new IllegalArgumentException("redirNum cannot be < 0: " + this.redirNum);
//  }
//
//  // ////////////////////////////////////////// additional methods
//
//  /**
//   * Returns redirect number. 
//   * 
//   * @return redirect number.
//   */
//  public int getRedirNum()
//  {
//    return redirNum;
//  }
//
//  protected FetchTask getWrappedTask()
//  {
//    return wrappedTask;
//  }
//
//  // ////////////////////////////////////////// FetchTask impl.
//
//  public CrawlerURL getCrawlerURL()
//  {
//    return wrappedTask.getCrawlerURL();
//  }
//
//  public DefaultFetchResult perform(ServiceProvider serviceProvider)
//  {
//    return wrappedTask.perform(serviceProvider);
//  }
//
//  public String getDBKey()
//  {
//    return wrappedTask.getDBKey();
//  }
//
//  public DBValue getDBValue()
//  {
//    return this;
//  }
//
//  public void writeTo(DBOutputStream tupleOutput)
//  {
//    tupleOutput.writeInt(redirNum);
//    tupleOutput.writeLong(wrappedTask.getClassCode());
//    wrappedTask.writeTo(tupleOutput);
//  }
//
//  public DBValue newInstance(DBInputStream tupleInput) throws DBException
//  {
//    int redirNum = tupleInput.readInt();
//    FetchTask ft = (FetchTask) DBValueFactory.readObject(tupleInput);
//    return new RedirectTaskWrapper(ft, redirNum);
//  }
//
//  public long getClassCode()
//  {
//    return -238749823054L;
//  }
//
//  /*
//   * (non-Javadoc)
//   * 
//   * @see java.lang.Object#toString()
//   */
//  public String toString()
//  {
//    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("wrappedTask",
//                                                                                wrappedTask)
//        .append("redirNum", redirNum).toString();
//  }
//
//  /*
//   * (non-Javadoc)
//   * 
//   * @see java.lang.Object#equals(java.lang.Object)
//   */
//  public boolean equals(final Object other)
//  {
//    if (!(other instanceof RedirectTaskWrapper))
//      return false;
//    RedirectTaskWrapper castOther = (RedirectTaskWrapper) other;
//    return new EqualsBuilder().append(wrappedTask, castOther.wrappedTask)
//        .append(redirNum, castOther.redirNum).isEquals();
//  }
//
//}
