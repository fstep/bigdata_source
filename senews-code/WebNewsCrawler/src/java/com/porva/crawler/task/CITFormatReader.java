/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.task;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.net.MalformedCrawlerURLException;

/**
 * {@link InitialTaskReader} implementation that can read initial tasks from special text file.
 * 
 * @author Poroshin V.
 * @date Sep 22, 2005
 */
public class CITFormatReader implements InitialTaskReader
{
  private File citFile;

  private BufferedReader citFileReader;

  private static Log logger = LogFactory.getLog(CITFormatReader.class);

  private boolean isClosed = false;

  /**
   * Constructs a new CITFormatReader object with specified CIT filename.
   * 
   * @param fileInCITFormat
   * @throws NullArgumentException if <code>fileInCITFormat</code> is <code>null</code>.
   * @throws IllegalArgumentException if <code>fileInCITFormat</code> cannot be opened for
   *           reading.
   */
  public CITFormatReader(final File fileInCITFormat)
  {
    this.citFile = fileInCITFormat;
    if (this.citFile == null)
      throw new NullArgumentException("fileInCITFormat");

    try {
      citFileReader = new BufferedReader(new FileReader(this.citFile));
    } catch (FileNotFoundException e) {
      throw new IllegalArgumentException("Cannot read citFile file " + this.citFile + " : " + e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.task.InitialTaskReader#getNextTask()
   */
  public Task getNextTask() throws IOException, TaskFormatException
  {
    Task task;

    Map<String, String> taskMap = readTask(citFileReader);
    if (taskMap == null)
      return null;

    String taskName = taskMap.get("task");
    if (taskName == null) {
      logger.warn("Failed to read initial task: task name is not specified.");
      throw new TaskFormatException("Failed to read initial task: task name is not specified.");
    }
    if (taskName.equals("FETCH_URL")) {
      task = readFetchURLTask(taskMap);
    } else if (taskName.equals("FETCH_RSS")) {
      task = readFetchRSSTask(taskMap);
    } else if (taskName.equals("GOOGLE_WEB_SEARCH")) {
      task = readGoogleWebSearchTask(taskMap);
    } else {
      logger.warn("Unknow task to load: " + taskName);
      throw new TaskFormatException("Unknow task to load: " + taskName);
    }
    return task;
  }
  
  /**
   * task=GOOGLE_WEB_SEARCH
   * query=
   * depth=
   * num=
   */
  private Task readGoogleWebSearchTask(Map<String, String> taskMap) throws TaskFormatException
  {
    assert taskMap != null;
    
    String query = readRequiredStringParam("query", taskMap);
    int depth = readRequiredIntParam("depth", taskMap);
    int num = readRequiredIntParam("num", taskMap);
    return TaskFactory.newGoogleTask(num, query, depth);
  }

  /**
   * Creates a new FetchURLTask.
   */
  private Task readFetchURLTask(Map<String, String> taskMap) throws TaskFormatException
  {
    assert taskMap != null;

    String uri = readRequiredStringParam("url", taskMap);
    int depth = readRequiredIntParam("depth", taskMap);

    try {
      return TaskFactory.newFetchURL(new CrawlerURL(depth, uri));
    } catch (MalformedCrawlerURLException e) {
      // todo: logger.warn("Ca");
      throw new TaskFormatException(e);
    }
  }

  private Task readFetchRSSTask(Map<String, String> taskMap) throws TaskFormatException
  {
    assert taskMap != null;

    String uri = readRequiredStringParam("url", taskMap);
    int depth = readRequiredIntParam("depth", taskMap);

    try {
      return TaskFactory.newFetchRSS(new CrawlerURL(depth, uri));
    } catch (MalformedCrawlerURLException e) {
      // todo: logger.warn("Ca");
      throw new TaskFormatException(e);
    }
  }

  private String readRequiredStringParam(String name, Map<String, String> taskMap)
      throws TaskFormatException
  {
    assert name != null;
    assert taskMap != null;

    if (taskMap.containsKey(name))
      return taskMap.get(name);
    throw new TaskFormatException("Invalid task format: required param '" + name + "' is missing.");
  }

  private Integer readRequiredIntParam(String name, Map<String, String> taskMap)
      throws TaskFormatException
  {
    assert name != null;
    assert taskMap != null;

    try {
      if (taskMap.containsKey(name)) {
        return Integer.parseInt(taskMap.get(name));
      } else {
        throw new TaskFormatException("Invalid task format: required param '" + name
            + "' is missing.");
      }
    } catch (NumberFormatException e) {
      throw new TaskFormatException("Invalid task format", e);
    }
  }

  private Integer readIntParam(String name, Map<String, String> taskMap) throws TaskFormatException
  {
    assert name != null;
    assert taskMap != null;

    try {
      if (taskMap.containsKey(name)) {
        return Integer.parseInt(taskMap.get(name));
      } else {
        return null;
      }
    } catch (NumberFormatException e) {
      throw new TaskFormatException("Invalid task format", e);
    }
  }

  private Map<String, String> readTask(BufferedReader citFileReader) throws IOException
  {
    String line;
    Map<String, String> map = new HashMap<String, String>();

    while (true) {
      if ((line = citFileReader.readLine()) == null)
        return null; // denotes and of stream
      if (isCommentedLine(line) || isEmptyLine(line))
        continue;
      if (line.startsWith("task_end"))
        break;

      Pattern p = Pattern.compile("([^=]+)=(.+)");
      Matcher matcher = p.matcher(line);
      if (matcher.matches()) {
        String key = matcher.group(1);
        String val = matcher.group(2);
        map.put(key, val);
      }
    }
    return map;
  }

  private boolean isCommentedLine(String line)
  {
    assert line != null;

    if (line.matches("^\\s*#.*"))
      return true;
    return false;
  }

  private boolean isEmptyLine(String line)
  {
    assert line != null;

    if (line.matches("^\\s*$"))
      return true;
    return false;
  }

  public void close() throws IOException
  {
    citFileReader.close();
    isClosed = true;
  }

  public boolean isClosed()
  {
    return isClosed;
  }

}
