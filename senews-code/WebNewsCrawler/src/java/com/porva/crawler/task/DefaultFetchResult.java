/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.task;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.porva.crawler.db.*;
import com.porva.crawler.net.DefaultResponseImpl;
import com.porva.crawler.net.Response;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Default result of fetch task.<br>
 * Complete fetch result is always has not <code>null</code> Response and failed fetch result is
 * always with <code>null</code> Response.<br>
 * Complete fetch result always have COMPLETE status and failed result always don't have COMPLETE
 * status.
 * 
 * @author Poroshin V.
 * @date Oct 7, 2005
 */
public class DefaultFetchResult implements FetchResult
{

  private Status status;

  private FetchTask fetchTask;

  private Response response;

  // ////////////////////////////////////////////////////// creation methods

  private DefaultFetchResult()
  {
  }

  /**
   * Creates a new {@link DefaultFetchResult}.
   * 
   * @param fetchTask creation task.
   * @param status status of result.
   * @param response resulted response.
   * @throws NullArgumentException if <code>fetchTask</code> or <code>status</code> is
   *           <code>null</code>.
   * @throws NullArgumentException if complete task has null response.
   */
  public DefaultFetchResult(final FetchTask fetchTask, final Status status, final Response response)
  {
    this.fetchTask = fetchTask;
    this.status = status;
    this.response = response;

    if (fetchTask == null)
      throw new NullArgumentException("fetchTask");
    if (status == null)
      throw new NullArgumentException("status");
    if (response == null && status == Status.COMPLETE)
      throw new NullArgumentException("Complete task cannot have null response.");
  }

  /**
   * Creates a new complete {@link DefaultFetchResult} instance.
   * 
   * @param fetchTask creation task.
   * @param response resulted response.
   * @return new {@link DefaultFetchResult} instance.
   * @throws NullArgumentException if <code>fetchTask</code> or <code>status</code> is
   *           <code>null</code>.
   * @throws IllegalArgumentException if <code>response</code> is <code>null</code>.
   */
  public static FetchResult newCompleteResult(final FetchTask fetchTask, final Response response)
  {
    return new DefaultFetchResult(fetchTask, Status.COMPLETE, response);
  }

  /**
   * Creates a new failed {@link DefaultFetchResult} instance.
   * 
   * @param fetchTask creation task.
   * @param status failed status.
   * @return new {@link DefaultFetchResult} instance.
   * @throws NullArgumentException if <code>fetchTask</code> or <code>status</code> is
   *           <code>null</code>.
   * @throws IllegalArgumentException if <code>status</code> is COMPLETE.
   */
  public static FetchResult newFailedResult(final FetchTask fetchTask, final Status status)
  {
    return new DefaultFetchResult(fetchTask, status, null);
  }

  // /////////////////////////////////////////////////// get methods

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.task.TaskResult#getTask()
   */
  public FetchTask getTask()
  {
    return fetchTask;
  }

  /**
   * Returns status of fetch result.
   * 
   * @return status of fetch result.
   */
  public Status getStatus()
  {
    return status;
  }

  /**
   * Returns response of the fetch reasult or <code>null</code> if result is failed.
   * 
   * @return response of the fetch reasult or <code>null</code> if result is failed.
   */
  public Response getResponse()
  {
    return response;
  }

  // //////////////////////////////////////////////////// DBValue impl.

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DBValue#writeTo(com.porva.crawler.db.DBOutputStream)
   */
  public void writeTo(final DBOutputStream tupleOutput)
  {
    tupleOutput.writeLong(fetchTask.getClassCode());
    fetchTask.writeTo(tupleOutput);
    tupleOutput.writeByte(status.asByte());
    tupleOutput.writeBoolean(response != null); // isNotNullResponse flag
    if (response != null)
      response.writeTo(tupleOutput);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DBValue#newInstance(com.porva.crawler.db.DBInputStream)
   */
  public DBValue newInstance(final DBInputStream tupleInput) throws DBException
  {
    try {
      FetchTask ft = (FetchTask) CrawlerDBValueFactory.instance.readObject(tupleInput);

      Status status = Status.getInstance(tupleInput.readByte());
      Response rp = null;
      boolean isNotNullResponse = tupleInput.readBoolean();
      if (isNotNullResponse)
        rp = (Response) CrawlerDBValueFactory.instance
            .newDBValueInstance(tupleInput, DefaultResponseImpl.class);
      return new DefaultFetchResult(ft, status, rp);
    } catch (Exception e) {
      throw new DBException(e);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DBValue#getClassCode()
   */
  public long getClassCode()
  {
    return 52983437717093L;
  }

  // //////////////////////////////////////////////////////// DBRecord impl.

  /**
   * Returns database key as fetched url string.
   */
  public String getDBKey()
  {
    return fetchTask.getCrawlerURL().getURLString();
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DBRecord#getDBValue()
   */
  public DBValue getDBValue()
  {
    return this;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).appendSuper(super.toString())
        .append("status", status).append("fetchTask", fetchTask).append("response", response)
        .toString();
  }

  public boolean equals(final Object other)
  {
    if (!(other instanceof DefaultFetchResult))
      return false;
    DefaultFetchResult castOther = (DefaultFetchResult) other;
    boolean isEquals = false;
    if (response == null) {
      if (castOther.response == null) {
        isEquals = new EqualsBuilder().append(status, castOther.status).append(fetchTask,
                                                                               castOther.fetchTask)
            .isEquals();
      }
    } else {
      if (castOther.response != null) {
        isEquals = new EqualsBuilder().append(status, castOther.status).append(fetchTask,
                                                                               castOther.fetchTask)
            .append(response.getBodyChecksum(), castOther.response.getBodyChecksum())
            .append(response.getCode(), castOther.response.getCode()).isEquals();
      }
    }
    return isEquals;
  }

  public int hashCode()
  {
    return new HashCodeBuilder().append(status).append(fetchTask).append(response).toHashCode();
  }

}
