/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.task;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.NullArgumentException;

/**
 * Writes tasks to special formatted file.
 * 
 * @author Poroshin V.
 * @date Jan 22, 2006
 */
public class CITFormatWriter
{
  private CITFormatWriter() {}
  
  public static void writeTasks(List<Task> tasks, String file) throws IOException
  {
    if (tasks == null)
      throw new NullArgumentException("tasks");
    if (file == null)
      throw new NullArgumentException("file");
    
    FileWriter fout = new FileWriter(file);
    
    for (Task task : tasks) {
      if (task instanceof FetchURL) {
        FetchURL fetchURL = (FetchURL)task;
        fout.write("task=FETCH_URL\n");
        fout.write("url=" + fetchURL.getCrawlerURL().getURLString() + "\n");
        fout.write("depth=" + fetchURL.getCrawlerURL().getDepth() + "\n");
        fout.write("task_end\n\n");
      }
      /**
       * task=GOOGLE_WEB_SEARCH
       * query=
       * depth=
       * num=
       */
      else if (task instanceof GoogleTask) {
        GoogleTask googleTask = (GoogleTask)task;
        fout.write("task=GOOGLE_WEB_SEARCH\n");
        fout.write("query=" + googleTask.getQuery() + "\n");
        fout.write("depth=" + googleTask.getDepth() + "\n");
        fout.write("num=" + googleTask.getNum() + "\n");
        fout.write("task_end\n\n");
      }
      /**
       * task=FETCH_RSS
       * url=http://www.researchbuzz.org/rss_search_engines.xml
       * depth=0
       * task_end 
       */
      else if (task instanceof FetchRSSTask) {
        FetchRSSTask fetchRssTask = (FetchRSSTask)task;
        fout.write("task=FETCH_RSS\n");
        fout.write("url=" + fetchRssTask.getCrawlerURL().getURLString() + "\n");
        fout.write("depth=" + fetchRssTask.getCrawlerURL().getDepth() + "\n");
        fout.write("task_end\n");
      }
      
      else {
        throw new NotImplementedException("storing of this task is not implemented: " + task);
      }
    }
    fout.close();
  }
}
