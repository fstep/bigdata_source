/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.task;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.crawler.db.DBValue;
import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.net.HttpClient;
import com.porva.crawler.net.HttpClientException;
import com.porva.crawler.net.MalformedCrawlerURLException;
import com.porva.crawler.net.Response;
import com.porva.crawler.service.FilterService;
import com.porva.crawler.service.LinksExtractorService;
import com.porva.crawler.service.NoSuchServiceException;
import com.porva.crawler.service.ServiceProvider;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * This class provides default implementation of {@link FetchTask} interface.
 * 
 * @author Poroshin V.
 * @param <T> type of FetchResult.
 * @date Sep 18, 2005
 */
public abstract class DefaultFetchTask implements FetchTask
{
  private CrawlerURL crawlerURL;

  private static Log logger = LogFactory.getLog(DefaultFetchTask.class);

  static final CrawlerURL defCrawlerURL;
  static {
    try {
      defCrawlerURL = new CrawlerURL(1, "http://unknown");
    } catch (MalformedCrawlerURLException e) {
      logger.fatal("Cannot initialize FetchURL", e);
      throw new UnhandledException("Cannot initialize FetchURL", e);
    }
  }

  // ////////////////////////////////////////////////// constructors

  /**
   * Creates a new {@link DefaultFetchTask} object.<br>
   * 
   * @param taskType type of task.
   * @param crawlerURL cralwer url to fetch.
   * @throws NullArgumentException if <code>taskType</code> or <code>crawlerURL</code> is
   *           <code>null</code>.
   */
  protected DefaultFetchTask(final CrawlerURL crawlerURL)
  {
    this.crawlerURL = crawlerURL;
    if (this.crawlerURL == null)
      throw new NullArgumentException("crawlerURL");
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.task.FetchTask#getCrawlerURL()
   */
  public CrawlerURL getCrawlerURL()
  {
    return crawlerURL;
  }

  // /////////////////////////////////////////////////// useful protected methods

  protected Response fetchURL(final ServiceProvider serviceProvider) throws HttpClientException,
      NoSuchServiceException
  {
    if (serviceProvider == null)
      throw new NullArgumentException("serviceProvider");

    HttpClient httpClient = (HttpClient) serviceProvider.getService("workerservice.httpClient");
    return httpClient.get(getCrawlerURL());
  }

  protected List<CrawlerURL> extractLinks(final ServiceProvider serviceProvider,
                                          final FetchResult taskResult,
                                          final List<String> links) throws NoSuchServiceException
  {
    if (serviceProvider == null)
      throw new NullArgumentException("serviceProvider");
    if (taskResult == null)
      throw new NullArgumentException("taskResult");

    LinksExtractorService linksExtractor = (LinksExtractorService) serviceProvider
        .getService("workerservice.linksExtractor");

    List<CrawlerURL> extractedLinks = new ArrayList<CrawlerURL>();
    int extractedLinksNum = 0;
    if (links != null)
      extractedLinksNum = linksExtractor.resolveLinks(taskResult.getTask().getCrawlerURL(), links,
                                                      extractedLinks);
    else
      extractedLinksNum = linksExtractor.extractLinks(taskResult, extractedLinks);

    if (logger.isInfoEnabled())
      logger.info("Extracted " + extractedLinksNum + " links from: " + taskResult.getTask());

    return extractedLinks;
  }

  protected boolean isValidFetchTask(final ServiceProvider serviceProvider)
      throws NoSuchServiceException
  {
    if (serviceProvider == null)
      throw new NullArgumentException("serviceProvider");
    
    FilterService filter = (FilterService) serviceProvider.getService("workerservice.filter");
    if (!filter.isValidFetchTask(this))
      return false;
    
    return filter.isValidRobotRules(this);
  }
  
  protected boolean isValidFetchTaskToProduce(final ServiceProvider serviceProvider)
      throws NoSuchServiceException
  {
    if (serviceProvider == null)
      throw new NullArgumentException("serviceProvider");

    FilterService filter = (FilterService) serviceProvider.getService("workerservice.filter");
    return filter.isValidToProduce(this);
  }

  protected boolean isRedirect(final Response response)
  {
    if (response == null)
      throw new NullArgumentException("response");

    return (response.getCode() >= 300 && response.getCode() < 400);
  }

  /**
   * Returns {@link CrawlerURL} object of redirected location in <code>respones</code> or
   * <code>null</code> if construction of redirected location has failed. <br>
   * Returned {@link CrawlerURL} object has 'location' url in response and redirect number + 1.
   * 
   * @param response redirected response.
   * @return redirected location in <code>respones</code> or <code>null</code> if construction
   *         of redirected location has failed.
   * @throws NullArgumentException if <code>response</code> is <code>null</code>.
   */
  protected CrawlerURL getRedirectURL(final Response response)
  {
    if (response == null)
      throw new NullArgumentException("response");

    String location = response.getHeaderValue("location");
    CrawlerURL redirectURL = null;

    if (location != null) {
      try {
        redirectURL = new CrawlerURL(getCrawlerURL().getDepth(), location, getCrawlerURL()
            .getRedirNum() + 1);
      } catch (MalformedCrawlerURLException e) {
        logger.warn("Cannot follow redirected location " + location + ": " + e.toString());
      }
    } else {
      // The response is invalid and did not provide the new location for the resource.
      logger.warn("Redirected location is empty.");
    }

    return redirectURL;
  }

  protected boolean isOk(final Response response)
  {
    if (response == null)
      throw new NullArgumentException("response");

    return (response.getCode() == 200);
  }

  public String getDBKey()
  {
    return this.getCrawlerURL().getURLString();
  }

  public DBValue getDBValue()
  {
    return this;
  }

  // ////////////////////////////////////////////////// Object
  
  public boolean equals(final Object other)
  {
    if (!(other instanceof DefaultFetchTask))
      return false;
    DefaultFetchTask castOther = (DefaultFetchTask) other;
    return new EqualsBuilder().append(crawlerURL, castOther.crawlerURL).isEquals();
  }

  public int hashCode()
  {
    return new HashCodeBuilder().append(crawlerURL).toHashCode();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
        .appendSuper(super.toString()).append("crawlerURL", crawlerURL).toString();
    
  }

}
