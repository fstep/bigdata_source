/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.task;

import com.porva.crawler.net.CrawlerURL;

/**
 * Defines a {@link Task} that is used to download URLs. 
 * 
 * @author Poroshin V.
 * @param <T> type of task result.
 * @date Sep 18, 2005
 */
public interface FetchTask extends Task<FetchResult>                
{
  /**
   * Returns {@link CrawlerURL} object of this task.
   * @return {@link CrawlerURL} object of this task.
   */
  public CrawlerURL getCrawlerURL();
}
