/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.task;

import static com.porva.crawler.task.FetchResult.Status.COMPLETE;
import static com.porva.crawler.task.FetchResult.Status.HTTP_CLIENT_EXCEPTION;
import static com.porva.crawler.task.FetchResult.Status.TASK_FILTERED;

import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cyberneko.html.parsers.DOMParser;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.porva.crawler.CrawlerInit;
import com.porva.crawler.Frontier;
import com.porva.crawler.db.CrawlerDBValueFactory;
import com.porva.crawler.db.DBException;
import com.porva.crawler.db.DBInputStream;
import com.porva.crawler.db.DBOutputStream;
import com.porva.crawler.db.DBValue;
import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.net.HttpClientException;
import com.porva.crawler.net.Response;
import com.porva.crawler.service.FilterService;
import com.porva.crawler.service.NoSuchServiceException;
import com.porva.crawler.service.ServiceProvider;
import com.porva.html.CharsetDetectorService;
import com.porva.html.DOMDocumentParser;
import com.porva.html.HTMLParser;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;

/**
 * Task to fetch one URL.
 * 
 * @author Poroshin V.
 */
public class FetchURL extends DefaultFetchTask
{
  private static Log logger = LogFactory.getLog(FetchURL.class);

  protected FetchURL()
  {
    super(defCrawlerURL);
  }

  FetchURL(final CrawlerURL crawlerURL)
  {
    super(crawlerURL);
  }

  public FetchResult perform(final ServiceProvider serviceProvider)
  {
    if (serviceProvider == null)
      throw new NullArgumentException("serviceProvider");

    FetchURLResult taskResult = null;
    try {
      if (!isValidFetchTask(serviceProvider))
        return new FetchURLResult(this, TASK_FILTERED, null);

      Response response = fetchURL(serviceProvider);
      taskResult = new FetchURLResult(this, COMPLETE, response);
      if (!isValidResult(serviceProvider, taskResult))
        return new FetchURLResult(this, TASK_FILTERED, null);

      if (isRedirect(response))
        processRedirectResponse(serviceProvider, response);

      if (isOk(response))
        processOkResponse(serviceProvider, taskResult);

    } catch (NoSuchServiceException e) {
      throw new UnhandledException(e);
    } catch (HttpClientException e) {
      taskResult = new FetchURLResult(this, HTTP_CLIENT_EXCEPTION, null);
    }
    return taskResult;
  }

  private boolean isValidResult(ServiceProvider serviceProvider, FetchResult taskResult)
      throws NoSuchServiceException
  {
    FilterService filter = (FilterService) serviceProvider.getService("workerservice.filter");
    return filter.isValidFetchResult(taskResult);
  }

  void processRedirectResponse(ServiceProvider serviceProvider, Response response)
      throws NoSuchServiceException
  {
    CrawlerURL redirectURL = getRedirectURL(response);
    if (redirectURL != null) {
      FetchTask redirectTask = (FetchTask) TaskFactory.newFetchURL(redirectURL);
      ((Frontier) serviceProvider.getService("commonservice.frontier")).addTask(redirectTask);
    }
  }

  void processOkResponse(final ServiceProvider serviceProvider, FetchURLResult taskResult)
      throws NoSuchServiceException
  {
    List<String> parsedLinks = null;
    if (CrawlerInit.hasActiveParser(CrawlerInit.getProperties()))
      parsedLinks = applyParsers(serviceProvider, taskResult);

    if (isValidFetchTaskToProduce(serviceProvider)) { // todo: use also
      // TaskRsultFilter.isValidToProcuse
      List<CrawlerURL> links = extractLinks(serviceProvider, taskResult, parsedLinks);

      List<FetchTask> fetchTasks = linksToTasks(links);
      
      List<Task> tasks = filterTasks(fetchTasks, (FilterService) serviceProvider
          .getService("workerservice.filter"));

      addTasksToCrawler((Frontier) serviceProvider.getService("commonservice.frontier"), tasks);
    }
  }
  
  private Document html2Document(String content) throws SAXException, IOException 
  {
  DOMParser parser = new org.cyberneko.html.parsers.DOMParser();
  parser.parse(new InputSource(new StringReader(content)));
  return parser.getDocument();
  }

  
  List<String> applyParsers(final ServiceProvider serviceProvider, FetchURLResult taskResult)
      throws NoSuchServiceException
  {
    // 1. charset detection
    final CharsetDetectorService charsetDetector = (CharsetDetectorService) serviceProvider
        .getService("charset-detector");
    final Charset charset = charsetDetector.detect(taskResult.getResponse(), taskResult.getTask()
        .getCrawlerURL().getURL());
    taskResult.setCharset(charset.name());


    // 2. construct document
    String content = null;
    //Document document = null;
    try {
      content = new String(taskResult.getResponse().getBody(), charset.name());
      //document = html2Document(content);
    } catch (Exception e) {
      logger.warn("Failed to construct w3c document", e);
    }

//    if (document == null) { // this is probably not a html doc
//      taskResult = new FetchURLResult(this, TASK_FILTERED, null);
//      return null; // todo:
//    }

    // 3. html2xml
    List<String> parsedLinks = null;
    String xml = null;
    String title = null;
    if (CrawlerInit.getProperties().getProperty("parser.html2xml.active").equals("yes")) {
      try {
        //document = html2Document(content);
        DOMDocumentParser html2xml = (DOMDocumentParser) serviceProvider
            .getService(DOMDocumentParser.HTML2XML);
        xml = html2xml.process(content, taskResult.getTask().getCrawlerURL().getURI());
        parsedLinks = html2xml.getLinks();
        title = html2xml.getTitle();
//        if (xml != null)
//          System.out.println(xml);
      } catch (Exception e) {
        logger.info("html2xml exception", e);
      }
    }

    // 4. SESQ
    if (CrawlerInit.getProperties().getProperty("parser.sesq.active").equals("yes")) {
      try {
        if (xml == null) {
          HTMLParser sesqParser = (HTMLParser) serviceProvider.getService(HTMLParser.SESQ);
          xml = sesqParser.process(content, taskResult.getTask().getCrawlerURL().getURI());
          parsedLinks = sesqParser.getLinks();
          title = sesqParser.getTitle();
        }
      } catch (Exception e) {
        logger.info("SESQ exception", e);
      }
    }

    if (xml != null) {
      taskResult.setParsed(xml);
    } else if (CrawlerInit.getProperties().getProperty("parser.kce.active").equals("yes")) { // apply KCE
      DOMDocumentParser kceParser = (DOMDocumentParser) serviceProvider
          .getService(DOMDocumentParser.KCE);
      String cleaned = kceParser.process(content, taskResult.getTask().getCrawlerURL().getURI());
      taskResult.setParsed(cleaned);
      parsedLinks = kceParser.getLinks();
      title = kceParser.getTitle();
    }
    taskResult.setTitle(title);
    
    return parsedLinks;
  }

  List<Task> filterTasks(List<FetchTask> tasks, FilterService filterService)
  {
    List<Task> filteredTasks = new ArrayList<Task>();
    for (FetchTask task : tasks)
      if (filterService.isValidFetchTask(task))
        filteredTasks.add(task);
    return filteredTasks;
  }

  void addTasksToCrawler(final Frontier frontier, final List<Task> tasks)
  {
    int addedTasksNum = frontier.addTaskList(tasks);
    if (logger.isInfoEnabled())
      logger.info("Added " + addedTasksNum + " tasks to workload.");
  }

  List<FetchTask> linksToTasks(final List<CrawlerURL> links)
  {
    List<FetchTask> tasks = new ArrayList<FetchTask>();
    for (CrawlerURL crawlerURI : links)
      tasks.add(new FetchURL(crawlerURI));
    return tasks;
  }

  public void writeTo(final DBOutputStream tupleOutput)
  {
    if (tupleOutput == null)
      throw new NullArgumentException("tupleOutput");

    getCrawlerURL().writeTo(tupleOutput);
  }

  public DBValue newInstance(final DBInputStream tupleInput) throws DBException
  {
    if (tupleInput == null)
      throw new NullArgumentException("tupleInput");

    CrawlerURL crawlerURL = null;
    try {
      crawlerURL = (CrawlerURL) CrawlerDBValueFactory.instance.newDBValueInstance(tupleInput,
                                                                                  CrawlerURL.class);
    } catch (Exception e) {
      throw new DBException(e);
    }
    return new FetchURL(crawlerURL);
  }

  public long getClassCode()
  {
    return -1063210949870L;
  }

  private transient String toString;

  public String toString()
  {
    if (toString == null) {
      toString = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
          .appendSuper(super.toString()).toString();
    }
    return toString;
  }
  
  public boolean equals(final Object other)
  {
    return new EqualsBuilder().appendSuper(super.equals(other)).isEquals();
  }

  private transient int hashCode;

  public int hashCode()
  {
    if (hashCode == 0) {
      hashCode = new HashCodeBuilder().appendSuper(super.hashCode()).toHashCode();
    }
    return hashCode;
  }


}
