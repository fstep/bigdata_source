///*
// * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
// * redistribute it and/or modify it under the terms of the GNU General Public License as published
// * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
// * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
// * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
// * the GNU General Public License along with this program; if not, write to the Free Software
// * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// */
//package com.porva.crawler.task;
//
//import org.apache.commons.lang.NullArgumentException;
//import org.apache.commons.lang.builder.EqualsBuilder;
//import org.apache.commons.lang.builder.ToStringBuilder;
//import org.apache.commons.lang.builder.ToStringStyle;
//
//import com.porva.crawler.db.DBValue;
//
///**
// * Default implementation of {@link Task} interface.<br>
// * 
// * @author Poroshin V.
// * @date Oct 6, 2005
// * @param <T> type of task result.
// */
//public abstract class AbstractTask<T extends TaskResult> implements Task<T>
//{
////  private TaskFactory taskType = null;
////
////  /**
////   * Allocated new object.
////   * 
////   * @param taskType {@link TaskFactory} of object.
////   */
////  public AbstractTask(TaskFactory taskType)
////  {
////    this.taskType = taskType;
////    if (this.taskType == null)
////      throw new NullArgumentException("taskType");
////  }
////
////  public TaskFactory getTaskType()
////  {
////    return taskType;
////  }
//
//  // //////////////////////////////////////////////// DBValue interface impl
//  
//  /* (non-Javadoc)
//   * @see com.porva.crawler.db.DBRecord#getDBKey()
//   */
//  public String getDBKey()
//  {
//    return Integer.toString(this.hashCode());
//  }
//
//  /* (non-Javadoc)
//   * @see com.porva.crawler.db.DBRecord#getDBValue()
//   */
//  public DBValue getDBValue()
//  {
//    return this;
//  }
//
//  /*
//   * (non-Javadoc)
//   * 
//   * @see java.lang.Object#toString()
//   */
//  public String toString()
//  {
//    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
//        .appendSuper(super.toString()).toString();
//  }
//
//  /*
//   * (non-Javadoc)
//   * 
//   * @see java.lang.Object#equals(java.lang.Object)
//   */
//  public boolean equals(final Object other)
//  {
//    if (!(other instanceof Task))
//      return false;
//    Task castOther = (Task) other;
//    return new EqualsBuilder().append(getTaskType(), castOther.getTaskType()).isEquals();
//  }
//
//}
