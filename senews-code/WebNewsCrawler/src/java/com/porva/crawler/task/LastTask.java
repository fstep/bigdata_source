/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.task;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.porva.crawler.db.DBInputStream;
import com.porva.crawler.db.DBOutputStream;
import com.porva.crawler.db.DBValue;
import com.porva.crawler.service.ServiceProvider;

/**
 * Denotes the last task of crawling process. It is a marker task, i.e. it cannot be performed by 
 * itself but crawler should stop its work after receiving this task.
 * 
 * @author Poroshin V.
 * @date Oct 13, 2005
 */
public class LastTask implements Task
{
  private static final LastTask instance = new LastTask();

  static LastTask getInstance()
  {
    return instance;
  }

  private LastTask()
  {
  }

  public TaskResult perform(ServiceProvider serviceProvider)
  {
    throw new IllegalStateException();
  }

  public void writeTo(DBOutputStream tupleOutput)
  {
  }

  public DBValue newInstance(DBInputStream tupleInput)
  {
    return instance;
  }

  public long getClassCode()
  {
    return 382368885054351L;
  }

  private transient String toString;

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    if (toString == null) {
      toString = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE)
          .append("classCode", getClassCode()).toString();
    }
    return toString;
  }

  public String getDBKey()
  {
    return "last-task";
  }

  public DBValue getDBValue()
  {
    return instance;
  }

}
