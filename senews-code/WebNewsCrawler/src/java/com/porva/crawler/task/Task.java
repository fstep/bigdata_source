/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.task;

import com.porva.crawler.db.DBRecord;
import com.porva.crawler.db.DBValue;
import com.porva.crawler.service.ServiceProvider;

/**
 * Interface for task that can perfomed by crawler worker.<br>
 * Any task should also implements extended {@link DBRecord} and {@link DBValue} interfaces so
 * any task can be stored in database.
 * 
 * @author Poroshin V.
 * @date Sep 18, 2005
 * @param <T>
 */
public interface Task<T extends TaskResult> extends DBRecord, DBValue
{

  /**
   * Performs the task.
   * 
   * @param serviceProvider
   * @return {@link TaskResult} object as a result of task performing.
   * @throws org.apache.commons.lang.NullArgumentException if
   *           <code>serviceProvider</code> is <code>null</code>.
   */
  public T perform(ServiceProvider serviceProvider);
  
}
