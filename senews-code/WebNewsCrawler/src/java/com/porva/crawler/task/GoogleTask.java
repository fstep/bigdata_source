/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.task;

import static com.porva.crawler.task.FetchResult.Status.COMPLETE;
import static com.porva.crawler.task.FetchResult.Status.HTTP_CLIENT_EXCEPTION;
import static com.porva.crawler.task.FetchResult.Status.TASK_FILTERED;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.crawler.Frontier;
import com.porva.crawler.db.DBException;
import com.porva.crawler.db.DBInputStream;
import com.porva.crawler.db.DBOutputStream;
import com.porva.crawler.db.DBValue;
import com.porva.crawler.google.GoogleResultPageParser;
import com.porva.crawler.google.Hit;
import com.porva.crawler.google.WebSearchResult;
import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.net.HttpClient;
import com.porva.crawler.net.HttpClientException;
import com.porva.crawler.net.MalformedCrawlerURLException;
import com.porva.crawler.net.Response;
import com.porva.crawler.service.NoSuchServiceException;
import com.porva.crawler.service.ServiceProvider;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;

/**
 * Task to get top N Google hits for given query. <br>
 * <i>Warning:</i> This class simulates ordinary internet browser and retrieving results from
 * http://google.com search page. It does NOT use Google API.
 */
public class GoogleTask extends DefaultFetchTask
{

  private static Log logger = LogFactory.getLog(GoogleTask.class);

  // todo: agentString from init file
  /**
   * HTTP user agent string to access google.
   */
  public static final String agentString = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; MyIE2; Maxthon; .NET CLR 1.1.4322)";

  /**
   * Number of hits per result page. Changing this number might change Google search results!
   */
  public static int hitsPerPage = 100;

  private int depth = 0;

  private String query = null;

  private int num = 0;

  private GoogleResultPageParser parser = new GoogleResultPageParser();

  protected GoogleTask()
  {
    super(makeSearchURL("none"));
  }

  /**
   * Allocates new {@link GoogleTask} object.
   * 
   * @param num number of first hits to retrieve.
   * @param query query string to Google.
   * @param depth initial depth of hits.
   * @throws NullArgumentException if <code>query</code> string is null.
   * @throws IllegalArgumentException if <code>depth</code> < 0 or/and <code>num</code> <= 0.
   */
  GoogleTask(int num, final String query, int depth)
  {
    super(makeSearchURL(query));

    this.query = query;
    this.num = num;
    this.depth = depth;

    if (this.query == null)
      throw new NullArgumentException("Query string cannot be null.");
    else if (this.query.matches("^\\s*$"))
      throw new IllegalArgumentException("Query string cannot be empty.");
    if (this.num <= 0)
      throw new IllegalArgumentException("Number of hits cannot be less or equal 0.");
    if (this.depth < 0)
      throw new IllegalArgumentException("Depth of GoogleTask cannot be less then 0.");
  }

  static CrawlerURL makeSearchURL(final String query)
  {
    if (query == null)
      throw new NullArgumentException("Query string cannot be null.");

    StringBuffer sb = new StringBuffer();
    CrawlerURL crawlerURL = null;
    try {
      sb.append("http://www.google.com/search?").append("num=").append(
                                                                       Integer
                                                                           .toString(hitsPerPage))
          .append("&hl=en&lr=&start=0&q=").append(URLEncoder.encode(query, "UTF-8"));
      crawlerURL = new CrawlerURL(0, sb.toString());
    } catch (UnsupportedEncodingException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (MalformedCrawlerURLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    return crawlerURL;
  }

  /**
   * Returns initial depths of hits.
   * 
   * @return initial depth.
   */
  public int getDepth()
  {
    return depth;
  }

  /**
   * Returns number of retrieved hits.
   * 
   * @return number of retrieved hits.
   */
  public int getNum()
  {
    return num;
  }

  /**
   * Returns query to Google.
   * 
   * @return query string.
   */
  public String getQuery()
  {
    return query;
  }

  public FetchResult perform(final ServiceProvider serviceProvider)
  {
    if (serviceProvider == null)
      throw new NullArgumentException("serviceProvider");

    GoogleTaskResult taskResult = new GoogleTaskResult(this, HTTP_CLIENT_EXCEPTION, null, null);

    try {
      if (!isValidFetchTask(serviceProvider))
        return new FetchURLResult(this, TASK_FILTERED, null);

      Response response = fetchURL(serviceProvider);

      // if (!isValidResult(serviceProvider, taskResult))
      // return new FetchURLResult(this, TASK_FILTERED, null);

      if (isRedirect(response)) {
        CrawlerURL redirectURL = getRedirectURL(response);
        if (redirectURL != null) {
          response = fetchURL(serviceProvider);
        }
      }

      if (isOk(response))
        taskResult = processOkResponse(serviceProvider, response);

    } catch (NoSuchServiceException e) {
      throw new UnhandledException(e);
    } catch (HttpClientException e) {
      taskResult = new GoogleTaskResult(this, HTTP_CLIENT_EXCEPTION, null, null);
    }
    return taskResult;
  }

  protected Response fetchURL(final ServiceProvider serviceProvider) throws HttpClientException,
      NoSuchServiceException
  {
    if (serviceProvider == null)
      throw new NullArgumentException("serviceProvider");

    HttpClient httpClient = (HttpClient) serviceProvider.getService("workerservice.httpClient");
    String userAgent = (String) httpClient.getParameter("http.useragent");
    httpClient.setParameter("http.useragent", agentString);
    Response res = httpClient.get(getCrawlerURL());
    httpClient.setParameter("http.useragent", userAgent);

    return res;
  }

  private GoogleTaskResult processOkResponse(final ServiceProvider serviceProvider,
                                             final Response response) throws NoSuchServiceException
  {
    WebSearchResult finalRes = new WebSearchResult(query);
    WebSearchResult res = parser.parseWebResultPage(new String(response.getBody()), query);
    List<Task> tasks = new ArrayList<Task>();
    for (Hit hit : res.getResults()) {
      try {
        tasks.add(new FetchURL(new CrawlerURL(depth, hit.getURLString())));
        finalRes.getResults().add(hit);
        if (finalRes.getResults().size() >= num)
          break;
      } catch (MalformedCrawlerURLException e) {
        logger.warn("Google hit contains ivalid url: " + hit.getURLString());
      }
    }

    addTasksToCrawler((Frontier) serviceProvider.getService("commonservice.frontier"), tasks);
    return new GoogleTaskResult(this, COMPLETE, response, finalRes);
  }

  void addTasksToCrawler(final Frontier frontier, final List<Task> tasks)
  {
    int addedTasksNum = frontier.addTaskList(tasks);
    if (logger.isInfoEnabled())
      logger.info("Added " + addedTasksNum + " tasks to workload.");
  }

  public void writeTo(final DBOutputStream tupleOutput)
  {
    if (tupleOutput == null)
      throw new NullArgumentException("tupleOutput");

    tupleOutput.writeInt(num);
    tupleOutput.writeString(query);
    tupleOutput.writeInt(depth);
  }

  public DBValue newInstance(final DBInputStream tupleInput) throws DBException
  {
    if (tupleInput == null)
      throw new NullArgumentException("tupleInput");

    int num = tupleInput.readInt();
    String query = tupleInput.readString();
    int depth = tupleInput.readInt();

    return new GoogleTask(num, query, depth);
  }

  public long getClassCode()
  {
    return -3241391001L;
  }

  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("query", query)
        .append("depth", depth).append("num", num).toString();
  }

  public boolean equals(final Object other)
  {
    if (!(other instanceof GoogleTask))
      return false;
    GoogleTask castOther = (GoogleTask) other;
    return new EqualsBuilder().appendSuper(super.equals(other)).append(depth, castOther.depth)
        .append(query, castOther.query).append(num, castOther.num).isEquals();
  }

  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(depth).append(query)
        .append(num).toHashCode();
  }

  // public String toHtmlDocument()
  // {
  // String html = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"\n"
  // + "\"http://www.w3.org/TR/html4/loose.dtd\">\n" + "<html>\n" + "<head>\n"
  // + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\n"
  // + "<title>Google task result</title>\n" + "</head>\n" + "\n" + "<body>\n"
  // + "<h1>Google task result </h1>\n" + "<p> <strong>query: </strong> " + query + "<br>\n"
  // + " <strong>number of hits requested: </strong> " + num + " <br>\n"
  // + " <strong>initial depth:</strong> " + depth + "</p>\n"
  // + "<p><strong>results:</strong></p>\n" + "<table width=\"100%\" border=\"0\">\n";
  //
  // Vector hits = result.result.getResults();
  // for (Enumeration e = hits.elements(); e.hasMoreElements();) {
  // Hit hit = (Hit) e.nextElement();
  // html += " <tr>\n" + " <td width=\"4%\">" + hit.getNum() + "</td>\n"
  // + " <td width=\"96%\"><a href=\"" + hit.getURL() + "\">" + hit.getURL()
  // + "</a></td>\n" + " </tr>\n";
  // }
  // html += "</table>\n" + "<p><strong></strong></p>\n" + "<p>&nbsp;</p>\n" + "</body>\n"
  // + "</html>";
  //
  // return html;
  // }
}
