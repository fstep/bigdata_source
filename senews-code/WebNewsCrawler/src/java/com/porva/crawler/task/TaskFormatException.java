/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.task;

/**
 * Signals that task cannot be loaded because of its uncorrect format.<br>
 * This exception is used to in initial tasks readers to signal that task cannot be readed.
 * 
 * @author Poroshin V.
 * @date Sep 22, 2005
 */
public class TaskFormatException extends Exception
{
  /**
   * 
   */
  private static final long serialVersionUID = 7702401620308411352L;

  /**
   * Constructs an <code>TaskFormatException</code> with <code>null</code>
   * as its error detail message.
   */
  public TaskFormatException()
  {
    super();
  }

  /**
   * Constructs a <code>TaskFormatException</code> with the specified detail
   * message. The error message string <code>s</code> can later be
   * retrieved by the <code>{@link java.lang.Throwable#getMessage}</code>
   * method of class <code>java.lang.Throwable</code>.
   * @param msg the detail message.
   */
  public TaskFormatException(String msg)
  {
    super(msg);
  }

  /**
   * Constructs a new <code>TaskFormatException</code> exception with the specified 
   * detail message and cause.  
   *
   * @param  message the detail message (which is saved for later retrieval
   *         by the {@link #getMessage()} method).
   * @param  cause the cause (which is saved for later retrieval by the
   *         {@link #getCause()} method).  (A <tt>null</tt> value is
   *         permitted, and indicates that the cause is nonexistent or
   *         unknown.)
   */
  public TaskFormatException(String message, Throwable cause)
  {
    super(message, cause);
  }

  /**
   * Constructs a new <code>TaskFormatException</code> exception with the specified cause and a detail
   * message of <tt>(cause==null ? null : cause.toString())</tt>.
   *
   * @param  cause the cause (which is saved for later retrieval by the
   *         {@link #getCause()} method).  (A <tt>null</tt> value is
   *         permitted, and indicates that the cause is nonexistent or
   *         unknown.)
   */
  public TaskFormatException(Throwable cause)
  {
    super(cause);
  }


}
