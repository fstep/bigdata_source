/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.task;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.porva.crawler.db.*;
import com.porva.crawler.net.Response;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;

public class FetchRobotxtResult implements FetchResult
{

  private DefaultFetchResult defFetchResult;
  
  private FetchRobotxtResult redirectResult = null;

  private FetchRobotxtResult()
  {
  }

  public FetchRobotxtResult(final FetchTask fetchTask, final Status status, final Response response)
  {
    defFetchResult = new DefaultFetchResult(fetchTask, status, response);
  }

  private FetchRobotxtResult(DefaultFetchResult defFetchResult)
  {
    this.defFetchResult = defFetchResult;
  }

  public Status getStatus()
  {
    return defFetchResult.getStatus();
  }

  public Response getResponse()
  {
    return defFetchResult.getResponse();
  }

  public FetchTask getTask()
  {
    return defFetchResult.getTask();
  }

  public void writeTo(DBOutputStream tupleOutput)
  {
    tupleOutput.writeLong(defFetchResult.getClassCode());
    defFetchResult.writeTo(tupleOutput);
    // todo store additional fields
  }

  public DBValue newInstance(DBInputStream tupleInput) throws DBException
  {
    DefaultFetchResult dfr = (DefaultFetchResult) CrawlerDBValueFactory.instance.readObject(tupleInput);
    // todo read additional fields
    return new FetchRobotxtResult(dfr);
  }

  public String getDBKey()
  {
    return defFetchResult.getDBKey();
  }

  public DBValue getDBValue()
  {
    return this;
  }
  
  public FetchRobotxtResult getRedirectResult()
  {
    return redirectResult;
  }
  
  void setRedirectResult(final FetchRobotxtResult result)
  {
    if (result == null)
      throw new NullArgumentException("result");
    
    redirectResult = result;
  }
  
  /*
   * (non-Javadoc)
   * 
   * @see com.porva.crawler.db.DBValue#getClassCode()
   */
  public long getClassCode()
  {
    return 50768373324193L;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("defFetchResult",
                                                                            defFetchResult)
        .toString();
  }
  
  public boolean equals(final Object other)
  {
    if (!(other instanceof FetchRobotxtResult))
      return false;
    FetchRobotxtResult castOther = (FetchRobotxtResult) other;
    return new EqualsBuilder().append(defFetchResult, castOther.defFetchResult).isEquals();
  }

  public int hashCode()
  {
    return new HashCodeBuilder().append(defFetchResult).toHashCode();
  }

}
