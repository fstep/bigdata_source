/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.task;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.db.CrawlerDBValueFactory;
import com.porva.crawler.db.DBException;
import com.porva.crawler.db.DBInputStream;
import com.porva.crawler.db.DBOutputStream;
import com.porva.crawler.db.DBValue;
import com.porva.crawler.google.WebSearchResult;
import com.porva.crawler.net.Response;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;

/**
 * Result of performing of {@link GoogleTask}.
 */

public class GoogleTaskResult implements FetchResult
{

  private WebSearchResult result;

  private DefaultFetchResult defFetchResult;

  private GoogleTaskResult()
  {
  }
  
  private GoogleTaskResult(final DefaultFetchResult defaultResult, final WebSearchResult googleResult)
  {
    this.defFetchResult = defaultResult;
    this.result = googleResult;
    
    if (this.defFetchResult == null)
      throw new NullArgumentException("defFetchResult");
//    if (this.result == null)
//      throw new NullArgumentException("result");
  }

  public GoogleTaskResult(final FetchTask fetchTask, final Status status, final Response response,
                          final WebSearchResult googleResult)
  {
    defFetchResult = new DefaultFetchResult(fetchTask, status, response);
    result = googleResult;
  }

  /**
   * Returns {@link WebSearchResult} object.
   * 
   * @return {@link WebSearchResult} object.
   */
  public WebSearchResult getWebSearchResult()
  {
    return result;
  }

  public Status getStatus()
  {
    return defFetchResult.getStatus();
  }

  public Response getResponse()
  {
    return defFetchResult.getResponse();
  }

  public FetchTask getTask()
  {
    return defFetchResult.getTask();
  }

  public void writeTo(final DBOutputStream tupleOutput)
  {
    if (tupleOutput == null)
      throw new NullArgumentException("tupleOutput");

    tupleOutput.writeLong(defFetchResult.getClassCode());
    defFetchResult.writeTo(tupleOutput);
    // todo: save WebSearchResult
  }

  public DBValue newInstance(final DBInputStream tupleInput) throws DBException
  {
    if (tupleInput == null)
      throw new NullArgumentException("tupleInput");

    DefaultFetchResult dfr = (DefaultFetchResult) CrawlerDBValueFactory.instance
        .readObject(tupleInput);

    return new GoogleTaskResult(dfr, null); // FIXME
  }

  public long getClassCode()
  {
    return 13240112345L;
  }

  public String getDBKey()
  {
    return defFetchResult.getDBKey();
  }

  public DBValue getDBValue()
  {
    return this;
  }

  public boolean equals(final Object other)
  {
    if (!(other instanceof GoogleTaskResult))
      return false;
    GoogleTaskResult castOther = (GoogleTaskResult) other;
    return new EqualsBuilder().append(result, castOther.result).append(defFetchResult,
                                                                       castOther.defFetchResult)
        .isEquals();
  }

  public int hashCode()
  {
    return new HashCodeBuilder().append(result).append(defFetchResult).toHashCode();
  }
}
