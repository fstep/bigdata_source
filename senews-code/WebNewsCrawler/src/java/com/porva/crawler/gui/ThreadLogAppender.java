/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.Hashtable;
import java.util.Vector;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * Provides possibility to show log4j log for each {@link com.porva.util.thread.WorkerThread} 
 * thread in GUI table "Thread log".
 */

class ThreadLogAppender extends Handler
{
  private JTable jTable;
  private ThreadLogTableModel logTableModel = new ThreadLogTableModel();
  private boolean firstTime = true;
  private long startTime;
  private Hashtable<Integer,Integer> threadPos = new Hashtable<Integer,Integer>();
  private int curPos = 0;

  //the singleton instance
  private static ThreadLogAppender handler = null;


  /**
   * private constructor, preventing initialization
   */
  private ThreadLogAppender(JTable jTableForLog)
  {
    this.jTable = jTableForLog;
    jTable.setModel(logTableModel);

    jTable.getColumnModel().getColumn(2).setPreferredWidth(800);
    /*TableColumn column = null;
    for (int i = 0; i < 3; i++) {
        column = jTable.getColumnModel().getColumn(i);
        if (i == 0) {
            //column.setPreferredWidth(5);
          column.setWidth(10);
        } else if (i == 1) {
          column.setPreferredWidth(20);
          column.setWidth(20);
        }
    } */

  }

  /**
   * The getInstance method returns the singleton instance of the ThreadLogAppender object.
   * It is synchronized to prevent two threads trying to create an instance simultaneously.
   * @param jTableForLog
   * @return
   */
  public static synchronized ThreadLogAppender getInstance(JTable jTableForLog)
  {
    if (handler == null) {
      handler = new ThreadLogAppender(jTableForLog);
    }
    handler.curPos = 0; // todo: 
    return handler;
  }

  public void publish(LogRecord record)
  {
    if (firstTime) {
      startTime = System.currentTimeMillis();
      firstTime = false;
    }

    int threadName = record.getThreadID();
//    if (!threadName.startsWith("Thread")) // this looks not like CrawlerWorker thead -- do not log
//      return;

    String log [] = new String [3];
    int leftTime = (int)(System.currentTimeMillis() - startTime);
    int sec = leftTime / 1000;
    int hours = sec / (60*60);
    int min = sec/60 - hours*60;
    sec = sec - min*60;
    // todo: customizable time view
    log[0] = Integer.toString(hours) + ":" + Integer.toString(min) + ":" + Integer.toString(sec);
        //"." + Integer.toString(ms);


    //event.get
    Integer pos = (Integer)threadPos.get(new Integer(threadName));
    if (pos == null) {
      threadPos.put(new Integer(threadName), new Integer(curPos));
      pos =new Integer(curPos);
      curPos++;
    }

    log[1] = pos.toString();
    log[2] = record.getMessage();

    // todo:
    try {
      logTableModel.logData.set(pos.intValue(), log);
    } catch (Exception e) {
      logTableModel.logData.add(pos.intValue(), log);
    }

    logTableModel.fireTableDataChanged();
  }

  public void flush()
  {
  }

  public void close() throws SecurityException
  {
    curPos = 0;
  }

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class ThreadLogTableModel extends AbstractTableModel
{
  private static final long serialVersionUID = 215474636228955690L;
  protected Vector logData = new Vector();
  protected String[] columnNames = {"Time", "Thread", "Thread log message"};

  public int getColumnCount()
  {
    return columnNames.length;
  }

  public int getRowCount()
  {
    return logData.size();
  }

  public Object getValueAt(int rowIndex, int columnIndex)
  {
    return ((String[])logData.elementAt(rowIndex))[columnIndex];
  }

  public String getColumnName(int col) {
      return columnNames[col];
  }

  public void setValueAt(Object value, int row, int col)
  {
    ((String[])logData.elementAt(row))[col] = (String)value;
    fireTableCellUpdated(row, col);
  }

  public void clear()
  {
    logData.clear();
  }


}