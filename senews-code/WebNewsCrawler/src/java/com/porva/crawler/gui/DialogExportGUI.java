/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
/*
 * Created by JFormDesigner on Tue Feb 01 15:49:52 EET 2005
 */



/**
 * @author porva porva
 */
public class DialogExportGUI extends JDialog {
  private static final long serialVersionUID = -8342043965202165281L;
  public DialogExportGUI(Frame owner) {
		super(owner);
		initComponents();
	}

	public DialogExportGUI(Dialog owner) {
		super(owner);
		initComponents();
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		// Generated using JFormDesigner Evaluation license - evaluator evaluator
		dialogPane = new JPanel();
		contentPane = new JPanel();
		panel1 = new JPanel();
		checkBoxExportFetchURL = new JCheckBox();
		checkBoxExportFetchRSS = new JCheckBox();
		checkBoxExportGoogle = new JCheckBox();
		checkBoxExportRobotxt = new JCheckBox();
		jTextFieldDir = new JTextField();
		jButtonChooseDir = new JButton();
		label1 = new JLabel();
		buttonBar = new JPanel();
		okButton = new JButton();
		cancelButton = new JButton();
		CellConstraints cc = new CellConstraints();

		//======== this ========
		setTitle("Export databases of the project");
		Container contentPane2 = getContentPane();
		contentPane2.setLayout(new BorderLayout());

		//======== dialogPane ========
		{
			dialogPane.setBorder(Borders.DIALOG_BORDER);
			
			// JFormDesigner evaluation mark
//			dialogPane.setBorder(new javax.swing.border.CompoundBorder(
//				new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
//					"JFormDesigner Evaluation", javax.swing.border.TitledBorder.CENTER,
//					javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
//					java.awt.Color.red), dialogPane.getBorder())); dialogPane.addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});
//			
			dialogPane.setLayout(new BorderLayout());
			
			//======== contentPane ========
			{
				contentPane.setLayout(new FormLayout(
					new ColumnSpec[] {
						FormFactory.GLUE_COLSPEC,
						FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
						new ColumnSpec(Sizes.dluX(15))
					},
					new RowSpec[] {
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.LINE_GAP_ROWSPEC,
						new RowSpec("max(default;5dlu)"),
						FormFactory.LINE_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.LINE_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC
					}));
				
				//======== panel1 ========
				{
					panel1.setBorder(new TitledBorder("Select databases to export:"));
					panel1.setLayout(new FormLayout(
						new ColumnSpec[] {
							FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
							new ColumnSpec("max(default;3dlu)"),
							FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
							FormFactory.DEFAULT_COLSPEC,
							FormFactory.GLUE_COLSPEC
						},
						new RowSpec[] {
							FormFactory.DEFAULT_ROWSPEC,
							FormFactory.LINE_GAP_ROWSPEC,
							FormFactory.DEFAULT_ROWSPEC,
							FormFactory.LINE_GAP_ROWSPEC,
							FormFactory.DEFAULT_ROWSPEC,
							FormFactory.LINE_GAP_ROWSPEC,
							FormFactory.DEFAULT_ROWSPEC
						}));
					
					//---- checkBoxExportFetchURL ----
					checkBoxExportFetchURL.setText("Export Fetch URL tasks database");
					checkBoxExportFetchURL.setSelected(true);
					checkBoxExportFetchURL.setMnemonic('U');
					panel1.add(checkBoxExportFetchURL, cc.xy(4, 1));
					
					//---- checkBoxExportFetchRSS ----
					checkBoxExportFetchRSS.setText("Export Fetch RSS tasks database");
					checkBoxExportFetchRSS.setSelected(true);
					checkBoxExportFetchRSS.setMnemonic('S');
					panel1.add(checkBoxExportFetchRSS, cc.xy(4, 3));
					
					//---- checkBoxExportGoogle ----
					checkBoxExportGoogle.setText("Export Google tasks database");
					checkBoxExportGoogle.setSelected(true);
					checkBoxExportGoogle.setMnemonic('G');
					panel1.add(checkBoxExportGoogle, cc.xy(4, 5));
					
					//---- checkBoxExportRobotxt ----
					checkBoxExportRobotxt.setText("Export robots.txt files");
					checkBoxExportRobotxt.setSelected(true);
					checkBoxExportRobotxt.setMnemonic('R');
					panel1.add(checkBoxExportRobotxt, cc.xy(4, 7));
				}
				contentPane.add(panel1, cc.xywh(1, 1, 3, 1));
				contentPane.add(jTextFieldDir, cc.xy(1, 7));
				
				//---- jButtonChooseDir ----
				jButtonChooseDir.setText("...");
				contentPane.add(jButtonChooseDir, cc.xy(3, 7));
				
				//---- label1 ----
				label1.setText("Export databases to directory:");
				contentPane.add(label1, cc.xy(1, 5));
			}
			dialogPane.add(contentPane, BorderLayout.CENTER);
			
			//======== buttonBar ========
			{
				buttonBar.setBorder(Borders.BUTTON_BAR_GAP_BORDER);
				buttonBar.setLayout(new FormLayout(
					new ColumnSpec[] {
						FormFactory.GLUE_COLSPEC,
						FormFactory.BUTTON_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC,
						FormFactory.BUTTON_COLSPEC
					},
					RowSpec.decodeSpecs("pref")));
				
				//---- okButton ----
				okButton.setText("OK");
				buttonBar.add(okButton, cc.xy(2, 1));
				
				//---- cancelButton ----
				cancelButton.setText("Cancel");
				buttonBar.add(cancelButton, cc.xy(4, 1));
			}
			dialogPane.add(buttonBar, BorderLayout.SOUTH);
		}
		contentPane2.add(dialogPane, BorderLayout.CENTER);
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	// Generated using JFormDesigner Evaluation license - evaluator evaluator
	private JPanel dialogPane;
	private JPanel contentPane;
	private JPanel panel1;
	JCheckBox checkBoxExportFetchURL;
	JCheckBox checkBoxExportFetchRSS;
	JCheckBox checkBoxExportGoogle;
	JCheckBox checkBoxExportRobotxt;
	JTextField jTextFieldDir;
	JButton jButtonChooseDir;
	private JLabel label1;
	private JPanel buttonBar;
	JButton okButton;
	JButton cancelButton;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
