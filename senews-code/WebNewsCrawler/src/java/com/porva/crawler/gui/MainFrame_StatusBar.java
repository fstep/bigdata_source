/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

import javax.swing.*;

import com.porva.crawler.Crawler.CrawlerStatus;

import java.awt.*;

/**
 * Status bar of {@link MainFrame} frame.
 * 
 * @author Poroshin V.
 */
class MainFrame_StatusBar extends JPanel
{
  // private MainFrame adaptee;

  private static final long serialVersionUID = -2419962591572550123L;

  private JLabel statusBarProcess = new JLabel();

  private GridLayout gridLayout = new GridLayout();

  private JLabel statusBarWaiting = new JLabel();

  private JLabel statusBarRunning = new JLabel();

  private JLabel statusBarComplete = new JLabel();

  private JProgressBar jProgressComplete = new JProgressBar(0, 100);

  /**
   * Constructs a new status bar visual element.
   */
  public MainFrame_StatusBar()
  {
    // this.adaptee = adaptee;
    this.setBorder(BorderFactory.createLoweredBevelBorder());
    this.setLayout(gridLayout);
    statusBarProcess.setMaximumSize(new Dimension(100, 18));
    statusBarProcess.setMinimumSize(new Dimension(100, 18));
    statusBarProcess.setToolTipText("");
    statusBarProcess.setText("Ready");
    gridLayout.setColumns(6);
    statusBarWaiting.setEnabled(true);
    statusBarWaiting.setBorder(BorderFactory.createEtchedBorder());
    statusBarWaiting.setDebugGraphicsOptions(0);
    statusBarWaiting.setDoubleBuffered(false);
    statusBarWaiting.setMaximumSize(new Dimension(100, 18));
    statusBarWaiting.setMinimumSize(new Dimension(100, 18));
    statusBarWaiting.setDisplayedMnemonic('0');
    statusBarWaiting.setText(" ");
    statusBarRunning.setBorder(BorderFactory.createEtchedBorder());
    statusBarRunning.setDebugGraphicsOptions(0);
    statusBarRunning.setText(" ");
    statusBarComplete.setBorder(BorderFactory.createEtchedBorder());
    statusBarComplete.setText(" ");
    jProgressComplete.setValue(jProgressComplete.getMinimum());

    this.add(statusBarProcess, null);
    this.add(statusBarWaiting, null);
    this.add(statusBarRunning, null);
    this.add(statusBarComplete, null);
    this.add(jProgressComplete, null);
  }
  
  public void clear()
  {
    statusBarProcess.setText("Ready");
    statusBarWaiting.setText("  ");
    statusBarRunning.setText("  ");
    statusBarComplete.setText("  ");
    
    jProgressComplete.setStringPainted(false);
    jProgressComplete.setValue(jProgressComplete.getMinimum());
  }

  /**
   * Updates status bar view according to status of the crawler.
   * 
   * @param newStatus new status of the crawler.
   * @param adaptee
   */
  public void update(CrawlerStatus newStatus, CrawlerMain_Visual adaptee)
  {
    if (newStatus != null)
      statusBarProcess.setText(newStatus.toString());

    if (newStatus == CrawlerStatus.UNDEF) {
      statusBarProcess.setText("Ready");
      statusBarWaiting.setText("  ");
      statusBarRunning.setText("  ");
      statusBarComplete.setText("  ");
      return;
    }
    jProgressComplete.setStringPainted(true);

    int waiting = 0;
    int running = 0;
    int complete = 0;
    
    if (adaptee.waitingTableModel != null) {
      waiting = adaptee.waitingTableModel.getRowCount();
      statusBarWaiting.setText("Waiting: " + Integer.toString(waiting));
    }

    if (adaptee.runningTableModel != null) {
      running = adaptee.runningTableModel.getRowCount();
      statusBarRunning.setText("Running: " + Integer.toString(running));
    }

    if (adaptee.completeTableModel != null) {
      complete = adaptee.completeTableModel.getRowCount();
      statusBarComplete.setText("Complete: " + Integer.toString(complete));
    }
    
    int total = waiting + running + complete;

    if (total != 0) {
      jProgressComplete.setValue((complete*100) / total);
    }
  }
}
