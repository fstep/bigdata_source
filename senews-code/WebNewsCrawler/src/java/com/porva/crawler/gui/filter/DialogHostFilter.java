/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui.filter;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import com.porva.crawler.filter.Filter;
import com.porva.crawler.filter.FilterFactory;
import com.porva.util.gui.JDialogStandart;

/**
 * GUI dialog to specify allowed hosts to construct {@link com.porva.crawler.filter.HostFilter}
 * object.
 */
class DialogHostFilter extends DialogDomainFilterGUI
{
  private static final long serialVersionUID = 2921950579785965207L;

  private Filter filter = null;

  private DefaultListModel listModel = null;

  private String[] allowedHosts;

  DialogHostFilter(Frame owner, Filter filter)
  {
    super(owner);
    super.setModal(true);
    super.setTitle("Host filter");
    this.filter = filter;
    if (this.filter == null)
      throw new IllegalArgumentException("filter");

    parseFilterConfigStr(this.filter.getConfigStr());

    labelListOfDomains.setText("List of allowed hosts:");
    labelAddBox.setText("Add host here");

    jTextFieldAdd.setDocument(new HostDocument());
    jListHosts.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    listModel = new DefaultListModel();
    for (String allowedHost : allowedHosts)
      listModel.addElement(allowedHost);
    jListHosts.setModel(listModel);

    pack();
    JDialogStandart.centerInParent(this);

    okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonOk_actionPerformed(e);
      }
    });
    getRootPane().setDefaultButton(okButton);

    cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonCancel_actionPerformed(e);
      }
    });
    AbstractAction cancelAction = new AbstractAction()
    {
      private static final long serialVersionUID = -6571333581511840527L;

      public void actionPerformed(ActionEvent e)
      {
        jButtonCancel_actionPerformed(e);
      }
    };
    JDialogStandart.addCancelByEscapeKey(this.getRootPane(), cancelAction);

    jButtonAdd.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonAdd_actionPerformed(e);
      }
    });

    jButtonRemove.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonRemove_actionPerformed(e);
      }
    });

    radioButtonAllow.setSelected(true);
    radioButtonAllow.setMnemonic(KeyEvent.VK_A);
    radioButtonDisallow.setEnabled(false);
  }

  private void parseFilterConfigStr(final String configStr)
  {
    if (configStr == null)
      throw new IllegalArgumentException("configStr cannot be null");

    allowedHosts = configStr.split(",");
  }

  public void jButtonRemove_actionPerformed(ActionEvent e)
  {
    Object selected = jListHosts.getSelectedValue();
    if (selected == null)
      return;
    String host = (String) selected;
    listModel.removeElement(selected);
    jTextFieldAdd.setText(host);
  }

  public void jButtonAdd_actionPerformed(ActionEvent e)
  {
    String hostToAdd = jTextFieldAdd.getText();
    if (hostToAdd == null || hostToAdd.length() == 0)
      return;
    listModel.addElement(hostToAdd);
    jTextFieldAdd.setText("");
  }

  public void jButtonOk_actionPerformed(ActionEvent e)
  {
    String[] allowedHosts = new String[jListHosts.getModel().getSize()];
    for (int i = 0; i < jListHosts.getModel().getSize(); i++) {
      allowedHosts[i] = (String)jListHosts.getModel().getElementAt(i);
    }

    try {
      FilterFactory.initHostFilter(filter, allowedHosts);
    } catch (Exception e1) {
      JOptionPane.showMessageDialog(this, e1.getMessage(), "HostFilter error",
                                    JOptionPane.ERROR_MESSAGE);
      return;
    }

    dispose();
  }

  public void jButtonCancel_actionPerformed(ActionEvent e)
  {
    dispose();
  }

  // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  class HostDocument extends PlainDocument
  {
    private static final long serialVersionUID = -8057858669198447628L;

    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException
    {
      if (str == null || str.equals(" ")) {
        return;
      }
      super.insertString(offs, str, a);
    }
  }

}
