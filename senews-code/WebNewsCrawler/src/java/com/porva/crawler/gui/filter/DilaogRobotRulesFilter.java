package com.porva.crawler.gui.filter;

import com.porva.crawler.filter.FileTypesFilter;
import com.porva.crawler.filter.RobotRulesFilter;
import com.porva.util.gui.JDialogStandart;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Feb 1, 2005
 * Time: 1:40:35 PM
 * To change this template use File | Settings | File Templates.
 */
class DilaogRobotRulesFilter extends DilaogRobotRulesFilterGUI
{

  private RobotRulesFilter filter = null;

  DilaogRobotRulesFilter(Frame owner, RobotRulesFilter filter)
  {
    super(owner);
    super.setModal(true);
    super.setTitle("Robot Rules filter");
    this.filter = filter;
    pack();
    JDialogStandart.centerInParent(this);

    textFieldDBName.setText(filter.getDbName());
    spinnerDefLifetime.setValue(new Integer(filter.getDefLifetime()));
    spinnerMinLifetime.setValue(new Integer(filter.getMinLifiteme()));

    okButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jButtonOk_actionPerformed(e);
      }
    });

    cancelButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jButtonCancel_actionPerformed(e);
      }
    });
  }

  public void jButtonOk_actionPerformed(ActionEvent e)
  {
    try {
      filter.initFilter(textFieldDBName.getText(),
              ((Integer)spinnerDefLifetime.getValue()).intValue(),
              ((Integer)spinnerMinLifetime.getValue()).intValue());
    } catch (Exception e1) {
      JOptionPane.showMessageDialog(this,
          e1.getMessage(),
          "Robot Rules filter error",
          JOptionPane.ERROR_MESSAGE);
      return;
    }

    dispose();
  }

  public void jButtonCancel_actionPerformed(ActionEvent e)
  {
    dispose();
  }


}
