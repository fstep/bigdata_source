package com.porva.crawler.gui.filter;

import com.porva.crawler.filter.AmountLimitFilter;
import com.porva.util.gui.JDialogStandart;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Feb 1, 2005
 * Time: 9:57:16 PM
 * To change this template use File | Settings | File Templates.
 */
class DialogAmountLimitFilter extends DialogAmountLimitFilterGUI
{
  private AmountLimitFilter filter = null;

  DialogAmountLimitFilter(Frame owner, AmountLimitFilter filter)
  {
    super(owner);
    super.setModal(true);
    super.setTitle("Amount Limit filter");
    this.filter = filter;

    spinnerTotalNum.setValue(new Integer(filter.getTotalNum()));
    spinnerPerHostNum.setValue(new Integer(filter.getPerHostNum()));

    pack();
    JDialogStandart.centerInParent(this);

    okButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jButtonOk_actionPerformed(e);
      }
    });

    cancelButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
         dispose();
      }
    });
  }

  public void jButtonOk_actionPerformed(ActionEvent e)
  {
    int totalNum = ((Integer)spinnerTotalNum.getValue()).intValue();
    int perHostNum = ((Integer)spinnerPerHostNum.getValue()).intValue();
    try {
      filter.initFilter(totalNum, perHostNum);
      //better to do smth like: filter = FiltersFactory.AMOUNT_FILTER.newInstance(totalNum, perHostNum);
    } catch(Exception e1) {
      JOptionPane.showMessageDialog(this,
          e1.getMessage(),
          "Amount Limit filter error",
          JOptionPane.ERROR_MESSAGE);
    }

    dispose();
  }
}
