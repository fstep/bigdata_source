/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import com.porva.crawler.net.CrawlerURL;
import com.porva.crawler.net.MalformedCrawlerURLException;
import com.porva.crawler.task.FetchRSSTask;
import com.porva.crawler.task.FetchURL;
import com.porva.crawler.task.GoogleTask;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskFactory;
import com.porva.util.gui.JDialogStandart;

/**
 * GUI dialog to edit {@link com.porva.crawler.task.Task} objects .<br>
 */
class DialogTaskEdit extends DialogAddEditTaskGUI
{
  private static final long serialVersionUID = 903930838923619269L;

  private JTable jTable = null;

  private int selected;

  /**
   * Allocates and shows a new {@link DialogTaskEdit} dialog.
   * 
   * @param owner parent frame.
   * @param jTable
   */
  public DialogTaskEdit(final Frame owner, JTable jTable)
  {
    super(owner);
    super.setTitle("Edit task");
    super.setModal(true);
    this.jTable = jTable;

    selected = jTable.getSelectedRow();
    if (selected == -1)
      return; // todo: dialog box: select task in table and press Edit button
    // Object ob = jTable.getValueAt(selected, 0);
    TaskTableModel model = (TaskTableModel) jTable.getModel();
    Task task = model.getData().get(selected);
    // if (!(ob instanceof Task)) {
    // System.err.println("Element in table is not Task but: " + ob); // todo: error
    // return;
    // }
    //
    // Task task = (Task) ob;
    if (task instanceof FetchURL) {
      FetchURL fetchTask = (FetchURL) task;
      jTextFieldURL.setText(fetchTask.getCrawlerURL().getURLString());
      jSpinnerDepthFetchTask.setValue(new Integer(fetchTask.getCrawlerURL().getDepth()));
      jTabbedPane.setSelectedComponent(panelTaskFetchTask);
    } else if (task instanceof GoogleTask) {
      GoogleTask googleTask = (GoogleTask) task;
      jTextFieldQuery.setText(googleTask.getQuery());
      jSpinnerDepthGoogleTask.setValue(new Integer(googleTask.getDepth()));
      jSpinnerHitsNum.setValue(new Integer(googleTask.getNum()));
      jTabbedPane.setSelectedComponent(panelTaskGoogleTask);
    } else if (task instanceof FetchRSSTask) {
      FetchRSSTask fetchRssTask = (FetchRSSTask)task;
      jTextFieldRSSURL.setText(fetchRssTask.getCrawlerURL().getURLString());
      jSpinnerDepthFetchRSSTask.setValue(fetchRssTask.getCrawlerURL().getDepth());
      jTabbedPane.setSelectedComponent(panelTaskFetchRssTask);
    }
      
    okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonOk_actionPerformed(e);
      }
    });
    getRootPane().setDefaultButton(okButton);

    cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonCancel_actionPerformed(e);
      }
    });

    AbstractAction cancelAction = new AbstractAction()
    {
      private static final long serialVersionUID = -6571333581511840527L;

      public void actionPerformed(ActionEvent e)
      {
        jButtonCancel_actionPerformed(e);
      }
    };
    JDialogStandart.addCancelByEscapeKey(this.getRootPane(), cancelAction);

    pack();
    JDialogStandart.centerInParent(this);
  }

  /**
   * Cancel button or 'escape' reaction.
   * 
   * @param e
   */
  public void jButtonCancel_actionPerformed(ActionEvent e)
  {
    this.dispose();
  }

  /**
   * Ok button reaction.
   * 
   * @param e
   */
  public void jButtonOk_actionPerformed(ActionEvent e)
  {
    if (selected == -1)
      return;

    try {
      Task task = null;
      AbstractTableModel tableModel = (AbstractTableModel) jTable.getModel();
      if (jTabbedPane.getSelectedComponent() == panelTaskFetchTask) {
        String url = jTextFieldURL.getText();
        int depth = ((Integer) jSpinnerDepthFetchTask.getValue()).intValue();
        task = TaskFactory.newFetchURL(new CrawlerURL(depth, url));
      } else if (jTabbedPane.getSelectedComponent() == panelTaskGoogleTask) {
        String query = jTextFieldQuery.getText();
        int depth = ((Integer) jSpinnerDepthGoogleTask.getValue()).intValue();
        int hitsNum = ((Integer) jSpinnerHitsNum.getValue()).intValue();
        task = TaskFactory.newGoogleTask(hitsNum, query, depth);
      } else if (jTabbedPane.getSelectedComponent() == panelTaskFetchRssTask) {
        String url = jTextFieldRSSURL.getText();
        int depth = ((Integer) jSpinnerDepthFetchRSSTask.getValue()).intValue();
        task = TaskFactory.newFetchRSS(new CrawlerURL(depth, url));
      }
      tableModel.setValueAt(task, selected, 0);
      tableModel.fireTableDataChanged();
    } catch (MalformedCrawlerURLException ex) {
      JOptionPane
          .showMessageDialog(this, ex.getMessage(), "Invalid url", JOptionPane.ERROR_MESSAGE);
      return;
    }

    this.dispose();
  }

}
