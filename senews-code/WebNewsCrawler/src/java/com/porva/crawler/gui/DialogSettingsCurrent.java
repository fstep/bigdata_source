/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import com.porva.crawler.CrawlerInit;
import com.porva.crawler.task.CITFormatReader;
import com.porva.crawler.task.CITFormatWriter;
import com.porva.crawler.task.Task;

/**
 * GUI dialog of current project settings.
 */
class DialogSettingsCurrent extends DialogSettings
{
  private static final long serialVersionUID = 9039990660979844638L;

  private boolean isNewProject = false;

  private MainFrame adaptee = null;

  public DialogSettingsCurrent(final MainFrame owner, boolean isNewProject)
  {
    super(owner, CrawlerInit.getProperties());
    super.setTitle("Project properties");
    adaptee = owner;
    this.isNewProject = isNewProject;

    jButtonAddTask.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonAddTask_actionPerformed(e);
      }
    });
    jButtonAddTask.setMnemonic(KeyEvent.VK_A);

    jButtonEditTask.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonEditTask_actionPerformed(e);
      }
    });
    jButtonEditTask.setMnemonic(KeyEvent.VK_E);

    jButtonRemoveTask.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonRemoveTask_actionPerformed(e);
      }
    });
    jButtonRemoveTask.setMnemonic(KeyEvent.VK_R);

    jTableTasks.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    ListSelectionModel rowSM = jTableTasks.getSelectionModel();
    rowSM.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        jTableTasks_selectionChanged(e);
      }
    });
    jButtonEditTask.setEnabled(false);
    jButtonRemoveTask.setEnabled(false);

  }

  protected void setProp(Properties prop)
  {
    super.setProp(prop);

    // init tasks table:
    CITFormatReader taskReader = new CITFormatReader(new File((String) CrawlerInit.getProperties()
        .get("workload.waiting.filename")));
    List<Task> tasks = new ArrayList<Task>();
    Task task;
    try {
      while ((task = taskReader.getNextTask()) != null) {
        tasks.add(task);
      }
    } catch (Exception e) {
      // todo
    }

    AbstractTableModel tasksModel = new TaskTableModel(tasks);
    jTableTasks.setModel(tasksModel);
    tasksModel.fireTableDataChanged();
  }

  public void jButtonOk_actionPerformed(ActionEvent e)
  {
    try {

      // save all project files
      CrawlerInit.getProperties().putAll(getProp());
      FileOutputStream fout = new FileOutputStream((String) CrawlerInit.getProperties()
          .get("project.file"));
      CrawlerInit.getProperties().store(fout, null);
      fout.close();

      String filename = (String) CrawlerInit.getProperties().get("workload.waiting.filename");
      TaskTableModel queueTableModel = (TaskTableModel) jTableTasks.getModel();
      CITFormatWriter.writeTasks(queueTableModel.getData(), filename);
    } catch (IOException e1) {
      JOptionPane.showMessageDialog(this, "Failed to save settings file", "Settings error",
                                    JOptionPane.ERROR_MESSAGE);
      return;
    }
//    if (isNewProject) {
      adaptee.openProject();
//    }
    this.dispose();
  }

  public void jButtonAddTask_actionPerformed(ActionEvent e)
  {
    DialogTaskAdd dialogTaskAdd = new DialogTaskAdd(adaptee, (AbstractTableModel) jTableTasks
        .getModel());
    dialogTaskAdd.setVisible(true);
  }

  public void jButtonEditTask_actionPerformed(ActionEvent e)
  {
    DialogTaskEdit dialogTaskAdd = new DialogTaskEdit(adaptee, jTableTasks);
    dialogTaskAdd.setVisible(true);
  }

  public void jButtonRemoveTask_actionPerformed(ActionEvent e)
  {
    int selected = jTableTasks.getSelectedRow();
    if (selected == -1)
      return;
    TaskTableModel queueTableModel = (TaskTableModel) jTableTasks.getModel(); // todo: chang to more
    // general type
    queueTableModel.removeElementAt(selected);
    queueTableModel.fireTableDataChanged();
  }

  // todo: up and down buttons
  public void jTableTasks_selectionChanged(ListSelectionEvent e)
  {
    ListSelectionModel lsm = (ListSelectionModel) e.getSource();
    if (lsm.isSelectionEmpty()) {
      jButtonEditTask.setEnabled(false);
      jButtonRemoveTask.setEnabled(false);
      // jButtonUpTask.setEnabled(false);
      // jButtonDownTask.setEnabled(false);
    } else {
      jButtonEditTask.setEnabled(true);
      jButtonRemoveTask.setEnabled(true);
      // if (lsm.getMinSelectionIndex() > 0)
      // jButtonUpTask.setEnabled(true);
      // else
      // jButtonUpTask.setEnabled(false);

      // todo:
      // if (lsm.getMinSelectionIndex() < jTable.getColumnCount() + 1)
      // jButtonDown.setEnabled(true);
      // else
      // jButtonDown.setEnabled(false);
    }
  }

}
