/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import java.util.Vector;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * Provides possibility to show log4j log for all events of application in GUI table "Common log".
 */

class CommonLogAppender extends Handler
{
  private JTable jTable;
  private CommonLogTableModel commonLogTableModel = new CommonLogTableModel();
  private boolean firstTime = true;
  private long startTime;

  //the singleton instance
  private static CommonLogAppender handler = null;

  /**
   * private constructor, preventing initialization
   */
  private CommonLogAppender(JTable jTableForLog)
  {
    this.jTable = jTableForLog;
    jTable.setModel(commonLogTableModel);

    TableColumn column = null;
    for (int i = 0; i < 3; i++) {
        column = jTable.getColumnModel().getColumn(i);
        if (i == 0) {
          //column.setPreferredWidth(100);
        } else if (i == 2) {
          column.setPreferredWidth(800);
        }
    }
  }

  /**
   * The getInstance method returns the singleton instance of the CommonLogAppender object.
   * It is synchronized to prevent two threads trying to create an instance simultaneously.
   * @param jTableForLog
   * @return
   */
  public static synchronized CommonLogAppender getInstance(JTable jTableForLog)
  {
    if (handler == null) {
      handler = new CommonLogAppender(jTableForLog);
    }
    return handler;
  }

  public synchronized void publish(LogRecord record)
  {
    if (firstTime) {
      startTime = System.currentTimeMillis();
      firstTime = false;
    }

    String log [] = new String [3];
    int leftTime = (int)(System.currentTimeMillis() - startTime);
    //int ms = leftTime % 1000;
    int sec = leftTime / 1000;
    int hours = sec / (60*60);
    int min = sec/60 - hours*60;
    sec = sec - min*60;

    // todo: customizable time view
    log[0] = Integer.toString(hours) + ":" + Integer.toString(min) + ":" + Integer.toString(sec);
        //"." + Integer.toString(ms);
    log[1]  = record.getLevel().toString();
    log[2]  = record.getMessage();

    commonLogTableModel.addLogMsg(log);

    // todo: check size of .logData and if it is more then some value, delate all
    // todo: previous log data

    jTable.changeSelection(commonLogTableModel.getRowCount() - 1, 0, true, true);
    commonLogTableModel.fireTableDataChanged();
  }

  public void close()
  {
  }

  public void flush()
  {
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class CommonLogTableModel extends AbstractTableModel
{
  private static final long serialVersionUID = -6340873686548365646L;
  private Vector<String[]> logData = new Vector<String[]>();
  protected String[] columnNames = {"Time", "Level", "Common log message"};
  public static final int NUM = 100; // todo: from app init file

  public int getColumnCount()
  {
    return columnNames.length;
  }

  public int getRowCount()
  {
    return logData.size();
  }

  public Object getValueAt(int rowIndex, int columnIndex)
  {
    return ((String[])logData.elementAt(rowIndex))[columnIndex];
  }

  public String getColumnName(int col)
  {
      return columnNames[col];
  }

  public void addLogMsg(String[] logMsg)
  {
    if (logData.size() > NUM)
      logData.remove(0);
    logData.add(logMsg);
  }

  public void clear()
  {
    logData.clear();
  }
  
  /*
  * Don't need to implement this method unless your table's
  * data can change.
  */
  /*public void setValueAt(Object value, int row, int col)
  {
    ((String[])logData.elementAt(row))[col] = (String)value;
    fireTableCellUpdated(row, col);
  }*/

  /*public Class getColumnClass(int c)
  {
    return getValueAt(0, c).getClass();
  } */

}