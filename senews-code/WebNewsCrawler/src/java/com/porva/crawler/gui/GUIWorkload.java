/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.db.CrawlerDBFactory;
import com.porva.crawler.db.DBException;
import com.porva.crawler.db.DBFunctor;
import com.porva.crawler.task.FetchTask;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskFactory;
import com.porva.crawler.task.TaskResult;
import com.porva.crawler.workload.DBWorkload;
import com.porva.crawler.workload.WorkloadEntryStatus;

public class GUIWorkload extends DBWorkload
{
  private List<String> waiting = new ArrayList<String>();
  private List<String> running = new ArrayList<String>();
  private List<String> complete = new ArrayList<String>();
  
  public GUIWorkload(CrawlerDBFactory dbFactory) throws DBException
  {
    super(dbFactory);

    CountFunctor countFunctor = new CountFunctor();
    urlStatusDB.iterateOverAll(countFunctor);
  }
  
  public Task getTask()
  {
    Task task = super.getTask();
    if (task != TaskFactory.newEmptyTask() && task != TaskFactory.newLastTask()) {
      waiting.remove(getTaskStr(task));
      running.add(getTaskStr(task));
    }
    return task;
  }
  
  public boolean addTask(final Task task)
  {
    boolean isAdded = super.addTask(task);
    if (isAdded)
      waiting.add(getTaskStr(task));
    return isAdded;
  }
  
  public boolean completeTask(final Task task, final TaskResult result)
  {
    boolean isCompleted = super.completeTask(task, result);
    if (isCompleted) {
      running.remove(getTaskStr(task));
      complete.add(getTaskStr(task));
    }
    return isCompleted;
  }
  
  public List<String> getWaiting()
  {
    return waiting;
  }
  
  public List<String> getRunning()
  {
    return running;
  }

  public List<String> getComplete()
  {
    return complete;
  }

  class CountFunctor implements DBFunctor<WorkloadEntryStatus>
  {
      public void process(final String key, final WorkloadEntryStatus value)
    {
      if (key == null)
        throw new NullArgumentException("key");
      if (value == WorkloadEntryStatus.COMPLETE)
        complete.add(key);
      else if (value == WorkloadEntryStatus.FAILED)
        complete.add(key);
      else if (value == WorkloadEntryStatus.WAITING)
        waiting.add(key);
      else if (value == WorkloadEntryStatus.RUNNING)
        running.add(key);
//      else if (value == WorkloadEntryStatus.UNKNOWN)
//        unknownNum++;
    }
  };
  
  
  private String getTaskStr(Task task)
  {
    assert task != null;
    
    if (task instanceof FetchTask) {
      FetchTask fetchTask = (FetchTask)task;
      return fetchTask.getCrawlerURL().getURLString();
    }
    return task.toString();
  }
  

}
