/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

import com.porva.util.ResourceLoader;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Tool bar of {@link MainFrame} frame. 
 */

class MainFrame_ToolBar extends JToolBar
{
  private static final long serialVersionUID = 27883566828496252L;

  MainFrame adaptee;

  JButton jButtonNewProject = new JButton();
  JButton jButtonOpenProject = new JButton();
  JButton jButtonExportProject = new JButton();
  JButton jButtonProjectSettings = new JButton();
  JButton jButtonPlay = new JButton();
  JButton jButtonPause = new JButton();
  JButton jButtonStop = new JButton();

  public MainFrame_ToolBar(MainFrame adaptee)
  {
    this.adaptee = adaptee;

    this.add(jButtonNewProject);
    this.add(jButtonOpenProject);
    this.add(jButtonExportProject);
    this.addSeparator();
    this.add(jButtonProjectSettings);
    this.addSeparator();
    this.add(jButtonPlay);
    this.add(jButtonPause);
    this.add(jButtonStop);

    jButtonExportProject.setEnabled(false);
    jButtonPlay.setEnabled(false);
    jButtonPause.setEnabled(false);
    jButtonStop.setEnabled(false);
    jButtonProjectSettings.setEnabled(false);

    jButtonNewProject.setToolTipText("New Project Wizard...");
    jButtonNewProject.setIcon(ResourceLoader.getIcon("Menu.File.NewProject"));
    jButtonNewProject.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        jButtonNewProject_actionPerformed(e);
      }
    });

    jButtonOpenProject.setToolTipText("Open Project...");
    jButtonOpenProject.setIcon(ResourceLoader.getIcon("Menu.File.OpenProject"));
    jButtonOpenProject.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        jButtonOpenProject_actionPerformed(e);
      }
    });

    jButtonExportProject.setToolTipText("Export Project...");
    jButtonExportProject.setIcon(ResourceLoader.getIcon("Menu.File.Export"));
    jButtonExportProject.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        jButtonExportProject_actionPerformed(e);
      }
    });

    jButtonProjectSettings.setToolTipText("Project Settings...");
    jButtonProjectSettings.setIcon(ResourceLoader.getIcon("Menu.Settings.ProjectSettings"));
    jButtonProjectSettings.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        jButtonProjectSettings_actionPerformed(e);
      }
    });

    // Download -> Start
    jButtonPlay.setToolTipText("Start");
    jButtonPlay.setIcon(ResourceLoader.getIcon("Menu.Download.Start"));
    jButtonPlay.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        jButtonPlay_actionPerformed(e);
      }
    });

    // Download -> Pause
    jButtonPause.setToolTipText("Pause");
    jButtonPause.setIcon(ResourceLoader.getIcon("Menu.Download.Pause"));
    jButtonPause.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        jButtonPause_actionPerformed(e);
      }
    });

    // Download -> Suspend
    jButtonStop.setToolTipText("Suspend");
    jButtonStop.setIcon(ResourceLoader.getIcon("Menu.Download.Suspend"));
    jButtonStop.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        jButtonStop_actionPerformed(e);
      }
    });


  }

  public void jButtonNewProject_actionPerformed(ActionEvent e)
  {
    adaptee.actionNewProject(e);
  }

  public void jButtonOpenProject_actionPerformed(ActionEvent e)
  {
    adaptee.actionFileOpenProject(e);
  }

  public void jButtonExportProject_actionPerformed(ActionEvent e)
  {
    adaptee.actionExportProject(e);
  }

  public void jButtonProjectSettings_actionPerformed(ActionEvent e)
  {
    adaptee.actionProjectSettings(e);
  }

  public void jButtonPlay_actionPerformed(ActionEvent e)
  {
    if (jButtonPlay.getToolTipText().equals("Start")) {
      adaptee.actionDownloadStart(e);
    } else if (jButtonPlay.getToolTipText().equals("Resume")) {
      adaptee.actionDownloadResume(e);
    }

  }

  public void jButtonPause_actionPerformed(ActionEvent e)
  {
    adaptee.actionDownloadPause(e);
  }

  public void jButtonStop_actionPerformed(ActionEvent e)
  {
    adaptee.actionDownloadSuspend(e);
  }



}
