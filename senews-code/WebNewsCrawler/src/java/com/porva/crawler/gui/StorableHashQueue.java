/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

import java.util.Hashtable;
import java.util.Vector;
import java.util.logging.Logger;

/**
 * Task:
 * Author:
 * Date: Sep 10, 2004
 * Time: 12:17:51 PM
 */
class StorableHashQueue
{
  private Vector<String> vector = new Vector<String>();
  private Hashtable<String,Object> hash = new Hashtable<String,Object>();
  private Logger logger = Logger.getLogger("com.porva.crawler.gui.StorableHashQueue");

  public Object elementAt(int index)
  {
    return vector.elementAt(index);
  }

  public int size()
  {
    return vector.size();
  }

  public void put(String key, Object  value)
  {
    vector.addElement(key);
    hash.put(key, value);
  }

  public Object get(String key)
  {
    return hash.get(key);
  }

  public boolean isEmpty()
  {
    if (hash.isEmpty() && vector.isEmpty())
      return true;
    if (!hash.isEmpty() && !vector.isEmpty())
      return false;

    logger.warning("isEmpty error!");
    return false;
  }

  public Object firstElement()
  {
    try {
      String key = (String) vector.firstElement();
      return hash.get(key);
    }  catch (Exception e) {
      return null;
    }
  }

  public void remove(String key)
  {
    hash.remove(key);
    try {
      vector.removeElement(key);
    } catch (Exception e) {
      logger.warning("No element to remove: element key " +  key);
    }
  }

  public void clear()
  {
    vector.clear();;
    hash.clear();
    //todo: clear db as  well
  }

  public void store(String dbname)
  {
    //todo:
  }

  public void load(String filename)
  {
    /*try {
      BufferedReader fileReader = new BufferedReader(new FileReader(filename));
      String task = null;
      while ((task = fileReader.readLine()) != null) {
        taskVector.add(new FetchTask(task, 0));
        this.put(waitingTask.getUID(), waitingTask);
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
    } catch (IOException e) {
      e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
    }

    CrawlerWorkerTask[] initialTasks = new CrawlerWorkerTask[taskVector.size()];
    for (int i = 0; i < taskVector.size(); i++) {
      initialTasks[i] = (CrawlerWorkerTask)taskVector.get(i);
    }*/

  }
}
