/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

import com.porva.util.gui.TabbedPaneWithCloseButtons;

import javax.swing.*;
import javax.swing.border.SoftBevelBorder;
import java.awt.*;
/*
 * Created by JFormDesigner on Sun Feb 06 11:46:38 EET 2005
 */



/**
 * @author porva porva
 */
class MainFrameGUI extends JFrame {
  private static final long serialVersionUID = 8621392500709937187L;
  public MainFrameGUI() {
		initComponents();
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		// Generated using JFormDesigner Evaluation license
		jSplitPane1 = new JSplitPane();
		jSplitPane2 = new JSplitPane();
		jQueueTabbedPane = new JTabbedPane();
		jWaitingScrollPane = new JScrollPane();
		jWaitingTable = new JTable();
		jRunningScrollPane = new JScrollPane();
		jRunningTable = new JTable();
		jCompleteScrollPane = new JScrollPane();
		jCompleteTable = new JTable();
		jPluginsTabbedPane = new TabbedPaneWithCloseButtons();
		jLogTabbedPane = new JTabbedPane();
		jCommonLogScrollPane = new JScrollPane();
		jCommonLogTable = new JTable();
		jThreadLogScrollPane = new JScrollPane();
		jThreadLogTable = new JTable();

		//======== this ========
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());

		//======== jSplitPane1 ========
		{
			jSplitPane1.setBorder(new SoftBevelBorder(SoftBevelBorder.LOWERED));
			jSplitPane1.setOrientation(JSplitPane.VERTICAL_SPLIT);
			
			//======== jSplitPane2 ========
			{
				
				//======== jQueueTabbedPane ========
				{
					
					//======== jWaitingScrollPane ========
					{
						jWaitingScrollPane.setViewportView(jWaitingTable);
					}
					jQueueTabbedPane.addTab("Waiting", jWaitingScrollPane);
					
					//======== jRunningScrollPane ========
					{
						jRunningScrollPane.setViewportView(jRunningTable);
					}
					jQueueTabbedPane.addTab("Running", jRunningScrollPane);
					
					//======== jCompleteScrollPane ========
					{
						jCompleteScrollPane.setViewportView(jCompleteTable);
					}
					jQueueTabbedPane.addTab("Complete", jCompleteScrollPane);
				}
				jSplitPane2.setLeftComponent(jQueueTabbedPane);
				jSplitPane2.setRightComponent(jPluginsTabbedPane);
			}
			jSplitPane1.setLeftComponent(jSplitPane2);
			
			//======== jLogTabbedPane ========
			{
				
				//======== jCommonLogScrollPane ========
				{
					jCommonLogScrollPane.setViewportView(jCommonLogTable);
				}
				jLogTabbedPane.addTab("Common Log", jCommonLogScrollPane);
				
				//======== jThreadLogScrollPane ========
				{
					jThreadLogScrollPane.setViewportView(jThreadLogTable);
				}
				jLogTabbedPane.addTab("Thread Log", jThreadLogScrollPane);
			}
			jSplitPane1.setRightComponent(jLogTabbedPane);
		}
		contentPane.add(jSplitPane1, BorderLayout.CENTER);
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	// Generated using JFormDesigner Evaluation license
	JSplitPane jSplitPane1;
	JSplitPane jSplitPane2;
	JTabbedPane jQueueTabbedPane;
	private JScrollPane jWaitingScrollPane;
	JTable jWaitingTable;
	private JScrollPane jRunningScrollPane;
	JTable jRunningTable;
	private JScrollPane jCompleteScrollPane;
	JTable jCompleteTable;
	public JTabbedPane jPluginsTabbedPane;
	JTabbedPane jLogTabbedPane;
	private JScrollPane jCommonLogScrollPane;
	JTable jCommonLogTable;
	private JScrollPane jThreadLogScrollPane;
	JTable jThreadLogTable;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
