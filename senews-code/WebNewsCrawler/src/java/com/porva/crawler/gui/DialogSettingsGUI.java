/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

import java.awt.*;
import javax.swing.*;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
/*
 * Created by JFormDesigner on Thu Feb 16 17:56:32 EET 2006
 */



/**
 * @author evaluator evaluator
 */
public class DialogSettingsGUI extends JDialog {
  private static final long serialVersionUID = 1200688874314371394L;
  public DialogSettingsGUI(Frame owner) {
		super(owner);
		initComponents();
	}

	public DialogSettingsGUI(Dialog owner) {
		super(owner);
		initComponents();
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		// Generated using JFormDesigner Evaluation license - evaluator evaluator
		DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
		dialogPane = new JPanel();
		contentPane = new JPanel();
		jTabbedPane = new JTabbedPane();
		jPanelTasksFilters = new JPanel();
		scrollPane1 = new JScrollPane();
		jTableTasks = new JTable();
		jButtonAddTask = new JButton();
		jButtonEditTask = new JButton();
		jButtonRemoveTask = new JButton();
		jPanelFilters = new JPanel();
		scrollPaneFilters2 = new JScrollPane();
		tableFilters = new JTable();
		scrollPaneParsers = new JScrollPane();
		tableParsers = new JTable();
		jButtonEditFilter = new JButton();
		labelHelp = new JLabel();
		jPanelCrawler = new JPanel();
		goodiesFormsSeparator5 = compFactory.createSeparator("General crawler settings:");
		label12 = new JLabel();
		jSpinnerDepthLimit = new JSpinner();
		label14 = new JLabel();
		jSpinnerdFetcherThreads = new JSpinner();
		label15 = new JLabel();
		jSpinnerLifetime = new JSpinner();
		label17 = new JLabel();
		jComboBoxOrderAlgorithm = new JComboBox();
		goodiesFormsSeparator6 = compFactory.createSeparator("Crawling polliteness:");
		label16 = new JLabel();
		label13 = new JLabel();
		jSpinnerServerThreads = new JSpinner();
		jSpinnerServerDelay = new JSpinner();
		goodiesFormsSeparator7 = compFactory.createSeparator("Project paths:");
		panel4 = new JPanel();
		label21 = new JLabel();
		jTextFieldProjectName = new JTextField();
		label18 = new JLabel();
		jTextFieldProjectFile = new JTextField();
		label19 = new JLabel();
		jTextFieldDbDir = new JTextField();
		label20 = new JLabel();
		jTextFieldWaitingFile = new JTextField();
		jPanelHttp = new JPanel();
		goodiesFormsSeparator3 = compFactory.createSeparator("HTTP User Agent");
		label1 = new JLabel();
		jComboBoxPredefinedAgents = new JComboBox();
		label2 = new JLabel();
		jTextFieldName = new JTextField();
		label3 = new JLabel();
		jTextFieldRobotsAgents = new JTextField();
		label4 = new JLabel();
		jTextFieldDescription = new JTextField();
		label5 = new JLabel();
		jTextFieldUrl = new JTextField();
		label6 = new JLabel();
		jTextFieldMail = new JTextField();
		label7 = new JLabel();
		jTextFieldVersion = new JTextField();
		goodiesFormsSeparator4 = compFactory.createSeparator("HTTP connection settings");
		label8 = new JLabel();
		jSpinnerHttpTimeout = new JSpinner();
		label9 = new JLabel();
		jSpinnerContentLimit = new JSpinner();
		label10 = new JLabel();
		jTextFieldProxy = new JTextField();
		label11 = new JLabel();
		jCheckBoxIsHttp11 = new JCheckBox();
		jSpinnerMaxRedirects = new JSpinner();
		buttonBar = new JPanel();
		okButton = new JButton();
		cancelButton = new JButton();
		CellConstraints cc = new CellConstraints();

		//======== this ========
		setResizable(true);
		Container contentPane2 = getContentPane();
		contentPane2.setLayout(new BorderLayout());

		//======== dialogPane ========
		{
			dialogPane.setBorder(Borders.DIALOG_BORDER);
			
			// JFormDesigner evaluation mark
			
			dialogPane.setLayout(new BorderLayout());
			
			//======== contentPane ========
			{
				contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
				
				//======== jTabbedPane ========
				{
					jTabbedPane.setFocusTraversalPolicyProvider(false);
					
					//======== jPanelTasksFilters ========
					{
						jPanelTasksFilters.setBorder(Borders.DLU4_BORDER);
						jPanelTasksFilters.setLayout(new FormLayout(
							new ColumnSpec[] {
								FormFactory.GLUE_COLSPEC,
								FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
								new ColumnSpec(Sizes.dluX(45))
							},
							new RowSpec[] {
								new RowSpec("max(default;5dlu)"),
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								new RowSpec("max(default;12dlu)"),
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.GLUE_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								new RowSpec("max(default;10dlu)")
							}));
						
						//======== scrollPane1 ========
						{
							
							//---- jTableTasks ----
							jTableTasks.setPreferredScrollableViewportSize(new Dimension(200, 200));
							scrollPane1.setViewportView(jTableTasks);
						}
						jPanelTasksFilters.add(scrollPane1, cc.xywh(1, 3, 1, 21));
						
						//---- jButtonAddTask ----
						jButtonAddTask.setText("Add...");
						jPanelTasksFilters.add(jButtonAddTask, cc.xy(3, 3));
						
						//---- jButtonEditTask ----
						jButtonEditTask.setText("Edit...");
						jPanelTasksFilters.add(jButtonEditTask, cc.xy(3, 5));
						
						//---- jButtonRemoveTask ----
						jButtonRemoveTask.setText("Remove");
						jPanelTasksFilters.add(jButtonRemoveTask, cc.xy(3, 7));
					}
					jTabbedPane.addTab("Tasks", jPanelTasksFilters);
					
					
					//======== jPanelFilters ========
					{
						jPanelFilters.setBorder(Borders.DLU4_BORDER);
						jPanelFilters.setLayout(new FormLayout(
							new ColumnSpec[] {
								FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
								FormFactory.DEFAULT_COLSPEC,
								FormFactory.GLUE_COLSPEC,
								FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
								new ColumnSpec(Sizes.dluX(45))
							},
							new RowSpec[] {
								new RowSpec("max(default;5dlu)"),
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.GLUE_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.GLUE_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								new RowSpec("max(default;40dlu)"),
								FormFactory.LINE_GAP_ROWSPEC,
								new RowSpec("max(default;10dlu)")
							}));
						
						//======== scrollPaneFilters2 ========
						{
							
							//---- tableFilters ----
							tableFilters.setPreferredScrollableViewportSize(new Dimension(200, 200));
							scrollPaneFilters2.setViewportView(tableFilters);
						}
						jPanelFilters.add(scrollPaneFilters2, cc.xywh(2, 3, 4, 1));
						
						//======== scrollPaneParsers ========
						{
							scrollPaneParsers.setViewportView(tableParsers);
						}
						jPanelFilters.add(scrollPaneParsers, cc.xywh(2, 6, 4, 2));
						
						//---- jButtonEditFilter ----
						jButtonEditFilter.setText("Edit...");
						jPanelFilters.add(jButtonEditFilter, cc.xy(5, 9));
						
						//---- labelHelp ----
						labelHelp.setText("Help text here");
						jPanelFilters.add(labelHelp, cc.xywh(3, 11, 3, 1));
					}
					jTabbedPane.addTab("Filters", jPanelFilters);
					
					
					//======== jPanelCrawler ========
					{
						jPanelCrawler.setBorder(Borders.DLU4_BORDER);
						jPanelCrawler.setLayout(new FormLayout(
							new ColumnSpec[] {
								FormFactory.GLUE_COLSPEC,
								FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
								new ColumnSpec(Sizes.dluX(40))
							},
							new RowSpec[] {
								new RowSpec("max(default;5dlu)"),
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								new RowSpec("max(default;5dlu)"),
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								new RowSpec("max(default;5dlu)"),
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC
							}));
						jPanelCrawler.add(goodiesFormsSeparator5, cc.xywh(1, 3, 3, 1));
						
						//---- label12 ----
						label12.setText("Max. depth of links to follow:");
						jPanelCrawler.add(label12, cc.xy(1, 5));
						
						//---- jSpinnerDepthLimit ----
						jSpinnerDepthLimit.setFont(new Font("Tahoma", Font.PLAIN, 14));
						jPanelCrawler.add(jSpinnerDepthLimit, cc.xy(3, 5));
						
						//---- label14 ----
						label14.setText("Num. of fetchers threads:");
						jPanelCrawler.add(label14, cc.xy(1, 7));
						
						//---- jSpinnerdFetcherThreads ----
						jSpinnerdFetcherThreads.setModel(new SpinnerNumberModel(1, 1, 1000, 1));
						jSpinnerdFetcherThreads.setFont(new Font("Tahoma", Font.PLAIN, 14));
						jPanelCrawler.add(jSpinnerdFetcherThreads, cc.xy(3, 7));
						
						//---- label15 ----
						label15.setText("Lifetime of downloaded content (ms):");
						jPanelCrawler.add(label15, cc.xy(1, 9));
						
						//---- jSpinnerLifetime ----
						jSpinnerLifetime.setFont(new Font("Tahoma", Font.PLAIN, 14));
						jPanelCrawler.add(jSpinnerLifetime, cc.xy(3, 9));
						
						//---- label17 ----
						label17.setText("Selection order for waiting tasks:");
						jPanelCrawler.add(label17, cc.xy(1, 11));
						jPanelCrawler.add(jComboBoxOrderAlgorithm, cc.xywh(3, 11, 1, 1, CellConstraints.DEFAULT, CellConstraints.FILL));
						jPanelCrawler.add(goodiesFormsSeparator6, cc.xywh(1, 15, 3, 1));
						
						//---- label16 ----
						label16.setText("Delay to aceess one server:");
						jPanelCrawler.add(label16, cc.xy(1, 17));
						
						//---- label13 ----
						label13.setText("Num. of simultaneous acesses to one server:");
						jPanelCrawler.add(label13, cc.xy(1, 19));
						
						//---- jSpinnerServerThreads ----
						jSpinnerServerThreads.setModel(new SpinnerNumberModel(0, 0, 1000, 1));
						jSpinnerServerThreads.setFont(new Font("Tahoma", Font.PLAIN, 14));
						jPanelCrawler.add(jSpinnerServerThreads, cc.xy(3, 19));
						
						//---- jSpinnerServerDelay ----
						jSpinnerServerDelay.setModel(new SpinnerNumberModel(0, 0, 1000000, 100));
						jSpinnerServerDelay.setFont(new Font("Tahoma", Font.PLAIN, 14));
						jPanelCrawler.add(jSpinnerServerDelay, cc.xy(3, 17));
						jPanelCrawler.add(goodiesFormsSeparator7, cc.xywh(1, 23, 3, 1));
						
						//======== panel4 ========
						{
							panel4.setLayout(new FormLayout(
								new ColumnSpec[] {
									FormFactory.GLUE_COLSPEC,
									new ColumnSpec("max(default;143dlu)")
								},
								new RowSpec[] {
									FormFactory.DEFAULT_ROWSPEC,
									FormFactory.LINE_GAP_ROWSPEC,
									FormFactory.DEFAULT_ROWSPEC,
									FormFactory.LINE_GAP_ROWSPEC,
									FormFactory.DEFAULT_ROWSPEC,
									FormFactory.LINE_GAP_ROWSPEC,
									FormFactory.DEFAULT_ROWSPEC
								}));
							
							//---- label21 ----
							label21.setText("Name:");
							panel4.add(label21, cc.xy(1, 1));
							panel4.add(jTextFieldProjectName, cc.xy(2, 1));
							
							//---- label18 ----
							label18.setText("Config file:");
							panel4.add(label18, cc.xy(1, 3));
							panel4.add(jTextFieldProjectFile, cc.xy(2, 3));
							
							//---- label19 ----
							label19.setText("DB dir:");
							panel4.add(label19, cc.xy(1, 5));
							panel4.add(jTextFieldDbDir, cc.xy(2, 5));
							
							//---- label20 ----
							label20.setText("Waiting file:");
							panel4.add(label20, cc.xy(1, 7));
							panel4.add(jTextFieldWaitingFile, cc.xy(2, 7));
						}
						jPanelCrawler.add(panel4, cc.xywh(1, 25, 3, 1));
					}
					jTabbedPane.addTab("Crawler", jPanelCrawler);
					
					
					//======== jPanelHttp ========
					{
						jPanelHttp.setBorder(Borders.DLU4_BORDER);
						jPanelHttp.setLayout(new FormLayout(
							new ColumnSpec[] {
								FormFactory.GLUE_COLSPEC,
								FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
								new ColumnSpec(Sizes.dluX(105))
							},
							new RowSpec[] {
								new RowSpec("max(default;5dlu)"),
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								new RowSpec("max(default;5dlu)"),
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								new RowSpec("max(default;12dlu)"),
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC
							}));
						jPanelHttp.add(goodiesFormsSeparator3, cc.xywh(1, 3, 3, 1));
						
						//---- label1 ----
						label1.setText("Select HTTP agent:");
						jPanelHttp.add(label1, cc.xy(1, 5));
						jPanelHttp.add(jComboBoxPredefinedAgents, cc.xywh(3, 5, 1, 1, CellConstraints.DEFAULT, CellConstraints.FILL));
						
						//---- label2 ----
						label2.setText("Agent name:");
						jPanelHttp.add(label2, cc.xy(1, 7));
						jPanelHttp.add(jTextFieldName, cc.xy(3, 7));
						
						//---- label3 ----
						label3.setText("Robots.txt agents:");
						jPanelHttp.add(label3, cc.xy(1, 9));
						jPanelHttp.add(jTextFieldRobotsAgents, cc.xy(3, 9));
						
						//---- label4 ----
						label4.setText("Agent description:");
						jPanelHttp.add(label4, cc.xy(1, 11));
						jPanelHttp.add(jTextFieldDescription, cc.xy(3, 11));
						
						//---- label5 ----
						label5.setText("Agent URL:");
						jPanelHttp.add(label5, cc.xy(1, 13));
						jPanelHttp.add(jTextFieldUrl, cc.xy(3, 13));
						
						//---- label6 ----
						label6.setText("Agent e-mail:");
						jPanelHttp.add(label6, cc.xy(1, 15));
						jPanelHttp.add(jTextFieldMail, cc.xy(3, 15));
						
						//---- label7 ----
						label7.setText("Agent version:");
						jPanelHttp.add(label7, cc.xy(1, 17));
						jPanelHttp.add(jTextFieldVersion, cc.xy(3, 17));
						jPanelHttp.add(goodiesFormsSeparator4, cc.xywh(1, 21, 3, 1));
						
						//---- label8 ----
						label8.setText("HTTP timeout (ms):");
						jPanelHttp.add(label8, cc.xy(1, 23));
						
						//---- jSpinnerHttpTimeout ----
						jSpinnerHttpTimeout.setModel(new SpinnerNumberModel(700, 0, 1000000, 100));
						jSpinnerHttpTimeout.setFont(new Font("Tahoma", Font.PLAIN, 14));
						jPanelHttp.add(jSpinnerHttpTimeout, cc.xywh(3, 23, 1, 1, CellConstraints.DEFAULT, CellConstraints.FILL));
						
						//---- label9 ----
						label9.setText("Content limit (bytes):");
						jPanelHttp.add(label9, cc.xy(1, 25));
						
						//---- jSpinnerContentLimit ----
						jSpinnerContentLimit.setModel(new SpinnerNumberModel(600, 0, 100000000, 100));
						jSpinnerContentLimit.setFont(new Font("Tahoma", Font.PLAIN, 14));
						jPanelHttp.add(jSpinnerContentLimit, cc.xywh(3, 25, 1, 1, CellConstraints.DEFAULT, CellConstraints.FILL));
						
						//---- label10 ----
						label10.setText("Proxy (host:port):");
						jPanelHttp.add(label10, cc.xy(1, 27));
						jPanelHttp.add(jTextFieldProxy, cc.xywh(3, 27, 1, 1, CellConstraints.DEFAULT, CellConstraints.FILL));
						
						//---- label11 ----
						label11.setText("Max. redirects:");
						jPanelHttp.add(label11, cc.xy(1, 29));
						
						//---- jCheckBoxIsHttp11 ----
						jCheckBoxIsHttp11.setText("Use HTTP 1.1");
						jCheckBoxIsHttp11.setHorizontalAlignment(SwingConstants.TRAILING);
						jPanelHttp.add(jCheckBoxIsHttp11, cc.xywh(1, 31, 3, 1));
						
						//---- jSpinnerMaxRedirects ----
						jSpinnerMaxRedirects.setModel(new SpinnerNumberModel(25, 0, 100, 1));
						jSpinnerMaxRedirects.setFont(new Font("Tahoma", Font.PLAIN, 14));
						jPanelHttp.add(jSpinnerMaxRedirects, cc.xywh(3, 29, 1, 1, CellConstraints.LEFT, CellConstraints.FILL));
					}
					jTabbedPane.addTab("HTTP", jPanelHttp);
					
				}
				contentPane.add(jTabbedPane);
			}
			dialogPane.add(contentPane, BorderLayout.CENTER);
			
			//======== buttonBar ========
			{
				buttonBar.setBorder(Borders.BUTTON_BAR_GAP_BORDER);
				buttonBar.setLayout(new FormLayout(
					new ColumnSpec[] {
						FormFactory.GLUE_COLSPEC,
						FormFactory.BUTTON_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC,
						FormFactory.BUTTON_COLSPEC
					},
					RowSpec.decodeSpecs("pref")));
				
				//---- okButton ----
				okButton.setText("OK");
				buttonBar.add(okButton, cc.xy(2, 1));
				
				//---- cancelButton ----
				cancelButton.setText("Cancel");
				buttonBar.add(cancelButton, cc.xy(4, 1));
			}
			dialogPane.add(buttonBar, BorderLayout.SOUTH);
		}
		contentPane2.add(dialogPane, BorderLayout.CENTER);
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	// Generated using JFormDesigner Evaluation license - evaluator evaluator
	private JPanel dialogPane;
	private JPanel contentPane;
	JTabbedPane jTabbedPane;
	private JPanel jPanelTasksFilters;
	private JScrollPane scrollPane1;
	JTable jTableTasks;
	JButton jButtonAddTask;
	JButton jButtonEditTask;
	JButton jButtonRemoveTask;
	private JPanel jPanelFilters;
	private JScrollPane scrollPaneFilters2;
	JTable tableFilters;
	private JScrollPane scrollPaneParsers;
	JTable tableParsers;
	JButton jButtonEditFilter;
	JLabel labelHelp;
	private JPanel jPanelCrawler;
	private JComponent goodiesFormsSeparator5;
	private JLabel label12;
	JSpinner jSpinnerDepthLimit;
	private JLabel label14;
	JSpinner jSpinnerdFetcherThreads;
	private JLabel label15;
	JSpinner jSpinnerLifetime;
	private JLabel label17;
	JComboBox jComboBoxOrderAlgorithm;
	private JComponent goodiesFormsSeparator6;
	private JLabel label16;
	private JLabel label13;
	JSpinner jSpinnerServerThreads;
	JSpinner jSpinnerServerDelay;
	private JComponent goodiesFormsSeparator7;
	private JPanel panel4;
	private JLabel label21;
	JTextField jTextFieldProjectName;
	private JLabel label18;
	JTextField jTextFieldProjectFile;
	private JLabel label19;
	JTextField jTextFieldDbDir;
	private JLabel label20;
	JTextField jTextFieldWaitingFile;
	private JPanel jPanelHttp;
	private JComponent goodiesFormsSeparator3;
	private JLabel label1;
	JComboBox jComboBoxPredefinedAgents;
	private JLabel label2;
	JTextField jTextFieldName;
	private JLabel label3;
	JTextField jTextFieldRobotsAgents;
	private JLabel label4;
	JTextField jTextFieldDescription;
	private JLabel label5;
	JTextField jTextFieldUrl;
	private JLabel label6;
	JTextField jTextFieldMail;
	private JLabel label7;
	JTextField jTextFieldVersion;
	private JComponent goodiesFormsSeparator4;
	private JLabel label8;
	JSpinner jSpinnerHttpTimeout;
	private JLabel label9;
	JSpinner jSpinnerContentLimit;
	private JLabel label10;
	JTextField jTextFieldProxy;
	private JLabel label11;
	JCheckBox jCheckBoxIsHttp11;
	JSpinner jSpinnerMaxRedirects;
	private JPanel buttonBar;
	JButton okButton;
	JButton cancelButton;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
