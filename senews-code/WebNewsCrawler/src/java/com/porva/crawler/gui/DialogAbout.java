/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;

import com.porva.util.ResourceLoader;
import com.porva.util.gui.JDialogStandart;

/**
 * About dialog box.
 */
class DialogAbout extends DialogAboutGUI
{
  
  private static final long serialVersionUID = 7503380346760914915L;

  /**
   * Constructs and show a new about dialog box.
   * 
   * @param parent parent frame.
   */
  public DialogAbout(final Frame parent)
  {
    super(parent);
    super.setModal(true);
    super.setTitle("About");
    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    pack();
    JDialogStandart.centerInParent(this);

    this.labelLogo.setIcon(ResourceLoader.getIcon("Logo"));
    this.labelLogo.setText("");
    this.labelAboutText.setText("<html>Web Crawler<br>ver. 1.0<br>" +
            "<br>PORVA software 2005<br></html>");

    okButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jButtonCancel_actionPerformed(e);
      }
    });
    getRootPane().setDefaultButton(okButton);

    AbstractAction cancelAction = new AbstractAction()
    {
      private static final long serialVersionUID = -6571333581511840527L;

      public void actionPerformed(ActionEvent e)
      {
        jButtonCancel_actionPerformed(e);
      }
    };
    JDialogStandart.addCancelByEscapeKey(this.getRootPane(), cancelAction);
  }

  //Close the dialog
  void jButtonCancel_actionPerformed(ActionEvent e)
  {
    dispose();
  }
  
}