/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui.filter;

import java.awt.*;
import javax.swing.*;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
/*
 * Created by JFormDesigner on Tue Feb 01 13:39:25 EET 2005
 */



/**
 * @author porva porva
 */
class DilaogRobotRulesFilterGUI extends JDialog {
  private static final long serialVersionUID = -3857179452335011526L;
  DilaogRobotRulesFilterGUI(Frame owner) {
		super(owner);
		initComponents();
	}

	DilaogRobotRulesFilterGUI(Dialog owner) {
		super(owner);
		initComponents();
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		// Generated using JFormDesigner Evaluation license
		dialogPane = new JPanel();
		contentPane = new JPanel();
		label1 = new JLabel();
		textFieldDBName = new JTextField();
		label3 = new JLabel();
		label2 = new JLabel();
		spinnerDefLifetime = new JSpinner();
		spinnerMinLifetime = new JSpinner();
		buttonBar = new JPanel();
		okButton = new JButton();
		cancelButton = new JButton();
		CellConstraints cc = new CellConstraints();

		//======== this ========
		Container contentPane2 = getContentPane();
		contentPane2.setLayout(new BorderLayout());

		//======== dialogPane ========
		{
			dialogPane.setBorder(Borders.DIALOG_BORDER);
			
			dialogPane.setLayout(new BorderLayout());
			
			//======== contentPane ========
			{
				contentPane.setLayout(new FormLayout(
					new ColumnSpec[] {
						FormFactory.GLUE_COLSPEC,
						FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
						new ColumnSpec("max(default;50dlu)")
					},
					new RowSpec[] {
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.LINE_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.LINE_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.LINE_GAP_ROWSPEC,
						new RowSpec("max(default;10dlu)")
					}));
				
				//---- label1 ----
				label1.setText("Database name:");
				contentPane.add(label1, cc.xy(1, 1));
				contentPane.add(textFieldDBName, cc.xy(3, 1));
				
				//---- label3 ----
				label3.setText("Minimun lifetime:");
				contentPane.add(label3, cc.xy(1, 5));
				
				//---- label2 ----
				label2.setText("Default lifetime:");
				contentPane.add(label2, cc.xy(1, 3));
				
				//---- spinnerDefLifetime ----
				spinnerDefLifetime.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
				contentPane.add(spinnerDefLifetime, cc.xy(3, 3));
				
				//---- spinnerMinLifetime ----
				spinnerMinLifetime.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
				contentPane.add(spinnerMinLifetime, cc.xy(3, 5));
			}
			dialogPane.add(contentPane, BorderLayout.CENTER);
			
			//======== buttonBar ========
			{
				buttonBar.setBorder(Borders.BUTTON_BAR_GAP_BORDER);
				buttonBar.setLayout(new FormLayout(
					new ColumnSpec[] {
						FormFactory.GLUE_COLSPEC,
						FormFactory.BUTTON_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC,
						FormFactory.BUTTON_COLSPEC
					},
					RowSpec.decodeSpecs("pref")));
				
				//---- okButton ----
				okButton.setText("OK");
				buttonBar.add(okButton, cc.xy(2, 1));
				
				//---- cancelButton ----
				cancelButton.setText("Cancel");
				buttonBar.add(cancelButton, cc.xy(4, 1));
			}
			dialogPane.add(buttonBar, BorderLayout.SOUTH);
		}
		contentPane2.add(dialogPane, BorderLayout.CENTER);
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	// Generated using JFormDesigner Evaluation license
	private JPanel dialogPane;
	private JPanel contentPane;
	private JLabel label1;
	JTextField textFieldDBName;
	private JLabel label3;
	private JLabel label2;
	JSpinner spinnerDefLifetime;
	JSpinner spinnerMinLifetime;
	private JPanel buttonBar;
	JButton okButton;
	JButton cancelButton;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
