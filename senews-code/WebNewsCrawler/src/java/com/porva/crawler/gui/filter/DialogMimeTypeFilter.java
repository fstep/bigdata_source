/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui.filter;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.porva.crawler.filter.Filter;
import com.porva.crawler.filter.FilterFactory;
import com.porva.util.gui.CheckNode;
import com.porva.util.gui.CheckRenderer;
import com.porva.util.gui.JDialogStandart;
import com.porva.util.gui.NodeSelectionListener;

class DialogMimeTypeFilter extends DialogFileTypesFilterGUI
{
  private static final long serialVersionUID = -8808787807472524760L;

  private Filter filter;

  // private static final String[] supportedProtocols = new String[] { "http" };
  //
  private String[] checkedMimeTypes;

  DialogMimeTypeFilter(final Frame owner, Filter filter)
  {
    super(owner);
    super.setModal(true);
    super.setTitle("Mime-type filter");
    this.filter = filter;

    if (this.filter == null)
      throw new IllegalArgumentException("filter");

    pack();
    JDialogStandart.centerInParent(this);

    checkedMimeTypes = filter.getConfigStr().split(",");

    CheckNode top = new CheckNode("All mime-types");

    List<MimeType> allTypes = null;
    try {
      allTypes = MimeTypesLoader.load();
    } catch (IOException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    for (MimeType mimeType : allTypes) {
      CheckNode node = new CheckNode(mimeType.name);
      node.setSelected(isChecked(mimeType));
      getMimeTypeCommonNode(mimeType.type, top).add(node);
    }

    // for (String supportedProtocol : supportedProtocols) {
    // CheckNode checkNode = new CheckNode(supportedProtocol);
    // checkNode.setSelected(isChecked(supportedProtocol));
    // top.add(checkNode);
    // }

    DefaultTreeModel defaultTreeModel = new DefaultTreeModel(top);
    jTreeFileTypes.setModel(defaultTreeModel);
    jTreeFileTypes.setCellRenderer(new CheckRenderer());
    jTreeFileTypes.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    jTreeFileTypes.putClientProperty("JTree.lineStyle", "Angled");
    jTreeFileTypes.addMouseListener(new NodeSelectionListener(jTreeFileTypes));

    radioButtonDisallow.setEnabled(false);
    radioButtonAllow.setEnabled(true);
    radioButtonAllow.setSelected(true);
    labelFileTypes.setText("Select allowed mime-types:");
    radioButtonAllow.setMnemonic(KeyEvent.VK_A);
    labelHelpTip.setText(""); // todo;

    okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonOk_actionPerformed(e);
      }
    });
    getRootPane().setDefaultButton(okButton);

    cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonCancel_actionPerformed(e);
      }
    });

    AbstractAction cancelAction = new AbstractAction()
    {
      private static final long serialVersionUID = -6571333581511840527L;

      public void actionPerformed(ActionEvent e)
      {
        jButtonCancel_actionPerformed(e);
      }
    };
    JDialogStandart.addCancelByEscapeKey(this.getRootPane(), cancelAction);
  }

  private boolean isChecked(final MimeType mimeType)
  {
    assert mimeType != null;
    for (String checked : checkedMimeTypes)
      if (mimeType.name.equals(checked))
        return true;
    return false;
  }

  Map<String, CheckNode> commonNodes = new HashMap<String, CheckNode>();

  private CheckNode getMimeTypeCommonNode(final String mimeType, CheckNode top)
  {
    if (commonNodes.get(mimeType) == null) {
      commonNodes.put(mimeType, new CheckNode(mimeType));
      top.add(commonNodes.get(mimeType));
    }
    return commonNodes.get(mimeType);
  }

  private void getSelectedLeafNodesStr(CheckNode node, List<String> list)
  {
    if (node.isLeaf() && node.isSelected())
      list.add(node.toString());

    for (Enumeration en = node.children(); en.hasMoreElements();) {
      getSelectedLeafNodesStr((CheckNode) en.nextElement(), list);
    }
  }

  /**
   * Button 'Ok' action. Constructs a new ProtocolFilter.
   * 
   * @param e
   */
  public void jButtonOk_actionPerformed(ActionEvent e)
  {
    List<String> checkedNodes = new ArrayList<String>();
    getSelectedLeafNodesStr((CheckNode) jTreeFileTypes.getModel().getRoot(), checkedNodes);
    String[] allowedMimeTypes = new String[checkedNodes.size()];
    checkedNodes.toArray(allowedMimeTypes);

    try {
      FilterFactory.initAllowMimeTypesFilter(filter, allowedMimeTypes);
    } catch (Exception e1) {
      JOptionPane.showMessageDialog(this, "Select at least one mime-type",
                                    "Mime-type filter error", JOptionPane.ERROR_MESSAGE);
      return;
    }

    dispose();
  }

  /**
   * Cancel action.
   * 
   * @param e
   */
  public void jButtonCancel_actionPerformed(ActionEvent e)
  {
    dispose();
  }

}

class MimeTypesLoader
{
  public static final String mimeTypesFile = System.getProperty("user.dir") + File.separator
      + "conf" + File.separator + "mime.types";

  public static List<MimeType> load() throws IOException
  {
    List<MimeType> allTypes = new ArrayList<MimeType>();

    FileReader fr = new FileReader(mimeTypesFile);
    BufferedReader br = new BufferedReader(fr);
    String str;
    while ((str = br.readLine()) != null) {
      if (str.startsWith("#"))
        continue;
      String[] mime = str.split("\t");
      MimeType mimeType = new MimeType(mime[0].trim(), mime[1].trim(), (mime.length > 2 ? mime[2]
          : null), (mime.length > 3 ? mime[3] : null));
      allTypes.add(mimeType);
    }
    return allTypes;
  }
}

class MimeType
{
  String type;

  String subtype;

  String ext;

  String desc;

  String name;

  MimeType(final String type, final String subtype, final String ext, final String desc)
  {
    this.type = type;
    this.subtype = subtype;
    this.ext = ext;
    this.desc = desc;

    if (this.type == null)
      throw new NullArgumentException("type");
    if (this.subtype == null)
      throw new NullArgumentException("subtype");

    this.name = this.type + "/" + this.subtype;
  }

  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("type", type)
        .append("subtype", subtype).append("ext", ext).append("desc", desc).toString();
  }

}