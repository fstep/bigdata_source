/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 * 
 */

class QueueTableModel extends AbstractTableModel
{
  /*
  private StorableHashQueue storableHashQueue;

  public QueueTableModel(StorableHashQueue storableHashQueue)
  {
    this.storableHashQueue = storableHashQueue;
  }

  public int getColumnCount()
  {
    return 1;
  }

  public int getRowCount()
  {
    return storableHashQueue.size();
  }

  public Object getValueAt(int rowIndex, int columnIndex)
  {
    return storableHashQueue.elementAt(rowIndex);
  }

  public String getColumnName(int col)
  {
      return "URL"; //todo
  }

  public void clear()
  {
    storableHashQueue.clear();
  }
  */

  private static final long serialVersionUID = -3948318237971193474L;
  private List<String> data;

  public QueueTableModel(List<String> data)
  {
    if (data == null)
      throw new NullPointerException();
    this.data = data;
  }

  public int getRowCount()
  {
    return data.size();
  }

  public int getColumnCount()
  {
    return 1;
  }

  public Object getValueAt(int rowIndex, int columnIndex)
  {
    return data.get(rowIndex);
  }

  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    try {
      data.remove(rowIndex);
    } catch (ArrayIndexOutOfBoundsException e) {}
    data.add(rowIndex, aValue.toString());
  }

  public String getColumnName(int col)
  {
      return "Task";
  }

  public List getData()
  {
    return data;
  }

  public Object removeElementAt(int idx)
  {
    Object ret = null;
    try {
      ret = data.remove(idx);
    } catch (ArrayIndexOutOfBoundsException e) {}
    return ret;
  }

}
