/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui.filter;

import java.awt.*;
import javax.swing.*;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
/*
 * Created by JFormDesigner on Tue Feb 01 19:47:06 EET 2005
 */



/**
 * @author porva porva
 */
class DialogDomainFilterGUI extends JDialog {
  private static final long serialVersionUID = -6449871802551846521L;
  public DialogDomainFilterGUI(Frame owner) {
		super(owner);
		initComponents();
	}

	public DialogDomainFilterGUI(Dialog owner) {
		super(owner);
		initComponents();
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		// Generated using JFormDesigner Evaluation license
		DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
		dialogPane = new JPanel();
		contentPane = new JPanel();
		labelListOfDomains = new JLabel();
		labelAddBox = new JLabel();
		scrollPane = new JScrollPane();
		jListHosts = new JList();
		jTextFieldAdd = new JTextField();
		jButtonAdd = new JButton();
		jButtonRemove = new JButton();
		goodiesFormsSeparator1 = compFactory.createSeparator("");
		radioButtonDisallow = new JRadioButton();
		radioButtonAllow = new JRadioButton();
		goodiesFormsSeparator2 = compFactory.createSeparator("");
		buttonBar = new JPanel();
		okButton = new JButton();
		cancelButton = new JButton();
		CellConstraints cc = new CellConstraints();

		//======== this ========
		Container contentPane2 = getContentPane();
		contentPane2.setLayout(new BorderLayout());

		//======== dialogPane ========
		{
			dialogPane.setBorder(Borders.DIALOG_BORDER);
			
			dialogPane.setLayout(new BorderLayout());
			
			//======== contentPane ========
			{
				contentPane.setLayout(new FormLayout(
					new ColumnSpec[] {
						FormFactory.GLUE_COLSPEC,
						FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
						new ColumnSpec("max(default;50dlu)")
					},
					new RowSpec[] {
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.LINE_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.LINE_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.LINE_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.LINE_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.LINE_GAP_ROWSPEC,
						new RowSpec("max(default;12dlu)"),
						FormFactory.LINE_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.LINE_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.LINE_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.LINE_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.LINE_GAP_ROWSPEC,
						FormFactory.GLUE_ROWSPEC,
						FormFactory.LINE_GAP_ROWSPEC,
						new RowSpec("max(default;5dlu)")
					}));
				
				//---- labelListOfDomains ----
				labelListOfDomains.setText("List of  domains:");
				contentPane.add(labelListOfDomains, cc.xy(1, 1));
				
				//---- label2 ----
				labelAddBox.setText("Add domain here:");
				contentPane.add(labelAddBox, cc.xy(3, 1));
				
				//======== scrollPane ========
				{
					scrollPane.setViewportView(jListHosts);
				}
				contentPane.add(scrollPane, cc.xywh(1, 3, 1, 19));
				contentPane.add(jTextFieldAdd, cc.xy(3, 3));
				
				//---- jButtonAdd ----
				jButtonAdd.setText("<< Add");
				contentPane.add(jButtonAdd, cc.xy(3, 7));
				
				//---- jButtonRemove ----
				jButtonRemove.setText("Remove");
				contentPane.add(jButtonRemove, cc.xy(3, 9));
				contentPane.add(goodiesFormsSeparator1, cc.xy(3, 13));
				
				//---- radioButtonDisallow ----
				radioButtonDisallow.setText("Disallow");
				contentPane.add(radioButtonDisallow, cc.xy(3, 17));
				
				//---- radioButtonAllow ----
				radioButtonAllow.setSelected(true);
				radioButtonAllow.setText("Allow");
				contentPane.add(radioButtonAllow, cc.xy(3, 15));
				contentPane.add(goodiesFormsSeparator2, cc.xy(3, 19));
			}
			dialogPane.add(contentPane, BorderLayout.CENTER);
			
			//======== buttonBar ========
			{
				buttonBar.setBorder(Borders.BUTTON_BAR_GAP_BORDER);
				buttonBar.setLayout(new FormLayout(
					new ColumnSpec[] {
						FormFactory.GLUE_COLSPEC,
						FormFactory.BUTTON_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC,
						FormFactory.BUTTON_COLSPEC
					},
					RowSpec.decodeSpecs("pref")));
				
				//---- okButton ----
				okButton.setText("OK");
				buttonBar.add(okButton, cc.xy(2, 1));
				
				//---- cancelButton ----
				cancelButton.setText("Cancel");
				buttonBar.add(cancelButton, cc.xy(4, 1));
			}
			dialogPane.add(buttonBar, BorderLayout.SOUTH);
		}
		contentPane2.add(dialogPane, BorderLayout.CENTER);

		//---- buttonGroup1 ----
		ButtonGroup buttonGroup1 = new ButtonGroup();
		buttonGroup1.add(radioButtonDisallow);
		buttonGroup1.add(radioButtonAllow);
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	// Generated using JFormDesigner Evaluation license
	private JPanel dialogPane;
	private JPanel contentPane;
	JLabel labelListOfDomains;
	JLabel labelAddBox;
	private JScrollPane scrollPane;
	JList jListHosts;
	JTextField jTextFieldAdd;
	JButton jButtonAdd;
	JButton jButtonRemove;
	private JComponent goodiesFormsSeparator1;
	JRadioButton radioButtonDisallow;
	JRadioButton radioButtonAllow;
	private JComponent goodiesFormsSeparator2;
	private JPanel buttonBar;
	JButton okButton;
	JButton cancelButton;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
