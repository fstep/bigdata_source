/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui.filter;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import com.porva.crawler.filter.Filter;
import com.porva.crawler.filter.FilterFactory;
import com.porva.util.gui.JDialogStandart;

/**
 * TODO
 * 
 * @author Poroshin V.
 * @date Jan 24, 2006
 */
class DialogDomainFilter extends DialogDomainFilterGUI
{
  private static final long serialVersionUID = -1075531539874809273L;

  private Filter filter;

  private DefaultListModel listModel = null;

  private String[] domains;

  private boolean isAllow;

  DialogDomainFilter(final Frame owner, Filter filter)
  {
    super(owner);
    super.setModal(true);
    super.setTitle("Domain filter");
    this.filter = filter;
    if (this.filter == null)
      throw new IllegalArgumentException("filter");

    parseFilterConfigStr(this.filter.getConfigStr());

    jTextFieldAdd.setDocument(new HostDocument());
    jListHosts.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    listModel = new DefaultListModel();
    for (int i = 0; i < domains.length; i++)
      listModel.addElement(domains[i]);
    jListHosts.setModel(listModel);

    pack();
    JDialogStandart.centerInParent(this);

    okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonOk_actionPerformed(e);
      }
    });
    getRootPane().setDefaultButton(okButton);

    cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonCancel_actionPerformed(e);
      }
    });

    jButtonAdd.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonAdd_actionPerformed(e);
      }
    });

    jButtonRemove.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonRemove_actionPerformed(e);
      }
    });

    AbstractAction cancelAction = new AbstractAction()
    {
      private static final long serialVersionUID = -6571333581511840527L;

      public void actionPerformed(ActionEvent e)
      {
        jButtonCancel_actionPerformed(e);
      }
    };
    JDialogStandart.addCancelByEscapeKey(this.getRootPane(), cancelAction);

    radioButtonAllow.setMnemonic(KeyEvent.VK_A);
    radioButtonAllow.setSelected(isAllow);
    radioButtonAllow.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setListOfDomainsLabelText();
      }
    });

    radioButtonDisallow.setMnemonic(KeyEvent.VK_D);
    radioButtonDisallow.setSelected(!isAllow);
    radioButtonDisallow.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setListOfDomainsLabelText();
      }
    });

    setListOfDomainsLabelText();
  }

  private void setListOfDomainsLabelText()
  {
    isAllow = radioButtonAllow.isSelected();
    if (isAllow)
      labelListOfDomains.setText("List of allowed domains:");
    else
      labelListOfDomains.setText("List of disallowed domains:");
  }

  public void jButtonRemove_actionPerformed(ActionEvent e)
  {
    Object selected = jListHosts.getSelectedValue();
    if (selected == null)
      return;
    String host = (String) selected;
    listModel.removeElement(selected);
    jTextFieldAdd.setText(host);
  }

  public void jButtonAdd_actionPerformed(ActionEvent e)
  {
    String hostToAdd = jTextFieldAdd.getText();
    if (hostToAdd == null || hostToAdd.length() == 0)
      return;
    listModel.addElement(hostToAdd);
    jTextFieldAdd.setText("");
  }

  public void jButtonCancel_actionPerformed(ActionEvent e)
  {
    dispose();
  }

  public void jButtonOk_actionPerformed(ActionEvent e)
  {
    List<String> domainsList = new ArrayList<String>();
    for (int i = 0; i < jListHosts.getModel().getSize(); i++)
      domainsList.add((String) jListHosts.getModel().getElementAt(i));
    String[] domains = new String[domainsList.size()];
    domainsList.toArray(domains);

    try {
      FilterFactory.initDomainFilter(filter, radioButtonAllow.isSelected(), domains);
    } catch (Exception e1) {
      JOptionPane.showMessageDialog(this, e1.getMessage(), "Domain filter error",
                                    JOptionPane.ERROR_MESSAGE);
      return;
    }

    dispose();
  }

  private void parseFilterConfigStr(final String configStr)
  {
    if (configStr == null)
      throw new IllegalArgumentException("params cannot be null");

    String paramStr = configStr;

    paramStr = paramStr.toLowerCase();
    String domainsStr = paramStr;
    if (domainsStr.startsWith("allow:")) {
      domainsStr = domainsStr.substring(6, domainsStr.length());
      isAllow = true;
    } else if (domainsStr.startsWith("disallow:")) {
      domainsStr = domainsStr.substring(9, domainsStr.length());
      isAllow = false;
    } else {
      throw new IllegalArgumentException(
          "Invalid initializaition string: allow or disallow must be specified!");
    }

    if (domainsStr == null || domainsStr.equals("")) {
      domains = new String[] {};
      return;
    }

    if (domainsStr == null || !domainsStr.matches("^([a-z0-9.]+)(,[a-z0-9.]+)*$"))
      throw new IllegalArgumentException("Invalid initialization of DomainFilter with string: "
          + paramStr);

    domains = domainsStr.split(",");
  }

  // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  class HostDocument extends PlainDocument
  {
    private static final long serialVersionUID = -5511520380981392125L;

    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException
    {
      if (str == null || str.equals(" ")) {
        return;
      }
      super.insertString(offs, str, a);
    }
  }

}
