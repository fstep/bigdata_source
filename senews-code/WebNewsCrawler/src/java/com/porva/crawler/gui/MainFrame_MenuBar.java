/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

//import com.porva.crawler.plugin.*;
import com.porva.crawler.plugin.DBStat;
import com.porva.crawler.plugin.HtmlViewer;
import com.porva.crawler.plugin.PluginManager;
import com.porva.crawler.plugin.HyperGraph;
import com.porva.crawler.plugin.SourceViewer;
import com.porva.util.ResourceLoader;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

/**
 * Menu bar of {@link MainFrame} frame. 
 */

class MainFrame_MenuBar extends JMenuBar
{
  private static final long serialVersionUID = -101651207105436589L;

  private MainFrame adaptee;

  private JMenu jMenuFile = new JMenu();
  JMenuItem jMenuFileNewProject = new JMenuItem();
  JMenuItem jMenuFileOpenProject = new JMenuItem();
  JMenuItem jMenuFileCloseProject = new JMenuItem();
  JMenuItem jMenuFileExportProject = new JMenuItem();
  private JMenuItem jMenuFileExit = new JMenuItem();

  private JMenu jMenuDownload = new JMenu();
  JMenuItem jMenuDownloadStart = new JMenuItem();
  JMenuItem jMenuDownloadSuspend = new JMenuItem();
  JMenuItem jMenuDownloadPause = new JMenuItem();

  private JMenu jMenuSettings = new JMenu();
  JMenuItem jMenuProjectSettings = new JMenuItem();
  JMenuItem jMenuProjectDefaultSettings = new JMenuItem();


  JMenu jMenuPlugins = new JMenu();
  private JMenuItem jMenuPluginsDBStat = new JMenuItem();
  private JMenuItem jMenuPluginsSelfRefMap = new JMenuItem();
  private JMenuItem jMenuPluginsHtmlViewer = new JMenuItem();
  private JMenuItem jMenuPluginsSourceViewer = new JMenuItem();
  //...

  private JMenu jMenuHelp = new JMenu();
  private JMenuItem jMenuHelpTopics = new JMenuItem();
  private JMenuItem jMenuHelpAbout = new JMenuItem();


  public MainFrame_MenuBar(MainFrame adaptee)
  {
    this.adaptee = adaptee;

    jMenuFile.setText("File");
    jMenuFile.setMnemonic(KeyEvent.VK_F);
    jMenuFile.add(jMenuFileNewProject);
    jMenuFile.add(jMenuFileOpenProject);
    jMenuFile.addSeparator();
    jMenuFile.add(jMenuFileExportProject);
    jMenuFile.addSeparator();
    jMenuFile.add(jMenuFileCloseProject);
    jMenuFileExportProject.setEnabled(false);
    jMenuFileCloseProject.setEnabled(false);
    jMenuFile.addSeparator();
    jMenuFile.add(jMenuFileExit);

    jMenuDownload.setText("Download");
    jMenuDownload.setMnemonic(KeyEvent.VK_D);
    jMenuDownload.add(jMenuDownloadStart);
    jMenuDownload.add(jMenuDownloadPause);
    jMenuDownload.add(jMenuDownloadSuspend);

    jMenuSettings.setText("Settings");
    jMenuSettings.setMnemonic(KeyEvent.VK_S);
    jMenuSettings.add(jMenuProjectSettings);
    jMenuSettings.add(jMenuProjectDefaultSettings);
    jMenuProjectSettings.setEnabled(false);

    jMenuPlugins.setText("Plug-ins");
    jMenuPlugins.setMnemonic(KeyEvent.VK_P);
    jMenuPlugins.add(jMenuPluginsDBStat);
    jMenuPlugins.add(jMenuPluginsSelfRefMap);
    jMenuPlugins.add(jMenuPluginsHtmlViewer);
    jMenuPlugins.add(jMenuPluginsSourceViewer);
    jMenuPlugins.setEnabled(false);

    jMenuHelp.setText("Help");
    jMenuHelp.setMnemonic(KeyEvent.VK_H);
    jMenuHelp.add(jMenuHelpTopics);
    jMenuHelp.add(jMenuHelpAbout);

    this.add(jMenuFile);
    this.add(jMenuDownload);
    this.add(jMenuSettings);
    this.add(jMenuPlugins);
    this.add(jMenuHelp);

    jMenuFileNewProject.setText("New Project...");
    jMenuFileNewProject.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
    jMenuFileNewProject.setMnemonic(KeyEvent.VK_N);
    jMenuFileNewProject.setIcon(ResourceLoader.getIcon("Menu.File.NewProject"));
    jMenuFileNewProject.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        jMenuFileNewProject_actionPerformed(e);
      }
    });

    // File -> Open Project...
    jMenuFileOpenProject.setText("Open Project...");
    jMenuFileOpenProject.setMnemonic(KeyEvent.VK_O);
    jMenuFileOpenProject.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
    jMenuFileOpenProject.setIcon(ResourceLoader.getIcon("Menu.File.OpenProject"));
    jMenuFileOpenProject.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        jMenuFileOpen_actionPerformed(e);
      }
    });


    // File -> Close Project...
    jMenuFileCloseProject.setText("Close Project");
    jMenuFileCloseProject.setMnemonic(KeyEvent.VK_C);
    jMenuFileCloseProject.setIcon(ResourceLoader.getIcon("Menu.Empty"));
    jMenuFileCloseProject.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        jMenuFileCloseProject_actionPerformed(e);
      }
    });

    jMenuFileExportProject.setText("Export Project...");
    jMenuFileExportProject.setMnemonic(KeyEvent.VK_E);
    jMenuFileExportProject.setIcon(ResourceLoader.getIcon("Menu.File.Export"));
    jMenuFileExportProject.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        jMenuFileExportProject_actionPerformed(e);
      }
    });


    // File -> Exit
    jMenuFileExit.setText("Exit");
    jMenuFileExit.setMnemonic(KeyEvent.VK_X);
    jMenuFileExit.setIcon(ResourceLoader.getIcon("Menu.Empty"));
    jMenuFileExit.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        jMenuFileExit_actionPerformed(e);
      }
    });

    // Download -> Start
    jMenuDownloadStart.setText("Start");
    jMenuDownloadStart.setMnemonic(KeyEvent.VK_S);
    jMenuDownloadStart.setIcon(ResourceLoader.getIcon("Menu.Download.Start"));
    jMenuDownloadStart.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        jMenuDownloadStart_actionPerformed(e);
      }
    });

    // Download -> Pause
    jMenuDownloadPause.setText("Pause");
    jMenuDownloadPause.setMnemonic(KeyEvent.VK_P);
    jMenuDownloadPause.setIcon(ResourceLoader.getIcon("Menu.Download.Pause"));
    jMenuDownloadPause.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        jMenuDownloadPause_actionPerformed(e);
      }
    });

    // Download -> Suspend
    jMenuDownloadSuspend.setText("Suspend");
    jMenuDownloadSuspend.setMnemonic(KeyEvent.VK_U);
    jMenuDownloadSuspend.setIcon(ResourceLoader.getIcon("Menu.Download.Suspend"));
    jMenuDownloadSuspend.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        jMenuDownloadSuspend_actionPerformed(e);
      }
    });

    // Settings -> Default Project settings...
    jMenuProjectDefaultSettings.setText("Default Project settings...");
    jMenuProjectDefaultSettings.setMnemonic(KeyEvent.VK_D);
    jMenuProjectDefaultSettings.setIcon(ResourceLoader.getIcon("Menu.Empty"));
    jMenuProjectDefaultSettings.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        jMenuDefaultSettings_actionPerformed(e);
      }
    });

    // Settings -> Project settings...
    jMenuProjectSettings.setText("Project settings...");
    jMenuProjectSettings.setMnemonic(KeyEvent.VK_S);
    jMenuProjectSettings.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK|ActionEvent.ALT_MASK));
    jMenuProjectSettings.setIcon(ResourceLoader.getIcon("Menu.Settings.ProjectSettings"));
    jMenuProjectSettings.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        jMenuSettings_actionPerformed(e);
      }
    });


    // Plugins -> DB Stat
    jMenuPluginsDBStat.setText("DB Stat");
    jMenuPluginsDBStat.setMnemonic(KeyEvent.VK_S);
    jMenuPluginsDBStat.setIcon(ResourceLoader.getIcon("Menu.Plugins.DBStat"));
    jMenuPluginsDBStat.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        jMenuPluginsDBStat_actionPerformed(e);
      }
    });

    // Plugins -> SelfRef map
    jMenuPluginsSelfRefMap.setText("Hyperlink Graph");
    jMenuPluginsSelfRefMap.setMnemonic(KeyEvent.VK_Y);
    jMenuPluginsSelfRefMap.setIcon(ResourceLoader.getIcon("Menu.Plugins.SelfRefMap"));
    jMenuPluginsSelfRefMap.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        jMenuPluginsSelfRefMap_actionPerformed(e);
      }
    });

    // Plugins -> HTML view
    jMenuPluginsHtmlViewer.setText("HTML Viewer");
    jMenuPluginsHtmlViewer.setMnemonic(KeyEvent.VK_H);
    jMenuPluginsHtmlViewer.setIcon(ResourceLoader.getIcon("Menu.Plugins.HtmlViewer"));
    jMenuPluginsHtmlViewer.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        jMenuPluginsHtmlViewer_actionPerformed(e);
      }
    });

    jMenuPluginsSourceViewer.setText("Source Viewer");
    jMenuPluginsSourceViewer.setMnemonic(KeyEvent.VK_O);
    jMenuPluginsSourceViewer.setIcon(ResourceLoader.getIcon("Menu.Plugins.SourceViewer"));
    jMenuPluginsSourceViewer.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        jMenuPluginsSourceViewer_actionPerformed(e);
      }
    });

    // Help -> Help Topics
    jMenuHelpTopics.setText("Online User Manual");
    jMenuHelpTopics.setMnemonic(KeyEvent.VK_O);
    jMenuHelpTopics.setIcon(ResourceLoader.getIcon("Menu.Help.OnlineTopics"));
    jMenuHelpTopics.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        jMenuHelpTopics_actionPerformed(e);
      }
    });


    // Help -> About
    jMenuHelpAbout.setText("About");
    jMenuHelpAbout.setMnemonic(KeyEvent.VK_A);
    jMenuHelpAbout.setIcon(ResourceLoader.getIcon("Menu.Help.About"));
    jMenuHelpAbout.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        jMenuHelpAbout_actionPerformed(e);
      }
    });


    jMenuDownloadSuspend.setEnabled(false);
    jMenuDownloadPause.setEnabled(false);
    jMenuDownloadStart.setEnabled(false);
  }

  public void jMenuFileNewProject_actionPerformed(ActionEvent e)
  {
    adaptee.actionNewProject(e);
  }

  public void jMenuFileOpen_actionPerformed(ActionEvent e)
  {
    adaptee.actionFileOpenProject(e);
  }

  public void jMenuFileCloseProject_actionPerformed(ActionEvent e)
  {
    adaptee.actionFileCloseProject(e);
  }

  public void jMenuFileExportProject_actionPerformed(ActionEvent e)
  {
    adaptee.actionExportProject(e);
  }

  public void jMenuFileExit_actionPerformed(ActionEvent e)
  {
    adaptee.actionFileExit(e);
  }

  // Download

  public void jMenuDownloadStart_actionPerformed(ActionEvent e)
  {
    if (jMenuDownloadStart.getText().equals("Start")) {
      adaptee.actionDownloadStart(e);
    } else if (jMenuDownloadStart.getText().equals("Resume")) {
      adaptee.actionDownloadResume(e);
    }
  }

  public void jMenuDownloadSuspend_actionPerformed(ActionEvent e)
  {
    adaptee.actionDownloadSuspend(e);
  }

  public void jMenuDownloadPause_actionPerformed(ActionEvent e)
  {
    adaptee.actionDownloadPause(e);
  }

  public void  jMenuHelpAbout_actionPerformed(ActionEvent e)
  {
    adaptee.actionHelpAbout(e);
  }

  // settings

  public void jMenuDefaultSettings_actionPerformed(ActionEvent e)
  {
    adaptee.actionProjectDefaultSettings(e);
  }

  public void jMenuSettings_actionPerformed(ActionEvent e)
  {
    adaptee.actionProjectSettings(e);
  }


  // plugins

  public void jMenuPluginsDBStat_actionPerformed(ActionEvent e)
  {
    try {
      PluginManager.runPlugin(DBStat.class);
    } catch (Exception e1) {
      e1.printStackTrace();
      // todo error dialog and log
    }
  }

  public void jMenuPluginsSelfRefMap_actionPerformed(ActionEvent e)
  {
    try {
      PluginManager.runPlugin(HyperGraph.class);
    } catch (Exception e1) {
      e1.printStackTrace();
      // todo error dialog and log
    }
  }

   public void  jMenuPluginsHtmlViewer_actionPerformed(ActionEvent e)
   {
     try {
       PluginManager.runPlugin(HtmlViewer.class);
     } catch (Exception e1) {
       e1.printStackTrace();
       // todo error dialog and log
     }
   }

  public void jMenuPluginsSourceViewer_actionPerformed(ActionEvent e)
  {
    try {
      PluginManager.runPlugin(SourceViewer.class);
    } catch (Exception e1) {
      e1.printStackTrace();
      // todo error dialog and log
    }
  }

  public void jMenuHelpTopics_actionPerformed(ActionEvent e)
  {
    adaptee.actionHelpTopics(e);
  }

//
//  private ActionListener getHelpActionListener()
//  {
//    // Find the HelpSet file and create the HelpSet object:
//    ActionListener helper = null;
//
//    String helpHS = "crawler.hs";
//    URL url = Configuration.searchResouce(helpHS);
//    if (url == null) {
//      //logger.error("HelpSet file not found: " + helpHS);
//      return null;
//    }
//
//    try {
//      HelpSet hs = new HelpSet(null, url);
//      //JHelpContentViewer viewer1 = new JHelpContentViewer(hs);
//      //JHelpNavigator xnav = (JHelpNavigator) hs.getNavigatorView("TOC").createNavigator(viewer1.getModel());
//
//      HelpBroker hb = hs.createHelpBroker();
//      //hb.enableHelpKey(adaptee.getRootPane(), "overview", hs);
//      helper = new CSH.DisplayHelpFromSource(hb);
//    } catch (HelpSetException e) {
//      e.printStackTrace();
//    }
//    return helper;
//  }


}
