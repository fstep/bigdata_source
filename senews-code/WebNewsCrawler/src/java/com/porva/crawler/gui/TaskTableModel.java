/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

import javax.swing.table.AbstractTableModel;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.task.FetchTask;
import com.porva.crawler.task.Task;

import java.util.List;

/**
 * Table model to show {@link com.porva.crawler.task.Task} tasks in GUI table.
 */
class TaskTableModel extends AbstractTableModel
{
  private static final long serialVersionUID = 4448565541097064714L;
  private List<Task> data;

  /**
   * Allocates a new {@link TaskTableModel} object.
   * 
   * @param data list of tasks.
   */
  public TaskTableModel(List<Task> data)
  {
    if (data == null)
      throw new NullArgumentException("data");
    this.data = data;
  }

  public int getRowCount()
  {
    return data.size();
  }

  public int getColumnCount()
  {
    return 1;
  }

  public Object getValueAt(int rowIndex, int columnIndex)
  {
    if (columnIndex == 0) {
      Task task = data.get(rowIndex);
      if (task instanceof FetchTask) {
        final FetchTask fetchTask = (FetchTask)task;
        return fetchTask.getCrawlerURL().getURLString();
      } else {
        return task;
      }
    }
    return null;
  }

  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    if (columnIndex == 0) {
      try {
        data.remove(rowIndex);
      } catch (Exception e) {}
        data.add(rowIndex, (Task)aValue);
    }
  }

  public String getColumnName(int col)
  {
    if (col == 0)
      return "Task";
    return null;
  }

  /**
   * Returns list of tasks in this table.
   * 
   * @return list of tasks.
   */
  public List<Task> getData()
  {
    return data;
  }

  /**
   * Removes element at specified index.
   * 
   * @param idx index in table to remove task at it.
   * @return removed task or <code>null</code> if task was not removed.
   */
  public Object removeElementAt(int idx)
  {
    Object ret = null;
    try {
      ret = data.remove(idx);
    } catch (ArrayIndexOutOfBoundsException e) {}
    return ret;
  }


}
