/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui.filter;

/*
 * JTableFilters.java is a 1.4 application that requires no other files.
 */

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.filter.Filter;
import com.porva.crawler.service.Service;
import com.porva.html.DOMDocumentParser;

/**
 * JTableFilters is just like SimpleTableDemo, except that it uses a custom TableModel.
 * @param <T> type of service in this filter
 */
public class ServiceTableModel<T extends Service> extends AbstractTableModel
{
  private static final long serialVersionUID = 7089598469886569306L;

  private List<Boolean> isActiveService = new ArrayList<Boolean>();

  private List<T> services = new ArrayList<T>();
  
  private String[] columnNames = { " ", "Filter Name", "Initialization String", };
  
  public int getColumnCount()
  {
    return columnNames.length;
  }

  public int getRowCount()
  {
    return services.size();
  }

  public String getColumnName(int col)
  {
    return columnNames[col];
  }

  public Object getValueAt(int row, int col)
  {
    T filter = services.get(row);
    switch (col) {
    case 0:
      return isActiveService.get(row);
    case 1:
      return filter.getServiceName();
    case 2:
      if (filter instanceof Filter)
        return ((Filter) filter).getConfigStr();
      else if (filter instanceof DOMDocumentParser) {
        return "todo";
      }
    }
    return null;
  }

  /*
   * JTable uses this method to determine the default renderer/ editor for each cell. If we didn't
   * implement this method, then the last column would contain text ("true"/"false"), rather than a
   * check box.
   */
  public Class<?> getColumnClass(int col)
  {
    switch (col) {
    case 0:
      return Boolean.class;
    case 1:
      return String.class;
    case 2:
      return String.class;
    }
    return null;
  }

  public boolean isCellEditable(int row, int col)
  {
    switch (col) {
    case 0:
      return true;
    case 1:
      return false;
    case 2:
      return true;
    }
    return false;
  }

  public void setValueAt(Object value, int row, int col)
  {
    if (col == 0)
      isActiveService.set(row, (Boolean) value);
    fireTableCellUpdated(row, col);
  }

  /**
   * Adds <code>filter</code> to this table model.
   * 
   * @param service filter object.
   * @param isActive <code>true</code> if filter is active; <code>false</code> otherwise.
   */
  public void addService(T service, boolean isActive)
  {
    if (service == null)
      throw new NullArgumentException("filter");

    services.add(service);
    isActiveService.add(isActive);
    fireTableDataChanged();
  }

  /**
   * Returns all filters in this table model.
   * 
   * @return list of all filters in this table model.
   */
  public List<T> getServices()
  {
    return services;
  }

  /**
   * Returns all checked filters in this table model.
   * 
   * @return list of all checked filters in this table model.
   */
  public List<T> getCheckedServices()
  {
    List<T> activeServices = new ArrayList<T>();
    for (int i = 0; i < services.size(); i++) {
      if (isActiveService.get(i))
        activeServices.add(services.get(i));
    }
    return activeServices;
  }

}
