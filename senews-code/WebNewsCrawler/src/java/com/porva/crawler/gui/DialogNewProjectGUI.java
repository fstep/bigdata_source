/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

import java.awt.*;
import javax.swing.*;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
/*
 * Created by JFormDesigner on Mon Jan 24 15:18:49 EET 2005
 */



/**
 * @author porva porva
 */
class DialogNewProjectGUI extends JDialog {
  private static final long serialVersionUID = 5655380313785865512L;
  public DialogNewProjectGUI(Frame owner) {
		super(owner);
		initComponents();
	}

	public DialogNewProjectGUI(Dialog owner) {
		super(owner);
		initComponents();
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		// Generated using JFormDesigner Evaluation license
		dialogPane = new JPanel();
		contentPane = new JPanel();
		label2 = new JLabel();
		label1 = new JLabel();
		jTextFieldProjectName = new JTextField();
		label6 = new JLabel();
		jTextFieldProjectDir = new JTextField();
		buttonProjectDir = new JButton();
		label3 = new JLabel();
		jTextFieldProjectFile = new JTextField();
		buttonProjectFile = new JButton();
		label4 = new JLabel();
		jTextFieldDbDir = new JTextField();
		buttonDbDir = new JButton();
		jTextFieldWaitingFile = new JTextField();
		label5 = new JLabel();
		buttonTaskFile = new JButton();
		buttonBar = new JPanel();
		okButton = new JButton();
		cancelButton = new JButton();
		CellConstraints cc = new CellConstraints();

		//======== this ========
		Container contentPane2 = getContentPane();
		contentPane2.setLayout(new BorderLayout());

		//======== dialogPane ========
		{
			dialogPane.setBorder(Borders.DIALOG_BORDER);
			
			dialogPane.setLayout(new BorderLayout());
			
			//======== contentPane ========
			{
				contentPane.setLayout(new FormLayout(
					new ColumnSpec[] {
						FormFactory.GLUE_COLSPEC,
						FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
						new ColumnSpec(Sizes.DLUX14)
					},
					new RowSpec[] {
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.LINE_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.LINE_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.LINE_GAP_ROWSPEC,
						new RowSpec(Sizes.DLUY14),
						FormFactory.LINE_GAP_ROWSPEC,
						new RowSpec("max(default;5dlu)"),
						FormFactory.LINE_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.LINE_GAP_ROWSPEC,
						new RowSpec(Sizes.DLUY14),
						FormFactory.LINE_GAP_ROWSPEC,
						new RowSpec("max(default;5dlu)"),
						FormFactory.LINE_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.LINE_GAP_ROWSPEC,
						new RowSpec(Sizes.DLUY14),
						FormFactory.LINE_GAP_ROWSPEC,
						new RowSpec("max(default;5dlu)"),
						FormFactory.LINE_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.LINE_GAP_ROWSPEC,
						new RowSpec(Sizes.DLUY14),
						FormFactory.LINE_GAP_ROWSPEC,
						new RowSpec("max(default;5dlu)"),
						FormFactory.LINE_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.LINE_GAP_ROWSPEC,
						new RowSpec(Sizes.DLUY14),
						FormFactory.LINE_GAP_ROWSPEC,
						new RowSpec("max(default;12dlu)")
					}));
				
				//---- label2 ----
				label2.setText("Name:");
				contentPane.add(label2, cc.xy(1, 5));
				
				//---- label1 ----
				label1.setFont(new Font("Tahoma", Font.BOLD, 14));
				label1.setText("Please enter a project name for a new Crawler project.");
				contentPane.add(label1, cc.xywh(1, 1, 3, 1));
				contentPane.add(jTextFieldProjectName, cc.xywh(1, 7, 3, 1));
				
				//---- label6 ----
				label6.setText("Project directory:");
				contentPane.add(label6, cc.xy(1, 11));
				contentPane.add(jTextFieldProjectDir, cc.xy(1, 13));
				
				//---- buttonProjectDir ----
				buttonProjectDir.setText("...");
				contentPane.add(buttonProjectDir, cc.xy(3, 13));
				
				//---- label3 ----
				label3.setText("Project file location:");
				contentPane.add(label3, cc.xy(1, 17));
				contentPane.add(jTextFieldProjectFile, cc.xy(1, 19));
				
				//---- buttonProjectFile ----
				buttonProjectFile.setText("...");
				contentPane.add(buttonProjectFile, cc.xy(3, 19));
				
				//---- label4 ----
				label4.setText("Database directory:");
				contentPane.add(label4, cc.xy(1, 23));
				contentPane.add(jTextFieldDbDir, cc.xy(1, 25));
				
				//---- buttonDbDir ----
				buttonDbDir.setText("...");
				contentPane.add(buttonDbDir, cc.xy(3, 25));
				contentPane.add(jTextFieldWaitingFile, cc.xy(1, 31));
				
				//---- label5 ----
				label5.setText("Intial tasks file:");
				contentPane.add(label5, cc.xy(1, 29));
				
				//---- buttonTaskFile ----
				buttonTaskFile.setText("...");
				contentPane.add(buttonTaskFile, cc.xy(3, 31));
			}
			dialogPane.add(contentPane, BorderLayout.CENTER);
			
			//======== buttonBar ========
			{
				buttonBar.setBorder(Borders.BUTTON_BAR_GAP_BORDER);
				buttonBar.setLayout(new FormLayout(
					new ColumnSpec[] {
						FormFactory.GLUE_COLSPEC,
						FormFactory.BUTTON_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC,
						FormFactory.BUTTON_COLSPEC
					},
					RowSpec.decodeSpecs("pref")));
				
				//---- okButton ----
				okButton.setText("OK");
				buttonBar.add(okButton, cc.xy(2, 1));
				
				//---- cancelButton ----
				cancelButton.setText("Cancel");
				buttonBar.add(cancelButton, cc.xy(4, 1));
			}
			dialogPane.add(buttonBar, BorderLayout.SOUTH);
		}
		contentPane2.add(dialogPane, BorderLayout.CENTER);
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	// Generated using JFormDesigner Evaluation license
	private JPanel dialogPane;
	private JPanel contentPane;
	private JLabel label2;
	private JLabel label1;
	JTextField jTextFieldProjectName;
	private JLabel label6;
	JTextField jTextFieldProjectDir;
	JButton buttonProjectDir;
	private JLabel label3;
	JTextField jTextFieldProjectFile;
	JButton buttonProjectFile;
	private JLabel label4;
	JTextField jTextFieldDbDir;
	JButton buttonDbDir;
	JTextField jTextFieldWaitingFile;
	private JLabel label5;
	JButton buttonTaskFile;
	private JPanel buttonBar;
	JButton okButton;
	JButton cancelButton;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
