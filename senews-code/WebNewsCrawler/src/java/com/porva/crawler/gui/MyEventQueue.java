/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
//// credits to Santhosh Kumar T - santhosh@in.fiorano.com
//// http://www.javalobby.org/java/forums/m91840438.html
//
//package com.porva.crawler.gui;
//
//import java.awt.*;
//import java.awt.event.ActionEvent;
//import java.awt.event.MouseEvent;
//
//import javax.swing.AbstractAction;
//import javax.swing.JPopupMenu;
//import javax.swing.MenuSelectionManager;
//import javax.swing.SwingUtilities;
//import javax.swing.text.JTextComponent;
//
//public class MyEventQueue extends EventQueue {
//  
//  protected void dispatchEvent(AWTEvent event){ 
//    super.dispatchEvent(event); 
//    
//    // interested only in mouseevents 
//    if(!(event instanceof MouseEvent)) 
//      return; 
//    
//    MouseEvent me = (MouseEvent)event; 
//    
//    // interested only in popuptriggers 
//    if(!me.isPopupTrigger()) 
//      return; 
//    
//    // me.getComponent(...) retunrs the heavy weight component on which event occured 
//    Component comp = SwingUtilities.getDeepestComponentAt(me.getComponent(), me.getX(), me.getY()); 
//    
//    // interested only in textcomponents 
//    if(!(comp instanceof JTextComponent)) 
//      return; 
//    
//    // no popup shown by user code 
//    if(MenuSelectionManager.defaultManager().getSelectedPath().length>0) 
//      return; 
//    
//    // create popup menu and show 
//    JTextComponent tc = (JTextComponent)comp; 
//    JPopupMenu menu = new JPopupMenu(); 
//    menu.add(new CutAction(tc)); 
////    menu.add(new CopyAction(tc)); 
////    menu.add(new PasteAction(tc)); 
////    menu.add(new DeleteAction(tc)); 
////    menu.addSeparator(); 
////    menu.add(new SelectAllAction(tc)); 
//    
//    Point pt = SwingUtilities.convertPoint(me.getComponent(), me.getPoint(), tc);
//    menu.show(tc, pt.x, pt.y);
//  } 
//} 
//
////@author Santhosh Kumar T - santhosh@in.fiorano.com 
//class CutAction extends AbstractAction{ 
//  JTextComponent comp; 
//  
//  public CutAction(JTextComponent comp){ 
//    super("Cut"); 
//    this.comp = comp; 
//  } 
//  
//  public void actionPerformed(ActionEvent e) { 
//    comp.cut(); 
//  } 
//  
//  public boolean isEnabled(){ 
//    return comp.isEditable() 
//    && comp.isEnabled() 
//    && comp.getSelectedText()!=null; 
//  } 
//}