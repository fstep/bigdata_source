/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

import java.awt.*;
import javax.swing.*;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
/*
 * Created by JFormDesigner on Tue Feb 01 16:28:24 EET 2005
 */



/**
 * @author porva porva
 */
class DialogAboutGUI extends JDialog {
  private static final long serialVersionUID = 8300385408310403081L;
  DialogAboutGUI(Frame owner) {
		super(owner);
		initComponents();
	}

	DialogAboutGUI(Dialog owner) {
		super(owner);
		initComponents();
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		// Generated using JFormDesigner Evaluation license
		dialogPane = new JPanel();
		contentPane = new JPanel();
		labelLogo = new JLabel();
		labelAboutText = new JLabel();
		buttonBar = new JPanel();
		okButton = new JButton();
		CellConstraints cc = new CellConstraints();

		//======== this ========
		Container contentPane2 = getContentPane();
		contentPane2.setLayout(new BorderLayout());

		//======== dialogPane ========
		{
			dialogPane.setBorder(Borders.DIALOG_BORDER);
			
			dialogPane.setLayout(new BorderLayout());
			
			//======== contentPane ========
			{
				contentPane.setBorder(Borders.DLU4_BORDER);
				contentPane.setLayout(new FormLayout(
					"default, max(default;100dlu):grow",
					"max(default;60dlu):grow"));
				
				//---- labelLogo ----
				labelLogo.setText("text");
				contentPane.add(labelLogo, cc.xy(1, 1));
				
				//---- labelAboutText ----
				labelAboutText.setHorizontalAlignment(SwingConstants.RIGHT);
				labelAboutText.setText("test");
				contentPane.add(labelAboutText, cc.xy(2, 1));
			}
			dialogPane.add(contentPane, BorderLayout.CENTER);
			
			//======== buttonBar ========
			{
				buttonBar.setBorder(Borders.BUTTON_BAR_GAP_BORDER);
				buttonBar.setLayout(new FormLayout(
					new ColumnSpec[] {
						FormFactory.GLUE_COLSPEC,
						FormFactory.BUTTON_COLSPEC
					},
					RowSpec.decodeSpecs("pref")));
				
				//---- okButton ----
				okButton.setText("OK");
				buttonBar.add(okButton, cc.xy(2, 1));
			}
			dialogPane.add(buttonBar, BorderLayout.SOUTH);
		}
		contentPane2.add(dialogPane, BorderLayout.CENTER);
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	// Generated using JFormDesigner Evaluation license
	private JPanel dialogPane;
	private JPanel contentPane;
	JLabel labelLogo;
	JLabel labelAboutText;
	private JPanel buttonBar;
	JButton okButton;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
