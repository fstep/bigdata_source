/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui.filter;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;

import com.porva.crawler.filter.FileTypeFilter;
import com.porva.crawler.filter.Filter;
import com.porva.crawler.filter.FilterFactory;
import com.porva.util.gui.CheckNode;
import com.porva.util.gui.CheckRenderer;
import com.porva.util.gui.JDialogStandart;
import com.porva.util.gui.NodeSelectionListener;

/**
 * GUI dialog to specify allowed file types to construct {@link FileTypeFilter} object.
 */
class DialogFileTypesFilter extends DialogFileTypesFilterGUI
{
  private static final long serialVersionUID = 287615527861077609L;

  private Filter filter = null;

  private boolean isAllow;

  private String[] fileTypes;

  DialogFileTypesFilter(final Frame owner, final Filter filter)
  {
    super(owner);
    super.setModal(true);
    super.setTitle("File type filter");
    this.filter = filter;
    pack();
    JDialogStandart.centerInParent(this);

    // jTreeExludeTypes = new CheckTree();
    initFilter(filter.getConfigStr());

    // CheckNode todoNode = new CheckNode("TODO: add more types");
    // top.add(todoNode);

    DefaultTreeModel defaultTreeModel = new DefaultTreeModel(buildTree());
    jTreeFileTypes.setModel(defaultTreeModel);
    jTreeFileTypes.setCellRenderer(new CheckRenderer());
    jTreeFileTypes.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    jTreeFileTypes.putClientProperty("JTree.lineStyle", "Angled");
    jTreeFileTypes.addMouseListener(new NodeSelectionListener(jTreeFileTypes));

    // defaultTreeModel.addTreeModelListener(new MyTreeModelListener());

    okButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonOk_actionPerformed(e);
      }
    });
    getRootPane().setDefaultButton(okButton);

    cancelButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        jButtonCancel_actionPerformed(e);
      }
    });
    AbstractAction cancelAction = new AbstractAction()
    {
      private static final long serialVersionUID = -6571333581511840527L;

      public void actionPerformed(ActionEvent e)
      {
        jButtonCancel_actionPerformed(e);
      }
    };
    JDialogStandart.addCancelByEscapeKey(this.getRootPane(), cancelAction);

    radioButtonAllow.setMnemonic(KeyEvent.VK_A);
    radioButtonAllow.setSelected(isAllow);
    radioButtonAllow.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setListOfDomainsLabelText();
        setHelpTip();
      }
    });

    radioButtonDisallow.setMnemonic(KeyEvent.VK_D);
    radioButtonDisallow.setSelected(!isAllow);
    radioButtonDisallow.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setListOfDomainsLabelText();
        setHelpTip();
      }
    });

    jTreeFileTypes.addTreeSelectionListener(new TreeSelectionListener()
    {
      public void valueChanged(TreeSelectionEvent evt)
      {
        setListOfDomainsLabelText();
        setHelpTip();
      }
    });

    setListOfDomainsLabelText();
    setHelpTip();
  }

  private void setHelpTip()
  {
//    String[] checkedFileTypes = getCheckedFileTypes();
//    StringBuffer sb = new StringBuffer();
//    sb.append("<html>");
//    if (isAllow)
//      sb.append("<b>Allowed file types to download:</b> ");
//    else
//      sb.append("<b>Disallowed file types to download:</b> ");
//    for (String str : checkedFileTypes)
//      sb.append(str).append(" ");
//    sb.append("</html>");
//
//    labelHelpTip.setText(sb.toString());
    labelHelpTip.setText(""); // todo:
  }

  private void setListOfDomainsLabelText()
  {
    isAllow = radioButtonAllow.isSelected();
    if (isAllow)
      labelFileTypes.setText("List of allowed file types:");
    else
      labelFileTypes.setText("List of disallowed file types:");
  }

  // private void constructTree(DefaultMutableTreeNode topNode, Object ob)
  // {
  // if (ob instanceof java.util.List) {
  // java.util.List list = (java.util.List)ob;
  // Iterator it = list.iterator();
  // while (it.hasNext()) {
  // Object ob2 = it.next();
  // if (ob2 instanceof java.util.List)
  // //constructTree();
  // }
  // } else if (ob instanceof String) {
  //
  // }
  //
  // }

  private void initFilter(String configStr)
  {
    configStr = configStr.toLowerCase();
    String fileTypesStr = configStr;
    if (fileTypesStr.startsWith("allow:")) {
      fileTypesStr = fileTypesStr.substring(6, fileTypesStr.length());
      isAllow = true;
    } else if (fileTypesStr.startsWith("disallow:")) {
      fileTypesStr = fileTypesStr.substring(9, fileTypesStr.length());
      isAllow = false;
    } else {
      throw new IllegalArgumentException(
          "Invalid initializaition string: allow or disallow must be specified!");
    }

    if (fileTypesStr == null || fileTypesStr.trim().equals(""))
      throw new IllegalArgumentException(
          "Invalid SchemeFilter initialization: use comma separated string of allowed protocols!");

    fileTypes = fileTypesStr.split(",");
  }

  private void getSelectedLeafNodesStr(CheckNode node, List<String> list)
  {
    if (node.isLeaf() && node.isSelected())
      list.add(node.toString());

    for (Enumeration en = node.children(); en.hasMoreElements();) {
      getSelectedLeafNodesStr((CheckNode) en.nextElement(), list);
    }
  }

  private String[] getCheckedFileTypes()
  {
    List<String> checkedNodes = new ArrayList<String>();
    getSelectedLeafNodesStr((CheckNode) jTreeFileTypes.getModel().getRoot(), checkedNodes);
    String[] checkedFileTypes = new String[checkedNodes.size()];
    checkedNodes.toArray(checkedFileTypes);
    return checkedFileTypes;
  }

  public void jButtonOk_actionPerformed(ActionEvent e)
  {
    String[] checkedFileTypes = getCheckedFileTypes();

    try {
      FilterFactory.initFileTypeFilter(filter, radioButtonAllow.isSelected(), checkedFileTypes);
    } catch (Exception e1) {
      JOptionPane.showMessageDialog(this, e1.getMessage(), "FileTypes filter error",
                                    JOptionPane.ERROR_MESSAGE);
      return;
    }

    dispose();
  }

  public void jButtonCancel_actionPerformed(ActionEvent e)
  {
    dispose();
  }

  private CheckNode buildTree()
  {
    CheckNode topNode = new CheckNode("All file types");
    final String fileTypesFile = System.getProperty("user.dir") + File.separator + "conf"
        + File.separator + "file.types";

    try {
      FileReader fr = new FileReader(fileTypesFile);
      BufferedReader br = new BufferedReader(fr);
      String str;
      while ((str = br.readLine()) != null) {
        if (str.startsWith("#")) // skip comments
          continue;

        String[] types = str.split("\t", 2);
        CheckNode node = new CheckNode(types[0]);
        for (String type : types[1].split(" ")) {
          CheckNode leafNode = new CheckNode(type);
          node.add(leafNode);
          for (String checkedType : fileTypes)
            if (checkedType.equalsIgnoreCase(type))
              leafNode.setSelected(true);
        }
        topNode.add(node);
      }
    } catch (IOException e) {
      // TODO
      e.printStackTrace();
    }

    return topNode;
  }

}
