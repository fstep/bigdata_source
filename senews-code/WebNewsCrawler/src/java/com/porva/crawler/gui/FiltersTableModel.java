/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

import java.util.List;

import javax.swing.table.AbstractTableModel;

/**
 * Table model to show available {@link UriFilter} filters in GUI table.
 */

class FiltersTableModel extends AbstractTableModel
{
  private static final long serialVersionUID = -9101452227592148741L;
  private List<Object> filters;

  public FiltersTableModel(List<Object> data)
  {
    if (data == null)
      throw new NullPointerException();
    this.filters = data;
  }

  public int getRowCount()
  {
    return filters.size();
  }

  public int getColumnCount()
  {
    return 2;
  }

  public Object getValueAt(int rowIndex, int columnIndex)
  {
//    if (columnIndex == 0)
//      return FilterType.getFiterTypeOf((UriFilter)filters.get(rowIndex)).toString();
//    if (columnIndex == 1)
//      return filters.get(rowIndex);
    return null;
  }

  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    if (columnIndex == 1) {
      try {
        filters.remove(rowIndex);
      } catch (ArrayIndexOutOfBoundsException e) {}
      filters.add(rowIndex, aValue);
    }
  }

  public String getColumnName(int col)
  {
    if (col == 0)
      return "FilterType";
    if (col == 1)
      return "Filter";
    return null;
  }

  public List getData()
  {
    return filters;
  }

  public Object removeElementAt(int idx)
  {
    Object ret = null;
    try {
      ret = filters.remove(idx);
    } catch (ArrayIndexOutOfBoundsException e) {}
    return ret;
  }



}
