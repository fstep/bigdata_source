/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Small dialog box to lock application during critical processes.<br>
 * This dialog appeares as moda;, so user cannot interruct with GUI of progrlam until {@link LockDialog}
 * dialog is closed.  
 */

class LockDialog extends JDialog
{
  private static final long serialVersionUID = -6037943527626361177L;
  JPanel jPanel1 = new JPanel();
  JLabel jLabel = new JLabel();
  BorderLayout borderLayout1 = new BorderLayout();
  final int pause = 100000; // max time to wait
  Thread splashThread;
  String labelText;

  public LockDialog(Frame parent, String labelText)
  {
    super(parent);
    this.labelText = labelText;
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);

    //this.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
    this.setTitle("Please wait...");

    JLabel label = new JLabel();
    label.setText("              " + labelText + "             ");
    label.setFont(new java.awt.Font("Dialog", 0, 20));
    getContentPane().add(label, BorderLayout.CENTER);
    pack();

    Dimension frmSize = parent.getSize();
    Point loc = parent.getLocation();
    Dimension dlgSize = this.getPreferredSize();
    this.setLocation((frmSize.width - dlgSize.width) / 2 + loc.x, (frmSize.height - dlgSize.height) / 2 + loc.y);


    this.setModal(true);
    this.setResizable(false);

    final Runnable closerRunner = new Runnable() {
      public void run() {
        setVisible(false);
        dispose();
      }
    };

    Runnable waitRunner = new Runnable() {
      public void run() {
        try {
          setVisible(true);
          Thread.sleep(pause);
          SwingUtilities.invokeAndWait(closerRunner);
        }
        catch (Exception e) {
          //e.printStackTrace();
          // can catch InvocationTargetException
          // can catch InterruptedException
        }
      }
    };

    splashThread = new Thread(waitRunner, "SplashThread");
    splashThread.start();
  }

  public void close()
  {
    setVisible(false);
    splashThread.interrupt();
    dispose();
  }

}
