/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.gui;

import java.awt.*;
import javax.swing.*;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
/*
 * Created by JFormDesigner on Tue Jan 25 10:28:46 EET 2005
 */



/**
 * @author porva porva
 */
public class DialogAddEditTaskGUI extends JDialog {
  private static final long serialVersionUID = -7687471813968337594L;
  public DialogAddEditTaskGUI(Frame owner) {
		super(owner);
		initComponents();
	}

	public DialogAddEditTaskGUI(Dialog owner) {
		super(owner);
		initComponents();
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		// Generated using JFormDesigner Evaluation license
		dialogPane = new JPanel();
		contentPane = new JPanel();
		jTabbedPane = new JTabbedPane();
		panelTaskFetchTask = new JPanel();
    panelTaskFetchRssTask = new JPanel();
		label1 = new JLabel();
		jTextFieldURL = new JTextField();
		label2 = new JLabel();
		jSpinnerDepthFetchTask = new JSpinner();
		panelTaskGoogleTask = new JPanel();
		label3 = new JLabel();
		jTextFieldQuery = new JTextField();
		label4 = new JLabel();
		jSpinnerDepthGoogleTask = new JSpinner();
		label5 = new JLabel();
		jSpinnerHitsNum = new JSpinner();
		buttonBar = new JPanel();
		okButton = new JButton();
		cancelButton = new JButton();
		CellConstraints cc = new CellConstraints();
    
    label11 = new JLabel();
    label22 = new JLabel();
    jSpinnerDepthFetchRSSTask = new JSpinner();
    jTextFieldRSSURL = new JTextField();

		//======== this ========
		Container contentPane2 = getContentPane();
		contentPane2.setLayout(new BorderLayout());

		//======== dialogPane ========
		{
			dialogPane.setBorder(Borders.DIALOG_BORDER);
			
			dialogPane.setLayout(new BorderLayout());
			
			//======== contentPane ========
			{
				contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
				
				//======== jTabbedPane ========
				{
					
					//======== panelTaskFetchTask ========
					{
						panelTaskFetchTask.setBorder(Borders.DLU4_BORDER);
						panelTaskFetchTask.setLayout(new FormLayout(
							new ColumnSpec[] {
								FormFactory.GLUE_COLSPEC,
								FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
								new ColumnSpec("max(default;25dlu)")
							},
							new RowSpec[] {
								new RowSpec("max(default;5dlu)"),
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								new RowSpec("max(default;5dlu)"),
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC
							}));
						
						//---- label1 ----
						label1.setText("URL to download:");
						panelTaskFetchTask.add(label1, cc.xy(1, 3));
						panelTaskFetchTask.add(jTextFieldURL, cc.xywh(1, 5, 3, 1));
						
						//---- label2 ----
						label2.setText("Initial depth:");
						panelTaskFetchTask.add(label2, cc.xy(1, 9));
						
						//---- jSpinnerDepthFetchTask ----
						jSpinnerDepthFetchTask.setFont(new Font("Tahoma", Font.PLAIN, 14));
						jSpinnerDepthFetchTask.setModel(new SpinnerNumberModel(0, 0, 1000, 1));
						panelTaskFetchTask.add(jSpinnerDepthFetchTask, cc.xy(3, 9));
					}
					jTabbedPane.addTab("Fetch task", panelTaskFetchTask);
          
          //======== panelFetchRssTask ========
          {
            panelTaskFetchRssTask.setBorder(Borders.DLU4_BORDER);
            panelTaskFetchRssTask.setLayout(new FormLayout(
              new ColumnSpec[] {
                FormFactory.GLUE_COLSPEC,
                FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                new ColumnSpec("max(default;25dlu)")
              },
              new RowSpec[] {
                new RowSpec("max(default;5dlu)"),
                FormFactory.LINE_GAP_ROWSPEC,
                FormFactory.DEFAULT_ROWSPEC,
                FormFactory.LINE_GAP_ROWSPEC,
                FormFactory.DEFAULT_ROWSPEC,
                FormFactory.LINE_GAP_ROWSPEC,
                new RowSpec("max(default;5dlu)"),
                FormFactory.LINE_GAP_ROWSPEC,
                FormFactory.DEFAULT_ROWSPEC,
                FormFactory.LINE_GAP_ROWSPEC,
                FormFactory.DEFAULT_ROWSPEC,
                FormFactory.LINE_GAP_ROWSPEC,
                FormFactory.DEFAULT_ROWSPEC
              }));
            
            //---- label11 ----
            label11.setText("RSS URL to download:");
            panelTaskFetchRssTask.add(label11, cc.xy(1, 3));
            panelTaskFetchRssTask.add(jTextFieldRSSURL, cc.xywh(1, 5, 3, 1));
            
            //---- label22 ----
            label22.setText("Initial depth:");
            panelTaskFetchRssTask.add(label22, cc.xy(1, 9));
            
            //---- jSpinnerDepthFetchRSSTask ----
            jSpinnerDepthFetchRSSTask.setFont(new Font("Tahoma", Font.PLAIN, 14));
            jSpinnerDepthFetchRSSTask.setModel(new SpinnerNumberModel(0, 0, 1000, 1));
            panelTaskFetchRssTask.add(jSpinnerDepthFetchRSSTask, cc.xy(3, 9));
          }
          jTabbedPane.addTab("Fetch RSS", panelTaskFetchRssTask);

					
					//======== panelTaskGoogleTask ========
					{
						panelTaskGoogleTask.setBorder(Borders.DLU4_BORDER);
						panelTaskGoogleTask.setLayout(new FormLayout(
							new ColumnSpec[] {
								FormFactory.GLUE_COLSPEC,
								FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
								new ColumnSpec("max(default;25dlu)")
							},
							new RowSpec[] {
								new RowSpec("max(default;5dlu)"),
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								new RowSpec("max(default;5dlu)"),
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								new RowSpec("max(default;5dlu)"),
								FormFactory.LINE_GAP_ROWSPEC,
								FormFactory.DEFAULT_ROWSPEC,
								FormFactory.LINE_GAP_ROWSPEC,
								new RowSpec("max(default;12dlu)")
							}));
						
						//---- label3 ----
						label3.setText("Query:");
						panelTaskGoogleTask.add(label3, cc.xy(1, 3));
						panelTaskGoogleTask.add(jTextFieldQuery, cc.xywh(1, 5, 3, 1));
						
						//---- label4 ----
						label4.setText("Initial depth:");
						panelTaskGoogleTask.add(label4, cc.xy(1, 9));
						
						//---- jSpinnerDepthGoogleTask ----
						jSpinnerDepthGoogleTask.setFont(new Font("Tahoma", Font.PLAIN, 14));
						jSpinnerDepthGoogleTask.setModel(new SpinnerNumberModel(0, 0, 1000, 1));
						panelTaskGoogleTask.add(jSpinnerDepthGoogleTask, cc.xy(3, 9));
						
						//---- label5 ----
						label5.setText("Number of hits:");
						panelTaskGoogleTask.add(label5, cc.xy(1, 13));
						
						//---- jSpinnerHitsNum ----
						jSpinnerHitsNum.setFont(new Font("Tahoma", Font.PLAIN, 14));
						jSpinnerHitsNum.setModel(new SpinnerNumberModel(1, 1, 1000, 1));
						panelTaskGoogleTask.add(jSpinnerHitsNum, cc.xy(3, 13));
					}
					jTabbedPane.addTab("Google task", panelTaskGoogleTask);
				}
				contentPane.add(jTabbedPane);
			}
			dialogPane.add(contentPane, BorderLayout.CENTER);
			
			//======== buttonBar ========
			{
				buttonBar.setBorder(Borders.BUTTON_BAR_GAP_BORDER);
				buttonBar.setLayout(new FormLayout(
					new ColumnSpec[] {
						FormFactory.GLUE_COLSPEC,
						FormFactory.BUTTON_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC,
						FormFactory.BUTTON_COLSPEC
					},
					RowSpec.decodeSpecs("pref")));
				
				//---- okButton ----
				okButton.setText("OK");
				buttonBar.add(okButton, cc.xy(2, 1));
				
				//---- cancelButton ----
				cancelButton.setText("Cancel");
				buttonBar.add(cancelButton, cc.xy(4, 1));
			}
			dialogPane.add(buttonBar, BorderLayout.SOUTH);
		}
		contentPane2.add(dialogPane, BorderLayout.CENTER);
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	// Generated using JFormDesigner Evaluation license
	private JPanel dialogPane;
	private JPanel contentPane;
	JTabbedPane jTabbedPane;
	JPanel panelTaskFetchTask;
  JPanel panelTaskFetchRssTask;
	private JLabel label1;
	JTextField jTextFieldURL;
	private JLabel label2;
	JSpinner jSpinnerDepthFetchTask;
	JPanel panelTaskGoogleTask;
	private JLabel label3;
	JTextField jTextFieldQuery;
	private JLabel label4;
	JSpinner jSpinnerDepthGoogleTask;
	private JLabel label5;
	JSpinner jSpinnerHitsNum;
	private JPanel buttonBar;
	JButton okButton;
	JButton cancelButton;
  
  JLabel label11;
  JLabel label22;
  JSpinner jSpinnerDepthFetchRSSTask;
  JTextField jTextFieldRSSURL;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
