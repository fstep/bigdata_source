/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.google;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Parses Google web search result page to retrieve information from it.
 */
public class GoogleResultPageParser
{
  private transient String toString;

  /**
   * Allocates new {@link GoogleResultPageParser} object.
   */
  public GoogleResultPageParser()
  {
  }

  /**
   * Returns {@link WebSearchResult} object from Google web search result page.
   * 
   * @param resPage content of Google web result page.
   * @param query query that produced that page.
   * @return {@link WebSearchResult} object from Google web search result page.
   */
  public WebSearchResult parseWebResultPage(String resPage, String query)
  {
    WebSearchResult searchResult = new WebSearchResult(query);
    resPage = resPage.replaceAll("\\s+", " ");

    Pattern p = Pattern.compile(".*Results <b>\\d+</b> - <b>\\d+</b> of about <b>(.+?)</b>.*");
    Matcher m = p.matcher(resPage);

    if (m.matches()) {
      String totalDocs = m.group(1);

      if (totalDocs != null) {
        totalDocs = totalDocs.replaceAll(",", "");
        long nums = (new Long(totalDocs)).longValue();
        searchResult.setTotalResultsNum(nums);

      }
    }

    p = Pattern.compile("<p class=g>\\s*<a [^>]*href=([^>\\s]*) ?[^>]*>(.*?)</a>");
    m = p.matcher(resPage);
    int i = 0;
    while (m.find()) {
      i++;
      Hit res = new Hit();
      String url = m.group(1);
      if (url.startsWith("\"") || url.startsWith("\'"))
        url = url.substring(1, url.length());
      if (url.endsWith("\"") || url.endsWith("\'"))
        url = url.substring(0, url.length() - 1);
      
      res.setUrl(url);
      res.setNum(i);
      searchResult.addResult(res);
    }

    return searchResult;
  }
  
  public String toString()
  {
    if (toString == null) {
      toString = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).toString();
    }
    return toString;
  }
}
