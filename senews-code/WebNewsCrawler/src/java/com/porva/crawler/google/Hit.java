/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.google;


/**
 * Represents Google hit.
 */
public class Hit
{
  private long num = 0;
  private String url = null;
  private String title = null;
//  private String snippet = null;
//  private String date = null;
//  private int size = 0;

  /**
   * Allocates new {@link Hit} object.
   */
  public Hit()
  {
  }

  /**
   * Sets number of hit in Google's relevant list.
   * @param num number of hit in Google's relevant list.
   */
  protected void setNum(long num)   {    this.num = num;  }

  /**
   * Sets url of hit.
   * @param url
   */
  protected void setUrl(String url)   {    this.url = url;  }

  /**
   * Sets title of hit.
   * @param title
   */
  protected void setTitle(String title)   {    this.title  = title;  }

  /**
   * Returns URL of  hit.
   * @return URL of  hit.
   */
  public String getURLString() { return url; }
  
  public String getTitle() { return title; }

  /**
   * Returns string representation of current {@link Hit} object.
   * @return string representation of current {@link Hit} object.
   */
  public String toString()
  {
    String res = new String();
    if (num != 0)
      res = "num: " + Long.toString(num) + "\n";
    if (url != null)
      res += "url: " + url + "\n";
    return res;
  }

  /**
   * Compares current {@link Hit} object with given <code>anotherHit</code>.
   * @param anotherHit {@link Hit} object to compare with.
   * @return <code>true</code> if current {@link Hit} object has the same content as
   * given <code>anotherHit</code>..
   */
  public boolean equals(Object anotherHit)
  {
    try {
      Hit otherHit = (Hit) anotherHit;
      if (!otherHit.getURLString().equals(url))
        return false;
      // todo more comporasions
    } catch (Exception e) {
      return false;
    }
    return true;
  }

  public long getNum()
  {
    return num;
  }
}
