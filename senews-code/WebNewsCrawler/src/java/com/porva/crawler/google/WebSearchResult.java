/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.google;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 */

public class WebSearchResult
{
  private List<Hit> results = new ArrayList<Hit>();

  private String query;

  private long totalResNum; // total number of results

  /**
   * Allocates new {@link WebSearchResult} object with given <code>query</code>.
   * 
   * @param query query string.
   */
  public WebSearchResult(String query)
  {
    this.query = query;
  }

  /**
   * Adds another {@link Hit} object <code>res</code> to current {@link WebSearchResult} object.
   * 
   * @param res {@link Hit} object to add.
   */
  protected void addResult(Hit res)
  {
    results.add(res);
  }

  /**
   * Sets total number of hits retrieved.
   * 
   * @param num total number of hits retrieved.
   */
  protected void setTotalResultsNum(long num)
  {
    this.totalResNum = num;
  }

  /**
   * Returns list of hits parsed from Google web result page.
   * 
   * @return list of google hits.
   */
  public List<Hit> getResults()
  {
    return results;
  }

  /**
   * Returns total number of hits retrieved.
   * 
   * @return total number of hits retrieved.
   */
  public long getTotalResultsNum()
  {
    return totalResNum;
  }

  /**
   * Compares current {@link WebSearchResult} object with given <code>anotherWebSearchResult</code>.
   * 
   * @param anotherWebSearchResult {@link WebSearchResult} object to compare with.
   * @return <code>true</code> if current {@link WebSearchResult} object has the same content as
   *         given <code>anotherWebSearchResult</code>..
   */
  public boolean equals(Object anotherWebSearchResult)
  {
    try {
      WebSearchResult otherRes = (WebSearchResult) anotherWebSearchResult;
      if (results.size() != otherRes.getResults().size())
        return false;

      for (int i = 0; i < results.size(); i++) {
        if (!results.get(i).equals(otherRes.getResults().get(i)))
          return false;
      }
    } catch (Exception e) {
      return false;
    }

    return true;
  }

  public String toString()
  {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("results", results)
        .append("query", query).append("totalResNum", totalResNum).toString();
  }

}
