/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.crawler.Crawler.CrawlerStatus;
import com.porva.crawler.db.CrawlerDBFactory;
import com.porva.crawler.db.DBExporter;
import com.porva.crawler.db.DefaultExportFilter;
import com.porva.crawler.db.ExportFilter;
import com.porva.crawler.gui.CrawlerMain_Visual;

/**
 * Main class to start crawler.
 * 
 * @author Poroshin V.
 * @date Jan 22, 2006
 */
public class CrawlerMain
{

  private static Log logger = LogFactory.getLog(CrawlerMain.class);

  // //////////////////////////////////////////// command line functions 
  private static Options initCmdParams()
  {
    Options options = new Options();
    Option optionHelp = new Option("h", "display help message and exit");
    optionHelp.setRequired(false);
    options.addOption(optionHelp);

    Option optionCmd = new Option("cmd", "available commands: START|SERVER|GUI|EXPORT");
    optionCmd.setRequired(true);
    optionCmd.setArgs(1);
    optionCmd.setArgName("cmd");
    options.addOption(optionCmd);

    // cmd:SERVER options
    Option optionPort = new Option("p", "port number (required)");
    optionPort.setRequired(false);
    optionPort.setArgs(1);
    optionPort.setArgName("port");
    options.addOption(optionPort);

    Option optionConfigFile = new Option("c", "config file");
    optionConfigFile.setRequired(false);
    optionConfigFile.setArgs(1);
    optionConfigFile.setArgName("c");
    options.addOption(optionConfigFile);

    // cmd:DBTOOLS options
    // options for command EXPORT
    Option optionDBDir = new Option("dbDir", "directory of db and db enviroment");
    optionDBDir.setRequired(false);
    optionDBDir.setArgs(1);
    optionDBDir.setArgName("dbDir");
    options.addOption(optionDBDir);

    Option optionExportDir = new Option("exportDir", "directory to export database");
    optionExportDir.setRequired(false);
    optionExportDir.setArgs(1);
    optionExportDir.setArgName("exportDir");
    options.addOption(optionExportDir);

    Option optionFilters = new Option("filters", "export filters");
    optionFilters.setRequired(false);
    optionFilters.setArgs(1);
    optionFilters.setArgName("filters");
    options.addOption(optionFilters);
    
    Option optionOutFilters = new Option("outDir", "export filters");
    optionOutFilters.setRequired(false);
    optionOutFilters.setArgs(1);
    optionOutFilters.setArgName("outDir");
    options.addOption(optionOutFilters);

    return options;
  }

  private static void printHelpMsg()
  {
    Options options = initCmdParams();
//    "java -jar crawler-all.jar -cmd server -p 12345"
//    "java -jar crawler-all.jar -cmd export [-exportDir <exportDir>] [-filters <filters>]"
    HelpFormatter formatter = new HelpFormatter();
    System.out.println("Crawler server ver. 1.0");
    formatter.printHelp("parameters:", options);
    System.out.println("\nServer mode:");
    System.out.println(CrawlerServer.help);
  }

  static void init(final CommandLine cmd) throws Exception
  {
    if (cmd.hasOption("h")) {
      printHelpMsg();
      System.exit(1);
    }

    String command = cmd.getOptionValue("cmd");
    if (command.equalsIgnoreCase("export")) {
      processCmdExport(cmd);
    } else if (command.equalsIgnoreCase("join")) {
      System.out.println("Not implemented");
    } else if (command.equalsIgnoreCase("server")) {
      processCmdServer(cmd);
    } else if (command.equalsIgnoreCase("start")) {
      processCmdStart();
    } else if (command.equalsIgnoreCase("gui")) {
      processCmdGui();
    }
  }
  
  private static void processCmdGui()
  {
    new CrawlerMain_Visual();
  }

  private static void processCmdExport(final CommandLine cmd) throws Exception
  {
    // init export filters
    String filterParams = null;
    List<ExportFilter> filters = new ArrayList<ExportFilter>();
    if (cmd.hasOption("filters")) {
      filterParams = cmd.getOptionValue("filters");
      DefaultExportFilter filter = new DefaultExportFilter(filterParams);
      filters.add(filter);
    }

    
    Properties prop = new Properties();
    prop.load(new FileInputStream("conf/crawler.properties"));
    CrawlerDBFactory dbFactory = CrawlerInit.initDBFactory(prop);

    DBExporter exporter = new DBExporter();
    
    String exportDir = "export-" + System.currentTimeMillis();
    if (cmd.hasOption("outDir")) 
      exportDir = cmd.getOptionValue("outDir");
    File f = new File(exportDir);
    if (!f.exists())
      f.mkdir();
    
    if (logger.isInfoEnabled())
      logger.info("Exporting crawled data to the directory " + f);
    exporter.exportNews(dbFactory, f, filters);
    
    if (logger.isDebugEnabled()) {
      logger.debug("Exporting databases information to the file export-tostring");
      exporter.exportAllToString(dbFactory, new FileWriter("export-tostring"));
    }
    
    dbFactory.close();
  }

  private static void processCmdStart() throws Exception
  {
    Crawler crawler = CrawlerInit.constructCrawler(CrawlerInit.getDefaultProperties());
    logger.info("Crawler constructed:\n" + crawler);

    crawler.startWork();
    crawler.waitUntil(CrawlerStatus.DONE);
    CrawlerInit.finalizeCrawler();
  }

  private static void processCmdServer(CommandLine cmd) throws Exception
  {
    Properties prop = new Properties();

    if (cmd.hasOption("c")) {
      String configFileStr = cmd.getOptionValue("c");
      prop.load(new FileInputStream(configFileStr));
    } else {
      prop = CrawlerInit.getDefaultProperties();
    }

    int port = 12345;
    if (cmd.hasOption("p"))
      port = Integer.parseInt(cmd.getOptionValue("p"));

    Crawler crawler = CrawlerInit.constructCrawler(prop);
    logger.info("Crawler constructed:\n" + crawler);

    CrawlerServer crawlerServer = new CrawlerServer(port, crawler);
    crawlerServer.run();
    crawler.waitUntil(CrawlerStatus.DONE); // todo:
    CrawlerInit.finalizeCrawler();
  }

  private static CommandLine parseCmdParams(Options options, String[] args) throws ParseException
  {
    CommandLineParser parser = new BasicParser();
    CommandLine cmd = null;

    cmd = parser.parse(options, args);
    return cmd;
  }

  /**
   * @param argv
   */
  public static void main(String[] argv)
  {
    try {
      Options options = initCmdParams();
      CommandLine cmd = parseCmdParams(options, argv);
      init(cmd);
    } catch (ParseException e) {
      System.out.println("Command line error: " + e.getMessage());
      printHelpMsg();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
