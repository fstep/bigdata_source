/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

// todo: refactor

package com.porva.crawler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.crawler.db.CrawlerDBFactory;
import com.porva.crawler.db.DBException;
import com.porva.crawler.db.berkley.BerkleyCrawlerDBFactory;
import com.porva.crawler.filter.Filter;
import com.porva.crawler.filter.FilterFactory;
import com.porva.crawler.gui.CrawlerMain_Visual;
import com.porva.crawler.gui.CrawlerManager_Visual;
import com.porva.crawler.gui.GUIWorkload;
import com.porva.crawler.manager.DBManager;
import com.porva.crawler.net.HttpClient;
import com.porva.crawler.net.apache.ApacheHttpService;
import com.porva.crawler.plugin.DBStat;
import com.porva.crawler.plugin.HtmlViewer;
import com.porva.crawler.plugin.HyperGraph;
import com.porva.crawler.plugin.PluginManager;
import com.porva.crawler.plugin.SourceViewer;
import com.porva.crawler.service.FilterService;
import com.porva.crawler.service.ServiceException;
import com.porva.crawler.service.ServiceProvider;
import com.porva.crawler.task.CITFormatReader;
import com.porva.crawler.task.InitialTaskReader;
import com.porva.crawler.workload.DBWorkload;
import com.porva.crawler.workload.Workload;
import com.porva.html.CharsetDetectorService;
import com.porva.html.DOMDocumentParser;
import com.porva.html.HTML2XMLParser;
import com.porva.html.HTMLSAXLinksExtractor;
import com.porva.html.KCEParser;
import com.porva.html.SESQParser;

/**
 * Initialize the crawler.
 * 
 * @author Poroshin V.
 * @date Jan 22, 2006
 */
public class CrawlerInit
{
  private static CrawlerDBFactory dbFactory = null;

  private static GUIWorkload guiWorkload;

  private static ServiceProvider crawlerServices;

  private static Log logger = LogFactory.getLog(CrawlerInit.class);

  private static Properties properties = new Properties();

  private static Properties defaultProperties = new Properties();

  public static final String defaultPropertiesFile = System.getProperty("user.dir")
      + File.separator + "conf" + File.separator + "crawler.properties";

  static {
    try {
      logger.info("Loading default properties from: " + defaultPropertiesFile);
      defaultProperties.load(new FileInputStream(defaultPropertiesFile));
      properties.putAll(defaultProperties);
    } catch (FileNotFoundException e) {
      logger.warn("Cannot find default properties file: " + defaultPropertiesFile);
      e.printStackTrace();
    } catch (IOException e) {
      logger.warn("Cannot find default properties file: " + defaultPropertiesFile);
      e.printStackTrace();
    }
  }

  private CrawlerInit()
  {
  }

  /**
   * Static method to init database factory.
   * 
   * @param prop Properties to init database factory. Must contain "db.dir" value.
   * @return database factory.
   * @throws DBException if some database exception occured.
   * @throws NullArgumentException if <code>prop</code> is <code>null</code>.
   */
  public static CrawlerDBFactory initDBFactory(final Properties prop) throws DBException
  {
    if (prop == null)
      throw new NullArgumentException("prop");

    String dbDir = (String) prop.get("db.dir");
    if (dbDir == null)
      throw new IllegalArgumentException("properites do not have db.dir parameter");

    return new BerkleyCrawlerDBFactory(new File(dbDir));
  }

  /**
   * Returns properties used for build a crawler.
   * 
   * @return properties used for build a crawler.
   */
  public static Properties getProperties()
  {
    return properties;
  }

  /**
   * Returns default properties.
   * 
   * @return default properties.
   */
  public static Properties getDefaultProperties()
  {
    return defaultProperties;
  }

  /**
   * Init the crawler with console interface.
   * 
   * @param properties
   * @throws Exception
   * @throws NullArgumentException if <code>prop</code> is <code>null</code>.
   */
  public static Crawler constructCrawler(final Properties properties) throws Exception
  {
    if (properties == null)
      throw new NullArgumentException("properties");

    return init(propToMap(properties));
  }

  /**
   * Init the crawle with GUI.
   * 
   * @param properties
   * @param visualCrawler
   * @throws Exception
   */
  public static Crawler constructCrawler(final Properties properties,
                                         CrawlerMain_Visual visualCrawler) throws Exception
  {
    if (properties == null)
      throw new NullArgumentException("properties");

    return initWithGUI(visualCrawler, propToMap(properties));
  }

  public static GUIWorkload getGUIWorkload()
  {
    return guiWorkload;
  }

  public static void finalizeCrawler() throws Exception
  {
    PluginManager.closeManager();
    crawlerServices.stopAllServices();
    dbFactory.close();
  }

  // //////////////////////////////////////// private

  private static Map<String, String> propToMap(final Properties prop)
  {
    assert prop != null;

    final Map<String, String> map = new HashMap<String, String>();
    for (Map.Entry<Object, Object> entry : prop.entrySet())
      map.put((String) entry.getKey(), (String) entry.getValue());

    return map;
  }

  private static Crawler initWithGUI(CrawlerMain_Visual visualCrawler, Map<String, String> prop)
      throws Exception
  {
    assert prop != null;

    logger.info("Initializing crawler ...");

    dbFactory = initDB(prop);
    HttpClient httpClient = new ApacheHttpService(prop);
    List<Filter> filters = initFilters(prop, httpClient);

    crawlerServices = new ServiceProvider();
    crawlerServices.register(new FilterService(filters));

    // todo:
    crawlerServices.register(new CrawlerManager_Visual(visualCrawler, initTaskReader(prop),
        dbFactory));
    guiWorkload = new GUIWorkload(dbFactory);
    crawlerServices.register(guiWorkload);
    crawlerServices.register(new DefaultFrontier(crawlerServices));
    crawlerServices.register(httpClient);

    crawlerServices.register(new HTMLSAXLinksExtractor());
    crawlerServices.register(new CharsetDetectorService());

    if (prop.get("parser.html2xml.active").equals("yes"))
      crawlerServices.register(newHTML2XMLParser(prop));
    if (prop.get("parser.sesq.active").equals("yes"))
      crawlerServices.register(newSESQParser(prop));
    if (prop.get("parser.kce.active").equals("yes"))
      crawlerServices.register(new KCEParser());

    int looptime = Integer.parseInt(prop.get("crawler.loop-time"));
    int fetchersNum = Integer.parseInt(prop.get("crawler.workers-num"));

    PluginManager.registerObject(guiWorkload, Workload.class);
    PluginManager.registerObject(dbFactory, CrawlerDBFactory.class);
    PluginManager.registerPlugin(DBStat.class);
    PluginManager.registerPlugin(SourceViewer.class);
    PluginManager.registerPlugin(HtmlViewer.class);
    PluginManager.registerPlugin(HyperGraph.class);

    return new DefaultCrawler(looptime, crawlerServices, fetchersNum);
  }

  private static Crawler init(final Map<String, String> prop) throws Exception
  {
    assert prop != null;

    logger.info("Initializing crawler ...");

    dbFactory = initDB(prop);
    HttpClient httpClient = new ApacheHttpService(prop);
    List<Filter> filters = initFilters(prop, httpClient);

    crawlerServices = new ServiceProvider();
    crawlerServices.register(new FilterService(filters));

    // todo:
    crawlerServices.register(new DBManager(initTaskReader(prop), dbFactory));
    crawlerServices.register(new DBWorkload(dbFactory));
    crawlerServices.register(new DefaultFrontier(crawlerServices));
    crawlerServices.register(httpClient);

    crawlerServices.register(new HTMLSAXLinksExtractor());
    crawlerServices.register(new CharsetDetectorService());

    if (prop.get("parser.html2xml.active").equals("yes"))
      crawlerServices.register(newHTML2XMLParser(prop));
    if (prop.get("parser.sesq.active").equals("yes"))
      crawlerServices.register(newSESQParser(prop));
    if (prop.get("parser.kce.active").equals("yes"))
      crawlerServices.register(new KCEParser());

    int looptime = Integer.parseInt(prop.get("crawler.loop-time"));
    int fetchersNum = Integer.parseInt(prop.get("crawler.workers-num"));

    return new DefaultCrawler(looptime, crawlerServices, fetchersNum);
  }

  private static HTML2XMLParser newHTML2XMLParser(final Map<String, String> prop)
      throws ParserConfigurationException, IOException
  {
    assert prop != null;

    if (prop.get("parser.html2xml.properties-file") == null)
      throw new IllegalArgumentException(
          "properites do not contain 'parser.html2xml.properties-file' parameter");
    if (prop.get("parser.html2xml.scripts-dir") == null)
      throw new IllegalArgumentException(
          "properites do not contain 'parser.html2xml.properties-file' parameter");

    final File propFile = new File(prop.get("parser.html2xml.properties-file"));
    final File scriptsDir = new File(prop.get("parser.html2xml.scripts-dir"));

    return new HTML2XMLParser(propFile, scriptsDir);
  }

  private static SESQParser newSESQParser(final Map<String, String> prop)
  {
    assert prop != null;

    if (prop.get("parser.sesq.scripts-dir") == null)
      throw new IllegalArgumentException(
          "properites do not contain 'parser.sesq.scripts-dir' parameter");

    return new SESQParser(new File(prop.get("parser.sesq.scripts-dir")));
  }

  // //////////////////////////////////////////////////// default functions

  // todo:
  private static InitialTaskReader initTaskReader(final Map<String, String> prop)
  {
    assert prop != null;

    String initialTasks = prop.get("workload.waiting.filename");
    if (initialTasks == null)
      throw new IllegalArgumentException("workload.waiting.filename is not defined");

    return new CITFormatReader(new File(initialTasks));
  }

  // todo: implement ability to switch bw defferent db factories.
  private static CrawlerDBFactory initDB(final Map<String, String> prop) throws DBException
  {
    assert prop != null;

    String dbDir = prop.get("db.dir");
    if (dbDir == null)
      throw new IllegalArgumentException("");

    return new BerkleyCrawlerDBFactory(new File(dbDir));
  }

  private static List<Filter> initFilters(final Map<String, String> prop,
                                          final HttpClient httpClient) throws DBException,
      ServiceException
  {
    assert prop != null;
    assert httpClient != null;

    List<Filter> filters = new ArrayList<Filter>();
    String filtersStr = prop.get("filters.active-filters");
    if (filtersStr == null) {
      throw new IllegalArgumentException(
          "paramenter crawler.FetchTaskFilters is not defined in configuration");
    }
    String[] filtersNames = filtersStr.split(",");
    for (String filterName : filtersNames) {
      if (filterName.equals(""))
        continue;
      String params = prop.get(filterName);
      Filter filter = FilterFactory.newFilter(filterName, params, dbFactory, httpClient);
      // Filter filter = newFilter(filterName, params);
      filters.add(filter);
      logger.info("Loaded filter: " + filter);
    }
    return filters;
  }

  // ///////////////////////////////// static

  public static Map<DOMDocumentParser, Boolean> getParsers(final Properties prop)
  {
    Map<DOMDocumentParser, Boolean> parsers = new HashMap<DOMDocumentParser, Boolean>();
    try {
      if (prop.get("parser.html2xml.active").equals("yes"))
        parsers.put(newHTML2XMLParser(propToMap(prop)), true);
      else
        parsers.put(newHTML2XMLParser(propToMap(prop)), false);
      // if (prop.get("parser.sesq.active").equals("yes"))
      // parsers.put(newSESQParser(prop));
      if (prop.get("parser.kce.active").equals("yes"))
        parsers.put(new KCEParser(), true);
      else
        parsers.put(new KCEParser(), false);
    } catch (Exception e) {
      // TODO
      e.printStackTrace();
    }
    return parsers;
  }

  public static Map<Filter, Boolean> getFilters(final Properties prop)
  {
    Map<Filter, Boolean> filters = new HashMap<Filter, Boolean>();
    String filtersStr = (String) prop.get("filters.active-filters");
    if (filtersStr == null) {
      throw new IllegalArgumentException(
          "paramenter crawler.FetchTaskFilters is not defined in configuration");
    }

    List<String> activeFiltersNames = Arrays.asList(filtersStr.split(","));

    for (Map.Entry<Object, Object> entry : prop.entrySet()) {
      String filterName = (String) entry.getKey();
      if (!filterName.startsWith("filter."))
        continue;

      String params = (String) prop.get(filterName);
      if (filterName.equals(Filter.ROBOTS_RULES) || filterName.equals(Filter.REDIRECTS_LIMIT)
          || filterName.equals(Filter.DEPTH_LIMIT))
        continue;

      Filter filter = null;
      try {
        filter = FilterFactory.newFilter(filterName, params, null, null);
      } catch (Exception e) {
        logger.fatal("Failed to create filter name=" + filterName + " params=" + params, e);
        e.printStackTrace();
        throw new IllegalStateException("Failed to create filter name=" + filterName + " params="
            + params, e);
      }

      if (activeFiltersNames.contains(filterName))
        filters.put(filter, true);
      else
        filters.put(filter, false);

      logger.info("Loaded filter: " + filter);
    }
    return filters;
  }

  public static boolean hasActiveParser(final Properties prop)
  {
    if (prop == null)
      throw new NullArgumentException("prop");

    if (prop.get("parser.html2xml.active").equals("yes")
        || prop.get("parser.sesq.active").equals("yes")
        || prop.get("parser.kce.active").equals("yes"))
      return true;
    return false;
  }
}
