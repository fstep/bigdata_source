/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.crawler;

import com.porva.crawler.Crawler.CrawlerStatus;
import com.porva.net.tana.DefaultServerHandler;
import com.porva.net.tana.TanaServer;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.PropertyConfigurator;

import java.util.HashMap;
import java.util.Map;

/**
 * Crawler in server mode. Server wrapper controls crawling process and can react on command sended
 * via TANA protocol.
 */
public class CrawlerServer
{
  private int port;

  private TanaServer tanaServer;

  private CrawlerServerHandler serverHandler;

  private Crawler crawler;

  private static final Log logger = LogFactory.getLog(CrawlerServer.class);
  
  /** Available commands **/
  /**
   * Command to start a crawling process.
   */
  public static final String CMD_START = "start";
  
  /**
   * Command to pause a crawling process.
   */
  public static final String CMD_PAUSE = "pause";
  
  /**
   * Command to resume a crawling process after it has been stopped. 
   */
  public static final String CMD_RESUME = "resume";
  
  /**
   * Command to stop a crawling process.
   */
  public static final String CMD_STOP = "stop";
  
  /**
   * Command to shutdown a crawler.
   */
  public static final String CMD_SHUTDOWN = "shutdown";
  
  /**
   * Command to interrupt a crawling process.
   */
  public static final String CMD_EXIT = "exit";
  
  /**
   * Command to get statistic from the crawler.
   */
  public static final String CMD_STAT = "stat";
  
  /**
   * Command to change log4j properties file and reload it.<br>
   * Requires a parameter 'file'. 
   */
  public static final String CMD_LOG = "log";
  

	public static final String help = "Accepted TANA messages:\n" +
	"  'cmd' => 'one of the following commands:'\n" +
	"           '" + CMD_START + "' - start a crawling process\n" + 
	"           '" + CMD_PAUSE + "' - pause a crawling process\n" + 
	"           '" + CMD_RESUME + "' - resume a crawling process after it has been stopped\n" +
	"           '" + CMD_STOP + "' - stop a crawling process. Cannot be resumed\n" +
	"           '" + CMD_SHUTDOWN + "' - shutdown the crawler\n" +
	"           '" + CMD_EXIT + "' - interrupt a crawling process\n" +
	"           '" + CMD_STAT + "' - get statistic from the crawler\n" +
	"           '" + CMD_LOG + "' - change log4j properties file and reload it." +
	" Requires a parameter 'file'\n";


  /**
   * Allocates new instance of {@link CrawlerServer}.
   * 
   * @param port port number to start server on.
   * @param crawler initialized crawler object to wrap.
   * @throws NullArgumentException if <code>crawler</code> is <code>null</code>.
   */
  public CrawlerServer(int port, final Crawler crawler)
  {
    this.port = port;
    this.crawler = crawler;
    serverHandler = new CrawlerServerHandler();
    tanaServer = new TanaServer(this.port, serverHandler, true);

    if (this.crawler == null)
      throw new NullArgumentException("Crawler object for CrawlerServer cannot be null.");
  }

  /**
   * Starts crawler server and waits until its done.
   * 
   * @throws InterruptedException
   */
  public void run() throws InterruptedException
  {
    tanaServer.start();
    tanaServer.join();
  }

  // ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  private class CrawlerServerHandler extends DefaultServerHandler
  {
    CrawlerServerHandler()
    {
      super(logger, "Crawler");
    }

    public Map<String, byte[]> handleClientMsg(Map<String, byte[]> recvMap)
    {
      if (recvMap == null)
        throw new NullPointerException("Received TANA message cannot be null.");

      Map<String, byte[]> replyMap = new HashMap<String, byte[]>();
      try {
        String cmd = new String((byte[]) recvMap.get("cmd"));
        if (cmd == null) {
          logger.warn("Command task is not defined: use cmd parameter to specify task");
          replyMap.put("error",
                       "Command task is not defined: use cmd parameter to specify command task"
                           .getBytes());
          return replyMap;
        }

        logger.info("Crawler received command: " + cmd);
        if (cmd.equalsIgnoreCase(CMD_START)) {
          if (crawler.getStatus() == CrawlerStatus.READY)
            crawler.startWork();
          else
            replyMap.put("error", "Cannot start crawler. It was already started.".getBytes());
        } else if (cmd.equalsIgnoreCase(CMD_PAUSE)) {
          crawler.pauseWork();
        } else if (cmd.equalsIgnoreCase(CMD_RESUME)) {
          crawler.resumeWork();
        } else if (cmd.equalsIgnoreCase(CMD_STOP)) {
          if ((crawler.getStatus() != CrawlerStatus.PAUSED)
              && (crawler.getStatus() != CrawlerStatus.RUNNING)) {
            replyMap.put("error",
                         "Cannot stop crawler. Crawler was not started. Use cmd:start to start it."
                             .getBytes());
          } else {
            crawler.suspendWork();
          }
        } else if (cmd.equalsIgnoreCase(CMD_SHUTDOWN)) {
          if (crawler.getStatus() == CrawlerStatus.READY) {
            tanaServer.shutdown();
          }
          else if ((crawler.getStatus() != CrawlerStatus.DONE)
              && (crawler.getStatus() != CrawlerStatus.SUSPENDED)) {
            replyMap.put("error", "Cannot shutdown running crawler. Use cmd:stop before."
                .getBytes());
          } else {
            tanaServer.shutdown();
          }
        } else if (cmd.equalsIgnoreCase(CMD_EXIT)) {
          crawler.interruptWork();
          tanaServer.shutdown();
        } else if (cmd.equalsIgnoreCase(CMD_STAT)) {
          for (Map.Entry<String, String> entry : crawler.getStat().entrySet()) {
            replyMap.put(entry.getKey(), entry.getValue().getBytes());
          }
        } else if (cmd.equalsIgnoreCase(CMD_LOG)) {
          String file = new String((byte[]) recvMap.get("file"));
          PropertyConfigurator.configureAndWatch(file, 20 * 1000);
        } else {
          logger.warn("unknown command requested: " + cmd);
          replyMap.put("error", ("unknown command requested: " + cmd).getBytes());
        }

        replyMap.put("cmd", cmd.getBytes());
      } catch (Exception e) {
        replyMap.put("error", e.toString().getBytes());
      }
      return replyMap;
    }
  }
}
