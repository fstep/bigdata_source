/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler;

import java.util.List;

import org.apache.commons.lang.NullArgumentException;

import com.porva.crawler.service.CommonService;
import com.porva.crawler.task.Task;
import com.porva.crawler.task.TaskResult;
import com.porva.crawler.workload.WorkloadEntryStatus;
import com.porva.crawler.task.EmptyTask;

/**
 * Crawler's frontier interface.
 * Frontier is a thread-save object where workers threads will report to.
 * 
 * @author Poroshin V.
 * @date Sep 14, 2005
 */
public interface Frontier extends CommonService
{
  /**
   * Returns next {@link Task} task to perform.
   * 
   * @return next {@link Task} task to perform.<br>
   *         If there is no more tasks to process available in workload but
   *         there are still some running tasks then {@link EmptyTask} will be
   *         returned. If there is no any tasks to process available and no
   *         running tasks then task of type <code>LAST</code> will be returned.
   */
  public Task getTask();
  
  /**
   * Completes task and its result in manager and workload.
   * 
   * @param task processed task to complete.
   * @param result task result to complete.
   * @throws NullArgumentException if <code>task</code> or <code>result</code>
   *           is <code>null</code>.
   */
  public void completeTask(final Task task, final TaskResult result);

  /**
   * Adds new task to workload as {@link WorkloadEntryStatus#WAITING}.
   * 
   * @param task new task to add to workload.
   * @return <code>true</code> if task was added successfuly;
   *         <code>false</code> otherwise.
   */
  public boolean addTask(final Task task);

  /**
   * Adds list of tasks to workload as
   * {@link WorkloadEntryStatus#WAITING} tasks.
   * 
   * @param tasks list of tasks to add.
   * @return number of successfuly added tasks to workload.
   */
  public int addTaskList(final List<Task> tasks);

}
