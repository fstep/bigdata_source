/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.plugin;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

/**
 * Holds references to all registered (implemented {@link PluginUsable}) objects of application/
 * Used to register and run plugins and to provide access to all usefull objects for plugins.
 */

public class PluginManager
{
  // completely static
  private PluginManager() {}

  private static Hashtable<String,Object> registeredObjects  = new Hashtable<String,Object>();
  private static Vector<String> pluginClasses = new Vector<String>();
  private static Hashtable<Class,IPlugin> pluginObjects = new Hashtable<Class,IPlugin>();
  
  private static List<IPlugin> allPluginsObjects = new ArrayList<IPlugin>();

  /**
   * Registed object as usable by plugins.
   * @param href object to register.
   * @param cl object's class.
   */
  public static void registerObject(Object href, Class cl)
  {
    registeredObjects.put(cl.getName(), href);
  }

  /**
   * Returns registered object of given class <code>cl</code>.
   * This method is called by plugins to get needed objects of application.
   * @param cl Class of object
   * @return requested object if for given class <code>cl</code>. such an object was
   * registered or <code>null</code> otherwise.
   */
  protected static Object getObject(Class cl)
  {
    return registeredObjects.get(cl.getName());
  }

  /**
   * Register HtmlViewerControls.
   * @param pluginClass {@link Class} of HtmlViewerControls to register.
   */
  public static void registerPlugin(Class pluginClass)
  {
    pluginClasses.add(pluginClass.getName());
  }

  /**
   * Creates new HtmlViewerControls specified by class <code>pluginClass</code> and executes it.
   * @param pluginClass class of HtmlViewerControls to construct and execute.
   * @throws Exception
   */
  public static IPlugin runPlugin(Class pluginClass) throws Exception
  {
    if (!pluginClasses.contains(pluginClass.getName()))
      throw new Exception("Plugin class " + pluginClass.getName() + " is not registered!");

    IPlugin plugin = (IPlugin)(pluginClass.newInstance());  // todo: can rise cast exception -- catch it
    pluginObjects.put(pluginClass, plugin);
    allPluginsObjects.add(plugin);

    plugin.run();
    return plugin;
  }

  // todo: if there are several active objects for the same HtmlViewerControls ???
  /**
   * Returns current {@link IPlugin} object of given class <code>pluginClass</code>.<br>
   * This method does not construct any new {@link IPlugin} object!
   * @param pluginClass class of {@link IPlugin} object to get.
   * @return {@link IPlugin} object if such object exists; <code>null</code> otherwise.
   */
  public static IPlugin getPluginActiveObject(Class pluginClass)
  {
    Object pluginObject = pluginObjects.get(pluginClass);
    if (pluginObject == null)
      return null;
    return (IPlugin)pluginObject;
  }
  
  public static void closeManager() throws Exception
  {
    for (IPlugin plugin : allPluginsObjects)
      plugin.close();
  }

}
