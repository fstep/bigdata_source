/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.plugin;

import com.porva.crawler.CrawlerInit;
import com.porva.crawler.gui.MainFrame;
import com.porva.crawler.workload.Workload;
import com.porva.crawler.workload.WorkloadEntryStatus;
import com.porva.util.ResourceLoader;
import com.sleepycat.je.DatabaseException;

/**
 * Shows database statistic.
 * 
 * @see DBStatView
 */
public class DBStat implements IPlugin
{
  private int totalNum = 0;

  private int completeNum = 0;

  private int failedNum = 0;

  private int waitingNum = 0;

//  private int uptodateNum = 0;
//
//  private int oldNum = 0;
//
//  private long lifetime = 0;
//
//  private boolean isNewDB = false;
//
//  private static Log logger = LogFactory.getLog(DBStat.class);

  /**
   * Allocates new {@link DBStat} object.
   */
  protected DBStat()
  {
  }

  private boolean perform(Workload workload) throws DatabaseException
  {
    waitingNum = workload.getNumberOf(WorkloadEntryStatus.WAITING);
    completeNum = workload.getNumberOf(WorkloadEntryStatus.COMPLETE);
    failedNum = workload.getNumberOf(WorkloadEntryStatus.FAILED);
    totalNum = waitingNum + completeNum + failedNum;

    return true;
  }

  private String getReportAsString()
  {
    String dbDir = (String) CrawlerInit.getProperties().get("db.dir");

    StringBuffer sb = new StringBuffer();

    sb.append("DB path:   ").append(dbDir).append("\n\n").append("Total tasks:    ")
        .append(totalNum).append("\n").append("   completed:   ").append(completeNum).append("\n")
        .append("   failed:      ").append(failedNum).append("\n").append("   waiting:     ")
        .append(waitingNum).append("\n\n");
    
    return sb.toString();
  }

  public void run() throws Exception
  {
    Workload workload = (Workload) PluginManager.getObject(Workload.class);
    if (workload == null)
      throw new Exception("Workload class is not registered for using by plugins.");
    else {
      perform(workload);
      String report = getReportAsString();
      // System.out.println(getReportAsString());

      DBStatView view = new DBStatView(report);
      MainFrame mainFrame = (MainFrame) PluginManager.getObject(MainFrame.class);

      mainFrame.jPluginsTabbedPane.addTab("DB Stat", ResourceLoader.getIcon("Menu.Plugins.DBStat"),
                                          view);
      mainFrame.jPluginsTabbedPane.setSelectedComponent(view);
    }

  }

  public void close() throws Exception
  {
  }
}
