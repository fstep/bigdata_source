/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.plugin;

import calpa.html.CalHTMLPane;
import com.porva.util.ResourceLoader;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.*;
import java.net.MalformedURLException;
import java.net.URL;


public class HtmlViewerControls extends HtmlViewerControlsGUI
{
  private static final long serialVersionUID = -1557944349855789404L;
  private CalHTMLPane pane;
//  private static String IMG_PATH = "img/HtmlViewer/";

  private final Icon iconBackPassive = ResourceLoader.getIcon("Plugins.HtmlViewer.Back");
  private final Icon iconBackActive = ResourceLoader.getIcon("Plugins.HtmlViewer.Back-active");

  private final Icon iconForwardPassive = ResourceLoader.getIcon("Plugins.HtmlViewer.Forward");
  private final Icon iconForwardActive = ResourceLoader.getIcon("Plugins.HtmlViewer.Forward-active");

  private final Icon iconRefreshPassive = ResourceLoader.getIcon("Plugins.HtmlViewer.Refresh");
  private final Icon iconRefreshActive = ResourceLoader.getIcon("Plugins.HtmlViewer.Refresh-active");

  private final Icon iconStopPassive = ResourceLoader.getIcon("Plugins.HtmlViewer.Stop");
  private final Icon iconStopActive = ResourceLoader.getIcon("Plugins.HtmlViewer.Stop-active");

  private final Icon iconGoPassive = ResourceLoader.getIcon("Plugins.HtmlViewer.Go");
  private final Icon iconGoActive = ResourceLoader.getIcon("Plugins.HtmlViewer.Go-active");

  public HtmlViewerControls(CalHTMLPane pane)
  {
    this.pane = pane;

    buttonBack.setIcon(iconBackPassive);
    buttonBack.setText("");
    buttonBack.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        buttonBack_actionPerformed(e);
      }
    });
    buttonBack.addMouseListener(new MouseAdapter() {
      public void mouseEntered(MouseEvent e) {
        buttonBack.setIcon(iconBackActive);
        setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      }
      public void mouseExited(MouseEvent e)  {
        buttonBack.setIcon(iconBackPassive);
        setCursor(Cursor.getDefaultCursor());
      }
    });

    buttonForward.setIcon(iconForwardPassive);
    buttonForward.setText("");
    buttonForward.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        buttonForward_actionPerformed(e);
      }
    });
    buttonForward.addMouseListener(new MouseAdapter() {
      public void mouseEntered(MouseEvent e) {
        buttonForward.setIcon(iconForwardActive);
        setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      }
      public void mouseExited(MouseEvent e)  {
        buttonForward.setIcon(iconForwardPassive);
        setCursor(Cursor.getDefaultCursor());
      }
    });

    buttonStop.setIcon(iconStopPassive);
    buttonStop.setText("");
    buttonStop.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        buttonStop_actionPerformed(e);
      }
    });
    buttonStop.addMouseListener(new MouseAdapter() {
      public void mouseEntered(MouseEvent e) {
        buttonStop.setIcon(iconStopActive);
        setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      }
      public void mouseExited(MouseEvent e)  {
        buttonStop.setIcon(iconStopPassive);
        setCursor(Cursor.getDefaultCursor());
      }
    });


    buttonRefresh.setIcon(iconRefreshPassive);
    buttonRefresh.setText("");
    buttonRefresh.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        buttonRefresh_actionPerformed(e);
      }
    });
    buttonRefresh.addMouseListener(new MouseAdapter() {
      public void mouseEntered(MouseEvent e) {
        buttonRefresh.setIcon(iconRefreshActive);
        setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      }
      public void mouseExited(MouseEvent e)  {
        buttonRefresh.setIcon(iconRefreshPassive);
        setCursor(Cursor.getDefaultCursor());
      }
    });


    buttonGo.setIcon(iconGoPassive);
    buttonGo.setText("");
    buttonGo.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        buttonGo_actionPerformed(e);
      }
    });
    buttonGo.addMouseListener(new MouseAdapter() {
      public void mouseEntered(MouseEvent e) {
        buttonGo.setIcon(iconGoActive);
        setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      }
      public void mouseExited(MouseEvent e)  {
        buttonGo.setIcon(iconGoPassive);
        setCursor(Cursor.getDefaultCursor());
      }
    });

  }

  public void buttonBack_actionPerformed(ActionEvent e)
  {
    pane.goBack();
  }

  public void buttonForward_actionPerformed(ActionEvent e)
  {
    pane.goForward();
  }

  public void buttonStop_actionPerformed(ActionEvent e)
  {
    pane.stopAll();
  }

  public void buttonRefresh_actionPerformed(ActionEvent e)
  {
    pane.reloadDocument();
  }

  public void buttonGo_actionPerformed(ActionEvent e)
  {
    String address = textFieldAddress.getText();
    if (address.startsWith("http:") || address.startsWith("file:")) {
      URL url = null;
      try {
        url = new URL(address);
        pane.showHTMLDocument(url);
      } catch (MalformedURLException e1) {
        pane.showHTMLDocument("Cannot show address " +  address + ". " + e1.toString());
      }
    }
  }

  public void setAddressStr(String address)
  {
    textFieldAddress.setText(address);
  }

}
