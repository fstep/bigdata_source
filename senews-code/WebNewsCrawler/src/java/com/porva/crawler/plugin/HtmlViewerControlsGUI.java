/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.plugin;

import javax.swing.*;
import javax.swing.border.*;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
/*
 * Created by JFormDesigner on Tue Feb 01 00:28:57 EET 2005
 */



/**
 * @author porva porva
 */
public class HtmlViewerControlsGUI extends JPanel {
  private static final long serialVersionUID = -9080246335466833169L;
  public HtmlViewerControlsGUI() {
		initComponents();
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		// Generated using JFormDesigner Evaluation license
		buttonBack = new JButton();
		buttonForward = new JButton();
		buttonStop = new JButton();
		textFieldAddress = new JTextField();
		buttonRefresh = new JButton();
		buttonGo = new JButton();
		CellConstraints cc = new CellConstraints();

		//======== this ========
		setBorder(Borders.DLU4_BORDER);

		setLayout(new FormLayout(
			new ColumnSpec[] {
				new ColumnSpec("20px"),
				new ColumnSpec("20px"),
				new ColumnSpec("20px"),
				new ColumnSpec("20px"),
				FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
				FormFactory.GLUE_COLSPEC,
				FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
				new ColumnSpec("40px")
			},
			RowSpec.decodeSpecs("20px")));

		//---- buttonBack ----
		buttonBack.setBorder(null);
		buttonBack.setText("<<");
		buttonBack.setToolTipText("Back");
		add(buttonBack, cc.xy(1, 1));

		//---- buttonForward ----
		buttonForward.setBorder(null);
		buttonForward.setText(">>");
		buttonForward.setToolTipText("Forward");
		add(buttonForward, cc.xy(2, 1));

		//---- buttonStop ----
		buttonStop.setBorder(null);
		buttonStop.setText("Stop");
		buttonStop.setToolTipText("Stop");
		add(buttonStop, cc.xy(3, 1));
		add(textFieldAddress, cc.xy(6, 1));

		//---- buttonRefresh ----
		buttonRefresh.setBorder(null);
		buttonRefresh.setText("Refresh");
		buttonRefresh.setToolTipText("Refresh");
		add(buttonRefresh, cc.xy(4, 1));

		//---- buttonGo ----
		buttonGo.setBorder(null);
		buttonGo.setText("Go");
		add(buttonGo, cc.xy(8, 1));
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	// Generated using JFormDesigner Evaluation license
	JButton buttonBack;
	JButton buttonForward;
	JButton buttonStop;
	JTextField textFieldAddress;
	JButton buttonRefresh;
	JButton buttonGo;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
