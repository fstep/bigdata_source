/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.crawler.plugin;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.porva.crawler.db.CrawlerDBFactory;
import com.porva.crawler.db.DB;
import com.porva.crawler.gui.MainFrame;
import com.porva.crawler.task.FetchResult;
import com.porva.crawler.task.TaskResult;
import com.porva.util.ResourceLoader;

/**
 * Plugin to show source of downloaded document.<br>
 * This class combines HtmlViewerControls and its visual respresentation together.<br>
 * <br>
 * <img src="../../../../pic/HtmlViewerControls-sourceviewer.gif">
 */

public class SourceViewer extends JPanel implements IPlugin
{
  private static final long serialVersionUID = 7157739116779673558L;

  private DB<TaskResult> db = null;

  private BorderLayout borderLayout1 = new BorderLayout();

  private JScrollPane jScrollPane1 = new JScrollPane();

  private JTextArea jTextArea = new JTextArea();

  /**
   * Allocates new {@link SourceViewer} object.
   */
  protected SourceViewer()
  {
  }

  /**
   * Retrieves specified by key <code>uri</code> source of document from {@link FetchResultDB} and
   * showss it.
   * 
   * @param uri key in database of document which source to show.
   */
  public void showHtmlSource(String uri)
  {
    TaskResult taskResult = db.get(uri);
    if (taskResult == null)
      jTextArea.setText("Error: url " + uri + " is not presented in database.");
    else if (taskResult instanceof FetchResult) {
      FetchResult fetchResult = (FetchResult)taskResult;
      if (fetchResult.getStatus() != FetchResult.Status.COMPLETE)
        jTextArea.setText("Status: " + fetchResult.getStatus());
      else 
        jTextArea.setText(new String(fetchResult.getResponse().getBody()));
    }
    else
      jTextArea.setText("Task in not FetchTask");
  }

  /**
   * Run HtmlViewerControls. This method uses {@link PluginManager} class to access different
   * registered objects (objects impelemted {@link PluginUsable}) of application.
   * 
   * @throws Exception
   */
  public void run() throws Exception
  {
    MainFrame mainFrame = (MainFrame) PluginManager.getObject(MainFrame.class);
    CrawlerDBFactory dbFactory = (CrawlerDBFactory) PluginManager.getObject(CrawlerDBFactory.class);
    db = dbFactory.newResultDatabaseHandle();
    db.open();

    jTextArea.setText("Select link in complete table to see its HTML source.");
    jTextArea.setEditable(false);
    jScrollPane1.getViewport().add(jTextArea, null);
    setLayout(borderLayout1);
    add(jScrollPane1, BorderLayout.CENTER);

    mainFrame.jPluginsTabbedPane.addTab("Scource viewer", ResourceLoader
        .getIcon("Menu.Plugins.SourceViewer"), this);
    mainFrame.jPluginsTabbedPane.setSelectedComponent(this);
  }

  public void close() throws Exception
  {
    db.close();
  }
}
