///*
// *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
// *
// *   This program is free software; you can redistribute it and/or modify
// *   it under the terms of the GNU General Public License as published by
// *   the Free Software Foundation; either version 2 of the License, or
// *   (at your option) any later version.
// *
// *   This program is distributed in the hope that it will be useful,
// *   but WITHOUT ANY WARRANTY; without even the implied warranty of
// *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// *   GNU General Public License for more details.
// *
// *   You should have received a copy of the GNU General Public License
// *   along with this program; if not, write to the Free Software
// *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// */
//package com.porva.crawler;
//
//import com.porva.util.Configuration;
//
//public class CrawlerSettings
//{
//  public static final int DEF_FETCHERS_NUM = 10;
//  public static final int FETCHERS_NUM = Configuration.getInt("crawler.fetcherThreads", DEF_FETCHERS_NUM);
//
//  public static final String DEF_HTTP_AGENT_NAME = "NutchCVS";
//  public static final String HTTP_AGENT_NAME = Configuration.get("http.agent.name", DEF_HTTP_AGENT_NAME);
//
//  public static final String DEF_HTTP_AGENT_URL = "http://www.nutch.org/docs/en/bot.html";
//  public static final String HTTP_AGENT_URL = Configuration.get("http.agent.url", DEF_HTTP_AGENT_URL);
//
//  public static final String DEF_HTTP_ROBOTS_AGENTS = "NutchCVS,Nutch,*";
//  public static final String HTTP_ROBOTS_AGENTS = Configuration.get("http.robots.agents", DEF_HTTP_ROBOTS_AGENTS);
//
//  public static final String DEF_HTTP_AGENT_VER = "0.03-dev";
//  public static final String HTTP_AGENT_VER = Configuration.get("http.agent.version", DEF_HTTP_AGENT_VER);
//
//  public static final String DEF_HTTP_AGENT_EMAIL = "nutch-agent@lists.sourceforge.net";
//  public static final String HTTP_AGENT_EMAIL = Configuration.get("http.agent.email", DEF_HTTP_AGENT_EMAIL);
//
//  public static final String DEF_HTTP_AGENT_DESCRIPTION = "Nutch";
//  public static final String HTTP_AGENT_DESCRIPTION = Configuration.get("http.agent.description", DEF_HTTP_AGENT_DESCRIPTION);
//
//  public static final int DEF_HTTP_MAX_REDIRECTS = 4;
//  public static final int HTTP_MAX_REDIRECTS = Configuration.getInt("http.redirect.max", DEF_HTTP_MAX_REDIRECTS);
//
//  public static final String DEF_HTTP_PROXY_HOST = null;
//  public static final String HTTP_PROXY_HOST = Configuration.get("http.proxy.host", DEF_HTTP_PROXY_HOST);
//
//  public static final int DEF_HTTP_PROXY_PORT = 8080;
//  public static final int HTTP_PROXY_PORT = Configuration.getInt("http.proxy.port", DEF_HTTP_PROXY_PORT);
//
//  public static final int DEF_HTTP_TIMEOUT = 10000;
//  public static final int HTTP_TIMEOUT = Configuration.getInt("http.timeout", DEF_HTTP_TIMEOUT);
//
//  public static final int DEF_HTTP_CONTENT_LIMIT = 64*1024;
//  public static final int HTTP_CONTENT_LIMIT = Configuration.getInt("http.content.limit", DEF_HTTP_CONTENT_LIMIT);
//
//  public static final int DEF_DEPTH_LIMIT = 0;
//  public static final int DEPTH_LIMIT = Configuration.getInt("filter.depth-limit", DEF_DEPTH_LIMIT);
//
//  public static final long DEF_FETCH_TASK_LIFETIME = 86400000*5; // 5 days
//  public static final long FETCH_TASK_LIFETIME = Configuration.getLong("task.FetchTask.lifetime", DEF_FETCH_TASK_LIFETIME);
//  
//  public static final long DEF_ROBOTXT_LIFETIME = 86400000*30; // 30 days
//  public static final long ROBOTXT_LIFETIME = Configuration.getLong("task.RobotxtTask.lifetime", DEF_ROBOTXT_LIFETIME);
//
//  public static final long DEF_FETCHRSS_TASK_LIFETIME = 43200000; // 12 hours
//  public static final long FETCHRSS_TASK_LIFETIME = Configuration.getLong("task.FetchRssTask.lifetime", DEF_FETCHRSS_TASK_LIFETIME);
//
//  public static final String DEF_WAITING_FILE = "waiting.crl";
//  public static final String WAITING_FILE = Configuration.get("workload.waiting.filename", DEF_WAITING_FILE);
//
//  public static final String DEF_DB_ENV_DIR = "db";
//  public static final String DB_ENV_DIR = Configuration.get("db.dir", DEF_DB_ENV_DIR);
//
//  public static final String DEF_DB_RESULT = "mode:berkley";
//  public static final String DB_RESULT = Configuration.get("crawler.db.resultDb", DEF_DB_RESULT);
//
//  public static final long DEF_SERVER_DELAY = 10000;
//  public static final long SERVER_DELAY = Configuration.getLong("crawler.server.delay", DEF_SERVER_DELAY);
//
//  public static final int DEF_SERVER_THREADS = 1;
//  public static final int SERVER_THREADS = Configuration.getInt("crawler.server.activeThreads", DEF_SERVER_THREADS);
//  
//  public static final String FILTERS_STR = Configuration.get("filters.active-filters"); 
//
//}
