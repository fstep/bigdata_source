package com.porva.net.tana;

import org.apache.commons.lang.NullArgumentException;

import junit.framework.TestCase;

/**
 * Test case of {@link TanaMessageType}.
 *
 * @author Poroshin V.
 * @date Nov 3, 2005
 */
public class TanaMessageTypeTest extends TestCase
{

  /**
   * @throws Exception
   */
  public void testValueOf() throws Exception
  {
    assertTrue(TanaMessageType.FIX == TanaMessageType.getInstance("fix".getBytes()));
    assertTrue(TanaMessageType.ARB == TanaMessageType.getInstance("arb".getBytes()));
    
    try {
      TanaMessageType.getInstance("unknown".getBytes());
      fail();
    } catch (TanaSyntaxException e) {
    }
    try {
      byte[] nullVal = null;
      TanaMessageType.getInstance(nullVal);
      fail();
    } catch (NullArgumentException e) {
    }

  }

  /**
   * 
   */
  public void testToString()
  {
    assertEquals("arb", TanaMessageType.ARB.toString());
    assertEquals("fix", TanaMessageType.FIX.toString());
  }

}
