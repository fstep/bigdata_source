package com.porva.net.tana;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Map;

import junit.framework.TestCase;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Test case for {@link TanaSend}.
 *
 * @author Poroshin V.
 * @date Nov 3, 2005
 */
public class TanaSendTest extends TestCase
{
  TanaServer tanaServer;
  
  private int testPort = 15421;
  
  protected void setUp() throws Exception
  {
    tanaServer = new TanaServer(testPort, new TestServerHandler() , true);
    tanaServer.start();
    Thread.sleep(10);
  }
  
  protected void tearDown() throws IOException, InterruptedException
  {
    tanaServer.shutdown();
  }
  
  /////////////////////////
  // creation methods test 
  /////////////////////////
  
  /**
   * @throws Exception
   */
  public void testTanaSend() throws Exception
  {
    TanaSend tanaSend = new TanaSend("localhost", testPort);
    tanaSend.send("fix 1\n1 a: 1 b\nfix 0\n2 xx: 3 abc\n".getBytes());
    assertEquals("b", new String(tanaSend.receive().toMap().get("a")));

    tanaSend.send("fix 1\n2 xx: 3 abc\n".getBytes());
    assertEquals("abc", new String(tanaSend.receive().toMap().get("xx")));
    
    tanaSend = new TanaSend(null, testPort);
    
    try {
      tanaSend = new TanaSend("unknown_host", testPort);
      fail();
    } catch (UnknownHostException e) {
      // ok
    }
    
    try {
      tanaSend = new TanaSend(null, 43212);
      fail();
    } catch (IOException e) {
      // ok
    }

    
    try {
      tanaSend = new TanaSend("unknown_host", -1);
      fail();
    } catch (IllegalArgumentException e) {
      // ok
    }
  }
  
  static class TestServerHandler extends DefaultServerHandler
  {
    static final Log logger = LogFactory.getLog(TestServerHandler.class);

    TestServerHandler()
    {
      super(logger, "TestEcho");
    }

    public Map<String, byte[]> handleClientMsg(Map<String, byte[]> clientMsg)
    {
      return clientMsg;
    }
    
  }
  
}