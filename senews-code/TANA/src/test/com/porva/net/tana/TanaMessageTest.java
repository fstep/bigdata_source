package com.porva.net.tana;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import org.apache.commons.lang.NullArgumentException;

/**
 * Test case for {@link TanaMessage}.
 * 
 * @author Poroshin V.
 * @date Nov 3, 2005
 */
public class TanaMessageTest extends TestCase
{
  String[] correctMsgs = { "fix 2\n3 cmd: 8 shutdown\n2 ax: 4 test\n",
      "fix 1\n3 cmd: 0 \n", "fix 0\n", "fix 2\n3 cmd: 0 \n2 ax: 4 test\n"};
  
  static Map<String, byte[]> correctMap1 = new HashMap<String, byte[]>();

  static Map<String, byte[]> correctMap2 = new HashMap<String, byte[]>();

  static Map<String, byte[]> correctMap3 = new HashMap<String, byte[]>();
  
  static Map<String, byte[]> correctMap4 = new HashMap<String, byte[]>();

  String msgsStream = "fix 2\n3 cmd: 8 shutdown\n2 ax: 4 test\nfix 0\n";

  String[] incorrectMsgs = { "fix 1\n", "fix 0\n" + "3 cmd: 0\n",
      "fix 2\n" + "3 cmd: 8 shutdown\n2 ax: 4 tes\n", "fix 1\n" + "3 cmd: 0\n", };
  
//  private static final Log logger = LogFactory.getLog("");

  static {
    correctMap1.put("cmd", "shutdown".getBytes());
    correctMap1.put("ax", "test".getBytes());
    correctMap2.put("cmd", "".getBytes());
    correctMap4.put("cmd", "".getBytes());
    correctMap4.put("ax", "test".getBytes());
  }

  // ///////////////////////
  // static variables test
  // ///////////////////////

  /**
   * 
   */
  public void testEMPTY_MESSAGE()
  {
    assertNotNull(TanaMessage.EMPTY_FIX_MESSAGE);
    assertTrue(TanaMessage.EMPTY_FIX_MESSAGE.getType() == TanaMessageType.FIX);
    assertTrue(TanaMessage.EMPTY_FIX_MESSAGE.toMap().isEmpty());
  }

  /**
   * 
   */
  public void testGetFixMsg()
  {
    byte[] res = (new TanaMessage(TanaMessageType.FIX, correctMap1)).toBytes();
    assertEquals(new String(correctMsgs[0].getBytes()), new String(res));

    res = (new TanaMessage(TanaMessageType.FIX, correctMap2)).toBytes();
    assertEquals(new String(correctMsgs[1].getBytes()), new String(res));

    res = (new TanaMessage(TanaMessageType.FIX, correctMap3)).toBytes();
    assertEquals(new String(correctMsgs[2].getBytes()), new String(res));
    
    res = (new TanaMessage(TanaMessageType.FIX, correctMap4)).toBytes();
    assertEquals(new String(correctMsgs[3].getBytes()), new String(res));
  }

  // ///////////////////////
  // creation methods test
  // ///////////////////////

  /**
   * @throws TanaSyntaxException
   * @throws IOException
   */
  public void testNewMessageInputStream() throws TanaSyntaxException, IOException
  {
    for (String correctMsg : correctMsgs) {
      TanaMessage tanaMessage = new TanaMessage(TanaMessageType.FIX, new ByteArrayInputStream(
          correctMsg.getBytes()));
      assertNotNull(tanaMessage);
      assertTrue(tanaMessage.getType() == TanaMessageType.FIX);
      assertEquals(correctMsg, tanaMessage.toString());
    }

    // test stream of TANA messages
    InputStream in = new ByteArrayInputStream(msgsStream.getBytes());
    assertEquals("fix 2\n" + "3 cmd: 8 shutdown\n2 ax: 4 test\n", new TanaMessage(
        TanaMessageType.FIX, in).toString());
    assertEquals("fix 0\n", (new TanaMessage(TanaMessageType.FIX, in)).toString());

    try {
      for (String incorrectMsg : incorrectMsgs) {
        new TanaMessage(TanaMessageType.FIX, new ByteArrayInputStream(incorrectMsg.getBytes()));
        fail("incorrect tana message did not raise an exception: " + incorrectMsg);
      }
    } catch (TanaSyntaxException e) {
      // this is ok;
    }

    try {
      InputStream nullIn = null;
      new TanaMessage(TanaMessageType.FIX, nullIn);
      fail("null tana message did not raise an exception");
    } catch (NullArgumentException e) {
      // this is ok;
    }
    try {
      new TanaMessage(null, new ByteArrayInputStream(correctMsgs[0].getBytes()));
      fail("null tana message did not raise an exception");
    } catch (NullArgumentException e) {
      // this is ok;
    }

  }

  /**
   * @throws Exception
   */
  public void testNewFixMessageInputStream() throws Exception
  {
    for (String correctMsg : correctMsgs) {
      TanaMessage tanaMessage = new TanaMessage(TanaMessageType.FIX, new ByteArrayInputStream(
          correctMsg.getBytes()));
      assertNotNull(tanaMessage);
      assertTrue(tanaMessage.getType() == TanaMessageType.FIX);
      assertEquals(correctMsg, tanaMessage.toString());
    }

    // test stream of TANA messages
    InputStream in = new ByteArrayInputStream(msgsStream.getBytes());
    assertEquals("fix 2\n" + "3 cmd: 8 shutdown\n2 ax: 4 test\n", new TanaMessage(TanaMessageType.FIX, in)
        .toString());
    assertEquals("fix 0\n", new TanaMessage(TanaMessageType.FIX, in).toString());

    try {
      for (String incorrectMsg : incorrectMsgs) {
        new TanaMessage(TanaMessageType.FIX, new ByteArrayInputStream(incorrectMsg.getBytes()));
        fail("incorrect tana message did not raise an exception: " + incorrectMsg);
      }
    } catch (TanaSyntaxException e) {
      // this is ok;
    }

    try {
      InputStream nullIn = null;
      new TanaMessage(TanaMessageType.FIX, nullIn);
      fail("null tana message did not raise an exception");
    } catch (NullArgumentException e) {
      // this is ok;
    }

  }

  /**
   * Class under test for TanaMessage newFixMessage(Map)
   */
  public void testNewFixMessageMap()
  {
    TanaMessage tanaMessage = new TanaMessage(TanaMessageType.FIX, correctMap1);
    assertNotNull(tanaMessage);
    assertTrue(tanaMessage.getType() == TanaMessageType.FIX);
    assertEquals(correctMsgs[0], tanaMessage.toString());
    tanaMessage = new TanaMessage(TanaMessageType.FIX, correctMap2);
    assertNotNull(tanaMessage);
    assertTrue(tanaMessage.getType() == TanaMessageType.FIX);
    assertEquals(correctMsgs[1], tanaMessage.toString());
    tanaMessage = new TanaMessage(TanaMessageType.FIX, correctMap3);
    assertNotNull(tanaMessage);
    assertTrue(tanaMessage.getType() == TanaMessageType.FIX);
    assertEquals(correctMsgs[2], tanaMessage.toString());

    try {
      Map<String, byte[]> nullMap = null;
      new TanaMessage(TanaMessageType.FIX, nullMap);
      fail("null tana message did not raise an exception");
    } catch (NullPointerException e) {
      // this is ok;
    }
  }

  /**
   * Class under test for TanaMessage newArbMessage(Map)
   */
  public void testNewArbMessageMap()
  {
    // TODO implement testNewArbMessageMap test method
  }

  /**
   * Class under test for TanaMessage newMessage(Map)
   */
  public void testNewMessageMap()
  {
    TanaMessage tanaMessage = new TanaMessage(TanaMessageType.FIX, correctMap1);
    assertNotNull(tanaMessage);
    assertTrue(tanaMessage.getType() == TanaMessageType.FIX);
    assertEquals(correctMsgs[0], tanaMessage.toString());
    tanaMessage = new TanaMessage(TanaMessageType.FIX, correctMap2);
    assertNotNull(tanaMessage);
    assertTrue(tanaMessage.getType() == TanaMessageType.FIX);
    assertEquals(correctMsgs[1], tanaMessage.toString());
    tanaMessage = new TanaMessage(TanaMessageType.FIX, correctMap3);
    assertNotNull(tanaMessage);
    assertTrue(tanaMessage.getType() == TanaMessageType.FIX);
    assertEquals(correctMsgs[2], tanaMessage.toString());

    try {
      Map<String, byte[]> nullMap = null;
      new TanaMessage(TanaMessageType.FIX, nullMap);
      fail("null tana message did not raise an exception");
    } catch (NullPointerException e) {
      // this is ok;
    }
  }

  // ///////////////////////
  // getters/setters test
  // ///////////////////////

  /**
   * @throws TanaSyntaxException
   * @throws IOException
   */
  public void testGetType() throws TanaSyntaxException, IOException
  {
    TanaMessage tanaMessage = new TanaMessage(TanaMessageType.FIX, new ByteArrayInputStream(correctMsgs[0]
        .getBytes()));
    assertEquals(TanaMessageType.FIX, tanaMessage.getType());
  }

  /**
   * @throws TanaSyntaxException
   * @throws IOException
   */
  public void testToBytes() throws TanaSyntaxException, IOException
  {
    TanaMessage tanaMessage = new TanaMessage(TanaMessageType.FIX, new ByteArrayInputStream(correctMsgs[0]
        .getBytes()));
    assertEquals(new String(correctMsgs[0].getBytes()), new String(tanaMessage.toBytes()));
  }

  /**
   * Class under test for boolean equals(Object)
   * @throws Exception 
   */
  public void testEqualsObject() throws Exception
  {
    TanaMessage tanaMessage1 = new TanaMessage(TanaMessageType.FIX, new ByteArrayInputStream(correctMsgs[0]
        .getBytes()));
    TanaMessage tanaMessage2 = new TanaMessage(TanaMessageType.FIX, new ByteArrayInputStream(correctMsgs[0]
        .getBytes()));

    assertEquals(tanaMessage1, tanaMessage2);
    assertEquals(tanaMessage1, tanaMessage1);
    assertEquals(tanaMessage2, tanaMessage2);
  }

  /**
   * @throws Exception
   */
  public void testGetValueAsString() throws Exception
  {
    TanaMessage tanaMessage = new TanaMessage(TanaMessageType.FIX, new ByteArrayInputStream(correctMsgs[0]
        .getBytes()));
    assertEquals("shutdown", new String(tanaMessage.toMap().get("cmd")));
    assertEquals("test", new String(tanaMessage.toMap().get("ax")));

    tanaMessage = new TanaMessage(TanaMessageType.FIX, new ByteArrayInputStream(correctMsgs[1].getBytes()));
    assertEquals("", new String(tanaMessage.toMap().get("cmd")));
  }

  /**
   * @throws Exception
   */
  public void testGetValue() throws Exception
  {
    TanaMessage tanaMessage = new TanaMessage(TanaMessageType.FIX, new ByteArrayInputStream(correctMsgs[0]
        .getBytes()));
    assertEquals("shutdown", new String(tanaMessage.toMap().get("cmd")));
    assertEquals("test", new String(tanaMessage.toMap().get("ax")));

    tanaMessage = new TanaMessage(TanaMessageType.FIX, new ByteArrayInputStream(correctMsgs[1].getBytes()));
    assertEquals("", new String(tanaMessage.toMap().get("cmd")));
  }

  /**
   * @throws Exception
   */
  public void testGetPairsSize() throws Exception
  {
    TanaMessage tanaMessage = new TanaMessage(TanaMessageType.FIX, new ByteArrayInputStream(correctMsgs[0]
        .getBytes()));
    assertEquals(tanaMessage.toMap().size(), 2);

    tanaMessage = new TanaMessage(TanaMessageType.FIX, new ByteArrayInputStream(correctMsgs[1].getBytes()));
    assertEquals(tanaMessage.toMap().size(), 1);

    tanaMessage = new TanaMessage(TanaMessageType.FIX, new ByteArrayInputStream(correctMsgs[2].getBytes()));
    assertEquals(tanaMessage.toMap().size(), 0);
  }

}
