/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.net.tana;

import java.util.Map;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.logging.Log;

/**
 * Default implementaion of {@link ServerHandler} interface.<br>
 * This implementation just logs all events except {@link #handleClientMsg(Map)}.
 * 
 * @author Poroshin V.
 * @date Nov 3, 2005
 */
public abstract class DefaultServerHandler implements ServerHandler
{
  private Log logger;

  private String serverName;

  /**
   * Creates a new {@link DefaultServerHandler} object with specified <code>logger</code> to 
   * log events and <code>serverName</code>.
   * 
   * @param logger logger that will be used to log server's events.
   * @param serverName server name to display in log messages.
   * @throws NullArgumentException if <code>logger</code> is <code>null</code>.
   */
  public DefaultServerHandler(final Log logger, final String serverName)
  {
    this.logger = logger;
    this.serverName = serverName;
    
    if (this.logger == null)
      throw new NullArgumentException("logger");
    if (this.serverName == null)
      this.serverName = "";
  }

  /* (non-Javadoc)
   * @see com.porva.net.tana.ServerHandler#handleClientConnected(java.lang.String, int)
   */
  public void handleClientConnected(final String clientInfo, int clientNum)
  {
    if (logger.isInfoEnabled()) {
      StringBuffer sb = new StringBuffer();
      sb.append("Client number ").append(clientNum).append(" from ").append(clientInfo)
          .append(" connected.");
      logger.info(sb.toString());
    }
  }

  /* (non-Javadoc)
   * @see com.porva.net.tana.ServerHandler#handleClientDisconnected(java.lang.String, int)
   */
  public void handleClientDisconnected(final String clientInfo, int clientNum)
  {
    if (logger.isInfoEnabled()) {
      StringBuffer sb = new StringBuffer();
      sb.append("Client number ").append(clientNum).append(" from ").append(clientInfo)
          .append(" disconnected.");
      logger.info(sb.toString());
    }
  }

  /* (non-Javadoc)
   * @see com.porva.net.tana.ServerHandler#handleServerStarted(java.lang.String, int)
   */
  public void handleServerStarted(final String host, int port)
  {
    if (logger.isInfoEnabled()) {
      StringBuffer sb = new StringBuffer();
      sb.append(serverName).append(" server started on :").append(port);
      logger.info(sb.toString());
    }
  }

  /* (non-Javadoc)
   * @see com.porva.net.tana.ServerHandler#handleServerShutdown(java.lang.String)
   */
  public void handleServerShutdown(final String reason)
  {
    if (logger.isInfoEnabled()) {
      StringBuffer sb = new StringBuffer();
      sb.append(serverName).append(" server is going to shutdown: reason=").append(reason);
      logger.info(sb.toString());
    }
  }

  /* (non-Javadoc)
   * @see com.porva.net.tana.ServerHandler#handleErrorOccured(java.lang.String, java.lang.Throwable)
   */
  public void handleErrorOccured(final String message, final Throwable cause)
  {
    if (logger.isWarnEnabled())
      logger.warn(message, cause);
  }
}
