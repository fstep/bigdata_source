/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.net.tana;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.util.Map;

/**
 * Simple {@link TanaServer} echo server.
 */
public class EchoServer
{
  private int port;
  private EchoServerHandler echoServerHandler;
  private TanaServer tanaServer;
  private static final Log logger = LogFactory.getLog(EchoServer.class);

  /**
   * Constructs a new {@link EchoServer} object. 
   * 
   * @param port port number 
   * @param verbose <code>true</code> to be verbose; <code>false</code> otherwise.
   */
  public EchoServer(int port, boolean verbose)
  {
    this.port = port;
    echoServerHandler = new EchoServerHandler();
    tanaServer = new TanaServer(this.port, echoServerHandler, verbose);
  }
  
  /**
   * Shutdown the server.
   * 
   * @throws IOException
   */
  public void shutdown() throws IOException
  {
    tanaServer.shutdown();
  }

  /**
   * Start the server.
   */
  public void start()
  {
    tanaServer.start();
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  private static class EchoServerHandler extends DefaultServerHandler
  {
    EchoServerHandler()
    {
      super(logger, "Echo");
    }

    public Map<String, byte[]> handleClientMsg(Map<String, byte[]> clientMsg)
    {
      // echo reply
      return clientMsg;
    }
  }

  /**
   * @param args
   */
  public static void main(String[] args)
  {
    int port = 12345;
    boolean verbose = true;
    try {
      port = Integer.parseInt(args[0]);
      verbose = (new Boolean(args[1])).booleanValue();
    } catch(Exception e) {
      System.err.println("Usage: EchoServer <port_number> <verbose:true|false>\n");
    }
    EchoServer echoServer = new EchoServer(port, verbose);
    echoServer.start();
  }
}
