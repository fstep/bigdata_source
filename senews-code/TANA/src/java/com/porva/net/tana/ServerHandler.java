/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.net.tana;

import java.util.Map;

/**
 * Handler interface for {@link TanaServer} servers.
 * 
 * @author Poroshin V.
 * @date Nov 3, 2005
 */
public interface ServerHandler
{
  /**
   * Handles new client connection event. <br>
   * This method should be called then a new client is connected to server.
   * 
   * @param clientInfo some description of client.
   * @param clientNum number of client.
   */
  public void handleClientConnected(String clientInfo, int clientNum);

  /**
   * Handles client disconnection event. <br>
   * This method is called then client was disconnected from server.
   * 
   * @param clientInfo some description of client.
   * @param clientNum number of client.
   */
  public void handleClientDisconnected(String clientInfo, int clientNum);

  /**
   * Handles event of starting of server. <br>
   * This method is called then server is started.
   * 
   * @param host hostname where server is started.
   * @param port port number where server is started.
   */
  public void handleServerStarted(String host, int port);

  /**
   * Handles event server's shutting down. <br>
   * This method is called then server is shutting down.
   * 
   * @param reason some textual reason why server is shutting down.
   */
  public void handleServerShutdown(String reason);

  /**
   * Handles event when some error in server occured. <br>
   * This method is called then error in TanaServer occured. In most cases errors happend when
   * server receives invalid TANA messages or some I/O errors happend.
   * 
   * @param message error message; can be <code>null</code> if no message available.
   * @param cause error cause; can be <code>null</code> if no throwable cause available.
   */
  public void handleErrorOccured(String message, Throwable cause);

  /**
   * Handles event of getting new message from client. <br>
   * Use this method if you want to receive message from client and send reply message back to it.
   * If you don't want to send reply message back to client then return <code>null</code> from
   * this method.
   * 
   * @param clientMsg recieved client message as {@link Map} object.
   * @return reply message to send to client back or <code>null</code> to don't send reply
   *         message.
   */
  public Map<String, byte[]> handleClientMsg(Map<String, byte[]> clientMsg);
}
