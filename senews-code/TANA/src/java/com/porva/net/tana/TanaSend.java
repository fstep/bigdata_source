/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.net.tana;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.NullArgumentException;

/**
 * Utility class to send and receive TANA messages via TCP network.
 * @version 1.0.0
 */
public class TanaSend
{
  private final String host;
  private final int port;
  private Socket socket = null;

  //////////////////
  // constructors 
  //////////////////
  
  /**
   * Allocates new {@link TanaSend} object and connects socket to specified host and port.
   * @param host hostname to connect socket or <code>null</code> for lookback address.
   * @param port port number to connect socket. 
   * @throws IOException if connection to hostname:port failed.
   * @throws IllegalArgumentException if port number is out of range.
   * @throws java.net.UnknownHostException if hostname is unknown or invalid.
   */
  public TanaSend(final String host, final int port) throws IOException
  {
    this.host = host;
    this.port = port;
    
    connect();
  }
  
  ////////////////// 
  // public methods 
  //////////////////

  /**
   * Sends and receives TANA message as {@link Map} objects.
   * It is usefull to send TANA message and receive answer message. 
   * @param map {@link Map} objects to send.
   * @return received {@link Map} objects.
   * @throws TanaSyntaxException if syntax of the message in incorrect.
   * @throws IOException if some IO exception occurs.
   */
  public Map<String, byte[]> sendAndReceiveMap(final Map<String, byte[]> map) throws TanaSyntaxException, IOException
  {
    if (map == null)
      throw new NullArgumentException("map");
    
    send(map);
    return receive().toMap();
  }
  
  /**
   * Receives TANA message as {@link TanaMessage} object.
   * @return received TANA message as {@link TanaMessage} object.
   * @throws TanaSyntaxException if syntax of the message in incorrect.
   * @throws IOException if some IO exception occurs.
   */
  public TanaMessage receive() throws TanaSyntaxException, IOException
  {
    connect();
    return new TanaMessage(TanaMessageType.FIX, socket.getInputStream());
  }
  
  /**
   * Sends TANA message represented by {@link Map} objects.
   * @param map {@link Map} objects to send.
   * @throws IOException if some IO exception occurs.
   * @throws NullArgumentException if <code>map</code> is <code>null</code>.
   */
  public void send(final Map<String, byte[]> map) throws IOException
  {
    if (map == null)
      throw new NullArgumentException("map");
    
    send(new TanaMessage(TanaMessageType.FIX, map));
  }
  
  /**
   * Sends one TANA message represented by byte array.
   * @param msg byte array containing tana message from the beginning.
   * @throws TanaSyntaxException if syntax of the message in incorrect.
   * @throws IOException if some IO exception occurs.
   * @throws NullArgumentException if <code>msg</code> is <code>null</code>.
   */
  public void send(final byte[] msg) throws TanaSyntaxException, IOException
  {
    if (msg == null)
      throw new NullArgumentException("map");
    
    send(new TanaMessage(TanaMessageType.FIX, new ByteArrayInputStream(msg)));
  }
  
  /**
   * Sends TANA message represented {@link TanaMessage} object.
   * @param tanaMessage TANA message to send. 
   * @throws IOException if some IO exception occurs.
   * @throws NullArgumentException if <code>tanaMessage/code> is <code>null</code>.
   */
  public void send(final TanaMessage tanaMessage) throws IOException
  {
    if (tanaMessage == null)
      throw new NullArgumentException("tanaMessage");
    
    connect();
    
    OutputStream out = socket.getOutputStream();
    out.write(tanaMessage.toBytes());
    out.flush();
  }
  
//  public void sendAll(byte[] msg) throws IOException
//  {
//    InputStream in = new ByteArrayInputStream(msg);
//    
//    while (true) {
//      try {
//        send(TanaMessage.newFixMessage(in));
//      } catch (TanaException e) {
//        break;
//      }
//    }
//    
//  }
  
  /**
   * Connectes socket to specified host and port.
   * @throws IOException if some IO exception occurs.
   */
  public void connect() throws IOException
  {
    if (socket == null || !socket.isConnected())
      socket = new Socket(host, port);
  }
  
  /**
   * Closes connection.
   * @throws IOException if some IO exception occurs.
   */
  public void disconnect() throws IOException
  {
    socket.close();
  }

  //////////////////////////////////////////////////////////////////////////////
  
  /**
   * Sends TANA message to server and prints its answer as a string.
   * @param args command line arguments
   * @throws Exception
   */
  public static void main(String[] args) throws Exception
  {
    if (args.length < 2) {
    	printHelpMsg();
      System.exit(1);
    }
    String host = "localhost";
    int port = 12345;
    if (args[0].contains(":")) {
    	String[] hostPort = args[0].split(":", 2);
    	host =  hostPort[0];
    	port = Integer.parseInt(hostPort[1]);
    } else {
    	port = Integer.parseInt(args[0]);
    }

    Map<String, byte[]> map = new HashMap<String, byte[]>();
    if (args[1].equals("-f")) {
      String file = args[3];
      FileInputStream fin = new FileInputStream(file);
      TanaMessage tanaMessenger = new TanaMessage(TanaMessageType.FIX, fin);
      map = tanaMessenger.toMap();
    } 
    else if (args[1].equals("-echoserver")) {
      EchoServer echoServer = new EchoServer(port, true);
      echoServer.start();
      return;
    }
    else {
      for (int i = 1; i < args.length; i++) {
        String [] pair = args[i].split(":", 2);
        if (pair == null) {
          System.err.println("Invalid TANA message. Use key:value pairs.");
          System.err.print(args[i]);
        }
        map.put(pair[0], pair[1].getBytes());
      }
    }

    TanaSend tanaSend = new TanaSend(host, port);
    tanaSend.send(map);
    System.out.println(tanaSend.receive().toString());
  }
  
  private static void printHelpMsg()
  {
  	System.out.println("TanaSend utility ver. 1.0.0");
  	System.err.println("Usage: java -jar TanaSend.jar [host:]port \"message content\"");
  	System.err.println("       java -jar TanaSend.jar [host:]port -f file_name");
  	System.err.println("       java -jar TanaSend.jar [host:]port -echoserver");
  	System.err.println("Examples: java -jar TanaSend.jar localhost:12345 key1:val1 key2:\"value 2\"");
  	System.err.println("          java -jar TanaSend.jar localhost:12345 -f file_with_tana_msg");
  	System.err.println("          java -jar TanaSend.jar localhost:12345 -echoserver");
  }
}
