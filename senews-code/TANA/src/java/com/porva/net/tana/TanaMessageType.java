/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.net.tana;

import org.apache.commons.lang.NullArgumentException;

/**
 * Enumeration of TANA message types: FIX and ARB.
 *
 * @author Poroshin V.
 * @date Nov 3, 2005
 */
public enum TanaMessageType
{
  /**
   * FIX TANA message type.
   */
  FIX("fix"),

  /**
   * ARB TANA message type.
   */
  ARB("arb");
  
  private final String val;
  
  private TanaMessageType(final String str)
  {
    val = str;
  }
  
  /**
   * Returns an instance of {@link TanaMessageType} according to <code>msgType</code>.<br> 
   * Input parameter <code>msgType</code> should contains bytes: 'f','i','x' or 'a','r','b'.
   * 
   * @param msgType type of message as bytes.
   * @return instance of {@link TanaMessageType}.
   * @throws TanaSyntaxException if <code>msgType</code> is ivalid.
   * @throws NullArgumentException if <code>msgType</code> is <code>null</code>. 
   */
  public static TanaMessageType getInstance(final byte[] msgType) throws TanaSyntaxException
  {
    if (msgType == null)
      throw new NullArgumentException("msgType");
    
    if (msgType[0] == 'f' && msgType[1] == 'i' && msgType[2] == 'x')
      return FIX;
    else if (msgType[0] == 'a' && msgType[1] == 'r' && msgType[2] == 'b')
      return ARB;
    
    throw new TanaSyntaxException("Invalid message type: " + new String(msgType));
  }
  
  public String toString()
  {
    return val;
  }
  
  
}
