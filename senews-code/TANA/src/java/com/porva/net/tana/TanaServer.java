/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.net.tana;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.NullArgumentException;

/**
 * Multithreaded server's skeleton to send and receive TANA messages from several clients
 * simultaniously. To make server functional you need to write {@link ServerHandler} object to
 * handle serves's events.
 */
public class TanaServer extends Thread
{
  private ServerHandler serverHandler;

  private boolean verbose;

  private int port;

  private boolean isShutdown = false;
  
//  private static final Log logger = LogFactory.getLog(TanaServer.class);

  /**
   * Allocates a new {@link TanaServer} object.
   * 
   * @param port port number to start on.
   * @param serverHandler {@link ServerHandler} object to handle server's events.
   * @param verbose <code>true</code> to call <code>serverHandler</code> notification functions;
   *          <code>false</code> otherwise.
   * @throws NullArgumentException if <code>serverHandler</code> is <code>null</code>.
   * @throws IllegalArgumentException if <code>port</code> is invalid port number.
   */
  public TanaServer(final int port, final ServerHandler serverHandler, final boolean verbose)
  {
    this.port = port;
    this.serverHandler = serverHandler;
    this.verbose = verbose;

    if (this.port <= 0)
      throw new IllegalArgumentException("Port number cannot be <= 0: " + this.port);
    if (this.serverHandler == null)
      throw new NullArgumentException("serverHandler");
  }

  /**
   * Starts the Tana server.
   */
  public void run()
  {
    try {
      int clientNum = 1;
      ServerSocket server = new ServerSocket(port);

      if (verbose)
        serverHandler.handleServerStarted(server.getInetAddress().getCanonicalHostName(), port);

      while (!isShutdown) {
        Socket client = server.accept();

        if (verbose)
          serverHandler.handleClientConnected(client.getInetAddress().toString(), clientNum);

        Thread thread = new ClientHandleThread(clientNum, client);
        thread.start();

        clientNum++;
      }
    } catch (Exception e) {
      serverHandler.handleErrorOccured(null, e);
    }
  }

  /**
   * Shutdowns the server by sending special shutdown message.
   */
  public void shutdown()
  {
    serverHandler.handleServerShutdown("shutdown requsted by user.");
    isShutdown = true;

    Map<String, byte[]> msg = new HashMap<String, byte[]>();
    msg.put("post-shutdown", "".getBytes());
    try {
      TanaSend tanaSend = new TanaSend("localhost", port);
      tanaSend.send(msg);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  // ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  class ClientHandleThread extends Thread
  {
    private int num;

    private Socket client;

    ClientHandleThread(int i, Socket theClient)
    {
      num = i;
      client = theClient;
    }

    private void send(final Map<String, byte[]> replyMap, final OutputStream out) throws TanaSyntaxException,
        IOException
    {
      byte[] msg = new TanaMessage(TanaMessageType.FIX, replyMap).toBytes();
      out.write(msg);
      out.flush();
    }

    public void run()
    {
      try {
        OutputStream out = client.getOutputStream();

        PushbackInputStream pbin = new PushbackInputStream(client.getInputStream());

        handleConnection(pbin, out);

        // clean-up socket
        out.close();
        pbin.close();
        client.close();

        if (verbose)
          serverHandler.handleClientDisconnected(client.getInetAddress().toString(), num);
      } catch (IOException e) {
        serverHandler.handleErrorOccured(null, e);
      }
    }

    private void handleConnection(final PushbackInputStream in, final OutputStream out)
    {
      boolean done = false;
      while (!done) {
        try {
          if (isEndOfStream(in))
            return;

          Map<String, byte[]> recvMap = new TanaMessage(TanaMessageType.FIX, in).toMap();

          if (recvMap.containsKey("post-shutdown"))
            return;

          Map<String, byte[]> replyMap = serverHandler.handleClientMsg(recvMap);
          if (replyMap != null) {
            send(replyMap, out);
          }
        } catch (Exception e) {
          serverHandler.handleErrorOccured(null, e);
          done = true;
        }
      }
    }

    private boolean isEndOfStream(final PushbackInputStream in) throws IOException
    {
      int nextByte = -1; 
      try {  
        nextByte = in.read();
      } catch (IOException e) {
        return true;
      }
      if (nextByte == -1) {
        return true;
      }
      in.unread(nextByte);
      return false;
    }
  }

}
