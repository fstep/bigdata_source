/*
 *   Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package com.porva.net.tana;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.EqualsBuilder;

/**
 * Single TANA message implementation.
 * 
 * @author Poroshin V.
 * @date Nov 3, 2005
 */
public class TanaMessage
{
  private String msgStr;

  private byte[] msgBytes;

  private final Map<String, byte[]> msgMap = new HashMap<String, byte[]>();

  private TanaMessageType msgType = null;

  /**
   * Empty TANA message of 'fix' type.
   */
  public static final TanaMessage EMPTY_FIX_MESSAGE = new TanaMessage(TanaMessageType.FIX,
      new HashMap<String, byte[]>());

  // /////////////////////////////////////////////////////////// constructors

  /**
   * Constructs a new {@link TanaMessage} object of defined <code>messageType</code> and reads
   * message data from input stream <code>in</code>.
   * 
   * @param msgType {@link TanaMessageType} type of TANA message.
   * @param in {@link InputStream} object that contains content of message.
   * @throws TanaSyntaxException if readed TANA message is incorrect.
   * @throws IOException if an I/O error occurs.
   * @throws NullArgumentException if <code>messageType</code> or <code>in</code> is
   *           <code>null</code>.
   */
  public TanaMessage(final TanaMessageType msgType, final InputStream in)
      throws TanaSyntaxException, IOException
  {
    if (msgType == null)
      throw new NullArgumentException("msgType");
    if (in == null)
      throw new NullArgumentException("in");

    readMessageHeader(in);
    readMessageBody(in);
  }

  /**
   * Constructs a new {@link TanaMessage} object of defined <code>msgType</code> and reads message
   * data from <code>msgMap</code>.
   * 
   * @param msgType {@link TanaMessageType} type of TANA message.
   * @param msgMap map that contains content of message.
   * @throws NullArgumentException if <code>msgType</code> or <code>msgMap</code> is
   *           <code>null</code>.
   */
  public TanaMessage(final TanaMessageType msgType, final Map<String, byte[]> msgMap)
  {
    this.msgType = msgType;
    this.msgMap.putAll(msgMap); // make a defensive copy

    if (this.msgType == null)
      throw new NullArgumentException("messageType");
    if (this.msgMap == null)
      throw new NullArgumentException("msgMap");
  }

  // //////////////////////////////////////////////////// public methods

  /**
   * Returns {TanaMessageType} type of the this message.
   * 
   * @return type of the message.
   */
  public TanaMessageType getType()
  {
    return msgType;
  }

  /**
   * Returns this message as bytes.
   * 
   * @return the message as bytes.
   */
  public byte[] toBytes()
  {
    if (msgBytes == null)
      msgBytes = getFixMsg(msgMap);
    return msgBytes;
  }

  /**
   * Returns this message as string. <br>
   * String is created using system default encoding.
   * 
   * @return the message as string.
   */
  public String toString()
  {
    if (msgStr == null)
      msgStr = new String(this.toBytes());
    return msgStr;
  }

  /**
   * Returns this message as map. <br>
   * 
   * @return the message as map.
   */
  public Map<String, byte[]> toMap()
  {
    return msgMap;
  }

  public boolean equals(final Object other)
  {
    if (!(other instanceof TanaMessage))
      return false;
    TanaMessage castOther = (TanaMessage) other;
    return new EqualsBuilder().append(this.toBytes(), castOther.toBytes()).isEquals();
  }

  // ///////////////////////////////////////////////////////////// private methods

  /**
   * Reads and returns number <code>length</code> bytes from input stream <code>in</code>.
   * 
   * @param in InputStream to readBytes from.
   * @param length number of bytes to readBytes.
   * @return readed bytes
   * @throws TanaSyntaxException if <code>length</code> <= 0 or if number of actually readed bytes
   *           is different from requested <code>length</code>.
   * @throws IOException if an I/O error occurs.
   */
  private byte[] readBytes(final InputStream in, final int length) throws TanaSyntaxException,
      IOException
  {
    assert in != null;

    if (length <= 0)
      throw new TanaSyntaxException("It is supposed to read > 0 bytes but requested to read: "
          + length);

    byte[] buff = new byte[length];

    int readed = 0;
    int i = 0;
    for (; i < length; i++) {
      readed = in.read();
      if (readed == -1)
        break;
      buff[i] = (byte) readed;
    }

    if (i != length) {
      throw new TanaSyntaxException("Unexpected end of stream: expected to readBytes " + length
          + " bytes but readed " + readed + " bytes.");
    }
    return buff;
  }

  /**
   * Reads and returns integer number from input stream <code>in</code>.
   * 
   * @param in input stream to readBytes from.
   * @return readed integer number.
   * @throws TanaSyntaxException if number of actually readed bytes is different from requested
   *           <code>length</code>.
   * @throws IOException if an I/O error occurs.
   */
  private int readNumber(final InputStream in) throws TanaSyntaxException, IOException
  {
    assert in != null;

    StringBuffer strBuff = new StringBuffer();
    while (true) {
      int readed = in.read();
      if (readed == -1) {
        throw new TanaSyntaxException("Failed to readBytes number: unexpected end of stream.");
      }
      if (readed >= 48 && readed <= 57) { // this is digit in ASCII
        strBuff.append((char) readed);
      } else {
        break;
      }
    }
    return Integer.parseInt(strBuff.toString());
  }

  /**
   * Reads single byte (char) from input stream <code>in</code>.
   * 
   * @param in input stream to readBytes from.
   * @param expectedChar expected char to readBytes.
   * @throws IOException if an I/O error occurs.
   * @throws TanaSyntaxException if readed message is invalid.
   */
  private void readCharacter(final InputStream in, final int expectedChar) throws IOException,
      TanaSyntaxException
  {
    assert in != null;

    int readedByte = 0;
    readedByte = in.read();

    if (readedByte == -1)
      throw new TanaSyntaxException("Unexpected end of input stream.");
    if (readedByte != expectedChar)
      throw new TanaSyntaxException("Readed characted " + (char) readedByte + " but expected "
          + (char) expectedChar);
  }

  /**
   * Reads whole 'fix' type message from input stream <code>in</code>.
   * 
   * @param in input stream to read from.
   * @param map {@link Map} object of constructed message.
   * @throws TanaSyntaxException if readed message is invalid.
   * @throws IOException if an I/O error occurs.
   */
  private void readFixMsg(final InputStream in) throws TanaSyntaxException, IOException
  {
    assert in != null;

    int fieldsNum;
    int keyLength;
    int valueLength;
    byte[] buffKey;
    byte[] buffVal;

    readCharacter(in, 32);

    fieldsNum = readNumber(in);
    for (int i = 0; i < fieldsNum; i++) {

      // readBytes key
      keyLength = readNumber(in);
      buffKey = readBytes(in, keyLength);
      if (buffKey == null) {
        throw (new TanaSyntaxException(
            "Failed to readBytes fix msg: unexpected end of input stream"));
      }

      readCharacter(in, ':');
      readCharacter(in, ' ');

      // readBytes value
      valueLength = readNumber(in);
      if (valueLength != 0) {
        buffVal = readBytes(in, valueLength);
        if (buffVal == null) {
          throw (new TanaSyntaxException(
              "Failed to readBytes fix msg: unexpected end of input stream"));
        }
      } else {
        buffVal = new byte[0];
      }
      readCharacter(in, 10); // new line

      msgMap.put(new String(buffKey), buffVal);
    }
  }

  /**
   * Constructs a fix TANA message (as row of bytes ready to send via stream) from {@link Map}
   * object.
   * 
   * @param map {@link Map} object to construct TANA message from.
   * @return TANA message as row of bytes ready to send via stream.
   * @throws TanaSyntaxException if some error occured during message construction.
   */
  private byte[] getFixMsg(final Map<String, byte[]> msgMap)
  {
    assert msgMap != null;

    // calculate number of bytes for message
    int mapSize = new Integer(msgMap.size()).toString().getBytes().length;
    int size = 5 + mapSize;
    ByteBuffer buff = ByteBuffer.allocate(size);

    buff.put((byte) 'f');
    buff.put((byte) 'i');
    buff.put((byte) 'x');
    buff.put((byte) ' ');
    buff.put(new Integer(msgMap.size()).toString().getBytes());
    buff.put((byte) 10); // \n

    for (Map.Entry<String, byte[]> entry : msgMap.entrySet()) {
      byte[] key = entry.getKey().getBytes();
      byte[] val = entry.getValue();
      int keyLength = key.length;
      int valLength = val.length;
      int lengthKeyLength = new Integer(keyLength).toString().getBytes().length;
      int lenghtValLenght = new Integer(valLength).toString().getBytes().length;
      byte[] keyLengthBytes = new Integer(key.length).toString().getBytes();
      byte[] valLenghtBytes = new Integer(val.length).toString().getBytes();
      size += lengthKeyLength + 1 + keyLength + 2 + lenghtValLenght + 1 + valLength + 1;
      ByteBuffer newBuff = ByteBuffer.allocate(size);
      newBuff.put(buff.array()).put(keyLengthBytes).put((byte) ' ').put(key).put((byte) ':')
          .put((byte) ' ').put(valLenghtBytes).put((byte) ' ').put(val).put((byte) 10);
      buff = newBuff;
    }
    return buff.array();
  }

  private void readMessageBody(final InputStream in) throws TanaSyntaxException, IOException
  {
    assert in != null;

    switch (msgType) {
    case FIX:
      readFixMsg(in);
      break;
    case ARB:
      throw (new TanaSyntaxException("Failed to recv msg: 'arb' messages are not supported"));
    default:
      throw (new TanaSyntaxException("Failed to recv msg: unknown tana message type"));
    }
  }

  private void readMessageHeader(final InputStream in) throws TanaSyntaxException, IOException
  {
    assert in != null;

    byte[] msgType = readBytes(in, 3);
    this.msgType = TanaMessageType.getInstance(msgType);
  }

}
