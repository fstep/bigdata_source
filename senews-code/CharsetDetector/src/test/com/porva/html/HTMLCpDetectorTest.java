package com.porva.html;

import junit.framework.TestCase;

/**
 * Testing of {@link HTMLCpDetector}.
 * @author poroshin
 */
public class HTMLCpDetectorTest extends TestCase
{

  /**
   * Testing of {@link HTMLCpDetector#getResponseCharset(String)}.
   */
  public void testGetResponseCharset()
  {
    assertNull(HTMLCpDetector.getResponseCharset(null));
    assertNull(HTMLCpDetector.getResponseCharset("invalid"));
    assertEquals("utf-8", HTMLCpDetector.getResponseCharset("text/html; charset=utf-8"));
    assertEquals("utf-8", HTMLCpDetector.getResponseCharset("charset=utf-8 "));
  }

  /**
   * Testing of {@link HTMLCpDetector#getMetaTagCharset(byte[])}.
   */
  public void testGetMetaTagCharset() 
  {
    assertNull(HTMLCpDetector.getMetaTagCharset(null));
    assertNull(HTMLCpDetector.getMetaTagCharset("".getBytes()));
    
    String html = "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\">";
    assertEquals("windows-1252", HTMLCpDetector.getMetaTagCharset(html.getBytes()));
  }

}
