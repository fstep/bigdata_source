package com.porva.html;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.NullArgumentException;

import com.porva.html.CharsetDetector.DetectedCharset;

/**
 * Testing of {@link CharsetDetector}.
 * @author poroshin
 */
public class CharsetDetectorLongTest
{

  private FileFilter extensionFilter;

  private File collectionDir;

  private CharsetDetector detector;
  
  private Map<File, Charset> verification = new HashMap<File, Charset>();
  
  int failed = 0;
  
  int ok = 0;

  /**
   * Testing of {@link CharsetDetector} constructor.
   * 
   * @param collectionDir
   * @param detector
   */
  public CharsetDetectorLongTest(File collectionDir, CharsetDetector detector)
  {
    this.collectionDir = collectionDir;
    this.detector = detector;
    if (this.collectionDir == null)
      throw new NullArgumentException("collectionDir");
    if (this.detector == null)
      throw new NullArgumentException("detector");

    extensionFilter = new FileFilter()
    {
      public boolean accept(File pathname)
      {
        if (pathname.getPath().matches(".+\\.html?$") || pathname.getPath().matches(".+\\.xml$"))
          return true;
        return false;
      }
    };
  }

  private void processRecursive(File f) throws Exception
  {
    if (f == null)
      throw new NullArgumentException("f");
    if (!f.exists())
      throw new IllegalArgumentException(f.getAbsolutePath() + " does not exist.");

    if (f.isDirectory()) {
      verification.putAll(loadVerification(f));
      File[] childs = f.listFiles();
      for (int i = childs.length - 1; i >= 0; i--) {
        processRecursive(childs[i]);
      }
    } else if (this.extensionFilter.accept(f)) {
      this.process(f, verification);
    }
  }

  private Map<File, Charset> loadVerification(File dir) throws IOException
  {
    File f = new File(dir, "encoding.verify");
    Map<File, Charset> res = new HashMap<File, Charset>();
    if (f.exists()) {
      BufferedReader in = new BufferedReader(new FileReader(f));
      String line = null;
      while ((line = in.readLine()) != null) {
        String[] ar = line.split("\\s+:\\s+");
        File ff = new File(dir, ar[0]);
        if (!ff.exists())
          throw new IllegalStateException("File does not exist: " + ff);
        Charset charset = Charset.forName(ar[1].trim());
        res.put(ff, charset);
      }
    }
    return res;
  }

  static byte[] readFileAsBytes(File f) throws IOException
  {
    FileInputStream fin = new FileInputStream(f);
    byte[] buff = new byte[(int) f.length()];
    int readed = fin.read(buff);
    if (readed != f.length() || readed == -1)
      System.exit(1); // todo: do not System.exit(1)
    return buff;
  }

  void process() throws Exception
  {
    this.processRecursive(this.collectionDir);
  }

  private void process(File document, Map<File, Charset> verification) throws Exception
  {
    if (document == null)
      throw new NullArgumentException("document");
    if (verification == null)
      throw new NullArgumentException("verification");
      
    Charset expectedCharset = verification.get(document);
    if (expectedCharset == null)
      throw new IllegalArgumentException("Charset is not defied for file: " + document + "\t" + verification);
    
    DetectedCharset[] detectedCharsets = detector.detect(readFileAsBytes(document));
    
    if (detectedCharsets == null) { // failed to detect any charset
      System.out.println("[Failed] {expected " + expectedCharset.name() + " but none detected}\t"
          + document);
      return;
    }

    Charset detectedCharset = detectedCharsets[0].getCharset();
    if (detectedCharset.equals(expectedCharset)) {
      System.out.println("[Ok] {" + detectedCharsets[0].getCharset().name() + "}\t" + document);
      ok++;
    }
    else {
      System.out.println("[Failed] {expected " + expectedCharset.name() + " but detected "
          + detectedCharset.name() + "}\t" + document);
      failed++;
    }
  }

  /**
   * @param argv
   * @throws Exception
   */
  public static void main(String[] argv) throws Exception
  {
    String testDir = System.getProperty("user.dir") + File.separator + "test" + File.separator;

    File f = new File(testDir);
    CharsetDetector detector = new CpDetector();
    CharsetDetectorLongTest test = new CharsetDetectorLongTest(f, detector);
    test.process();
    
    System.out.println("=======================");
        
    System.out.println("[Ok] " + test.ok + "\t[Failed] " + test.failed);
  }

}
