package com.porva.html;

import java.io.File;

import junit.framework.TestCase;

import org.apache.commons.lang.NullArgumentException;

/**
 * Testing of {@link CpDetector}.
 * @author poroshin
 */
public class CpDetectorTest extends TestCase
{
  CharsetDetector charsetDetector = new CpDetector();

  /**
   * Testing of {@link CpDetector}. 
   * @throws Exception
   */
  public void testDetect() throws Exception
  {
    try {
      charsetDetector.detect(null);
      fail("Should throw NullArgumentException");
    } catch (NullArgumentException e) {
    }
    
    String testDir = System.getProperty("user.dir") + File.separator + "test" + File.separator;

    File f = new File(testDir);
    CharsetDetector detector = new CpDetector();
    CharsetDetectorLongTest test = new CharsetDetectorLongTest(f, detector);
    test.process();
    
    System.out.println("=======================");
    System.out.println("[Ok] " + test.ok + "\t[Failed] " + test.failed);
  }

}
