package com.porva.html;

import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import com.porva.net.tana.TanaMessage;
import com.porva.net.tana.TanaMessageType;
import com.porva.net.tana.TanaSend;

/**
 * Testing of {@link CpDetectorServer}.
 * @author poroshin
 */
public class CpDetectorServerTest extends TestCase
{
  CpDetectorServer cpDetectorServer = new CpDetectorServer(12345);
  
  protected void setUp() throws Exception
  {
    cpDetectorServer.start();
    Thread.sleep(10);
  }
  
  protected void tearDown()
  {
    cpDetectorServer.shutdown();
  }
  
  /**
   * Testing of {@link CpDetectorServer}.
   * @throws Exception
   */
  public void testCpDetectorServer() throws Exception
  {
  	try {
  		new CpDetectorServer(-1);
  		fail();
  	} catch (IllegalArgumentException e) {
		}
  	
    TanaSend tanaSend = new TanaSend("localhost", 12345);
    Map<String, byte[]> msg = new HashMap<String, byte[]>();
    msg.put("content", "basd s dfa dsf asd".getBytes());
    TanaMessage tm = new TanaMessage(TanaMessageType.FIX, msg);
    tanaSend.send(tm);
    String charset = new String(tanaSend.receive().toMap().get("charset"));
    assertEquals("US-ASCII", charset);
  }
}
