/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

package com.porva.html;

import java.io.IOException;

import org.apache.commons.lang.NullArgumentException;

/**
 * @author Poroshin V.
 * @date Oct 25, 2005
 */
public interface LanguageDetector
{
  /**
   * Result of language detection.
   * 
   * @author Poroshin V.
   * @date Oct 25, 2005
   */
  class DetectedLanguage
  {

    private String langCode; // 2-letter language codes defined in ISO 639

    private int confidence;

    /**
     * Constructs a new {@link DetectedLanguage} object.
     * 
     * @param langCode detected language.
     * @param confidence number of detected charset in [0..100] range or -1 if it is not calculated.
     * @throws NullArgumentException if <code>charset</code> is <code>null</code>.
     */
    DetectedLanguage(final String langCode, int confidence)
    {
      this.langCode = langCode;
      this.confidence = confidence;

      if (this.langCode == null)
        throw new NullArgumentException("langCode");
      if (this.confidence < -1 || this.confidence > 100)
        throw new IllegalArgumentException("confidence number must be in [0..100] or -1: "
            + this.confidence);
    }

    /**
     * Returns detected 2-letter language code in ISO 639.
     * 
     * @return detected language code in ISO 639.
     */
    public String getLanguage()
    {
      return langCode;
    }

    /**
     * Returns confidence number of detected charset.<br>
     * If confidence number is calculated then it should have value in [0..100] range. If confidence
     * number is not calculated then it should have -1 value.
     * 
     * @return confidence number in [0..100] range or -1 if it is not calculated.
     */
    public int getConfidence()
    {
      return confidence;
    }
  }

  /**
   * Returns array of most probable language results for given <code>content</code>.
   * 
   * @param content data to detect language of.
   * @return array of most probable language results or <code>null</code> if it is impossible to
   *         detect any language. Returned array is in decreasing order of probable language.
   * @throws IOException if some I/O exception occurs.
   * @throws NullArgumentException if <code>content</code> is <code>null</code>.
   */
  public DetectedLanguage[] detect(final byte[] content) throws IOException;
}
