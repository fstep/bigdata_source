/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

package com.porva.html;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.html.CharsetDetector.DetectedCharset;
import com.porva.net.tana.DefaultServerHandler;
import com.porva.net.tana.TanaServer;

/**
 * Encoding detection in a TANA server mode. 
 * 
 * @author Poroshin
 */
public class CpDetectorServer extends DefaultServerHandler
{
	/**
	 * Received TANA key. The value should contain content to detect its encoding.
	 */
	public static String KEY_CONTENT = "content";
	
	/**
	 * Replied TANA key if some error occures. The value is an error message.
	 */
	public static String KEY_ERROR = "error";
	
	/**
	 * Replied TANA key. The value is a detected encoding of 'content'.
	 */
	public static String KEY_CHARSET = "charset";
	
	/**
	 * Replied TANA key. The value is a list of aliases of detected encoding. 
	 */
	public static String KEY_ALIASES = "aliases";
	
  private TanaServer tanaServer;

  private static final Log logger = LogFactory.getLog(CpDetectorServer.class);

  private CharsetDetector cpDetector = new CpDetector();

  /**
   * Allocates a new {@link CpDetectorServer} object.
   * 
   * @param port tcp port number to start server on.
   * @throws IllegalArgumentException if port is an invalid port number.
   */
  public CpDetectorServer(int port)
  {
    super(logger, "CpDetector");
    tanaServer = new TanaServer(port, this, true);
  }

  public Map<String, byte[]> handleClientMsg(Map<String, byte[]> clientMsg)
  {
    Map<String, byte[]> reply = new HashMap<String, byte[]>();
    byte[] content = clientMsg.get(KEY_CONTENT);
    if (content == null) {
      reply.put(KEY_ERROR, "content is null".getBytes());
      return reply;
    }

    try {
      DetectedCharset[] charsets = cpDetector.detect(content);
      if (charsets == null)
        reply.put(KEY_ERROR, "failed to detect any charsets".getBytes());

      Charset charset = charsets[0].getCharset();
      reply.put(KEY_CHARSET, charset.name().toString().getBytes());
      StringBuffer sb = new StringBuffer();
      for (String alias : charset.aliases())
        sb.append(alias).append(" ");
      reply.put(KEY_ALIASES, sb.toString().getBytes());

    } catch (IOException e) {
      reply.put(KEY_ERROR, e.toString().getBytes());
    }
    return reply;
  }

  /**
   * Starts the server listening.
   */
  public void start()
  {
    tanaServer.start();
  }

  /**
   * Shuts down the server.
   */
  public void shutdown()
  {
    tanaServer.shutdown();
  }

  private static void printHelp()
  {
    System.out.println("Charset detector server.");
    System.out.println("ver 0.1");
    System.out.println("Usage: java -jar CharsetDetector.jar port_number");
    System.out.println("Accepted TANA messages: ");
    System.out.println("  '" + KEY_CONTENT + "' => " + "'content to detect its encoding'");
    System.out.println("Replied TANA messages: ");
    System.out.println("  '" + KEY_CHARSET + "' => " + "'detected encoding'");
    System.out.println("  '" + KEY_ALIASES + "' => " + "'list of aliases of detected encoding'");
    System.out.println("  '" + KEY_ERROR + "' => " + "'error message'");
  }

  /**
   * Run a standalone encoding detection server.
   * 
   * @param argv see command line help.
   */
  public static void main(String[] argv)
  {
    if (argv.length == 0 || argv[0].equalsIgnoreCase("-h") || 
    		argv[0].equalsIgnoreCase("--help")) {
      printHelp();
      System.exit(0);
    }

    int port = Integer.parseInt(argv[0]);
    CpDetectorServer cpDetectorServer = new CpDetectorServer(port);
    cpDetectorServer.start();
  }
}
