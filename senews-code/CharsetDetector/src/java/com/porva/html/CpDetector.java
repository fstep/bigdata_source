/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

package com.porva.html;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import org.apache.commons.lang.NullArgumentException;

import cpdetector.io.ASCIIDetector;
import cpdetector.io.CodepageDetectorProxy;
import cpdetector.io.JChardetFacade;
import cpdetector.io.ParsingDetector;

/**
 * Implementation of {@link CharsetDetector} interface using cpdetector library
 * http://cpdetector.sourceforge.net/
 * 
 * @author Poroshin V.
 * @date Oct 27, 2005
 */
public class CpDetector implements CharsetDetector
{
  private final CodepageDetectorProxy detector = CodepageDetectorProxy.getInstance();

  /**
   * Constructs a new {@link CpDetector} instance.
   */
  public CpDetector()
  {
    // Add the implementations of cpdetector.io.ICodepageDetector:
    // The first instance delegated to tries to detect the meta charset attribut in html pages.
    detector.add(new ParsingDetector(false)); // do not be verbose about parsing.
    // This one does the tricks of exclusion and frequency detection, if first implementation is
    // unsucessful:
    detector.add(JChardetFacade.getInstance()); // Another singleton.
    detector.add(ASCIIDetector.getInstance()); // Fallback, see javadoc.
  }

  /* (non-Javadoc)
   * @see com.porva.html.CharsetDetector#detect(byte[])
   */
  public DetectedCharset[] detect(final byte[] content) throws IOException
  {
    if (content == null)
      throw new NullArgumentException("content");

    final InputStream bin = new ByteArrayInputStream(content);
    final Charset charset = detector.detectCodepage(bin, content.length);

    if (charset == null)
      return null;

    DetectedCharset[] detectedCharset = new DetectedCharset[1];
    detectedCharset[0] = new DetectedCharset(charset, -1);

    return detectedCharset;
  }
}
