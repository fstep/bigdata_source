/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package com.porva.html;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.mozilla.intl.chardet.nsDetector;
import org.mozilla.intl.chardet.nsICharsetDetectionObserver;


/**
 * For testing.
 */
@Deprecated public class HtmlCharsetDetectorImpl1
{

  /**
   * All available charsets with aliases.
   */
  private static Map<String, Charset> availableCharsets = new HashMap<String, Charset>();

  static {
    for (Map.Entry<String, Charset> entry : Charset.availableCharsets().entrySet()) {
      availableCharsets.put(entry.getKey().toLowerCase(), entry.getValue());
      for (String alias : entry.getValue().aliases()) {
        availableCharsets.put(alias.toLowerCase(), entry.getValue());
      }
    }
  }

  /**
   * Default encoding.
   */
  public static final String DEF_CHARSET = "ISO-8859-1";

  /**
   * Returns {@link Charset} object that corresponds to given string <code>charset</code>.
   * 
   * @param charset
   * @return a {@link Charset} object for <code>charset</code>.
   */
  public Charset getCharset(final String charset)
  {
    if (charset == null)
      throw null;

    return availableCharsets.get(charset.toLowerCase());
  }

  /**
   * Returns array of probable charsets of <code>content</code>.
   * 
   * @param content content to analyze.
   * @param langHint language hint:
   * 
   * <pre>
   *        1 =&gt; Japanese 
   *        2 =&gt; Chinese
   *        3 =&gt; Simplified Chinese
   *        4 =&gt; Traditional Chinese
   *        5 =&gt; Korean
   *        6 =&gt; Dont know (default)
   * </pre>
   * 
   * @return array of probable charsets.
   * @throws IOException if some I/O exception occurs.
   * @throws IllegalArgumentException if <code>langHint</code> not in range [1,6].
   */
  public String[] getProbableCharsets(final byte[] content, int langHint) throws IOException
  {
    if (langHint < 1 || langHint > 6)
      throw new IllegalArgumentException("langHint not in range [1,6]: " + langHint);

    nsDetector detector = new nsDetector(langHint);

    // The Notify() will be called when a matching charset is found.
    detector.Init(new nsICharsetDetectionObserver()
    {
      public void Notify(String charset)
      {
      }
    });

    BufferedInputStream bin = new BufferedInputStream(new ByteArrayInputStream(content));

    byte[] buf = new byte[1024];
    int len;
    boolean done = false;
    boolean isAscii = true;

    while ((len = bin.read(buf, 0, buf.length)) != -1) {

      // Check if the stream is only ascii.
      if (isAscii)
        isAscii = detector.isAscii(buf, len);

      // DoIt if non-ascii and not done yet.
      if (!isAscii && !done)
        done = detector.DoIt(buf, len, false);
    }
    detector.DataEnd();
    bin.close();

    if (isAscii) {
      String[] ascii = { DEF_CHARSET, "ASCII" };
      return ascii;
    }

    return detector.getProbableCharsets();
  }

  // todo: optimize
  /**
   * Returns charset defined in html meta tag if it exists. <br>
   * 
   * @param content html content
   * @return charset defined in html meta tag or <code>null</code> if it is not exist.
   * @throws UnsupportedEncodingException
   */
  public String getMetaTagCharset(final byte[] content) throws UnsupportedEncodingException
  {
    String st = new String(content, "iso-8859-1");

    Pattern p = Pattern.compile(".*<meta [^>]* charset=([^\\s\">]+).*", Pattern.CASE_INSENSITIVE);
    Matcher matcher = p.matcher(st);
    if (matcher.matches()) {
      return matcher.group(1);
    }

    return null;
  }

  // todo: test it
  /**
   * Reutrns charset defined in "content-type" header of response if it exist.
   * 
   * @param contentTypeStr "content-type" header value.
   * @return charset defined in "content-type" header of response or <code>null</code> if it is
   *         not exist.
   */
  public String getResponseCharset(String contentTypeStr)
  {
    String charsetFromHttpHeader = null;
    String contentType = contentTypeStr.toLowerCase();
    Pattern pattern = Pattern.compile(".*charset=([^\\s\"']+).*");
    Matcher matcher = pattern.matcher(contentType);
    if (matcher.matches())
      charsetFromHttpHeader = matcher.group(1);
    if (charsetFromHttpHeader == null)
      return DEF_CHARSET;
    return charsetFromHttpHeader;
  }

  /**
   * For testing.
   * 
   * @param argv
   * @throws Exception
   */
  public static void main(String argv[]) throws Exception
  {
    System.out.println(availableCharsets.size());

    for (Map.Entry<String, Charset> entry : availableCharsets.entrySet()) {
      System.out.println(entry.getKey() + " => " + entry.getValue());
    }

    if (argv.length < 2) {

      System.out.println("Usage: HtmlCharsetDetector <url> <languageHint>");

      System.out.println("");
      System.out.println("Where <url> is http://...");
      System.out.println("For optional <languageHint>. Use following...");
      System.out.println(" 1 => Japanese");
      System.out.println(" 2 => Chinese");
      System.out.println(" 3 => Simplified Chinese");
      System.out.println(" 4 => Traditional Chinese");
      System.out.println(" 5 => Korean");
      System.out.println(" 6 => Dont know (default)");

      return;
    }

    Integer langHint = Integer.parseInt(argv[1]);
    URL url = new URL(argv[0]);
    byte[] content = getResponseBody(url.openStream(), 1024 * 1024);
    // System.out.println(new String(content));

    HtmlCharsetDetectorImpl1 htmlCharsetDetector = new HtmlCharsetDetectorImpl1();

    String[] estCharsets = htmlCharsetDetector.getProbableCharsets(content, langHint);
    for (String estCharset : estCharsets) {
      Charset charsetOb = htmlCharsetDetector.getCharset(estCharset);
      System.out.println("Estimated charset: " + estCharset + "\t system-charset: " + charsetOb);
    }

    String metatagCharset = htmlCharsetDetector.getMetaTagCharset(content);
    if (metatagCharset != null) {
      Charset charsetOb = htmlCharsetDetector.getCharset(metatagCharset);
      System.out.println("Metatag charset: " + metatagCharset + "\t system-charset: " + charsetOb);
    }
  }

  static byte[] getResponseBody(InputStream instream, int contentLimit) throws IOException
  {
    if (instream == null)
      return null;

    ByteArrayOutputStream outstream = new ByteArrayOutputStream();
    byte[] buffer = new byte[4096];
    int len;
    int readedLen = 0;
    while (((len = instream.read(buffer)) > 0) && readedLen <= contentLimit) {
      if (readedLen + len > contentLimit) {
        outstream.write(buffer, 0, contentLimit - readedLen);
        break;
      }
      outstream.write(buffer, 0, len);
      readedLen += len;
    }
    outstream.close();
    return outstream.toByteArray();
  }

}