/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

package com.porva.html;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.porva.html.CharsetDetector.DetectedCharset;

/**
 * Default implementation of {@link HTMLCharsetDetector} interface.
 * 
 * @author Poroshin V.
 * @date Oct 27, 2005
 */
public class HTMLCpDetector implements HTMLCharsetDetector
{
  private Charset defaultCharset;

  private CharsetDetector charsetDetector;

  private static Pattern charsetPattern = Pattern.compile(".*charset=([^\\s\"']+).*");

  private static Pattern metaPattern = Pattern.compile(".*<meta [^>]* charset=([^\\s\">]+).*",
                                                       Pattern.CASE_INSENSITIVE);

  private static final Log logger = LogFactory.getLog(HTMLCpDetector.class);

  /**
   * Constructs a new {@link HTMLCpDetector} object with specified <code>charsetDetector</code>
   * object and <code>defaultCharset</code>.<br>
   * <code>charsetDetector</code> object will be used to detect probable charset of content and
   * <code>defaultCharset</code> will be returned if it is impossible to make any other reasonable
   * guess about detected encoding.
   * 
   * @param charsetDetector charset detection engine to be used.
   * @param defaultCharset default charset.
   * @throws NullArgumentException if <code>charsetDetector</code> or <code>defaultCharset</code>
   *           is <code>null</code>.
   */
  public HTMLCpDetector(final CharsetDetector charsetDetector, final Charset defaultCharset)
  {
    this.charsetDetector = charsetDetector;
    this.defaultCharset = defaultCharset;
    if (this.charsetDetector == null)
      throw new NullArgumentException("charsetDetector");
    if (this.defaultCharset == null)
      throw new NullArgumentException("defaultCharset");
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.porva.html.HTMLCharsetDetector#detect(java.lang.String, byte[], java.lang.String,
   *      java.net.URL)
   */
  public Charset detect(final String httpCharsetStr,
                        final byte[] content,
                        final String htmlCharsetStr,
                        URL url)
  {
    if (content == null)
      throw new NullArgumentException("content");

    Charset httpCharset = null;
    Charset htmlCharset = null;

    if (httpCharsetStr != null)
      httpCharset = getCharset(httpCharsetStr);
    if (htmlCharsetStr != null)
      htmlCharset = getCharset(htmlCharsetStr);

    // detection algorithm
    // todo: test it and improve
    if (htmlCharset != null)
      return htmlCharset;
    if (httpCharset != null)
      return httpCharset;
    Charset detectedCharset = detect(content);
    if (detectedCharset != null)
      return detectedCharset;

    return defaultCharset;
  }

  private Charset getCharset(final String charset)
  {
    assert charset != null;

    Charset httpCharset = null;
    try {
      httpCharset = Charset.forName(charset);
    } catch (Exception e) {
      logger.warn("Failed to create Charset object from string: " + charset);
    }
    return httpCharset;
  }

  private Charset detect(byte[] content)
  {
    assert content != null;

    Charset detectedCharset = null;
    try {
      DetectedCharset[] detectedCharsets = charsetDetector.detect(content);
      if (detectedCharsets != null)
        detectedCharset = detectedCharsets[0].getCharset();
    } catch (IOException e) {
      logger.warn("Failed to detect charset.", e);
    }
    return detectedCharset;
  }

  /**
   * Reutrns charset defined in "content-type" header of HTTP response if it exists.<br>
   * Example of <code>contentTypeStr</code>: "text/html; charset=utf-8".
   * 
   * @param contentTypeStr "content-type" header value.
   * @return charset defined in "content-type" header of response or <code>null</code> if<br>
   *         1) <code>contentTypeStr</code> is null or <br>
   *         2) <code>contentTypeStr</code> does not contain charset<br>
   */
  public synchronized static String getResponseCharset(String contentTypeStr)
  {
    if (contentTypeStr == null)
      return null;

    String charsetFromHttpHeader = null;
    String contentType = contentTypeStr.toLowerCase();
    Matcher matcher = charsetPattern.matcher(contentType);
    if (matcher.matches())
      charsetFromHttpHeader = matcher.group(1);

    return charsetFromHttpHeader;
  }

  /**
   * Returns charset defined in html meta tag if it exists. <br>
   * 
   * @param content html content
   * @return charset defined in html meta tag or <code>null</code> if does not exist or
   *         <code>content</code> is <code>null</code>.
   */
  public synchronized static String getMetaTagCharset(final byte[] content)
  {
    if (content == null)
      return null;

    // TODO: there is no need to create string for all content 
    String st = new String(content);
    Matcher matcher = metaPattern.matcher(st);
    if (matcher.matches())
      return matcher.group(1);

    return null;
  }

}
