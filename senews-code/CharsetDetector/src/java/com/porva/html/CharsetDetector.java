/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

package com.porva.html;

import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Interface to detect charset of text as byte array.
 *
 * @author Poroshin V.
 * @date Oct 25, 2005
 */
public interface CharsetDetector
{
  /**
   * Result of charset detection.
   * 
   * @author Poroshin V.
   * @date Oct 25, 2005
   */
  public static class DetectedCharset
  {
    private Charset charset;

    private int confidence;

    /**
     * Constructs a new {@link DetectedCharset} object.
     * 
     * @param charset detected charset.
     * @param confidence number of detected charset in [0..100] range or -1 if it is not calculated.
     * @throws NullArgumentException if <code>charset</code> is <code>null</code>.
     */
    DetectedCharset(final Charset charset, int confidence)
    {
      this.charset = charset;
      this.confidence = confidence;

      if (this.charset == null)
        throw new NullArgumentException("charset");
      if (this.confidence < -1 || this.confidence > 100)
        throw new IllegalArgumentException("confidence number must be in [0..100] or -1: "
            + this.confidence);
    }

    /**
     * Returns detected charset.
     * 
     * @return detected charset.
     */
    public Charset getCharset()
    {
      return charset;
    }

    /**
     * Returns confidence number of detected charset.<br>
     * If confidence number is calculated then it should have value in [0..100] range. If confidence
     * number is not calculated then it should have -1 value.
     * 
     * @return confidence number in [0..100] range or -1 if it is not calculated.
     */
    public int getConfidence()
    {
      return confidence;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString()
    {
      return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("charset", charset)
          .append("confidence", confidence).toString();
    }
    
  }

  /**
   * Returns array of most probable charsets results for given <code>content</code>.
   * 
   * @param content data to detect charsets of.
   * @return array of most probable charsets results or <code>null</code> if it is impossible to
   *         detect any charset. Returned array is in decreasing order of probable charsets.
   * @throws IOException if some I/O exception occurs.
   * @throws NullArgumentException if <code>content</code> is <code>null</code>.
   */
  public DetectedCharset[] detect(final byte[] content) throws IOException;
}
