/*
 * Copyright (C) 2005 Poroshin Vladimir. All Rights Reserved. This program is free software; you can
 * redistribute it and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. You should have received a copy of
 * the GNU General Public License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

package com.porva.html;

import java.net.URL;
import java.nio.charset.Charset;

/**
 * Interface to detect charset of html pages with http response information.
 * 
 * @author Poroshin V.
 * @date Oct 27, 2005
 */
public interface HTMLCharsetDetector
{
  /**
   * Returns most probable charset of given html page <code>content</code> with its HTTP header
   * charset <code>httpCharset</code>, charset defined in HTML content <code>htmlCharset</code>
   * and its <code>url</code>.
   * 
   * @param httpCharset HTTP charset string, i.e. 'charset' value of 'Content-Type' HTTP header or
   *          <code>null</code> if there is no such value. <br>
   *          For instnce, if HTTP 'Content-Type' header is "text/html; charset=iso-8859-1" then
   *          <code>httpCharset</code> is 'iso-8859-1'.
   * @param content HTML page content as byte array.
   * @param htmlCharset HTML charset string, i.e. 'charset' value of 'content' attribute of meta
   *          tag.<br>
   *          For instance, if HTML content contains tag '<meta http-equiv="Content-Type"
   *          content="text/html; charset=UTF-8">' then <code>htmlCharset</code> is 'UTF-8'.
   * @param url URL of <code>content</code> or <code>null</code> if there is no such location.
   * @return most probable charset of <code>content</code>.
   */
  public Charset detect(final String httpCharset,
                        final byte[] content,
                        final String htmlCharset,
                        final URL url);
}
